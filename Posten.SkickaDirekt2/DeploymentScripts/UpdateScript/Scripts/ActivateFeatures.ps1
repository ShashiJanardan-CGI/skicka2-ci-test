function ActivateFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Farm,
		[switch]$Web,
		[Switch]$Site,
		[switch]$Subsite
	)

	if($Farm)
	{
		ActivateFarmFeatures $ConfigXML
	}

	if($Web)
	{
		
		# Sleep is needed to prevent errors like "A web configuration modification operation is already running."
		# if the problem persists simply restart the SharePoint Administration and SharePoint Timer jobs on
		# the problem web front end and then execute the �stsadm -o execadmsvcjobs� on that server 

		Start-Sleep -Seconds 10
		ActivateWebFeatures $ConfigXML

	}

	if($Site)
	{
		ActivateSiteFeatures $ConfigXML
	}

	if ($Subsite)
	{	
		ActivateSubsiteFeatures $ConfigXML
	}

}

function ActivateFarmFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
    PrintLineMSG "Installing (Activating) Farm Features Starts."
	
	Try
	{    
		$ConfigXML.Deployment.Standard.Farm.Features.Feature | ForEach-Object {
			[string]$guid = $_.Id 
        
			if ([string]::IsNullOrEmpty($guid))
			{
				[string]$name = $_.Name
				PrintLineMSG "No Farm level features specified."			
			}
			else
			{
				$feature = $null
				$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -Farm
				if($feature -eq $null)
				{
					PrintMSG "Activating feature $guid..." 
					Enable-SPFeature -Identity $guid 
					PrintLineMSG "DONE!"	
				}
				else
				{
					PrintLineMSG "Feature [$guid] is already Activated on Farm Scope. Success!"
				}
			}		
		}
	}
	Catch
	{
    	StopScript "ActivateFarmFeatures" $_
	} 
	Finally { } 

	PrintLineMSG "Farm Level Features are Installed."
	PrintSpliter
}

function ActivateWebFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
    PrintLineMSG "Installing (Activating) Web Features"
	Try
	{
		$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {
			$URL = $_.Url
			$WebName = $_.Name
			if ([string]::IsNullOrEmpty($URL))
			{
				StopScript "ActivateWebFeatures" "WebApplication URL is missing."
			}
			     
			$_.Features.Feature | ForEach-Object {
				[string]$guid = $_.Id
        
				if ([string]::IsNullOrEmpty($guid))
				{
					[string]$name = $_.Name
					PrintLineMSG "$name[$guid] is missing."
					StopScript "ActivateWebFeatures" "$name[$guid] is missing."
				}
				else
				{
					$feature = $null
					$webApplication = $null
					$webApplication = Get-SPWebApplication $WebName
					if($webApplication -eq $null)
					{
						StopScript "ActivateWebFeatures" "There is no Web Application on Provided URL [$URL]"
					}
					$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -WebApplication $webApplication
					if($feature -eq $null)
					{
						
						WaitForWebConfigTimerJobToComplete $webApplication
						PrintMSG "Activating feature $guid..." 
						Enable-SPFeature -Identity $guid -URL $webApplication.URL
						# sleep 30s to ensure changes is synced to all servers in farm
						#Start-Sleep -Seconds 20
						PrintLineMSG "DONE!"
						WaitForWebConfigTimerJobToComplete $webApplication
					}
					else
					{
						WaitForWebConfigTimerJobToComplete $webApplication
						PrintMSG "Feature [$guid] is already Activated. Reactivating..."
						Disable-SPFeature -Identity $guid -URL $webApplication.URL -Confirm:$false
						# sleep 30s to ensure changes is synced to all servers in farm
						#Start-Sleep -Seconds 20
						WaitForWebConfigTimerJobToComplete $webApplication
						Enable-SPFeature -Identity $guid -URL $webApplication.URL 
						#Start-Sleep -Seconds 20
						PrintLineMSG "Done"							
						WaitForWebConfigTimerJobToComplete $webApplication
					}
				}		
			}
		}
	}
	Catch
	{
    	StopScript "ActivateWebFeatures" $_
	} 
	Finally { } 

	PrintLineMSG "Web Level Features are Installed"
	PrintSpliter
}

function ActivateSiteFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
    PrintLineMSG "Installing (Activating) Site Collection Features Starts"
	
	Try
	{
		$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {             
		[string]$webName = $_.Name

			$_.SiteCollections.SiteCollection | ForEach-Object {
				$URL = $_.Url

				if ([string]::IsNullOrEmpty($URL))
					{
						StopScript "ActivateSiteFeatures" "Site URL is missing."
					}
			     
				$_.Features.Feature | ForEach-Object {
					[string]$guid = $_.Id
        
					if ([string]::IsNullOrEmpty($guid))
					{
						[string]$name = $_.Name
						PrintLineMSG "$name[$guid] is missing."
						StopScript "ActivateSiteFeatures" "$name[$guid] is missing."
					}
					else
					{
						$feature = $null
						$siteCollection = $null
						$siteCollection = Get-SPSite $URL
						if($siteCollection -eq $null)
						{
							StopScript "ActivateSiteFeatures" "There is no SiteCollection on Provided URL [$URL]"
						}
						$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -Site $siteCollection
						if($feature -eq $null)
						{
							PrintMSG "Activating feature $guid..." 
							Enable-SPFeature -Identity $guid -URL $URL
							PrintLineMSG "DONE!"	
						}
						else
						{
							PrintLineMSG "Feature [$guid] is already Activated on Site Scope. Success!"
						}
					}		
				}
			}
		}
	}
	Catch
	{
    	StopScript "ActivateSiteFeatures" $_
	} 
	Finally { } 
	
	PrintLineMSG "Site Level Features are Installed"
	PrintSpliter
}

function ActivateSubsiteFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
	PrintLineMSG "Installing (Activating) Subsite Features Starts"
	
	Try
	{
		$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {             
		[string]$webName = $_.Name
			
			$_.SiteCollections.SiteCollection.SubSites.Site | ForEach-Object {  
				$URL = $_.CreateSite.URL 

				if ([string]::IsNullOrEmpty($URL))
				{
					StopScript "ActivateSiteFeatures" "Subsite URL is missing."
				}
			     
				$_.Features.Feature | ForEach-Object {

					$guid = $_.Id
					
					if($guid -ne $null)
					{
						$feature = $null
						$Subsite = $null
						$Subsite = Get-SPWeb $URL
						if($Subsite -eq $null)
						{
							StopScript "ActivateSiteFeatures" "There is no SubSite on Provided URL [$URL]"
						}
						$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -Web $Subsite
						if($feature -eq $null)
						{
							PrintMSG "Activating feature $guid..." 
							Enable-SPFeature -Identity $guid -URL $URL
							PrintLineMSG "DONE!"	
						}
						else
						{
							PrintLineMSG "Feature [$guid] is already Activated on subsite Scope. Success!"
						}
					}		
				}
			}
		}
	}
	Catch
	{
    	StopScript "ActivateSiteFeatures" $_
	} 
	Finally { } 
	
	PrintLineMSG "SubSite Level Features are Installed"
	PrintSpliter
}

function CheckWebConfigTimerJob($webapp)
{
    $isRunning = $false
    
	#Get job definition / running jobs 
	$pendingJobs = $webapp.WebService.JobDefinitions | Where-Object {$_.Name -like "*job-webconfig*"}
	$runningJobs = $webapp.WebService.RunningJobs | Where-Object {$_.Name -like "*Web.Config*"}
	
	$isRunning = (($pendingJobs.Count  -gt 0) -or ($runningJobs.Count  -gt 0))

    return $isRunning
}


function WaitForWebConfigTimerJobToComplete($webapp)
{
	while(CheckWebConfigTimerJob $webapp)
	{
		Start-Sleep -Seconds 2
	}
}
