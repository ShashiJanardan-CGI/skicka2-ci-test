function ApplyWebGeneralSettings
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {  
		[string]$url = $_.Url           
		[string]$name = "DirectLinkWeb2"
		#$_.Name

		$webApplication = Get-SPWebApplication -ErrorAction SilentlyContinue | Where {$_.DisplayName -eq $name}
		if($webApplication -eq $null)
		{
			StopScript "ApplyWebGeneralSettings" "Web application [$url] doesn't exists."
		}
		
		 # Declare Variables here
		 $zone = $null
		 $membershipProviderName = $null
		 $RoleProviderName = $null
		 $LoginURL = $null

		 #Set Variables from Config File
		 $zone = $_.GenralSettings.Zone
		 $membershipProviderName = $_.GenralSettings.MemberShipProvider
		 $roleProviderName = $_.GenralSettings.RoleProviderName
		 $LoginURL = $_.GenralSettings.LoginURL

		 PrintSpliter
		 PrintLineMSG "Applying Web Applicatinos Settings for $name at [$url]"

		 PrintMSG "Setting Authentication Provider .... "
		 $authprv = New-SPAuthenticationProvider -ASPNETMembershipProvider $membershipProviderName -ASPNETRoleProviderName $roleProviderName		 
		 Set-SPWebApplication -Identity $webApplication -Zone $zone -AuthenticationProvider $authprv
		 PrintLineMSG " Done! "

		 PrintMSG "Setting SignIn URL .... "
		 Set-SPWebApplication -Identity $webApplication -Zone $zone -SignInRedirectURL $loginUrl
		 PrintLineMSG " Done! "


		 PrintLineMSG "Applying Web Applicatinos Settings for $name at [$url] Finished!"
		 PrintSpliter

	}
}