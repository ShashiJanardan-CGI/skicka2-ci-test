function WebConfigUpdate
{
	param
	(
		[parameter(Mandatory = $true)]
		[string] $Configfile
	)
   
    $Dllpath = "$pwd\DeploymentUtilities.dll"
    Add-Type -Path $dllpath
    $factory = new-object DeploymentUtilities.WebConfigurationFactory @($Configfile)
    # Adds changes to the web.config
	PrintMSG "Applying Web Config Changes to Web Applications specified in Configuration file .... "
    $factory.ApplyWebConfigurationChanges()
	PrintLineMSG " Done!"
}

function PortalWebConfigUpdate
{
	param
	(
		[parameter(Mandatory = $true)]
		[string] $Configfile
	)
   
	$configuration = new-object "System.Xml.XmlDocument"		
	$configuration.LoadXml((get-content $Configfile))
	
	$webConfigPath = $configuration.SelectSingleNode("Deployment/Standard/WebApplications/WebApplication/webconfigmodifications").GetAttribute("webConfigPath")
	$webConfigModifications = $configuration.SelectNodes("Deployment/Standard/WebApplications/WebApplication/webconfigmodifications/webconfigmodification")
	
	$webConfig = new-object "System.Xml.XmlDocument"
	$webConfig.LoadXml((get-content $webConfigPath))
	
	foreach($modification in $webConfigModifications)
	{	
		$parentNode = $webConfig.SelectSingleNode($modification.path)
		
		if ($parentNode -ne $null)
		{
			$matchingNodes = $parentNode.SelectNodes($modification.name)
			$ctr = 0
			
			if ($matchingNodes.Count -gt 0)
			{
				foreach ($targetNode in $matchingNodes)
				{
					if ($ctr -eq 0)
					{
						$updatedNode = $modification.value.FirstChild
						$targetNode.InnerXml = $updatedNode.InnerXml
						
						foreach ($attribute in $updatedNode.Attributes)
						{
							$targetNode.SetAttribute($attribute.LocalName, $updatedNode.GetAttribute($attribute.LocalName))
						}
						
						$ctr++
					}
					else
					{
						PrintLineMSG "Deleting... Found duplicate node:"
						$parentNode.RemoveChild($targetNode)
					}
				}
			}
			else
			{
				$updatedNode = $modification.value.FirstChild
				$newNode = $parentNode.FirstChild.CloneNode($false)
				$newNode.InnerXml = $updatedNode.InnerXml
				
				foreach ($attribute in $updatedNode.Attributes)
				{
					$newNode.SetAttribute($attribute.LocalName, $updatedNode.GetAttribute($attribute.LocalName))
				}
				
				PrintLineMSG "Adding node... Found a node from Configuration.xml not existing in web.config:"
				$parentNode.PrependChild($newNode)
			}
		}
	}
	
	$webConfig.Save($webConfigPath)
}