# Compile Utility Scripts
. .\Scripts\DeploySolutions.ps1
. .\Scripts\ActivateFeatures.ps1

function ConfigureFarm
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$DeploySolutions,
		[switch]$ActivateFeatures
	)
	
	if($DeploySolutions)
	{
		DeploySolutions $ConfigXML -Farm
	}
	
	if($ActivateFeatures)
	{
		ActivateFeatures $ConfigXML -Farm
	}
}

