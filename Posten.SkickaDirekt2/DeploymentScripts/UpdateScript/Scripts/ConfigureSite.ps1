# Compile Utility Scripts
. .\Scripts\ActivateFeatures.ps1
. .\Scripts\CreateSite.ps1


function ConfigureSite
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Create,
		[switch]$DeploySolutions,
		[switch]$ActivateFeatures,
		[switch]$DoSettings
	)

	if($Create)
	{
		CreateSite $ConfigXML
	}

	if($DeploySolutions)
	{
		
	}

	if($ActivateFeatures)
	{
		ActivateFeatures $ConfigXML -Subsite
		Start-Sleep -Seconds 3
	}


	if($ApplyConfigChanges)
	{
	}

	if($DoSettings)
	{

		
	}
}