# Compile Utility Scripts
. .\Scripts\ActivateFeatures.ps1
. .\Scripts\createSiteCollections.ps1


function ConfigureSiteCollection
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Create,
		[switch]$DeploySolutions,
		[switch]$ActivateFeatures,
		[switch]$DoSettings
	)

	if($Create)
	{
		CreateSiteCollection $ConfigXML
	}

	if($DeploySolutions)
	{
		
	}

	if($ActivateFeatures)
	{
		ActivateFeatures $ConfigXML -Site
	}

	
	if($ApplyConfigChanges)
	{
	}

	if($DoSettings)
	{
	}
}