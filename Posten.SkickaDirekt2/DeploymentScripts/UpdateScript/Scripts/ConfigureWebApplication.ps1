# Compile Utility Scripts
. .\Scripts\DeploySolutions.ps1
. .\Scripts\ActivateFeatures.ps1
. .\Scripts\CreateWebApplications.ps1
. .\Scripts\ApplyGeneralSettings.ps1
. .\Scripts\ApplyWebConfigs.ps1

function ConfigureWebApplication
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Create,
		[switch]$DeploySolutions,
		[switch]$ActivateFeatures,
		[switch]$DoSettings
	)

	if($Create)
	{
		 CreateWebApplication $ConfigXML
	}

	if($DeploySolutions)
	{
		DeploySolutions $ConfigXML
	}

	if($ActivateFeatures)
	{
		Start-Sleep -Seconds 4
		ActivateFeatures $ConfigXML -Web
	}

	if($DoSettings)
	{
		ApplyWebGeneralSettings $ConfigXML
	}
	
}