﻿# Compile Utility Scripts
. .\Scripts\ActivateFeatures.ps1

function CreateSite([string]$targetUrl, [string]$name) {
	write-output "Creating site at $targetUrl"
	$site = Get-SPWeb $targetUrl -ErrorVariable err -ErrorAction SilentlyContinue -AssignmentCollection $assignmentCollection

	if ($err)
	{
		New-SPWeb -Url $targetUrl -Name $_.SvName -Language 1053 -Template CMSPUBLISHING#0 | Out-Null
	}
	else
	{
		Write-Output "Site $targetUrl already exists, skipping..."
	}
}

function StartTimerJobs()
{

	$VariationsSpawnSites = "VariationsSpawnSites"

	Write-Host "Running timer job: Variations Propagate Site Job Definition"
	Get-SPTimerJob | ?{$_.Name -match $VariationsSpawnSites } | ?{$_.Parent -eq $wa} | Start-SPTimerJob
	
 	$job = Get-SPTimerJob -WebApplication $wa | ?{ $_.Name -like $VariationsSpawnSites }
    if ($job -eq $null) 
    {
        Write-Host 'Timer job not found'
    }
    else
    {
        $JobLastRunTime = $job.LastRunTime
        Write-Host -NoNewLine "Waiting to finish job $VariationsSpawnSites last run on $JobLastRunTime"
        
        while ($job.LastRunTime -eq $JobLastRunTime) 
        {
            Write-Host -NoNewLine .
            Start-Sleep -Seconds 2
        }
        Write-Host  "Finished waiting for job.."
    }
}

function RenameSiteFromSvToEn([string]$targetUrl, [string]$enUrl, [string]$enName) {
	$site = Get-SPWeb $targetUrl -ErrorVariable err -ErrorAction SilentlyContinue -AssignmentCollection $assignmentCollection	
	
	if ( !$err)
	{
		Write-Output "Changing name of $targetUrl to $enName"
		$web = Get-SPWeb $targetUrl
		$web.Title = $enName
		$web.Update()
		
		Write-Output "Changing relative url of $targetUrl to $enUrl"
		Set-SPWeb -Identity:$web -RelativeUrl $enUrl
	}
}

function ActivateProjectFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
        PrintLineMSG "Installing (Activating) Project specific Subsite Features Starts"
	
	Try
	{

			$siteUrl = $ConfigXML.Deployment.ProjectSpecific.Sites.SiteCollection.Url
			

			$ConfigXML.Deployment.ProjectSpecific.Features.Feature | ForEach-Object {
	
				$targetUrl = $siteUrl + '/sv-se/' + $_.SvUrl			

				if ([string]::IsNullOrEmpty($_.SvUrl))
					{
						StopScript "ActivateSiteFeatures" "SvURL is missing.For feature $_.Name"
					}
			     
				
					[string]$guid = $_.Id
        
					if ([string]::IsNullOrEmpty($guid))
					{
						[string]$name = $_.Name
						PrintLineMSG "$name[$guid] is missing."
						StopScript "ActivateSiteFeatures" "$name[$guid] is missing."
					}
					else
					{
						$feature = $null
						$Subsite = $null
						$Subsite = Get-SPWeb $targetUrl
						if($Subsite -eq $null)
						{
							StopScript "ActivateSiteFeatures" "There is no SubSite on Provided URL [$URL]"
						}
						$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -Web $Subsite
						if($feature -eq $null)
						{
							PrintMSG "Activating feature $guid..." 
							Enable-SPFeature -Identity $guid -URL $targetUrl
							PrintLineMSG "DONE!"	
						}
						else
						{
							PrintLineMSG "Feature [$guid] is already Activated on subsite Scope. Success!"
						}
					}		
				
			}
		
	}
	Catch
	{
    	StopScript "ActivateSiteFeatures" $_
	} 
	Finally { } 
	
	PrintLineMSG "SubSite Level Features are Installed"
	PrintSpliter
}


function CreateAllSites {
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
$siteUrl = $ConfigXML.Deployment.ProjectSpecific.Sites.SiteCollection.Url
$wa = Get-SPWebApplication $siteUrl

$ConfigXML.Deployment.ProjectSpecific.Sites.Site | ForEach-Object {
	$targetUrl = $siteUrl + '/sv-se/' + $_.SvUrl
	$name = $_.SvName
	CreateSite $targetUrl $name
}

#Wait for Site creation to end.
Start-Sleep -Seconds 3

#Activate Project Specific Features here. This will be propagated to target sites
ActivateProjectFeatures $ConfigXML
Start-Sleep -Seconds 5

StartTimerJobs

$ConfigXML.Deployment.ProjectSpecific.Sites.Site | ForEach-Object {
	$svUrl = $_.SvUrl
	$enUrl = $_.EnUrl
	$enName = $_.EnName
	$targetUrl = $siteUrl + '/en-us/' + $svUrl
	
	RenameSiteFromSvToEn $targetUrl $enUrl $enName	
}

}
