function DetermineVirtualRoot($vdir, $port)
{
	if (!$vdir)
	{
		"C:\Inetpub\wwwroot\wss\VirtualDirectories\" + $port
	}
	else
	{
		return $vdir
	}
}

function ConfigureServiceApplicationProxyGroup($webapp, $proxyGroupName)
{
	if ($webapp)
	{
		$proxyGroup = $null
		if (!$proxyGroupName -or ($proxyGroupName -eq "[default]"))
		{
			$proxyGroup = Get-SPServiceApplicationProxyGroup -Default
		}
		else
		{
			$proxyGroup = Get-SPServiceApplicationProxyGroup $proxyGroupName -ErrorAction SilentlyContinue
		}
		if (!$proxyGroup)
		{
			Write-Host "Cannot find service application proxy group [$proxyGroupName]." -f Red
		}
		else
		{
			if ($webapp.ServiceApplicationProxyGroup.Id -ne $proxyGroup.Id)
			{
				$webapp.ServiceApplicationProxyGroup = $proxyGroup
				Write-Host "Set service app proxy group of [$($webapp.Url)] to $($proxyGroup.FriendlyName)."
			}
		}
	}
}

function CreateWebApplication
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Creation of WebApplication(s) Start"

	$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {    
			[string]$url = $_.Url           
			[string]$name = $_.Name

	$sp = Get-SPWebApplication -ErrorAction SilentlyContinue | Where {$_.DisplayName -eq $name}
	if($sp -ne $null)
	{
		Write-Host "Web application [$url] already exists." -f Cyan

	}
	else
	{
	$_.CreateWebApplications.WebApplication | ForEach-Object {
			[string]$port= $_.Port
            [string]$path= $_.Path
            [string]$hostheader = $_.HostHeader    		
			[string]$poolName = $_.ApplicationPoolName
    		[string]$poolAccName = $_.ApplicationPoolAccountName
    		[string]$dbSrvName= $_.DataBaseServerName
    		[string]$dbName= $_.DataBaseName
			[bool]$allowanonymous= [bool]::Parse($_.AllowAnonymousAccess)
			[bool]$claimsMode= [bool]::Parse($_.AutheticationType.ClaimsMode)	
			
			if($claimsMode)
			{
				[string]$MemberShipProvider= "Blank_Membership_Provider"
				[string]$RoleProvider= "Blank_Role_Provider"
				
				Try
				{
					$authprv = New-SPAuthenticationProvider -ASPNETMembershipProvider $MemberShipProvider -ASPNETRoleProviderName $RoleProvider
					PrintMSG "Creating Web Application [$name] with Claims Authentication ... "					
					New-SPWebApplication -Name $name -Path $path -URL $url -HostHeader $hostheader -AllowAnonymous:$allowanonymous -ApplicationPool $poolName -ApplicationPoolAccount (Get-SPManagedAccount $poolAccName) -AuthenticationProvider $authprv -DatabaseName $dbName -DatabaseServer $dbSrvName
    				PrintMSG " DONE!"
					PrintLineMSG "Web Application [$name] Created. Success"
    				} 
				Catch
				{
    				StopScript "CreateWebApplication" $_
				} 
				Finally { } 
			}
			else
			{
			Try
				{              
					PrintMSG "Creating Web Application [$name] with Classic Mode Authentication... "					
    				New-SPWebApplication -Name $name -URL $url -HostHeader $hostheader -AllowAnonymousAccess:$allowanonymous -ApplicationPool $poolName -ApplicationPoolAccount(Get-SPManagedAccount $poolAccName) -Port $port      			
					PrintMSG " DONE!"
					PrintLineMSG "Web Application [$name] Created. Success"
				} 
				Catch
				{
    				StopScript "CreateWebApplication" $_
				} 
				Finally { }       
			}
		}
		}

	}
	PrintLineMSG "Creation of WebApplication(s) Ends"
	PrintSpliter
}

