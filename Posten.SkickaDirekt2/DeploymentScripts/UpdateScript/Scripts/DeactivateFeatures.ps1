function DisableFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Farm,
		[switch]$Web,
		[Switch]$Site,
		[Switch]$SubSite
	)

	if($Farm)
	{
		DisableFarmFeatures $ConfigXML
	}

	if($Web)
	{
		DisableWebFeatures $ConfigXML
	}

	if($Site)
	{
		DisableSiteFeatures $ConfigXML
	}
	
	if($Subsite)
	{
		DisableSubSiteFeatures $ConfigXML
	}
}

function DisableFarmFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
    PrintLineMSG "Dectivating Farm Features"
	try
	{
		$ConfigXML.Deployment.Standard.Farm.Features.Feature | ForEach-Object {
			[string]$guid = $_.Id 
        
			if ([string]::IsNullOrEmpty($guid))
			{
				[string]$name = $_.Name
				PrintLineMSG "$name[$guid] is missing."
				StopScript "DisableFarmFeatures" "$name[$guid] is missing."
			}
			else
			{
				$feature = $null
				$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -Farm
				if($feature -eq $null)
				{
					PrintLineMSG "Feature [$guid] is Not Activated on Farm Scope. Success!"
				}
				else
				{
					PrintMSG "De-Activating feature $guid..." 
					Disable-SPFeature -Identity $guid -Confirm:$false
					PrintLineMSG "DONE!"				
				}
			}		
		}
	}
	Catch
		{
    		StopScript "DisableFarmFeatures" $_
		} 
		Finally { } 

	PrintLineMSG "Farm Level Features are Deactivated"
	PrintSpliter
}

function DisableWebFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
    PrintLineMSG "De-Activating Web Features"
	Try
	{
		$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {
			$URL = $_.Url	
			$WebName = $_.Name

			if ([string]::IsNullOrEmpty($URL))
				{
					StopScript "ActivateWebFeatures" "WebApplication URL is missing."
				}
			     
			$_.Features.Feature | ForEach-Object {
				[string]$guid = $_.Id
        
				if ([string]::IsNullOrEmpty($guid))
				{
					[string]$name = $_.Name
					PrintLineMSG "$name[$guid] is missing."
					StopScript "DisableWebFeatures" "$name[$guid] is missing."
				}
				else
				{
					$feature = $null
					$webApplication = $null
					$webApplication = Get-SPWebApplication $WebName
					if($webApplication -eq $null)
					{
						StopScript "DisableWebFeatures" "There is no Web Application on Provided URL [$URL]"
					}
					$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -WebApplication $webApplication
					if($feature -eq $null)
					{
						PrintLineMSG "Feature [$guid] is Not Activated on WebApplication Scope. Success!"	
					}
					else
					{						
						WaitForWebConfigTimerJobToComplete $webApplication
						PrintMSG "De-Activating feature $guid..." 
						Disable-SPFeature -Identity $guid -URL $webApplication.URL -Confirm:$false
						PrintLineMSG "DONE!"
						WaitForWebConfigTimerJobToComplete $webApplication
					}
				}		
			}
		}
	}
	Catch
		{
    		StopScript "DisableWebFeatures" $_
		} 
		Finally { } 
	PrintLineMSG "Web Level Features are Deactivatd"
	PrintSpliter
}

function DisableSiteFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
    PrintLineMSG "De-Activating Site Collection Features"
	
	Try
	{
		$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {             
		[string]$webName = $_.Name

			$_.SiteCollections.SiteCollection | ForEach-Object {
				$URL = $_.Url

				if ([string]::IsNullOrEmpty($URL))
					{
						StopScript "DisableSiteFeatures" "Site URL is missing."
					}
			     
				$_.Features.Feature | ForEach-Object {
					[string]$guid = $_.Id
        
					if ([string]::IsNullOrEmpty($guid))
					{
						[string]$name = $_.Name
						PrintLineMSG "$name[$guid] is missing."
						StopScript "DisableSiteFeatures" "$name[$guid] is missing."
					}
					else
					{
						$feature = $null
						$siteCollection = $null
						$siteCollection = Get-SPSite $URL -ErrorAction SilentlyContinue
						if($siteCollection -eq $null)
						{
							#StopScript "DisableSiteFeatures" "There is no SiteCollection on Provided URL [$URL]"
							PrintLineMSG "There is no SiteCollection on Provided URL [$URL]. Success!"
						}
						else
						{
							$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -Site $siteCollection
							if($feature -eq $null)
							{
								PrintLineMSG "Feature [$guid] is Not Activated on Site Scope. Success!"	
							}
							else
							{
								PrintMSG "De-Activating feature $guid..." 
								Disable-SPFeature -Identity $guid -URL $URL -Confirm:$false
								PrintLineMSG "DONE!"
							}
						}
					}		
				}
			}
		}
	}
	Catch
		{
    		StopScript "DisableSiteFeatures" $_
		} 
		Finally { } 
	PrintLineMSG "Site Level Features are Deactivated"
	PrintSpliter
}

function DisableSubSiteFeatures
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
    PrintLineMSG "De-Activating SubSite Features"
	
	Try
	{
		$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {             
			[string]$webName = $_.Name
			$_.SiteCollections.SiteCollection | ForEach-Object {
            	$_.Subsites.Site | ForEach-Object {
    				
					$_.CreateSite |  ForEach-Object {
						$URL = $_.Url
					}
					 
    				if ([string]::IsNullOrEmpty($URL))
					{
						StopScript "DisableSubSiteFeatures" "SubSite URL is missing."
					}
    			     
    				$_.Features.Feature | ForEach-Object {
    				
						$guid = $_.Id
						$name = $_.Name
					
						if($guid -ne $null)
						{
							$feature = $null
    						$subsite = $null
    						$subsite = Get-SPWeb $URL -ErrorAction SilentlyContinue
							
    						if($subsite -eq $null)
    						{
    							StopScript "DisableSubSiteFeatures" "There is no SubSite on Provided URL [$URL]"
    						}
							
    						$feature = Get-SPFeature -Identity $guid -ErrorAction SilentlyContinue -Web $subsite
							
    						if($feature -eq $null)
    						{
								PrintLineMSG "Feature [$guid] is Not Activated on SubSite Scope. Success!"	
    						}
    						else
    						{
    							PrintMSG "De-Activating feature $guid..." 
								Disable-SPFeature -Identity $guid -URL $URL -Confirm:$false
								PrintLineMSG "DONE!"
    						}
    					}
                    }		
				}
			}
		}
	}
	Catch
	{
    	StopScript "DisableSubSiteFeatures" $_
	} 
	Finally { } 
	PrintLineMSG "SubSite Level Features are Deactivated"
	PrintSpliter
}


function CheckWebConfigTimerJob($webapp)
{
    $isRunning = $false
    
	#Get job definition / running jobs 
	$pendingJobs = $webapp.WebService.JobDefinitions | Where-Object {$_.Name -like "*job-webconfig*"}
	$runningJobs = $webapp.WebService.RunningJobs | Where-Object {$_.Name -like "*Web.Config*"}
	
	$isRunning = (($pendingJobs.Count  -gt 0) -or ($runningJobs.Count  -gt 0))

    return $isRunning
}


function WaitForWebConfigTimerJobToComplete($webapp)
{
	while(CheckWebConfigTimerJob $webapp)
	{
		Start-Sleep -Seconds 2
	}
}