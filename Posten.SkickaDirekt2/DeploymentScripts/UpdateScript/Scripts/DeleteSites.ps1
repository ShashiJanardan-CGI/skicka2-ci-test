﻿function DeleteAllSites {
param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

$siteUrl = $ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication.Url 

$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication.Sitecollections.SiteCollection.Sites.Site | ForEach-Object {
	$targetUrl = $siteUrl + '/sv-se/' + $_.SvUrl

	$site = Get-SPWeb $targetUrl -ErrorVariable err -ErrorAction SilentlyContinue -AssignmentCollection $assignmentCollection

	if ($err)
	{
		Write-Output "Site $targetUrl does not exist, skipping..."
	}
	else
	{
		write-output "Deleting site at $targetUrl"
		Remove-SPWeb -Confirm:$false $targetUrl
	}
	
		$targetUrl = $siteUrl + '/en-us/' + $_.EnUrl
		
			if ($err)
	{
		Write-Output "Site $targetUrl does not exist, skipping..."
	}
	else
	{
		write-output "Deleting site at $targetUrl"
		Remove-SPWeb -Confirm:$false $targetUrl
	}
}
}