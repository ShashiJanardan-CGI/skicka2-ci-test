
[xml]$ConfigXML = Get-Content "..\Configurations.xml" 
$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
	$webappurl = $_.Url
	$_.SiteCollections.SiteCollection.Sites | ForEach-Object { 

		$svParentUrl = $webappurl
		$enParentUrl = $webappurl
	
		if($_.SvUrl)
		{
			$svParentUrl = $webappurl + '/' + $_.SvUrl
			$enParentUrl = $webappurl + '/' + $_.EnUrl
		}
	
		$_.Site | ForEach-Object { 
		 
		$svUrl = $svParentUrl + '/' + $_.SvUrl
		$enUrl = $enParentUrl + '/' + $_.EnUrl
		 
		 if($_.IsRootWeb -or $_.IsLabel) { return }
		 
		 $svWeb = Get-SPWeb -Identity $svUrl -ErrorAction SilentlyContinue
		 if($svWeb){
			 Write-Host 'Removing ' $svUrl '...' -NoNewLine
			 Remove-SPWeb -Identity $svUrl -Confirm:$false
			 Write-Host 'Done'
		 }
		 else{
			 Write-Host 'Web does not exists:' $svUrl -f Red
		 }
		 
		 $enWeb = Get-SPWeb -Identity $enUrl -ErrorAction SilentlyContinue
		 if($enWeb){
			 Write-Host 'Removing ' $enUrl '...' -NoNewLine
			 Remove-SPWeb -Identity $enUrl -Confirm:$false
			 Write-Host 'Done'
		 }
		 else{
			 Write-Host 'Web does not exists:' $enUrl -f Red
		 }
		}
	}
}