function DeleteWebApplications
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Deletion of WebApplication(s) Start"

	$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {    
		[string]$url = $_.Url
		[string]$name = $_.Name   
	 	$_.CreateWebApplications.WebApplication | ForEach-Object {
			
			$webApplication = $null
			$webApplication = Get-SPWebapplication $name -ErrorAction SilentlyContinue
			
			if($webApplication -eq $null)
			{
				PrintLineMSG "The Specified Name of Web Application is Wrong OR Web Application doesn't Exist. Success!"
			}
			else
			{
				Try
				{ 
					PrintMSG "Deleting Web Application [$name] ... "     
					Remove-SPWebApplication -Identity $webApplication.URL -DeleteIISSite -RemoveContentDatabase -Confirm:$false
					PrintMSG " DONE! "
				} 
				Catch
				{
    				StopScript "DeleteWebApplications" $_
				} 
				Finally { }  
			}
		}
	}

	PrintLineMSG "Deletion of WebApplication(s) Ends"
	PrintSpliter
}