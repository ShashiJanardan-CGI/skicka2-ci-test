function DeploySolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Farm
	)

	if($Farm)
	{
		AddFarmSolutions $ConfigXML
		InstallFarmSolutions $ConfigXML
	}
	else
	{
		AddWebSolutions $ConfigXML
		InstallWebSolutions $ConfigXML
	}
}

function AddFarmSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Adding Farm Solution(s) Start"

    $ConfigXML.Deployment.Standard.Farm.Solutions.Solution | ForEach-Object {
        [string]$name = $_.Name
        [bool]$gac = [bool]::Parse($_.GACDeployment)
        [bool]$cas = [bool]::Parse($_.CASPolicies)
		
		Try
		{
			$solution = $null
			$solution = Get-SPSolution $name -ErrorAction SilentlyContinue
			if($solution -eq $null)
			{
				$wspPath = "$pwd\Packages\$name"
				#PrintLineMSG "Adding solution $name..."
				PrintMSG "Adding solution $name... "
				$solution = Add-SPSolution $wspPath
				PrintLineMSG "Done"
			}
			else
			{
				PrintLineMSG "$name Solution is Already in Store. Success!"
			}
		}
		Catch
		{
    		StopScript "AddFarmSolutions" $_
		} 
		Finally { } 
    }	
	
	PrintLineMSG "Adding Farm Solution(s) Ended."
	PrintSpliter
}

function AddWebSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Adding Web Solution(s) Starts."
	$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {
		$_.Solutions.Solution | ForEach-Object {
			[string]$name = $_.Name
			[bool]$gac = [bool]::Parse($_.GACDeployment)
			[bool]$cas = [bool]::Parse($_.CASPolicies)

			Try
			{
				$solution = $null
				$solution = Get-SPSolution $name -ErrorAction SilentlyContinue
				if($solution -eq $null)
				{
					$wspPath = "$pwd\Packages\$name"
					#PrintLineMSG "Adding solution $name..."
					PrintMSG "Adding solution $name..."
					$solution = Add-SPSolution $wspPath
					PrintLineMSG "Done"
				}
				else
				{
					PrintLineMSG "$name Solution is Already in Store. Success!"
				}
			}
			Catch
			{
    			StopScript "AddWebSolutions" $_
			} 
			Finally { } 
			}
	}
	
	PrintLineMSG "Adding Web Solutions Ended."
	PrintSpliter
}

function InstallFarmSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Installing (Deploying) Farm Solution(s) Starts."

	$ConfigXML.Deployment.Standard.Farm.Solutions.Solution | ForEach-Object {
        [string]$name = $_.Name
        [bool]$gac = [bool]::Parse($_.GACDeployment)
        [bool]$cas = [bool]::Parse($_.CASPolicies)

		Try
		{
			$solution = Get-SPSolution $name -ErrorAction SilentlyContinue
			if (!$solution)
			{
				PrintLineMSG "Can't find a solution with a name of $name to install. Fail!"
			}
				
			if(($solution).Deployed)
			{
				PrintLineMSG "Solution Already Install.  Success!"
			}
			else
    		{
				if (!$solution.ContainsWebApplicationResource) 
				{
    				PrintMSG "Installing $name ... "
    				$solution | Install-SPSolution -GACDeployment:$gac -CASPolicies:$cas -Confirm:$false

					#Block until we're sure the solution is deployed.
					do 
					{ 
						Start-Sleep 2
						PrintMSG " ... "
					} while (!((Get-SPSolution $name).Deployed))

					PrintLineMSG " Success!"
    			}
			}
		}
		Catch
		{
    		StopScript "InstallFarmSolutions" $_
		} 
		Finally { }
		
		#RestartTimerService 
    }

	PrintLineMSG "Deploying Farm Solutions Ended."
	PrintSpliter
}


function InstallWebSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Installing (Deploying) Web Solutions Starts."

	$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {
		$URL = $_.Url
		$webName = $_.Name
		
		$_.Solutions.Solution | ForEach-Object {
			[string]$name = $_.Name
			[bool]$gac = [bool]::Parse($_.GACDeployment)
			[bool]$cas = [bool]::Parse($_.CASPolicies)

			Try
			{
				$solution = Get-SPSolution $name -ErrorAction SilentlyContinue
				if (!$solution)
				{
					PrintLineMSG "Can't find a solution with a name of $name to install. Fail!"
				}
				
				if ($URL -eq $null -or $URL.Length -eq 0)
				{
    				Write-Warning "The solution $name contains web application resources but no web applications were specified to deploy to."
    				return
    			}
				                	
				$webApplication = $null
				$webApplication = Get-SPWebapplication $webName -ErrorAction SilentlyContinue
				if($webApplication -eq $null)
				{
					StopScript "InstallWebSolutions" "The Specified Name of Web Application is Wrong OR Web Application doesn't Exist."
				}
			
				PrintMSG "Installing solution $name to $URL ... "
				
				$proceedInstall = $true
				if($solution.Deployed)
				{
					#Check if solution is Globally Deployed
					if($solution.DeploymentState -eq [Microsoft.SharePoint.Administration.SPSolutionDeploymentState]::GlobalDeployed) { $proceedInstall = $false }
					
					#Check if solution is deployed in the web application
					$solution.DeployedWebApplications | % {if($_.DisplayName -eq $webName)  { $proceedInstall = $false } }
					
				}
				
				if ($proceedInstall)
				{
					$solution | Install-SPSolution -GACDeployment:$gac -CASPolicies:$cas -WebApplication $webApplication.URL -Confirm:$false
	
					#Block until we're sure the solution is deployed.
					do 
					{ 
						Start-Sleep 2
						PrintMSG "... "
					} while (!((Get-SPSolution $name).Deployed))
					
				}
				else
				{
					PrintMSG "The Solution $name has already been deployed." 
				}
				PrintLineMSG " Success!"
			}
			Catch
			{
    			StopScript "InstallWebSolutions" $_
			} 
			Finally { } 

			#RestartTimerService
		}
	}
	PrintLineMSG "Deploying Web Solutions Ended."
	PrintSpliter
}