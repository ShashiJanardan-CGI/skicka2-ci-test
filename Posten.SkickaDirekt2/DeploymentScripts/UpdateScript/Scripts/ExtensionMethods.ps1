# This File contains the (Common)Standard Functions required during the whole deployment Purposes 
# Try to Maximize this file and add reuseable functions here
# All Functions in this file are pre-compiled for the deployment process, and these can be called 
# directly from any script once these are compiled.
# NOTE: To Compile these functions Type . .\ExtensionMethods.ps1

function CheckDirectory
{
	Param([string]$DirectoryPath)

    # Create directory if does not exist
    if (!(Test-Path -Path $DirectoryPath)) 
	{ 
		New-Item $DirectoryPath -Type Directory > $null
	}
}

function ConfigureLogs
{
	Param
	(
		[string]$DirectoryPath
	)
	if ([string]::IsNullOrEmpty($DirectoryPath))
    {
        Write-Host "Missing Parameter for funtion[ConfigureLogs]"
        return
    }
	CheckDirectory -DirectoryPath $DirectoryPath
}

function GetConfigSections
{
    Param
	(
		[parameter(Mandatory = $true)]
		[string]$FilePath,
		[parameter(Mandatory = $true)]
		[ref]$ConfigSections
	)
    
    if ([string]::IsNullOrEmpty($FilePath))
    {
        Write-Host "File not found [$FilePath]"
        return
    }
    
    [xml]$ConfigurationFile = Get-Content $FilePath
    if ($ConfigurationFile -eq $null) 
    {
        return 
    }
    $ConfigSections.Value = $ConfigurationFile
}

function PrintSpliter
{
	write-output "*******************************************************************"
}

function PrintSectionSpliter
{
	Param
	(
		[parameter(Mandatory = $true)]
		[string]$MSG
	)
	write-output "############## [$MSG] ##############"
	write-output " "
}

function PrintLineMSG
{
	Param
	(
		[parameter(Mandatory = $true)]
		[string]$MSG
	)

	write-output $MSG
}

function PrintMSG
{
	Param
	(
		[parameter(Mandatory = $true)]
		[string]$MSG
	)

	write-host $MSG -NoNewLine
}


function GlobalPause
{
    Param([string]$Message = "Press any key to continue...")
    
	If ($psISE) 
    {
		# The "ReadKey" functionality is not supported in Windows PowerShell ISE.
		$Shell = New-Object -ComObject "WScript.Shell"
		$Button = $Shell.Popup("Click OK to continue.", 0, "Script Paused", 0)
		return
	}

	write-output -NoNewline $Message -foregroundcolor "magenta"

	$Ignore =
		16,  # Shift (left or right)
		17,  # Ctrl (left or right)
		18,  # Alt (left or right)
		20,  # Caps lock
		91,  # Windows key (left)
		92,  # Windows key (right)
		93,  # Menu key
		144, # Num lock
		145, # Scroll lock
		166, # Back
		167, # Forward
		168, # Refresh
		169, # Stop
		170, # Search
		171, # Favorites
		172, # Start/Home
		173, # Mute
		174, # Volume Down
		175, # Volume Up
		176, # Next Track
		177, # Previous Track
		178, # Stop Media
		179, # Play
		180, # Mail
		181, # Select Media
		182, # Application 1
		183  # Application 2

	While ($KeyInfo.VirtualKeyCode -Eq $Null -Or $Ignore -Contains $KeyInfo.VirtualKeyCode) 
    {
		$KeyInfo = $Host.UI.RawUI.ReadKey("NoEcho, IncludeKeyDown")
	}

	Write-Host
}

function StopScript
{
	param
	(
		[parameter(Mandatory = $true)]
		[string]$FunctionName,
		[parameter(Mandatory = $true)]
		[string]$ErrorMSG
	)

	Write-Host "Error Occoured during the Script Execution. Function Name [$FunctionName]."  -foregroundcolor "red"
	Write-Host "Custom Message [$ErrorMSG]" -foregroundcolor "red"
	Write-Host "Script is Stoping...." -foregroundcolor "red"		

	Exit
    
}

function GetServersInFarm()
{
	$farm = [Microsoft.SharePoint.Administration.SPFarm]::Local
	$servers = $farm.Servers
	
	foreach($server in $servers)
	{
		if($server.role -ne "invalid")
		{
			[array]$returnArray += $server
		}
	}
	return $returnArray
}