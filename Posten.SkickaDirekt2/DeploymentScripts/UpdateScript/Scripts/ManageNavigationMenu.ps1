﻿function DeleteNodeFromWebWithId {
    param ($id, $nav)
    $currentNode = $web.Navigation.GetNodeById($id)
    write-host "Deleting" $currentNode.Title "and all child navigation links..." -NoNewline
    $qlLibraries = $currentNode.Children
    $nav.Delete($currentNode)
	Write-Host "Done."
}

function CreateGlobalHeadingNode
{
	param ($heading)
	$createdHeading = CreateHeading $heading $globalNav
	AddChildNodesToHeading $createdHeading
}

function CreateCurrentHeadingNode
{
	param ($heading)
	$createdHeading = CreateHeading $heading $currentNav	
	AddChildNodesToHeading $createdHeading
}

function CreateHeading {
	param ($source, $targetNav)
	
	if($source.IsSecured -ne $null -and $source.IsSecured -eq $true) {
		#write-host "Creating Secured Heading: " $source.Title
		$headingNode = New-Object Microsoft.SharePoint.Navigation.SPNavigationNode($source.Title, $source.Url)
	} else {
		#always default to external links, no security trimming		
		$headingNode = New-Object Microsoft.SharePoint.Navigation.SPNavigationNode($source.Title, $source.Url, $true)
	}
	
    write-host "Creating Heading:" $_.Title
	Try
	{
		$heading = $targetNav.AddAsLast($headingNode)		
	}
	Catch
	{
    	write-host "	Error creating Heading: " $_.Title -f Red
	} 
	Finally { } 
	if($source.Target -ne $null) {
		AddTargetToNode $headingNode $source.Target
	}
	
	return $heading
}

function AddChildNodesToHeading
{
	param ($heading)
	
	$a=$_.NavLink.Count	
	if($a -eq $null -and $_.NavLink -ne $null) { CreateNavigationNode $_.NavLink}	
    if($a -gt 0){ $_.NavLink | ForEach-Object { CreateNavigationNode $_ } }
}

function CreateNavigationNode
{
	param ($source)
	
	$stringQ = "?"
	$urlParameter = $null
	$target = $source.Target	
	if($source.IsSecured -ne $null -and $source.IsSecured -eq $true) {		
		if($source.Url.Contains($stringQ)){
			$urlLink = $source.Url;
			$url = $urlLink.Substring(0, $urlLink.LastIndexof('?'))
			$urlParameter = $urlLink.Substring($urlLink.LastIndexof('?')+1)			
			$linkNode = New-Object Microsoft.SharePoint.Navigation.SPNavigationNode($source.Title, $url)
		}else{
			$linkNode = New-Object Microsoft.SharePoint.Navigation.SPNavigationNode($source.Title, $source.Url)
		}
	} else {
		#always default to external links, no security trimming		
		$linkNode = New-Object Microsoft.SharePoint.Navigation.SPNavigationNode($source.Title, $source.Url, $true)
	}
	
    write-host "	Creating Navigation Link:" $_.Title
	Try
	{
		$link = $heading.Children.AddAsLast($linkNode)
		
		#check if url contains parameters		
		if($source.Url.Contains($stringQ)){			
			if($link.Properties.ContainsKey("UrlQueryString"))
			{
				$link.Properties.Remove("UrlQueryString");
			}			
			$link.Properties.Add("UrlQueryString", $urlParameter);
			$link.Update();
		}
		
	}
	Catch
	{
    	write-host "	Error creating navigation link: " $source.Url -f Red
	} 
	Finally { } 
	$target = $_.Target
	if($target -ne $null) {
		AddTargetToNode $linkNode $target
	}
}

function AddTargetToNode {
	param ($node, $target)
	Write-Host "		with target: $target"
	$node.Properties.Add("Target", $target) 
	$node.Update()
}

function DeleteAndCreateNodesFromXml
{
	param ($XmlFilePath, $webPath)
	
    $xmlFile = [xml](Get-Content($XmlFilePath))
    Write-Host $xmlFile.Navigation.WebUrl

    #Get Web and Quick Launch objects
    $siteUr = $xmlFile.Navigation.WebUrl

    $site = New-Object Microsoft.SharePoint.SPSite($siteUr)
	
	$web = $site.OpenWeb($webPath)
	$web.AllowUnsafeUpdates = $true
	
	#Stop the web from inheriting global navigation from parent
	$web.Navigation.UseShared = $false;
	
    $currentGlobalLinks = @()
	$currentCurrentLinks = @()
		
    $globalNav = $web.Navigation.TopNavigationBar
	$currentNav = $web.Navigation.QuickLaunch
	
    $globalNav | ForEach-Object { $currentGlobalLinks = $currentGlobalLinks + $_.Id }  
    $currentGlobalLinks | ForEach-Object { DeleteNodeFromWebWithId $_ $globalNav }
	
	$currentNav | ForEach-Object { $currentCurrentLinks = $currentCurrentLinks + $_.Id }  
    $currentCurrentLinks | ForEach-Object { DeleteNodeFromWebWithId $_ $currentNav }

    #Create Global Navigation Links
    $xmlFile.Navigation.GlobalHeadings.Heading | ForEach-Object {
		CreateGlobalHeadingNode $_	
        $web.Update()
    }
	
	#Create Current Navigation Links
	    $xmlFile.Navigation.CurrentHeadings.Heading | ForEach-Object {
		CreateCurrentHeadingNode $_
        $web.Update()
    }
    
    $web.AllowUnsafeUpdates = $false
    $web.Dispose()
    $site.Dispose()
}