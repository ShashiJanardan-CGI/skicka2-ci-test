﻿	
function GetSPWeb($label, $webUrl, $IsRootWeb)
{
	$publishingweb = $null
	
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webappurl = $_.Url
		$_.SiteCollections.SiteCollection | ForEach-Object {
			$sitecoll = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($webappurl, $_.Url)			
			$_.Sites | ForEach-Object {
				if($label -eq "source") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.SourceUrl) }
				elseif($label -eq "en") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.EnUrl) }
				elseif($label -eq "sv") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.SvUrl) }
				
				$_.Site | ForEach-Object {					
					if($_.SvUrl -eq $webUrl) {
						if($label -eq "source") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.SvUrl) }
						elseif($label -eq "en") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.EnUrl) }
						elseif($label -eq "sv") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.SvUrl) }						
					}
				}
			}
		}
	}	
	
	$web = Get-SPWeb -Identity $websiteUrl -ErrorAction SilentlyContinue	
	
	if($web -eq $null) { Write-Host "Variation does not exists:" $siteUrl -f Yellow }
	if($IsRootWeb) { return $web.ParentWeb }
	
	return $web
}

function SetInheritancePermission
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml.XmlElement] $Permissions,
		[Microsoft.Sharepoint.SPWeb] $web
	)

	Write-Host "[Site Inheritance]"
	if($Permissions.Inherit -eq $null) {
		return
	}
	elseif ($Permissions.Inherit -eq $false) {
		Write-Host " - Breaking web inheritance..." -NoNewLine
		$web.ResetRoleInheritance()
		$web.BreakRoleInheritance($false)
		Write-Host "Done."
	}
	else {
		Write-Host " - Inheriting permission from parent..." -NoNewLine
		$web.ResetRoleInheritance()
		Write-Host "Done."
	}
}

function SetRolesDefinition
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml.XmlElement] $Permissions,
		[Microsoft.Sharepoint.SPWeb] $web
	)
	
	if($Permissions.RolesDefinitions -eq $null) { return }
	if(!$web.IsRootWeb -and ($Permissions.Inherit -ne $false)) { return } 
	
	if(!$web.IsRootWeb -and !$web.HasUniqueRoleDefinitions) {
		$web.RoleDefinitions.BreakInheritance($true, $true)
	}
	
	Write-Host "[Roles Definition]"
	$Permissions.RolesDefinitions.RolesDefinition | ForEach-Object {		
		if ($_ -ne $null) {
			[string] $name = $_.Name
			[string] $description = $_.Description
			[string] $basePermission = $_.BasePermission
	
			if($web.RoleDefinitions[$name] -ne $null) {
				Write-Host " - Role" $name "already exists. " -f Yellow
				Write-Host " - Updating Permission..." -NoNewLine
				$spRoleDefinition = New-Object Microsoft.SharePoint.SPRoleDefinition
				$spRoleDefinition = $web.RoleDefinitions[$name]
				$spRoleDefinition.Description = $description
				$spRoleDefinition.BasePermissions = $basePermission
				$spRoleDefinition.Update()
				Write-Host "Done."
			}
			else {
				Write-Host " - Creating role: " $name -NoNewLine
				$spRoleDefinition = New-Object Microsoft.SharePoint.SPRoleDefinition
				$spRoleDefinition.Name = $name
				$spRoleDefinition.Description = $description
				$spRoleDefinition.BasePermissions = $basePermission
				$web.RoleDefinitions.Add($spRoleDefinition)				
				Write-Host "...Done"
			}	
		}
	}
}

function AddUserPermission
{
	param
	(
		[parameter(Mandatory = $true)]
		[string] $role,
		[string] $user,
		[Microsoft.Sharepoint.SPWeb] $web
	)
	
	try	{
		$account = $web.EnsureUser($user);
	}
	catch {
		Write-Host " - Check if account exists: " -f Yellow -NoNewLine
		Write-Host $user
		return
	}
	
	#Check if user already exists in the Root Web
	if($web.IsRootWeb -and ($web.Users[$user] -ne $null))
	{
		Write-Host " - Account exists in Root Site:" -f Yellow -NoNewLine
		Write-Host $user "- Removing... " -NoNewLine
		$web.Users.Remove($user)
		Write-Host "Done"
	}
	
	$defaultRoles = @("Administrator", "Contributor", "Guest", "None", "WebDesigner", "Reader")
	if ($defaultRoles -contains $role)
	{
		switch ($role) 
		{
			"Administrator" {[Microsoft.Sharepoint.SPRoleDefinition]$roleDefinition = $web.RoleDefinitions.GetByType([Microsoft.Sharepoint.SPRoleType]::Administrator.value__)}
			"Contributor" 	{[Microsoft.Sharepoint.SPRoleDefinition]$roleDefinition = $web.RoleDefinitions.GetByType([Microsoft.Sharepoint.SPRoleType]::Contributor.value__)}
			"Guest" 		{[Microsoft.Sharepoint.SPRoleDefinition]$roleDefinition = $web.RoleDefinitions.GetByType([Microsoft.Sharepoint.SPRoleType]::Guest.value__)}
			"None" 			{[Microsoft.Sharepoint.SPRoleDefinition]$roleDefinition = $web.RoleDefinitions.GetByType([Microsoft.Sharepoint.SPRoleType]::None.value__)}
			"WebDesigner" 	{[Microsoft.Sharepoint.SPRoleDefinition]$roleDefinition = $web.RoleDefinitions.GetByType([Microsoft.Sharepoint.SPRoleType]::WebDesigner.value__)}
			default 		{[Microsoft.Sharepoint.SPRoleDefinition]$roleDefinition = $web.RoleDefinitions.GetByType([Microsoft.Sharepoint.SPRoleType]::Reader.value__)}
		}
    }
	else
	{
		[Microsoft.Sharepoint.SPRoleDefinition] $roleDefinition = $web.Site.RootWeb.RoleDefinitions[$role]
	}
	Write-Host " - Add:" $role ":" $user "..." -NoNewLine
	
	[Microsoft.Sharepoint.SPRoleAssignment]$roleAssigment = New-Object Microsoft.Sharepoint.SPRoleAssignment($account) #Microsoft.SharePoint
	$roleAssigment.RoleDefinitionBindings.Add($roleDefinition)
	$web.RoleAssignments.Add($roleAssigment)
	
	Write-Host " Done"
}

function DeleteUserPermission
{
	param
	(
		[parameter(Mandatory = $true)]
		[string] $user,
		[Microsoft.Sharepoint.SPWeb] $web
	)

	if($web.Users[$user] -ne $null)	{
		Write-Host " - Remove:" $user "... " -NoNewLine
		$web.Users.Remove($user)
		Write-Host "Done"
	}
	else {
		Write-Host " - Account not in this site: " -f Yellow -NoNewLine
		Write-Host $user
	}
}

function SetUserPermission
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml.XmlElement] $Permissions,
		[Microsoft.Sharepoint.SPWeb] $web,
		[string] $ADdomain
	)
	
	if(!$web.IsRootWeb -and ($Permissions.Inherit -ne $false)) { return } 
	
	Write-Host "[User Permissions]"
	$Permissions.Accounts.Account | ForEach-Object {
		$user = $ADdomain + "\" + $_.Id
				
		if($_.Action -eq "Add") {
			AddUserPermission $_.Permission $user $web
		}
		elseif($_.Action -eq "Remove") {
			DeleteUserPermission $user $web
		}
	}
}

function SetSitePermission($label)
{
	$domain = $ConfigXML.Deployment.ProjectSpecific.ActiveDirectory.Domain;

	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication.SiteCollections.SiteCollection.Sites.Site | ForEach-Object {		
		
		Start-SPAssignment -global
		$web = GetSPWeb $label $_.SvUrl $_.IsRootWeb
			
		if($web -eq $null) { Write-Host "Web does not exists" -f Red; return; }
		if($_.Permissions -eq $null) { return }
		if($_.IsRootWeb -and ($label -eq "en")) { return } # Skip redefining roles in root site when in EN
		
		$_.Permissions | ForEach-Object {
			Write-Host $web.Url -f White
			$web.AllowUnsafeUpdates = [bool]::Parse("true")
			
			# Set the Web Permission if Inherit from parent or separate permission settings
			SetInheritancePermission $_ $web
			
			# Role Definitions
			SetRolesDefinition $_ $web
			
			#Add Account Permission
			SetUserPermission $_ $web $domain
			
			$web.AllowUnsafeUpdates = [bool]::Parse("false")
			$web.Update()			
		}
		
		Stop-SPAssignment –global
	}
}

function ManagePermission
{		
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	SetSitePermission "sv"
	SetSitePermission "en"
}
