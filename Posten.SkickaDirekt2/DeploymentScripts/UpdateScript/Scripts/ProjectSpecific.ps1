﻿function GetPeerPublishingSite
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[string] $label,
		[string] $targetsiteUrl
	)
	
	$publishingweb = $null
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webapp = $_.Url		
		$_.SiteCollections.SiteCollection | ForEach-Object {
		
			$siteurl =  [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($webapp, $_.Url)
		
			$_.Sites | ForEach-Object {
			
				$labelUrl = $_.SourceUrl
				if($label -eq "en") { $labelUrl = $_.EnUrl }
				if($label -eq "sv") { $labelUrl = $_.SvUrl }
			
				$siteCollUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($siteurl, $labelUrl )
			
				$_.Site | ForEach-Object {				
					if($_.IsRootWeb) { return }
					
					#Gets SV Url
					$targetUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($siteCollUrl, $targetsiteUrl)
					$site = Get-SPWeb $targetUrl -ErrorAction SilentlyContinue
					if($site)
					{
						$publishingweb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($site);
					}
					else
					{
						Write-Host "Variation does not exists:" $targetUrl -f Yellow 
					}
				}
			}			
		}
	}
	
	return $publishingweb;
}

function CheckIfSiteExists($label)
{
	$allVariationsExists = $true
	Write-Host "Checking if sites in" $label.ToUpper() "have been propagated"
	
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webapp = $_.Url		
		$_.SiteCollections.SiteCollection | ForEach-Object {
		
			$siteurl =  [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($webapp, $_.Url)
		
			$_.Sites | ForEach-Object {
			
				$labelUrl = $_.SourceUrl
				if($label -eq "en") { $labelUrl = $_.EnUrl }
				if($label -eq "sv") { $labelUrl = $_.SvUrl }
			
				$siteCollUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($siteurl, $labelUrl )
			
				$_.Site | ForEach-Object {				
					if($_.IsRootWeb -or $_.IsLabel) { return }
					
					$targetUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($siteCollUrl, $_.SvUrl)
					$site = Get-SPWeb $targetUrl -ErrorAction SilentlyContinue
					if(!$site)
					{
						$allVariationsExists = $false 
						Write-Host "Missing Site:" $targetUrl -f Red
					}
				}
			}			
		}
	}

	return $allVariationsExists
}

function RunVariationsPropagateSiteJobDefinition
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webappurl = $_.Url
		$wa = Get-SPWebApplication $webappurl -ErrorAction SilentlyContinue 
		
		if($wa -eq $null) { Write-Host "Web application does not exist: " $webappurl  -f Red; return; }
		
		Write-Host "Starting Timer Job: Variations Propagate Site Job Definition"
		
		Get-SPTimerJob | ?{$_.Name -match "VariationsSpawnSites" } | ?{$_.Parent -eq $wa} | Start-SPTimerJob
		
		$job = Get-SPTimerJob -WebApplication $wa | ?{ $_.Name -like "VariationsSpawnSites" }
		if ($job -eq $null)  { Write-Host "Timer job not found"; return; }
	
		$JobLastRunTime = $job.LastRunTime
		while ($job.LastRunTime -eq $JobLastRunTime) 
		{
			Write-Host "." -NoNewLine 
			Start-Sleep -Seconds 2
		}
		
		Write-Host "Finished waiting for job"
		Write-Host ""
	}
	
	Start-Sleep -Seconds 5

}

function RunVariationsPropagatePageTimerJob
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webappurl = $_.Url
		$webapp = Get-SPWebApplication -Identity $webappurl -ErrorAction SilentlyContinue 
		
		if($webapp -eq $null) { Write-Host "Web application does not exist: " $webappurl; return; }	
		
		$job = Get-SPTimerJob -WebApplication $webapp | ?{ $_.Name -like "VariationsPropagatePage"}
		if ($job -eq $null) 
		{
			Write-Host "Timer job not found" -f Red
			return;
		}
		
		Write-Host "Starting timer job: Variations Propagate Page Job Definition " -NoNewLine
		Get-SPTimerJob | ?{$_.Name -match "VariationsPropagatePage"} | ?{$_.Parent -eq $webapp} | Start-SPTimerJob

		$JobLastRunTime = $job.LastRunTime
		while ($job.LastRunTime -eq $JobLastRunTime) 
		{
			Write-Host -NoNewLine "."
			Start-Sleep -Seconds 2
		}
		Write-Host  " Finished waiting for job."
		Write-Host ""
    }
}

function CheckPeerPageInVariation
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[string] $label
	)

	Write-Host "Checking if pages are propagated in " $label.ToUpper() "variation"
	
	$allPagesInVariation = $true;
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication.SiteCollections.SiteCollection.Sites.Site | ForEach-Object {
		
		if($_.IsRootWeb) { return }
	
		$pubweb = GetPublishingWeb "source" $_.SvUrl $_.SvUrl
		$targetpubweb = GetPeerPublishingSite $ConfigXML $label $_.SvUrl
		
		if($pubweb -eq $null) { return; }
		if($targetpubweb -eq $null) { return; }
		
		$result = $true;
		$pubweb.GetPublishingPages() | ForEach-Object {		
			[Microsoft.SharePoint.Publishing.PublishingPage]$pageFile = $_
			$result = CheckIfPageExists $targetpubweb $pageFile.Name	
			if(!$result) { $allPagesInVariation = $false; }
		}
	}
	
	Write-Host ""
	return $allPagesInVariation;
}

function CheckSitesPropagated
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	Write-Host ""
	$allsource = CheckIfSiteExists "sv"
	
	if($allsource) 
	{		
		Write-Host "All SV sites exists"
		Write-Host ""
		
		$i = 0;
		$maxAttempts = 2;
		
		$allVariationsEN = $false
		while (!$allVariationsEN ) 
		{
			$i++
		
			$allVariationsEN = CheckIfSiteExists "en"
			
			if($allVariationsEN) {  
				Write-Host "All EN sites exists"; 
				break; 
			}
			else { 
				RunVariationsPropagateSiteJobDefinition $ConfigXML 
			}
			
			Start-Sleep -Seconds 10
			
			if($i -eq $maxAttempts) { break; }
		}	
	}
}

function RenameSiteFromSvToEn([string]$targetUrl, [string]$enUrl, [string]$enName, [string]$svUrl) 
{
	$web = Get-SPWeb $targetUrl -ErrorAction SilentlyContinue
	
	if ($web)
	{
		Write-Host "Variation:" $web.ServerRelativeUrl
		Write-Host "  Change Name: " $web.Title "to" $enName
		$web.Title = $enName
		$web.Update()
		
		Write-Host "  Change Relative URL:" $svUrl "to" $enUrl
		Set-SPWeb -Identity:$web -RelativeUrl $enUrl
	}
}

function RenameSiteVariation
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webapp = $_.Url		
		$_.SiteCollections.SiteCollection | ForEach-Object {
			$siteurl =  [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($webapp, $_.Url)
			$_.Sites.Site | ForEach-Object {
			
				if($_.IsRootWeb -or $_.IsLabel) { return }
				
				$svUrl = $_.SvUrl
				$enUrl = $_.EnUrl
				$enName = $_.EnName
				
				$targetUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($siteurl, "/en-us/" + $svUrl)
				
				#check if site has already been renamed
				$site = Get-SPWeb $targetUrl -ErrorAction SilentlyContinue
				if($site)
				{
					RenameSiteFromSvToEn $targetUrl $enUrl $enName $svUrl 
				}
				else
				{
					$relativeUrl = "/en-us/" + $enUrl
					$newsiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($siteurl, $relativeUrl)
					$newsite = Get-SPWeb $newsiteUrl -ErrorAction SilentlyContinue
					if($newsite)
					{
						Write-Host "Site has already been renamed: $relativeUrl"
					}
					else
					{
						Write-Host "Site not found: $relativeUrl" -f Red
					}
				}
			}
		}
	}
}

function CheckIfPageExists($pubweb, $page)
{
	[Microsoft.SharePoint.Publishing.PublishingPage]$pageFile = $pubweb.GetPublishingPages() | Where-Object {$_.Name -eq $page}	
	if($pageFile) 
	{
		return $true
	}
	else
	{
		Write-Host "   Page not found: "-f Yellow	-NoNewLine
		Write-Host $page
		return $false
	}
}

function CheckIfTargetAlreadyRenamed($targetweb, $targetPage)
{
	[Microsoft.SharePoint.Publishing.PublishingPage]$pageFile = $targetpubweb.GetPublishingPages() | Where-Object {$_.Name -eq $targetPage}
	if($pageFile) { 
		return $true 
	}
	else { 
		return $false 
	}
}

function CheckIfTargetTitleAlreadyUpdated($targetweb, $targetPage, $targetTitle, $targetVariation)
{
	[Microsoft.SharePoint.Publishing.PublishingPage]$pageFile = $targetpubweb.GetPublishingPages() | Where-Object {$_.Name -eq $targetPage}
	
	$ret = $false
	if($pageFile) 
	{ 
		if($targetVariation -eq "en") { $title = "Title" }
		else { $title = "Rubrik" }
		if($pageFile.ListItem[$title] -eq $targetTitle) { $ret = $true }
	}
	 
	return $ret
	
}

function RenameTargetPage($pubweb, $sourceName, $targetName, $targetTitle, $targetVariation)
{
	[Microsoft.SharePoint.Publishing.PublishingPage]$page = $pubweb.GetPublishingPages() | Where-Object {$_.Name -eq $sourceName}							
	if($page)
	{
		Write-Host "   Rename: " $sourceName  "to"  $targetName -NoNewLine
		$page.CheckOut();
		
		$name = "Namn"
		$title = "Rubrik"
		
		if($targetVariation -eq "en")  
		{
			$name = "Name"
			$title = "Title"
		}
		
		$page.ListItem[$name] = $targetName
		$page.ListItem[$title] = $targetTitle 
		
		$page.Update();
		$page.ListItem.File.CheckIn("Update page name", [Microsoft.SharePoint.SPCheckinType]::MajorCheckIn);
		$page.ListItem.File.Publish("Update page name - Publish");
		
		Write-Host "...Done"
	}
	else
	{
		Write-Host "   Page does not exists:" $sourceName
	}
}

function RenameTargetTitle($pubweb, $sourceName, $targetName, $targetTitle, $targetVariation)
{
	[Microsoft.SharePoint.Publishing.PublishingPage]$page = $pubweb.GetPublishingPages() | Where-Object {$_.Name -eq $targetName}							
	
	if($page)
	{
		Write-Host "   Updating page title to" $targetTitle -NoNewLine
		$page.CheckOut();
			
		if($targetVariation -eq "en") { $title = "Title" }
		else { $title = "Rubrik" }
		
		$page.ListItem[$title] = $targetTitle 
		
		$page.Update();	
		$page.ListItem.File.CheckIn("Update page title", [Microsoft.SharePoint.SPCheckinType]::MajorCheckIn);
		$page.ListItem.File.Publish("Update page title - Publish");
		
		Write-Host "...Done"
	}
	else
	{
		Write-Host "   Page does not exists:" $targetName
	}
}

function GetPublishingWeb($label, $source, $target)
{
	$publishingweb = $null
	
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webappurl = $_.Url
		$_.SiteCollections.SiteCollection | ForEach-Object {
			$sitecoll = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($webappurl, $_.Url)			
			$_.Sites | ForEach-Object {
				if($label -eq "source") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.SourceUrl) }
				elseif($label -eq "en") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.EnUrl) }
				elseif($label -eq "sv") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.SvUrl) }
				
				$_.Site | ForEach-Object {					
					if($_.SvUrl -eq $source) {
						if($label -eq "source") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.SvUrl) }
						elseif($label -eq "en") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.EnUrl) }
						elseif($label -eq "sv") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.SvUrl) }						
					}
				}
			}
		}
	}	
	
	$web = Get-SPWeb -Identity $websiteUrl -ErrorAction SilentlyContinue	
	if($web -eq $null)  { Write-Host "Variation does not exists:" $websiteUrl -f Yellow }
	else { $publishingweb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($web); }
	
	return $publishingweb
	
}

function RenamePage($targetVariation)
{
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication.SiteCollections.SiteCollection.Sites.Site | ForEach-Object {
		
		if($_.IsRootWeb) { return }
		
		$pubweb = GetPublishingWeb "source" $_.SvUrl $_.SvUrl	
		$targetpubweb = GetPublishingWeb $targetVariation $_.SvUrl $_.EnUrl
		
		if($pubweb -eq $null) { return }
		if($targetpubweb -eq $null) { return }
		
		Write-Host "Variation:" $targetpubweb.Uri.AbsolutePath
		
		$_.Pages.Page | ForEach-Object {
			if($_  -eq $null) 
			{ 
				Write-Host "   (no page to rename)"
				return 
			}
			
			$sourcePage = $_.Source
			$targetPage = $_.Sv
			$pages = "Sidor"
			$targetPageTitle = $_.SvTitle
			if($targetVariation -eq "en") 
			{ 
				$targetPage = $_.En 
				$targetPageTitle = $_.EnTitle
				$pages = "Pages"
			}
			
			#check if target page has been propagated in variation
			$targetExists = CheckIfPageExists $targetpubweb $sourcePage
			$isRenamed = CheckIfTargetAlreadyRenamed $targetpubweb $targetPage
			
			if($isRenamed)
			{
				Write-Host "   Page already renamed: " -NoNewLine
				Write-Host $targetPage
				
				if($targetPageTitle -ne $null)
				{
					$isTitleUpdated = CheckIfTargetTitleAlreadyUpdated $targetpubweb $targetPage $targetPageTitle $targetVariation
					if(!$isTitleUpdated)
					{
						RenameTargetTitle $targetpubweb $sourcePage $targetPage $targetPageTitle $targetVariation
					}
				}
			}
			elseif($targetExists)
			{
				#Rename page
				RenameTargetPage $targetpubweb $sourcePage $targetPage $targetPageTitle $targetVariation
			}
			
		}
		$targetpubweb.Update();	
	}		
}

function PropagateSiteVariation
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	Write-Host ""
	$allsource = CheckIfSiteExists "sv"
	
	if($allsource) 
	{		
		Write-Host "All SV sites exists"
		Write-Host ""
		
		$i = 0;
		$maxAttempts = 2;
		
		$allVariationsEN = $false
		while (!$allVariationsEN ) 
		{
			$i++
			RunVariationsPropagateSiteJobDefinition $ConfigXML
			$allVariationsEN = CheckIfSiteExists "en"
			Start-Sleep -Seconds 10
			
			if($i -eq $maxAttempts) { break; }
		}	

		if($allVariationsEN) {  Write-Host "All EN sites exists" }
		
	}
}

function PropagatePages
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	$i = 0;
	$allPagesEN = $false
	$maxAttempts = 5;
	while (!$allPagesEN ) 
		{
			$i++
			RunVariationsPropagatePageTimerJob $ConfigXML
			
			$allPagesEN = CheckPeerPageInVariation $ConfigXML "en"
			
			if($allPagesEN) 
			{  
				Write-Host "All pages are propagated in EN" -f Green
				Write-Host ""
				EnsurePageIsPublished "en"
				break; 
			}
			else 
			{ 
				#Republish pages on first attempt if pages are not propagated
				if($i -eq 1) { RePublishSourcePage $ConfigXML }
				
				RunVariationsPropagatePageTimerJob $ConfigXML
				
			}
		
			if($i -eq $maxAttempts) { break; }
			Start-Sleep -Seconds 5
		}	
}

function RePublishSourcePage
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication.SiteCollections.SiteCollection.Sites.Site | ForEach-Object {
		
		if($_.IsRootWeb) { return }
		
		$pubweb = GetPublishingWeb "sv" $_.SvUrl $_.SvUrl	
		if($pubweb -eq $null) { return }
		
		Write-Host "Site:" $pubweb.Uri.AbsolutePath
		
		$pubweb.GetPublishingPages() | ForEach-Object {
			[Microsoft.SharePoint.Publishing.PublishingPage]$page = $_

			if($page.ListItem.File.Level.Equals([Microsoft.SharePoint.SPFileLevel]::Published))
			{
				Write-Host "  Republishing Page:"  $page.Name "..." -NoNewLine
				$page.ListItem.File.Unpublish("Deployment Script Propagate: Unpublish");
				$page.ListItem.File.Publish("Deployment Script Propagate: Publish");
				Write-Host "Done"				
			}
			else
			{
				if($page.ListItem.File.Level.Equals([Microsoft.SharePoint.SPFileLevel]::Checkout))
				{
					Write-Host "  Page is currently checked-out. Checking-in the page: "  $page.Name "..." -NoNewLine
					$page.ListItem.File.CheckIn("Check In - Deployment", [Microsoft.SharePoint.SPCheckinType]::MajorCheckIn)
					Write-Host "Done"
				}
			
				Write-Host "  Publishing Page:"  $page.Name "..." -NoNewLine
				$page.ListItem.File.Publish("Published - Deployment");
				Write-Host "Done"				
			}
		}
		
		$pubweb.Update();
	}		
}

function EnsurePageIsPublished($label)
{
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication.SiteCollections.SiteCollection.Sites.Site | ForEach-Object {
		
		if($_.IsRootWeb) { return }
		
		$pubweb = GetPublishingWeb $label $_.SvUrl $_.SvUrl	
		if($pubweb -eq $null) { return }
		
		Write-Host "Site:" $pubweb.Uri.AbsolutePath
		
		$pubweb.GetPublishingPages() | ForEach-Object {
			[Microsoft.SharePoint.Publishing.PublishingPage]$page = $_

			if($page.ListItem.File.Level.Equals([Microsoft.SharePoint.SPFileLevel]::Published))
			{
				#Check if published a  major version
				if($page.ListItem["Approval Status"] -eq 3)
				{
					Write-Host "  Publishing Page:"  $page.Name "..." -NoNewLine
					$page.ListItem.File.Publish("Published - Deployment");
					Write-Host "Done"	
				}
				else
				{
					Write-Host "  Page already published:"  $page.Name
				}
			}
			else
			{
				if($page.ListItem.File.Level.Equals([Microsoft.SharePoint.SPFileLevel]::Checkout))
				{
					Write-Host "  Page is currently checked-out. Checking-in the page: "  $page.Name "..." -NoNewLine
					$page.ListItem.File.CheckIn("Check In - Deployment", [Microsoft.SharePoint.SPCheckinType]::MajorCheckIn)
					Write-Host "Done"
				}
			
				Write-Host "  Publishing Page:"  $page.Name "..." -NoNewLine
				$page.ListItem.File.Publish("Published - Deployment");
				Write-Host "Done"				
			}
		}
		
		$pubweb.Update();
	}		
}

function AddMimeTypes
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webappurl = $_.Url
		$webapp = Get-SPWebApplication -Identity $webappurl -ErrorAction SilentlyContinue 
	
		if($webapp)
		{
			if($webapp.AllowedInlineDownloadedMimeTypes.Contains("text/html"))
			{	
				Write-Host "    - HTML: text/html already added" 
			}
			else
			{
				Write-Host "    - HTML"
				$webapp.AllowedInlineDownloadedMimeTypes.Add("text/html")
			}
			
			if($webapp.AllowedInlineDownloadedMimeTypes.Contains("application/pdf"))
			{
				Write-Host "    - PDF: application/pdf already added" 
			}
			else
			{
				Write-Host "    - PDF"
				$webapp.AllowedInlineDownloadedMimeTypes.Add("application/pdf")
			}
			$webapp.Update()
			Write-Host "Done"
		}
		else
		{
			Write-Host "Web Application does not exist: " $webappurl -f Red
			return
		}
	}
}


function GetSPWeb($label, $source)
{	
	$web  = $null
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication | ForEach-Object {
		$webappurl = $_.Url
		$_.SiteCollections.SiteCollection | ForEach-Object {
			$sitecoll = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($webappurl, $_.Url)			
			$_.Sites | ForEach-Object {
				if($label -eq "source") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.SourceUrl) }
				elseif($label -eq "en") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.EnUrl) }
				elseif($label -eq "sv") { $site = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($sitecoll, $_.SvUrl) }
				
				$_.Site | ForEach-Object {					
					if($_.SvUrl -eq $source) {
						if($label -eq "source") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.SvUrl) }
						elseif($label -eq "en") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.EnUrl) }
						elseif($label -eq "sv") { $websiteUrl = [Microsoft.SharePoint.Utilities.SPUtility]::ConcatUrls($site, $_.SvUrl) }						
					}
				}
			}
		}
	}	
	
	$web = Get-SPWeb -Identity $websiteUrl -ErrorAction SilentlyContinue	
	if($web -eq $null)  { Write-Host "Variation does not exists:" $websiteUrl -f Yellow }
	
	return $web
	
}

function UploadFile($web, $docLibrary, $filePath, $docName)
{	
	$folder = $web.GetFolder($docLibrary)
	
	$currentPath = (Get-Location -PSProvider FileSystem).ProviderPath
	$path = $currentPath + "\" + $filePath
	
	$file = Get-Item $path
	$fileURL = $docLibrary + "/" + $docName;
	
	Write-Host "    File Upload:" $fileURL "..." -NoNewLine
	#Overwrite files if exists
	[void] $folder.Files.Add($fileURL,$file.OpenRead(),$true) 
	Write-Host "Done"
}


function UploadDocuments
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	$ConfigXML.Deployment.ProjectSpecific.WebApplications.WebApplication.SiteCollections.SiteCollection.Sites | ForEach-Object {
		$_.Site | ForEach-Object {
			
			$sv = GetSPWeb "sv" $_.SvUrl
			$en = GetSPWeb "en" $_.SvUrl
		
			if($sv) {
				$_.Upload.File | ForEach-Object {
					if($_.SV -eq $null) { return }		
					Write-Host $sv.Url
					UploadFile $sv $_.SV.DocumentLibrary $_.SV.FilePath $_.SV.Name
				}
			}
			
			if($en) {			
				$_.Upload.File | ForEach-Object {
					if($_.EN -eq $null) { return }					
					Write-Host $en.Url
					UploadFile $en $_.EN.DocumentLibrary $_.EN.FilePath $_.EN.Name
				}
			}
		}
	}
}

function ApplyProjectSpecific
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	$mark = '##############'
	
	#Make sure the sites are propagated to all child
	Write-Host $mark "Propagate Pages To Variation" $mark
	PropagatePages $ConfigXML 
	Write-Host ""
	
	#Rename Sites From Swedish to English
	Write-Host $mark "Rename English Site Variations" $mark 
	RenameSiteVariation $ConfigXML
	Write-Host ""
	
	#Rename Pages in SV 
	Write-Host $mark "Rename SV Variation Pages" $mark
	RenamePage "sv"
	Write-Host ""
	
	#Publish Pages in SV
	Write-Host $mark "Ensure Pages are Published - SV" $mark 
	EnsurePageIsPublished "sv"
	Write-Host ""
	
	#Initiate propagation
	Write-Host "Propagate changes to Variation"
	RunVariationsPropagatePageTimerJob $ConfigXML 
	Write-Host "Done"
	Write-Host ""
	
	#Rename Pages in EN
	Write-Host $mark "Rename EN Variation Pages" $mark 
	RenamePage "en"
	Write-Host ""
	
	#Publish Pages in SV
	Write-Host $mark "Ensure Pages are Published - EN" $mark 
	EnsurePageIsPublished "en"
	Write-Host ""
	
	#Add Mime-Types in Web Application
	Write-Host $mark "Add Mime Types" $mark
	AddMimeTypes $ConfigXML
	Write-Host ""
	
	#Upload documents
	Write-Host $mark "Upload Documents" $mark
	UploadDocuments $ConfigXML
	Write-Host ""
}


