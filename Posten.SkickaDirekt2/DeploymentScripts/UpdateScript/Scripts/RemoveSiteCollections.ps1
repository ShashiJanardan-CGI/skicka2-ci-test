function RemoveSiteCollection
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)
	
	PrintSpliter
	PrintLineMSG "Removing of SiteCollection(s) Start"
	
	$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {             
		[string]$webName = $_.Name

		$_.SiteCollections.SiteCollection | ForEach-Object {
			[string]$url = $_.CreateSiteCollections.URL 
			[string]$name = $_.CreateSiteCollections.Name 
			[bool]$hostHeaderSite = [bool]::Parse($_.CreateSiteCollections.IsHostHeaderSite)

			$webApplication = $null
			$webApplication = Get-SPWebApplication -ErrorAction SilentlyContinue | Where {$_.DisplayName -eq $webName}							
			if($webApplication -eq $null)
			{
				StopScript "RemoveSiteCollection" "The Specified Web Application doesn't Exist OR UnAvailable."
			}
				
			if($hostHeaderSite)
			{
				$sp = $null
				$sp = Get-SPSite -Identity $url -ErrorAction SilentlyContinue

				if($sp -eq $null)
				{
					PrintLineMSG "Site Collection [$url] Doesn't exists. Success."
				} 
				else
				{
					Try
            		{              
                		PrintMSG "Deleting Site Collection .... $url"		
						Remove-SPSite -Identity $url -Confirm:$false
						PrintLineMSG "Site Created. DONE! "
					} 
            		Catch
            		{
    					StopScript "RemoveSiteCollection" $_
    				} 
            		Finally { }          
				}
			}
			else
			{
				$webURL = $webApplication.URL
				$siteURL = $webURL += $url		
				$sp = $null
				$sp = Get-SPSite -Identity $siteURL -ErrorAction SilentlyContinue

				if($sp -eq $null)
				{
					PrintLineMSG "Site Collection [$url] Doesn't exists. Success."
				} 
				else
				{
					Try
            		{              
                		PrintMSG "Deleting Site Collection .... $siteURL"		
						Remove-SPSite -Identity $siteURL -Confirm:$false
						PrintMSG "Site Deleted. DONE! "
					} 
            		Catch
            		{
    					StopScript "RemoveSiteCollection" $_
    				} 
            		Finally { }          
				}				
			}
		}
	}

	PrintLineMSG "Removing of SiteCollection(s) Ends."
	PrintSpliter	
}