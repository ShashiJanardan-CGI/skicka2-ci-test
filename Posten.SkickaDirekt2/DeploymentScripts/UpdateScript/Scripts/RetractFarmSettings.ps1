# Compile Utility Scripts
. .\Scripts\UninstallSolutions.ps1
. .\Scripts\DeactivateFeatures.ps1

function RetractFarmSettings
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,	
		[switch]$UnInstallSolutions,
		[switch]$DeactivateFeatures
	)
	
	if($DeactivateFeatures)
	{
		DisableFeatures $ConfigXML -Farm
	}

	if($UnInstallSolutions)
	{
		UnInstallSolutions $ConfigXML -Farm
	}
}