# Compile Utility Scripts
. .\Scripts\UninstallSolutions.ps1
. .\Scripts\DeactivateFeatures.ps1
. .\Scripts\DeleteWebApplications.ps1

function RetractWebApplication
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$DeactivateFeatures,
		[switch]$RetractConfigChanges,
		[switch]$RetractSettings,
		[switch]$UnInstallSolutions,
		[switch]$RemoveApplication
	)

	if($DeactivateFeatures)
	{
		DisableFeatures $ConfigXML -Web
	}

	if($RetractConfigChanges)
	{
	}

	if($RetractSettings)
	{
	}

	if($UnInstallSolutions)
	{
		UnInstallSolutions $ConfigXML
	}

	if($RemoveApplication)
	{
		DeleteWebApplications $ConfigXML
	}
}