$web = Get-SPWeb http://lt-extern-dr.portaltest.posten.se/dr/

$group = $web.SiteGroups["DRUsers"]
$role = $web.RoleDefinitions["Delta"]
$assignment = New-Object Microsoft.SharePoint.SPRoleAssignment($group)
$assignment.RoleDefinitionBindings.Add($role)

$listNames = ("FAQ","F�rger","Format",
"Helgdagar","Hj�lptexter", "Ingresstexter","Konfiguration","Kampanjkoder",
"ODR-kalender","Ordrar","Referensexemplar",
"Storlek","Systemkonfigurering","Teman","Tj�nster","Urval (ADR)",
"Urval (ODR)","Urvalskonfigurering","Versioner","Villkor", "Youtubevideos"
)


$listNames |ForEach-Object {
$list = $web.Lists[$_]
if (!$list.HasUniqueRoleAssignments) 
{
        $list.BreakRoleInheritance($true)
}

$list.RoleAssignments.Add($assignment)
}