function UnInstallSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Farm
	)

    if($Farm)
	{
		RetractFarmSolutions $ConfigXML
		RemoveSolutions $ConfigXML -Farm
	}
	else
	{
		RetractWebSolutions $ConfigXML
		RemoveSolutions $ConfigXML
	}
}

function RetractFarmSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Un-Installing (Retracting) Farm Solutions"
	Try
	{
		$ConfigXML.Deployment.Standard.Farm.Solutions.Solution | ForEach-Object {
			$name = $_.Name       
			if ([string]::IsNullOrEmpty($name))
			{
				PrintLineMSG "Nothing to retract."
			}
			else
			{
				$Solution = Get-SPSolution $name -ErrorAction SilentlyContinue
				if($Solution)
				{
					if ($Solution.Deployed)
					{
						PrintMSG "Retracting solution [$name]..."
						$Solution | UnInstall-SPSolution -Confirm:$false
            					
						#Block until we're sure the solution is deployed.
            			do 
						{ 
							Start-Sleep 2 
							PrintMSG "... "
						} while (((Get-SPSolution $name).Deployed))
						PrintLineMSG "DONE!"
					}
        		}
				else
				{
					PrintLineMSG "Can't find a solution with a name of $name to retract. Success!"
				}
			}
		}
	}
	Catch
		{
    		StopScript "RetractFarmSolutions" $_
		} 
		Finally { }
		 
	PrintLineMSG "Farm Solutions Retracted."	
	PrintSpliter
}


function RetractWebSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Un-Installing (Retracting) Web Solutions"
	Try
	{
		$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {
			$URL = $_.Url
			$WebName = $_.Name
			$_.Solutions.Solution | ForEach-Object {
				$name = $_.Name       
				if ([string]::IsNullOrEmpty($name))
				{
					PrintLineMSG "Nothing to retract."
				}
				else
				{
					$webApplication = $null
					$webApplication = Get-SPWebApplication $WebName
					if($webApplication -eq $null)
					{
						StopScript "ActivateWebFeatures" "There is no Web Application on Provided URL [$URL]"
					}

					$Solution = Get-SPSolution $name -ErrorAction SilentlyContinue
					if($Solution)
					{
						if ($Solution.Deployed)
						{
							PrintMSG "Retracting solution [$name] FROM [$webApplication.URL]"
							$Solution | UnInstall-SPSolution -WebApplication $webApplication.URL -Confirm:$false
            					
							#$counter = 1
							#$maximum = 50
							#while( $solution.JobExists -and ( $counter -lt $maximum ) ) {
							#	PrintMSG "... "
							#	Start-Sleep 2
							#	$counter++
							#}

							while( $solution.JobExists) {
								PrintMSG "... "
								Start-Sleep 2								
							}
							PrintLineMSG "DONE!"
						}
        			}
					else
					{
						PrintLineMSG "Can't find a solution with a name of $name to retract. Success!"
					}
				}
			}
		}
	}
	Catch
		{
    		StopScript "RetractWebSolutions" $_
		} 
		Finally { }
	PrintLineMSG "Web Solutions Retracted."	
	PrintSpliter
}

function RemoveSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Farm
	)

    if($Farm)
	{
		RemoveFarmSolutions $ConfigXML
	}
	else
	{
		RemoveWebSolutions $ConfigXML
	}
}

function RemoveFarmSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Removing Farm Solutions"
	Try
	{
		$ConfigXML.Deployment.Standard.Farm.Solutions.Solution | ForEach-Object {
			$name = $_.Name
        
			if ([string]::IsNullOrEmpty($name))
			{
				Write-Host "Nothing to remove."
			}
			else
			{
				$Solution = Get-SPSolution $name -ErrorAction SilentlyContinue
				if ($Solution)
				{
					if(!$Solution.Deployed)
					{
						PrintMSG "Removing $name solution ... "
        				$Solution | Remove-SPSolution -Confirm:$false -Force
						PrintLineMSG " Done!"
        			}
				}
				else
				{
					Write-Host "Can't find a solution with a name of $name to remove. Success!"
				}
			}
		}
	}
	Catch
		{
    		StopScript "RemoveFarmSolutions" $_
		} 
		Finally { }

	PrintLineMSG "Farm Solutions Removed"
	PrintSpliter
}

function RemoveWebSolutions
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Removing Web Solutions"
	Try
	{
		$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {
			$_.Solutions.Solution | ForEach-Object {
				$name = $_.Name
        
				if ([string]::IsNullOrEmpty($name))
				{
					PrintLineMSG "Nothing to remove."
				}
				else
				{	
					$solution = $null
					$Solution = Get-SPSolution $name -ErrorAction SilentlyContinue
					if ($Solution -eq $null)
					{
						PrintLineMSG "Can't find a solution with a name of $name to remove. Success!"
					}
					else
					{
							$counter = 1
							$maximum = 50
							while( $solution.JobExists -and ( $counter -lt $maximum ) ) {
								PrintMSG "..."
								Start-Sleep 2
								$counter++
							}

						if(!$Solution.Deployed)
						{
							PrintMSG "Removing $name solution..."
        					$Solution | Remove-SPSolution -Confirm:$false -Force
							PrintLineMSG " Done!"
        				}
						else
						{
							PrintLineMSG "Please Un-Install the Solution $solution Before Trying to Retract it."
						}
					}
				}
			}
		}
	}
	Catch
		{
    		StopScript "RemoveWebSolutions" $_
		} 
		Finally { }
	PrintLineMSG "Web Solutions Removed"
	PrintSpliter
}