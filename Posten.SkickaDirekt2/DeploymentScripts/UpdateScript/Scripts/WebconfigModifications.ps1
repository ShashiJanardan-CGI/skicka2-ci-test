
# Adding PowerShell Snap In
if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) 
{
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}

$virtualdirectorypath = "C:\inetpub\wwwroot\wss\VirtualDirectories"
$backupPath = "..\Backup\webconfig\VirtualDirectory_" + (Get-Date -Format yyyyMMdd_HHmmss)
$folders = Get-ChildItem -LiteralPath  $virtualdirectorypath | ? {$_.PSIsContainer }

function CreateFolder($folderName)
{
	$path = $backupPath + "\" + $folderName
	if (!(Get-Item -LiteralPath $path -ErrorAction SilentlyContinue))
	{
		New-Item $path -type directory > $null
	}
}

function CopyWebConfigFile($folderName)
{
	$config = "";
	$config = $virtualdirectorypath + "\" + $folderName + "\web.config"
	
	$destination = $backupPath + "\" + $folderName
	Copy-Item  $config $destination
}

function BackupWebconfig
{
	Write-Host "Backup Folder:" $backupPath -f Green

	foreach($folder in $folders)
	{
		Write-Host "Getting Backup of" $folder.Name -NoNewLine
		CreateFolder($folder.Name)
		CopyWebConfigFile($folder.Name)
		Write-Host "... Done"
	}
	
	Write-Host "Backup web.config files... Done" -f Green
}

function CheckWebConfigTimerJob($webapp)
{
    $isRunning = $false
    
	#Get job definition / running jobs 
	$pendingJobs = $webapp.WebService.JobDefinitions | Where-Object {$_.Name -like "*job-webconfig*"}
	$runningJobs = $webapp.WebService.RunningJobs | Where-Object {$_.Name -like "*Web.Config*"}
	
	$isRunning = (($pendingJobs.Count  -gt 0) -or ($runningJobs.Count  -gt 0))

    return $isRunning
}

function WaitForWebConfigTimerJobToComplete($webapp)
{
	Write-Host "Waiting for running webconfig modifications..." -f Gray -NoNewLine
	while(CheckWebConfigTimerJob $webapp)
	{
		Start-Sleep -Seconds 2
	}
	Write-Host "Done" -f Gray
	Write-Host ""
}

function UpdateWebConfig
{
	param ( $configXML )

	$configXML.Deployment.Standard.WebApplications.WebApplication | Foreach-Object {
		$webappURL = $_.Url
		$webapplication = Get-SPWebApplication -Identity $webappURL -ErrorAction SilentlyContinue
		if(!$webapplication) { Write-Host "Web application does not exists: " $webappURL -f Yellow; return; }
		
		$_.webconfigmodifications | Foreach-Object {
			if(!$_) { Write-Host "No webconfig modifications to implement" -f Yellow; return; }
			if(!$_.webconfigmodification) { Write-Host "No webconfig modifications to implement" -f Yellow; return; }
			
			$owner = $_.Owner
			
			Write-Host ""
			Write-Host "Web Application: " $webappURL
			
			$webapp = [Microsoft.SharePoint.Administration.SPWebService]::ContentService.WebApplications[$webapplication.Id]
			WaitForWebConfigTimerJobToComplete $webapp		
			
			$webappConfigModifcation = $webapp.WebConfigModifications
			
			$_.webconfigmodification | % { EnsureChildNode $webapp $owner $_.path $_.name $_.value.InnerXML	}
			
			Write-Host ""
			Write-Host "Applying changes to web application...." -f Gray -NoNewLine
			$webapp.WebService.ApplyWebConfigModifications()
			$webapp.Update()
			Write-Host "Done" -f Gray
			Write-Host ""
		}
	}
}

function EnsureChildNode($webapp, $owner, $path, $name, $value)
{	
	$modif = New-Object Microsoft.SharePoint.Administration.SPWebConfigModification
	$modif.Owner = $owner
	$modif.Path = $path
	$modif.Name = $name
	$modif.Value = $value
	$modif.Type = 0
	
	Write-Host "  [EnsureChildNode]" $name 
	
	$webapp.WebConfigModifications.Add($modif)
}

function EnsureAttribute($webapp, $owner, $path, $name, $value)
{
	$modif = New-Object Microsoft.SharePoint.Administration.SPWebConfigModification
	$modif.Owner = $owner
	$modif.Path = $path
	$modif.Name = $name
	$modif.Value = $value
	$modif.Type = 1
	
	Write-Host "	[Ensure Attribute] --> Name:" $name 
	
	$webapp.WebConfigModifications.Add($modif)
}
