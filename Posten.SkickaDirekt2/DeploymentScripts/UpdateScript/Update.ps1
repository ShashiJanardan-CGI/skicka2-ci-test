

[string] $ConfigFile = "Configurations.xml"


# Script references
. .\Scripts\ExtensionMethods.ps1
. .\Scripts\IISResetCommand.ps1
. .\Scripts\RestartTimerService.ps1


function CheckDirectory
{
    # Create directory if does not exist
    if (!(Test-Path -Path $DirectoryPath)) 
	{ 
	New-Item $DirectoryPath -Type Directory > $null
	}
}

$DateTime = Get-Date -UFormat "%Y-%m-%d %H%M%S"
$DirectoryPath = [System.IO.Path]::Combine($Pwd, "Logs")
CheckDirectory # calling function to check directory
$LogFile = $DirectoryPath + "\" + "CustomerPortal" + "Update" + $DateTime + ".log"

[xml]$ConfigXML = $null
# Read Configuration File and Get Sections of XML file
GetConfigSections -FilePath $ConfigFile -ConfigSections([ref]$ConfigXML)

#Begining of Start-transcript
#Start-Transcript -Path $LogFile -Force

# Ensure Powershell snap-in is loaded
if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}

RestartTimerService
IISResetCommand

PrintSectionSpliter "Update Farm Solution(s) Starts"

$ConfigXML.Deployment.Standard.Farm.Solutions.Solution | ForEach-Object {
	$name = $_.Name			
	$wspPath = "$pwd\Packages\$name"
	PrintMSG "Updating solution $name..."
	Update-SPSolution -Identity $name -LiteralPath $wspPath -GACDeployment

	$solution = Get-SPSolution $name ;
	while ($solution.JobExists -eq $true) {
		Write-Host ".." -NoNewline;
		sleep 2;
		$solution = Get-SPSolution $name ;
	}		
PrintLineMSG "Done"		
}
	
PrintSectionSpliter "Update Farm Solution(s) Ended."

RestartTimerService
IISResetCommand

# Stopping Start-transcript
#Stop-transcript