﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;

    [Serializable]
    public class AccountValidationResponseBE
    {
        #region Fields

        private string account;
        private string accountHolder;
        private bool ocr;
        private int refCheckStatus;
        private string reference;
        private bool valid;
        private bool validFormat;

        #endregion Fields

        #region Properties

        public string Account
        {
            get { return this.account; }
            set { this.account = value; }
        }

        public string AccountHolder
        {
            get { return this.accountHolder; }
            set { this.accountHolder = value; }
        }

        public bool Ocr
        {
            get { return this.ocr; }
            set { this.ocr = value; }
        }

        public int RefCheckStatus
        {
            get { return this.refCheckStatus; }
            set { this.refCheckStatus = value; }
        }

        public string Reference
        {
            get { return this.reference; }
            set { this.reference = value; }
        }

        public bool Valid
        {
            get { return this.valid; }
            set { this.valid = value; }
        }

        public bool ValidFormat
        {
            get { return this.validFormat; }
            set { this.validFormat = value; }
        }

        #endregion Properties
    }
}