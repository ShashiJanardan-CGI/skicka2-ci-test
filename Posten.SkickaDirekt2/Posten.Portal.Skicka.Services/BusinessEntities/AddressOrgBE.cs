﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using Posten.Portal.Skicka.PVT.Proxy;
    using System;

    [Serializable]
    public class AddressBE
    {
        #region Properties

        public string CareOf
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public string Company
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public string CountryName
        {
            get;
            set;
        }

        public string DoorCode
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string FullName
        {
            get;
            set;
        }

        public string Mobile
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

        public string Street
        {
            get;
            set;
        }

        public string Zip
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods
        public Party DataAccessObjectToBusinessEntity()
        {
            Party dataAccessObject = new Party();
            dataAccessObject.adress1 = this.CareOf;
            dataAccessObject.adress2 = this.Street;
            dataAccessObject.city = this.City;
            dataAccessObject.company = this.Company;
            dataAccessObject.country = this.Country;
            dataAccessObject.email = this.Email;
            dataAccessObject.mobile = this.Mobile;
            dataAccessObject.name = this.FullName;
            dataAccessObject.phone = this.PhoneNumber;
            dataAccessObject.zip = this.Zip;

            return dataAccessObject;
        }


        #endregion Methods
    }
}