﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    /// <summary>
    /// Contains information about the destination.
    /// </summary>
    [Serializable]
    [DataContract]
    public class DestinationBE
    {
        #region Constructors

        public DestinationBE()
        {
        }

        public DestinationBE(destination dataAccessObject)
        {
            this.DataAccessObjectToBusinessEntity(dataAccessObject);
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Unique id in PVT for this country/destination.
        /// </summary>
        [DataMember]
        public string DestinationID
        {
            get;
            set;
        }

        /// <summary>
        /// True if the destination is within EU.
        /// </summary>
        [DataMember]
        public bool EU
        {
            get;
            set;
        }

        /// <summary>
        /// True if the destination is within Europe.
        /// </summary>
        [DataMember]
        public bool Europe
        {
            get;
            set;
        }

        [DataMember]
        public string Information
        {
            get;
            set;
        }

        /// <summary>
        /// ISO in PVT for the destination.
        /// </summary>
        [DataMember]
        public string ISO
        {
            get;
            set;
        }

        /// <summary>
        /// Name in PVT of the destination.
        /// </summary>
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// ZipCodeDigits in PVT of the destination.
        /// </summary>
        [DataMember]
        public int ZipCodeDigits
        {
            get;
            set;
        }

        /// <summary>
        /// ZipCodeDigitsSpecified in PVT of the destination.
        /// </summary>
        [DataMember]
        public bool ZipCodeDigitsSpecified
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private void DataAccessObjectToBusinessEntity(destination dataAccessObject)
        {
            this.Information = dataAccessObject.information;
            this.Name = dataAccessObject.name;
            this.DestinationID = dataAccessObject.destinationId;
            this.ISO = dataAccessObject.iso2Code;
            this.EU = dataAccessObject.eu;
            this.Europe = dataAccessObject.europe;
            this.ZipCodeDigits = dataAccessObject.zipCodeDigits;
            this.ZipCodeDigitsSpecified = dataAccessObject.zipCodeDigitsSpecified;
        }

        #endregion Methods
    }
}