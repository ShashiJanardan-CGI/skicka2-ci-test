﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System.Collections.Generic;

    public class InformPutInCartResponseBE
    {
        #region Properties

        public List<InfoBE> AdditionalInfo
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        #endregion Properties
    }
}