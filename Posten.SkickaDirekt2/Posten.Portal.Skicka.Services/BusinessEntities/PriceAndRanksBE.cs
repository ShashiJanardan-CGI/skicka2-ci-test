﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.Utils;

    public class PriceAndRanksBE : BaseBE
    {
        #region Constructors

        public PriceAndRanksBE()
        {
        }

        public PriceAndRanksBE(PriceAndRankResponse priceAndRankResponse)
            : base(priceAndRankResponse.status, priceAndRankResponse.statusMsg, priceAndRankResponse.responseInfos)
        {
            if (priceAndRankResponse.status == responseStatus.SUCCESS)
            {
                this.ProductSuggestions = Array.ConvertAll(
                       priceAndRankResponse.suggestedProducts,
                       p => this.TranslatePriceAndRankToPriceAndRankBE(p));
            }
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public PriceAndRankBE[] ProductSuggestions
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private PriceAndRankBE TranslatePriceAndRankToPriceAndRankBE(PriceAndRank priceAndRank)
        {
            try
            {
                return new PriceAndRankBE(priceAndRank);
            }
            catch (Exception ex)
            {
                LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #endregion Methods
    }
}