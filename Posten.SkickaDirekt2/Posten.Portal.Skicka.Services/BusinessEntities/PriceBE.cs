﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class PriceBE
    {
        #region Constructors

        public PriceBE()
        {
        }

        public PriceBE(Price price)
        {
            this.Currency = price.currency;
            this.Amount = price.amount;
            this.AmountNoVat = price.amountNoVat;
            this.VATAmount = price.vat;
        }

        public PriceBE(Skicka.SkickaService.Proxy.price1 price)
        {
            this.Currency = price.currency;
            this.Amount = price.amount;
            this.AmountNoVat = price.amountNoVat;
            this.VATAmount = price.vat;
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public double Amount
        {
            get;
            set;
        }

        [DataMember]
        public double AmountNoVat
        {
            get;
            set;
        }

        [DataMember]
        public string Currency
        {
            get;
            set;
        }

        [DataMember]
        public double VATAmount
        {
            get;
            set;
        }

        [DataMember]
        public decimal VATPercentage
        {
            get;
            set;
        }

        #endregion Properties
    }
}