﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class ServiceDocumentBE
    {
        #region Constructors

        public ServiceDocumentBE(Document document)
        {
            this.DataAccessObjectToBusinessEntity(document);
        }

        public ServiceDocumentBE()
        {
        }

        #endregion Constructors

        #region Enumerations

        public enum ServiceDocumentCategoryEnum
        {
            TOLL_DOCUMENT,
            INFORMATION_DOCUMENT
        }

        public enum ServiceDocumentTypeEnum
        {
            PDF,
            WORD,
            EXCEL,
            TXT
        }

        #endregion Enumerations

        #region Properties

        public ServiceDocumentCategoryEnum DocumentCategory
        {
            get;
            set;
        }

        public long DocumentId
        {
            get;
            set;
        }

        public ServiceDocumentTypeEnum DocumentType
        {
            get;
            set;
        }

        [DataMember]
        public string DocumentTypeName
        {
            set
            {
            }

            get
            {
                return Enum.GetName(typeof(ServiceDocumentTypeEnum), this.DocumentType);
            }
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Url
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private void DataAccessObjectToBusinessEntity(Document document)
        {
            this.DocumentCategory = (ServiceDocumentCategoryEnum)Enum.Parse(typeof(ServiceDocumentCategoryEnum), Enum.GetName(typeof(documentCategory), document.documentCategory));
            this.DocumentType = (ServiceDocumentTypeEnum)Enum.Parse(typeof(ServiceDocumentTypeEnum), Enum.GetName(typeof(documentType), document.documentType));
            this.DocumentId = document.id;
            this.Name = document.name;
            this.Url = document.url;
        }

        #endregion Methods
    }
}