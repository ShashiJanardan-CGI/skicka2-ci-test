﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class ServiceTextBE
    {
        #region Constructors

        public ServiceTextBE()
        {
        }

        public ServiceTextBE(Text text)
        {
            this.LabelKey = (ServiceTextLabelEnum)Enum.Parse(typeof(ServiceTextLabelEnum), Enum.GetName(typeof(label), text.labelKey));
            this.LabelName = text.labelName;
            this.Text = text.text;
        }

        #endregion Constructors

        #region Enumerations

        public enum ServiceTextLabelEnum
        {
            RECEIVER,
            MAX_WEIGHT,
            DELIVERY,
            SUBMISSION,
            TRACEABLE,
            ID_REQUIRED,
            DELIVERY_TIME,
            GOODS_VALUE,
            SIZE
        }

        #endregion Enumerations

        #region Properties

        [DataMember]
        public ServiceTextLabelEnum LabelKey
        {
            get;
            set;
        }

        [DataMember]
        public string LabelName
        {
            get;
            set;
        }

        [DataMember]
        public string Text
        {
            get;
            set;
        }

        #endregion Properties
    }
}