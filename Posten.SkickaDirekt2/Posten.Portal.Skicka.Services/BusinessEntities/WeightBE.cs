﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class WeightBE
    {
        #region Properties

        [DataMember]
        public string DisplayUnit
        {
            get;
            set;
        }

        [DataMember]
        public decimal DisplayValue
        {
            get;
            set;
        }

        [DataMember]
        public int WeightInGrams
        {
            get;
            set;
        }

        #endregion Properties
    }
}