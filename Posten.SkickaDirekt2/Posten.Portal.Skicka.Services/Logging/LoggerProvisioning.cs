﻿namespace Posten.Portal.Skicka.Services.Logging
{
    using Microsoft.SharePoint.Administration;

    using Posten.Portal.Skicka.Services.Resources;

    public class LoggerProvisioning
    {
        #region Nested Types

        private class PortalLoggerProvisioning
        {
            #region Methods

            public static void ProvisionPortalLogger()
            {
                LoggingService service = SPFarm.Local.Services.GetValue<LoggingService>(Constants.LogApplicationName);

                if (service == null)
                {
                    service = new LoggingService(Constants.LogApplicationName, SPFarm.Local);
                    service.Update();

                    if (service.Status != SPObjectStatus.Online)
                    {
                        service.Provision();
                    }
                }
            }

            public static void UnProvisionPortalLogger()
            {
                LoggingService service = SPFarm.Local.Services.GetValue<LoggingService>(Constants.LogApplicationName);
                if (service != null)
                {
                    service.Delete();
                }
            }

            #endregion Methods
        }

        #endregion Nested Types
    }
}