﻿namespace Posten.Portal.Skicka.Services.PVT
{
    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.Utils;
    using System.Collections.Generic;
    using System.Web;

    public class PVTController 
    {
        HttpContext context;

        public PVTController(HttpContext currentContext)
        {
            context = currentContext;
        }

        #region Methods



        public DestinationsBE GetDestinations()
        {
//#if DEBUG
//            return new PVTServiceStub().GetDestinations();
//#else
            return new PVTServiceAgent(this.context).GetDestinations();
//#endif
        }

        public PriceAndRanksBE GetPreferredProduct(long productId, string destinationID, SizeBE measurements, int weight)
        {
//#if DEBUG
//            return new PVTServiceStub().GetPreferredProduct(productId, measurements, weight);
//#else
            return new PVTServiceAgent(context).GetPreferredProduct(productId, destinationID, measurements, weight);
//#endif
        }

        //        public PriceBE GetPrice(string toBeItemObject)
        //        {
        ////#if DEBUG
        ////            return new PVTServiceStub().GetPrice(toBeItemObject);
        ////#else
        //            return new PVTServiceAgent().GetPrice(toBeItemObject);
        ////#endif
        //        }
        public ProductsBE GetProducts(string countryCode)
        {
//#if DEBUG
//            return new PVTServiceStub().GetProducts(countryCode);
//#else
            return new PVTServiceAgent(context).GetProducts(countryCode);
//#endif
        }

        public PriceAndRanksBE GetProductSuggestions(string countryCode, SizeBE measurements, int weight, ConstantsAndEnums.Characteristics[] filters)
        {
//#if DEBUG
//            return new PVTServiceStub().GetProductSuggestions(countryCode, measurements, weight, filters);
//#else
            return new PVTServiceAgent(context).GetProductSuggestions(countryCode, measurements, weight, filters);
//#endif
        }

        public string GetVersion()
        {
//#if DEBUG
//            return new PVTServiceStub().GetVersion();
//#else
            return new PVTServiceAgent(context).GetVersion();
//#endif
        }

        public string TestService()
        {
//#if DEBUG
//            return new PVTServiceStub().TestService();
//#else
            return new PVTServiceAgent().TestService();
//#endif
        }

        //public AccountValidationResponseBE GetAccountValidationResponse(string accountNumber, string reference, string type)
        //{
        //    #if DEBUG
        //    return new PVTServiceStub().GetAccountValidationResponse(accountNumber, reference, type);
        //    #else
        //    return new PVTServiceAgent().GetAccountValidationResponse(accountNumber, reference, type);
        //    #endif
        //}
        public ValidationResponseBE ValidateItem(SizeBE size, int weight, List<string> additionalServiceIDs, AccountBE account, string countryISOCode, long selectedServiceID, AddressBE sender, AddressBE receiver)
        {
//#if DEBUG
//            return new PVTServiceStub().ValidateItem(size, weight, additionalServiceIDs, account, countryISOCode, selectedServiceID, sender, receiver);
//#else
            return new PVTServiceAgent(context).ValidateItem(size, weight, additionalServiceIDs, account, countryISOCode, selectedServiceID, sender, receiver);
//#endif
        }

        //public ValidationResponseBE ValidateService(string code, int length, int width, int height, int weight, List<AdditionalServiceBE> additionalServices, double valueAmount, string content, double codAmount, int diameter, string destinationId, int destinationZip, AddressBE fromAddress, AddressBE toaddress, double insuranceAmount, string marketplace)
        //{
        //    #if DEBUG
        //    return new PVTServiceStub().ValidateService(code, length, width, height, weight, additionalServices, valueAmount, content, codAmount, diameter, destinationId, destinationZip, fromAddress, toaddress, insuranceAmount, marketplace);
        //    #else
        //    return new PVTServiceAgent().ValidateService(code, length, width, height, weight, additionalServices, valueAmount, content, codAmount, diameter, destinationId, destinationZip, fromAddress, toaddress, insuranceAmount, marketplace);
        //    #endif
        //}
        public string ZipToCity(string zipCode)
        {
//#if DEBUG
//            return new PVTServiceStub().ZipToCity(zipCode);
//#else
            return new PVTServiceAgent(context).ZipToCity(zipCode);
//#endif
        }

        #endregion Methods
    }
}