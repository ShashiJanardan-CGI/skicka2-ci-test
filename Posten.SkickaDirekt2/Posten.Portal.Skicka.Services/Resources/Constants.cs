﻿namespace Posten.Portal.Skicka.Services.Resources
{
    public class Constants
    {
        #region Fields

        public const string LogApplicationName = "SkickaDirekt2";
        public const string LogCategoryDefault = "SkickaDirekt2";

        #endregion Fields
    }
}