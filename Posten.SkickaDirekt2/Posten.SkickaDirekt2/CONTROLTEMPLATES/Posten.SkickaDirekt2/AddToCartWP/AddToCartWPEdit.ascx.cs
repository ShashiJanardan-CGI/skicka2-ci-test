﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class AddToCartWPEdit : UserControl
    {
        #region Fields

        private string accountValidationProperty;
        private string addToCartButtonProperty;
        private string receiverValidationProperty;
        private string senderValidationProperty;

        #endregion Fields

        #region Properties

        public string AccountValidationProperty
        {
            get
            {
                return accountValidationPropertyEdit.Value;
            }
            set
            {
                accountValidationProperty = value;
            }
        }

        public string AddToCartButtonProperty
        {
            get
            {
                return addToCartButtonPropertyEdit.Value;
            }
            set
            {
                addToCartButtonProperty = value;
            }
        }

        public string ReceiverValidationProperty
        {
            get
            {
                return receiverValidationPropertyEdit.Value;
            }
            set
            {
                receiverValidationProperty = value;
            }
        }

        public string SenderValidationProperty
        {
            get
            {
                return senderValidationPropertyEdit.Value;
            }
            set
            {
                senderValidationProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(addToCartButtonPropertyEdit.Value))
            {
                addToCartButtonPropertyEdit.Value = addToCartButtonProperty;
            }
            if (string.IsNullOrEmpty(accountValidationPropertyEdit.Value))
            {
                accountValidationPropertyEdit.Value = accountValidationProperty;
            }
            if (string.IsNullOrEmpty(senderValidationPropertyEdit.Value))
            {
                senderValidationPropertyEdit.Value = senderValidationProperty;
            }
            if (string.IsNullOrEmpty(receiverValidationPropertyEdit.Value))
            {
                receiverValidationPropertyEdit.Value = receiverValidationProperty;
            }
        }

        #endregion Methods
    }
}