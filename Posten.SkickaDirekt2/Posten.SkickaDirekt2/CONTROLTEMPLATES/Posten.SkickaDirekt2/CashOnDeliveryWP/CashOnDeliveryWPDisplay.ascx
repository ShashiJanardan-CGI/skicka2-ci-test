﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CashOnDeliveryWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.CashOnDeliveryWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/CashOnDeliveryWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div style="display: none" id="CashOnDeliveryDisplay" data-bind="visible: true, if: SelectedProductSuggestion() && ((SelectedProductSuggestion().ShowCashOnDelivery() || SelectedProductSuggestion().ProductId == '2') || (SelectedProductSuggestion().SelectedAdditionalServices()[0] && SelectedProductSuggestion().SelectedAdditionalServices()[0].Code() == '01'))">
    <div class="borders rounded bottomMargin">
        <div class="paddings lightGradient header bottomBorder topRounded">
            <span runat="server" id="headerPropertyDisplay"></span>
        </div>
        <div class="paddings" data-bind="with: Account">
            <div class="leftPart">
                <span runat="server" id="accountTypeLabelPropertyDisplay"></span>
            </div>
            <div class="rightPart rightPartText">
                <input type="radio" id="cashOnDeliveryBank" name="cashOnDeliveryType" checked data-bind="checkedValue: $root.AccountType().BANKGIRO, checked: AccountType" />
                <label for="cashOnDeliveryBank" class="rightMargin">
                    <span runat="server" id="bankGiroLabelPropertyDisplay"></span>
                </label>
                <input type="radio" id="cashOnDeliveryPlus" name="cashOnDeliveryType" class="leftMargin" data-bind="checkedValue: $root.AccountType().PLUSGIRO, checked: AccountType" />
                <label for="cashOnDeliveryPlus">
                    <span runat="server" id="postalGiroLabelPropertyDisplay"></span>
                </label>
            </div>
            <div class="bothClear bottomMargin"></div>

            <div id="cashOnDeliveryAccountDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="accountNumberLabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="cashOnDeliveryAccountNo" data-bind="value: AccountNumber" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="cashOnDeliveryReferenceDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="referenceLabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="cashOnDeliveryReference" runat="server" data-bind="value: Reference" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="cashOnDeliveryAmountDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="amountLabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field smallField" id="cashOnDeliveryAmount" data-bind="value: Amount" />
                </div>
                <div class="bothClear"></div>
            </div>
        </div>
    </div>
</div>