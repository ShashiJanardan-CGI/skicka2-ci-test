﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class CashOnDeliveryWPDisplay : UserControl
    {
        #region Fields

        private string accountNumberLabelProperty;
        private string accountTypeLabelProperty;
        private string amountLabelProperty;
        private string bankGiroLabelProperty;
        private string headerProperty;
        private string postalGiroLabelProperty;
        private string referenceLabelProperty;
        private string referencePlaceholderProperty;

        #endregion Fields

        #region Properties

        public string AccountNumberLabelProperty
        {
            get
            {
                return accountNumberLabelProperty;
            }
            set
            {
                accountNumberLabelProperty = value;
            }
        }

        public string AccountTypeLabelProperty
        {
            get
            {
                return accountTypeLabelProperty;
            }
            set
            {
                accountTypeLabelProperty = value;
            }
        }

        public string AmountLabelProperty
        {
            get
            {
                return amountLabelProperty;
            }
            set
            {
                amountLabelProperty = value;
            }
        }

        public string BankGiroLabelProperty
        {
            get
            {
                return bankGiroLabelProperty;
            }
            set
            {
                bankGiroLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string PostalGiroLabelProperty
        {
            get
            {
                return postalGiroLabelProperty;
            }
            set
            {
                postalGiroLabelProperty = value;
            }
        }

        public string ReferenceLabelProperty
        {
            get
            {
                return referenceLabelProperty;
            }
            set
            {
                referenceLabelProperty = value;
            }
        }

        public string ReferencePlaceholderProperty
        {
            get
            {
                return referencePlaceholderProperty;
            }
            set
            {
                referencePlaceholderProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.headerPropertyDisplay.InnerText = headerProperty;
            this.accountTypeLabelPropertyDisplay.InnerText = accountTypeLabelProperty;
            this.bankGiroLabelPropertyDisplay.InnerText = bankGiroLabelProperty;
            this.postalGiroLabelPropertyDisplay.InnerText = postalGiroLabelProperty;
            this.accountNumberLabelPropertyDisplay.InnerText = accountNumberLabelProperty;
            this.referenceLabelPropertyDisplay.InnerText = referenceLabelProperty;
            this.cashOnDeliveryReference.Attributes["placeholder"] = referencePlaceholderProperty;
            this.amountLabelPropertyDisplay.InnerText = amountLabelProperty;
        }

        #endregion Methods
    }
}