﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CashOnDeliveryWPEdit.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.CashOnDeliveryWPEdit" %>

<div id="CashOnDeliveryEdit">
    <div class="borders rounded bottomMargin">
        <div class="paddings lightGradient header bottomBorder topRounded">
            <input id="headerPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
        <div class="paddings">
            <div class="leftPartEdit">
                <input id="accountTypeLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="rightPart rightPartText">
                <input type="radio" value="bankgiro" />
                <label for="cashOnDeliveryBank" class="rightMargin">
                    <input id="bankGiroLabelPropertyEdit" runat="server" type="text" class="inputEditNarrow" enableviewstate="true" />
                </label>
                <input type="radio" value="plusgiro" class="leftMargin" />
                <label for="cashOnDeliveryPlus">
                    <input id="postalGiroLabelPropertyEdit" runat="server" type="text" class="inputEditNarrow" enableviewstate="true" />
                </label>
            </div>
            <div class="bothClear bottomMargin"></div>

            <div id="cashOnDeliveryAccountDisplay" class="bottomMargin">
                <div class="leftPartEdit">
                    <input id="accountNumberLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
                </div>
                <div class="rightPartEdit">
                    <input type="text" class="field largestField" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="cashOnDeliveryReferenceDisplay" class="bottomMargin">
                <div class="leftPartEdit">
                    <input id="referenceLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
                </div>
                <div class="rightPartEdit">
                    <input type="text" class="field largestField" id="referencePlaceholderPropertyEdit" runat="server" enableviewstate="true" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="cashOnDeliveryAmountDisplay" class="bottomMargin">
                <div class="leftPartEdit">
                    <input id="amountLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
                </div>
                <div class="rightPartEdit">
                    <input type="text" class="field smallField" />
                </div>
                <div class="bothClear"></div>
            </div>
        </div>
    </div>
</div>