﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class CashOnDeliveryWPEdit : UserControl
    {
        #region Fields

        private string accountNumberLabelProperty;
        private string accountTypeLabelProperty;
        private string amountLabelProperty;
        private string bankGiroLabelProperty;
        private string headerProperty;
        private string postalGiroLabelProperty;
        private string referenceLabelProperty;
        private string referencePlaceholderProperty;

        #endregion Fields

        #region Properties

        public string AccountNumberLabelProperty
        {
            get
            {
                return accountNumberLabelPropertyEdit.Value;
            }
            set
            {
                accountNumberLabelProperty = value;
            }
        }

        public string AccountTypeLabelProperty
        {
            get
            {
                return accountTypeLabelPropertyEdit.Value;
            }
            set
            {
                accountTypeLabelProperty = value;
            }
        }

        public string AmountLabelProperty
        {
            get
            {
                return amountLabelPropertyEdit.Value;
            }
            set
            {
                amountLabelProperty = value;
            }
        }

        public string BankGiroLabelProperty
        {
            get
            {
                return bankGiroLabelPropertyEdit.Value;
            }
            set
            {
                bankGiroLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerPropertyEdit.Value;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string PostalGiroLabelProperty
        {
            get
            {
                return postalGiroLabelPropertyEdit.Value;
            }
            set
            {
                postalGiroLabelProperty = value;
            }
        }

        public string ReferenceLabelProperty
        {
            get
            {
                return referenceLabelPropertyEdit.Value;
            }
            set
            {
                referenceLabelProperty = value;
            }
        }

        public string ReferencePlaceholderProperty
        {
            get
            {
                return referencePlaceholderPropertyEdit.Value;
            }
            set
            {
                referencePlaceholderProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(headerPropertyEdit.Value))
            {
                this.headerPropertyEdit.Value = headerProperty;
            }
            if (string.IsNullOrEmpty(accountTypeLabelPropertyEdit.Value))
            {
                this.accountTypeLabelPropertyEdit.Value = accountTypeLabelProperty;
            }
            if (string.IsNullOrEmpty(bankGiroLabelPropertyEdit.Value))
            {
                this.bankGiroLabelPropertyEdit.Value = bankGiroLabelProperty;
            }
            if (string.IsNullOrEmpty(postalGiroLabelPropertyEdit.Value))
            {
                this.postalGiroLabelPropertyEdit.Value = postalGiroLabelProperty;
            }
            if (string.IsNullOrEmpty(accountNumberLabelPropertyEdit.Value))
            {
                this.accountNumberLabelPropertyEdit.Value = accountNumberLabelProperty;
            }
            if (string.IsNullOrEmpty(referenceLabelPropertyEdit.Value))
            {
                this.referenceLabelPropertyEdit.Value = referenceLabelProperty;
            }
            if (string.IsNullOrEmpty(referencePlaceholderPropertyEdit.Value))
            {
                this.referencePlaceholderPropertyEdit.Value = referencePlaceholderProperty;
            }
            if (string.IsNullOrEmpty(amountLabelPropertyEdit.Value))
            {
                this.amountLabelPropertyEdit.Value = amountLabelProperty;
            }
        }

        #endregion Methods
    }
}