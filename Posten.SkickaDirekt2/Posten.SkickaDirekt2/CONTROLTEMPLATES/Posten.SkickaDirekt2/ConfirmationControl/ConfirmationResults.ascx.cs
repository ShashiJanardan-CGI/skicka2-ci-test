﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;

    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.SkickaService;
    using Microsoft.SharePoint;
    using Posten.SkickaDirekt2.Resources;
using Posten.Portal.Skicka.Services.Logging;

    public partial class ConfirmationResults : System.Web.UI.UserControl
    {
        #region Fields

        private bool isSecondParagraph = false;
        private string ppid = string.Empty;
        private string ssid = string.Empty;
        private string homeUrl;
        private LogController logger = new LogController();
        #endregion Fields

        
        private ISkickaService iSkickaService = new SkickaServiceAgent();


        #region Properties

        public string BookingId
        {
            get
            {
                return (string)this.ViewState["bookingidValue"];
            }

            set
            {
                this.ViewState["bookingidValue"] = value;
            }
        }

        public bool IsSecondParagraph
        {
            get
            {
                return this.isSecondParagraph;
            }

            set
            {
                this.isSecondParagraph = value;
            }
        }

        public string Ppid
        {
            get
            {
                return this.ppid;
            }

            set
            {
                this.ppid = value;
            }
        }

        public string Ssid
        {
            get
            {
                return this.ssid;
            }

            set
            {
                this.ssid = value;
            }
        }

        public string HomeUrl
        {
            get
            {
                return homeUrl;
            }

            set
            {
                homeUrl = value;
            }
        }

        #endregion Properties

        #region Methods

        public bool CheckSecondParagraph()
        {
            bool labelExists = false;
            try
            {
                bool[] ba = this.LabelEnvelopeInfo(this.GetConsignments(this.BookingId));
                for (int i = 0; i < ba.Length; i++)
                {
                    if (ba.ElementAt(i))
                    {
                        labelExists = true;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
            }

            return labelExists;
        }

        public List<OrderLabelBE> GetAvailableOrderLabels()
        {
            ISkickaService skickaService = this.iSkickaService;
            List<OrderLabelBE> orderLbs = new List<OrderLabelBE>();
            try
            {
                orderLbs = skickaService.GetAvailableOrderLabels();
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
            }

            return orderLbs;
        }

        public string GetBookingId()
        {
            return this.BookingId;
        }

        public List<ConsignmentBE> GetConsignments(string bookingId)
        {
            ISkickaService skickaService = this.iSkickaService;
            List<ConsignmentBE> cblist = new List<ConsignmentBE>();
            try
            {
                cblist = skickaService.GetConsignments(bookingId);
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
                return null;
            }

            return cblist;
        }

        public void GetPdfUrl()
        {
            try
            {
                string pdfUrl = "/skicka/skicka.waybill?bookingid=" + this.BookingId + "&amp;saveFile=yes";
                this.pdfLink.HRef = pdfUrl;
            }
            catch (Exception ex)
            {
                this.pdfLink.HRef = "#";
                this.pdfLink.InnerHtml = "<span><strong>Frakthandling kan ej visas!</strong></span>";

                logger.LogError(ex);
            }
        }

        public void GetReceiverInformation(string bookid)
        {
            try
            {
                // Resolve, using inversion of control, which class to use
                //IResources resources = IoC.Resolve<IResources>();
                ISkickaService skickaService = this.iSkickaService;
                ReceivingPlaceBE recplace = new ReceivingPlaceBE();

                try
                {
                    recplace = skickaService.FindReceivingPlace(bookid);
                    if (recplace == null)
                    {
                        this.leftBoxlist.Visible = false;
                        this.recStoptimesLi.Visible = false;
                        //this.errorMsg.InnerHtml = resources.GetDynamicText("skicka.confirmation.step2.error.nossid");
                        this.errorMsg.Visible = true;
                        throw new NullReferenceException("ReceivingPlace is null");
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex);
                }

                string recName = "Namn: inga uppgifter";
                string recAddress = "Adress: inga uppgifter";
                string recZip = "Inga uppgifter";
                string recCity = "Inga uppgifter";
                string recZipnCity = "Postnr: Inga uppgifter";
                List<string> recStopTime = new List<string>();
                List<string> recOpenTimes = new List<string>();

                if (recplace.Name != string.Empty)
                {
                    recName = recplace.Name;
                }

                if (recplace.Address != string.Empty)
                {
                    recAddress = recplace.Address;
                }

                if (recplace.Postnummer != string.Empty)
                {
                    recZip = recplace.Postnummer;
                }

                if (recplace.Postort != string.Empty)
                {
                    recCity = recplace.Postort;
                }

                if (recplace.StoppingTimes != null)
                {
                    recStopTime = recplace.StoppingTimes;
                }

                if (recplace.OpenTimes != null)
                {
                    recOpenTimes = recplace.OpenTimes;
                }

                if (recplace.Postnummer != string.Empty && recplace.Postort != string.Empty)
                {
                    recZipnCity = recZip + "  " + recCity;
                }

                this.receiverName.InnerHtml = recName;
                this.receiverAddress.InnerHtml = recAddress;
                this.receiverZipNCity.InnerHtml = recZipnCity;
                this.receiverPhone.InnerHtml = "";//"Telefon: 0771-37 10 15";

                // probably not working
                recStopTime.Reverse(0, 2);

                receiverStoppingTimes.InnerHtml = "<b>Sista inlämningstid</b>";
                foreach (string stopTime in recStopTime)
                {
                    receiverStoppingTimes.InnerHtml += "<br /><br />" + stopTime;
                }

                this.receiverOpenTimes.InnerHtml = "<b>Öppettider</b><br />";
                foreach (string dayItem in recOpenTimes)
                {
                    this.receiverOpenTimes.InnerHtml += dayItem + "<br />";
                }

                this.Ppid = recplace.ProductionPointId;
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
            }
        }

        // TODO: Hardcoded rules which should be moved to ISP
        public bool[] LabelEnvelopeInfo(List<ConsignmentBE> clist)
        {
            bool[] labels = { false, false, false, false, false };
            try
            {
                foreach (ConsignmentBE co in clist)
                {
                    if (co.ToAddress.Country != "SE" && (co.ProductCode == "1a_klassbrev_utrikes.Rek_2000" || co.ProductCode == "1a_klassbrev_utrikes.Rek_10000") || (co.ProductCode == "1a_klassbrev_utrikes.Expressbrev_utrikes"))
                    {
                        labels[0] = true;
                    }

                    if (co.ProductCode == "1a_klassbrev_inrikes.Expressbrev_inrikes" || co.ProductCode == "1a_klassbrev_utrikes.Expressbrev_utrikes")
                    {
                        labels[1] = true;
                    }

                    if (co.ToAddress.Country != "SE" && (co.ProductCode == "1a_klassbrev_utrikes.Rek_2000" || co.ProductCode == "1a_klassbrev_utrikes.Rek_10000") && co.AdditionalServices != null)
                    {
                        foreach (AdditionalServiceBE asbe in co.AdditionalServices ?? new List<AdditionalServiceBE>())
                        {
                            if (asbe.AdditionalServiceCode == "MB")
                            {
                                labels[2] = true;
                            }
                        }
                    }

                    if ((co.ProductCode == "1a_klassbrev_inrikes.Expressbrev_inrikes") && co.AdditionalServices != null)
                    {
                        foreach (AdditionalServiceBE asbe in co.AdditionalServices ?? new List<AdditionalServiceBE>())
                        {
                            if (asbe.AdditionalServiceCode == "LX")
                            {
                                labels[3] = true;
                            }
                        }
                    }

                    if (co.ToAddress.Country != "SE")
                    {
                        labels[4] = true;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
                return null;
            }

            return labels;
        }
               

        public void ViewLabelingInfo()
        {
            List<ConsignmentBE> cments = new List<ConsignmentBE>();
            try
            {
                cments = this.GetConsignments(this.GetBookingId());
                string baseService = string.Empty;
                string expresService = string.Empty;
                string mottagBevis = string.Empty;
                string lordag = string.Empty;
                string foreignService = string.Empty;

                if (this.LabelEnvelopeInfo(cments).ElementAt(0))
                {
                    brev_utrikes.Visible = true;
                    baseService = "Rek eller Expressbrev utrikes";
                }

                if (this.LabelEnvelopeInfo(cments).ElementAt(1))
                {
                    express_both.Visible = true;
                    expresService = "Expressbrev in- eller utrikes";
                }

                if (this.LabelEnvelopeInfo(cments).ElementAt(2))
                {
                    mottag_utrikes.Visible = true;
                    mottagBevis = "Mottagningsbevis utrikes";
                }

                if (this.LabelEnvelopeInfo(cments).ElementAt(3))
                {
                    express_lordag.Visible = true;
                    lordag = "Lördagsutdelning";
                }

                if (this.LabelEnvelopeInfo(cments).ElementAt(4))
                {
                    utrikes_alla.Visible = true;
                    foreignService = "Utrikes";
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex);
            }
        }

        protected string GetNewRowCode(int nr)
        {
            string t = string.Empty;
            if (nr % 4 == 0)
            {
                t = "</div><div class='checkboxRow' id='CheckboxRow'>";
            }

            return t;
        }

       
        protected void Page_Load(object sender, EventArgs e)
        {            
            homeUrl = System.Configuration.ConfigurationManager.AppSettings[Constants.CartWebSealUrl];

            //OrderLabelRepeater.DataSource = this.GetAvailableOrderLabels();
            //OrderLabelRepeater.DataBind();
            
            this.ViewLabelingInfo();
            this.GetPdfUrl();
            this.GetReceiverInformation(this.BookingId);
            
            if (this.CheckSecondParagraph())
            {
                secondParagraph.Visible = true;
            }
           
        }

       
        #endregion Methods
    }
}