﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomsDocumentsWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.CustomsDocumentsWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/CustomsDocumentsWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="CustomsDocumentsDisplay" style="display: none" data-bind="with: SelectedProduct, visible: true">
    <div data-bind="if: ServiceDocuments.length > 0">
        <div class="borders rounded bottomMargin">
            <div class="paddings lightGradient header bottomBorder topRounded">
                <span runat="server" id="headerPropertyDisplay"></span>
            </div>
            <div class="paddings">
                <div data-bind="foreach: ServiceDocuments">
                    <a class="customsDocumentsLink" target="_blank" data-bind="attr: { href: Url, title: Name, alt: Name }">
                        <img data-bind="attr: { alt: DocumentType, src: '/_layouts/images/posten.skickadirekt2/' + DocumentTypeName + '.png', alt: Name }" />
                        <br />
                        <span data-bind="text: Name" />
                    </a>
                </div>
                <div class="bothClear"></div>
                <div class="leftFloat leftMargin">
                    <a id="customsDocumentsInfoLink" target="_blank" runat="server" class="blueLink bold arrowLink">
                        <span runat="server" id="aboutLabelPropertyDisplay"></span>
                    </a>
                </div>
                <div class="bothClear"></div>
            </div>
        </div>
    </div>
</div>