﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class CustomsDocumentsWPEdit : UserControl
    {
        #region Fields

        private string aboutLabelProperty;
        private string headerProperty;
        private string customsDocumentsInfoLinkProperty;

        #endregion Fields

        #region Properties

        public string AboutLabelProperty
        {
            get
            {
                return aboutLabelPropertyEdit.Value;
            }
            set
            {
                aboutLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerPropertyEdit.Value;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string CustomsDocumentsInfoLinkProperty
        {
            get
            {
                return customsDocumentsInfoLinkProperty;
            }
            set
            {
                customsDocumentsInfoLinkProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(headerPropertyEdit.Value))
            {
                headerPropertyEdit.Value = headerProperty;
            }
            if (string.IsNullOrEmpty(aboutLabelPropertyEdit.Value))
            {
                aboutLabelPropertyEdit.Value = aboutLabelProperty;
            }
            if (string.IsNullOrEmpty(customsDocumentsInfoLinkPropertyEdit.Value))
            {
                customsDocumentsInfoLinkPropertyEdit.Value = customsDocumentsInfoLinkProperty;
            }
        }

        #endregion Methods
    }
}