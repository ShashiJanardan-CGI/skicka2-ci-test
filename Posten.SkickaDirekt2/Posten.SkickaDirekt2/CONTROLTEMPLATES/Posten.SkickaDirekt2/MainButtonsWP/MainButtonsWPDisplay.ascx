﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainButtonsWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.MainButtonsWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/MainButtonsWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="MainButtonsDisplay">
    <div id="MainErrorMessageSize" style="display:none" class="errorMessage rounded paddings bottomMargin " data-bind="visible: (ShowErrorMessageArea() || (WeightValidationErrorsIsValid() == false) || (SizeValidationErrorsIsValid() == false)) ">
        <div style="display:none" data-bind="visible: (IsServiceGuideSelected() || SelectedProduct() !== undefined) && SizeValidationErrorsIsValid()== false">
            <span runat="server" id="sizeValidationPropertyDisplay" ></span>
        </div>

        <div style="display:none" data-bind="visible: ((IsServiceGuideSelected() || SelectedProduct() !== undefined) && WeightValidationErrorsIsValid()== false)">
            <span runat="server" id="weightValidationPropertyDisplay" ></span>
        </div>

        <div style="display:none" data-bind="visible: ServiceError() && ServiceError().GeneralError" >
            <span runat="server" id="generalServerErrorDisplay" style="display:none" ></span>
            <span id="serverErrorDisplay" data-bind="if: ServiceError()"> <span id="serverErrorText" data-bind="html: ServiceError().Message"></span></span>
        </div>
    </div>
   
    <div class="buttonContainer leftFloat bottomMargin noneDisplay">
        <input type="button" class="largeButton mainButton" id="resetButton" runat="server" data-bind="click: ResetClick" />
    </div>

    <div class="rightFloat ajaxLargeLeftContainer">
        <span class="ajaxLarge ajaxLeft" id="showProposalsProgress" style="display: none;" data-bind="visible: ShowProposalsProgress()"></span>
        <div class="buttonContainer rightFloat bottomMargin">
            <input type="button" class="largeButton mainButton" disabled="disabled" id="showProposalsButton" runat="server" data-bind="disable: (!(SelectedProduct() != undefined || IsServiceGuideSelected()) || ShowProposalsProgress()), click: ShowProposalsClick" />
        </div>
    </div>
    <div class="bothClear"></div>
</div>

<script type="text/html" id="skickaDirektMessageTemplate">
    <span data-bind="if: field.isModified() && !field.isValid(),
    attr: { title: field.error }">
        <br />
        <span class="errorText" data-bind="text: field.error"></span>
    </span>
</script>