﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class MainButtonsWPDisplay : UserControl
    {
        #region Fields

        private string generalServerErrorProperty;
        private string resetButtonProperty;
        private string showProposalsButtonProperty;
        private string sizeValidationProperty;
        private string weightValidationProperty;

        #endregion Fields

        #region Properties

        public string GeneralServerErrorProperty
        {
            get { return generalServerErrorProperty; }
            set { generalServerErrorProperty = value; }
        }

        public string ResetButtonProperty
        {
            get
            {
                return resetButtonProperty;
            }
            set
            {
                resetButtonProperty = value;
            }
        }

        public string ShowProposalsButtonProperty
        {
            get
            {
                return showProposalsButtonProperty;
            }
            set
            {
                showProposalsButtonProperty = value;
            }
        }

        public string SizeValidationProperty
        {
            get
            {
                return sizeValidationProperty;
            }
            set
            {
                sizeValidationProperty = value;
            }
        }

        public string WeightValidationlProperty
        {
            get
            {
                return weightValidationProperty;
            }
            set
            {
                weightValidationProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.resetButton.Value = resetButtonProperty;
            this.showProposalsButton.Value = showProposalsButtonProperty;
            this.sizeValidationPropertyDisplay.InnerText = sizeValidationProperty;
            this.weightValidationPropertyDisplay.InnerText = weightValidationProperty;
            this.generalServerErrorDisplay.InnerText = generalServerErrorProperty;
        }

        #endregion Methods
    }
}