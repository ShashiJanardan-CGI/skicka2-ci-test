﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationTabsWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.NavigationTabsWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="NavigationTabsJSLink" Name="/Posten.SkickaDirekt2/JS/NavigationTabsWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="NavigationTabsDisplay">
    <ul class="navigationTabList">
        <li class="navigationTab"><a id="tab1Url" runat="server"><span id="tab1TitlePropertyDisplay" runat="server"></span></a></li>
        <li class="navigationTab"><a id="tab2Url" runat="server" href="javascript:SessionFormData(2);"><span id="tab2TitlePropertyDisplay" runat="server"></span></a></li>
        <li class="navigationTab"><a id="tab3Url" runat="server" href="javascript:SessionFormData(3);"><span id="tab3TitlePropertyDisplay" runat="server"></span></a></li>
    </ul>
</div>

<script type="text/javascript">
    var Tab1UrlProperty = "<%= Tab1UrlProperty%>";
    var Tab2UrlProperty = "<%= Tab2UrlProperty%>";
    var Tab3UrlProperty = "<%= Tab3UrlProperty%>";
</script>