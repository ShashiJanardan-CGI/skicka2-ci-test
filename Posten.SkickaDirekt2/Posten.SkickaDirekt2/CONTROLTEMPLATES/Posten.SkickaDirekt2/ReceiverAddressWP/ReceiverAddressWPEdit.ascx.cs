﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class ReceiverAddressWPEdit : UserControl
    {
        #region Fields

        private string address2LabelProperty;
        private string address2TooltipProperty;
        private string addressLabelProperty;
        private string cityLabelProperty;
        private string cityPlaceholderProperty;
        private string companyLabelProperty;
        private string companyNameLabelProperty;
        private string email2LabelProperty;
        private string emailLabelProperty;
        private string emailTooltipProperty;
        private string entryCodeLabelProperty;
        private string headerProperty;
        private string mobilePhoneLabelProperty;
        private string mobilePhoneTooltipProperty;
        private string nameLabelProperty;
        private string privateLabelProperty;
        private string receiverTypeLabelProperty;
        private string telephoneLabelProperty;
        private string telephoneTooltipProperty;
        private string zipLabelProperty;

        #endregion Fields

        #region Properties

        public string Address2LabelProperty
        {
            get
            {
                return address2LabelPropertyEdit.Value;
            }
            set
            {
                address2LabelProperty = value;
            }
        }

        public string Address2TooltipProperty
        {
            get
            {
                return address2TooltipPropertyEdit.Value;
            }
            set
            {
                address2TooltipProperty = value;
            }
        }

        public string AddressLabelProperty
        {
            get
            {
                return addressLabelPropertyEdit.Value;
            }
            set
            {
                addressLabelProperty = value;
            }
        }

        public string CityLabelProperty
        {
            get
            {
                return cityLabelPropertyEdit.Value; ;
            }
            set
            {
                cityLabelProperty = value;
            }
        }

        public string CityPlaceholderProperty
        {
            get
            {
                return cityPlaceholderPropertyEdit.Value;
            }
            set
            {
                cityPlaceholderProperty = value;
            }
        }

        public string CompanyLabelProperty
        {
            get
            {
                return companyLabelPropertyEdit.Value;
            }
            set
            {
                companyLabelProperty = value;
            }
        }

        public string CompanyNameLabelProperty
        {
            get
            {
                return companyNameLabelPropertyEdit.Value;
            }
            set
            {
                companyNameLabelProperty = value;
            }
        }

        public string Email2LabelProperty
        {
            get
            {
                return email2LabelPropertyEdit.Value;
            }
            set
            {
                email2LabelProperty = value;
            }
        }

        public string EmailLabelProperty
        {
            get
            {
                return emailLabelPropertyEdit.Value;
            }
            set
            {
                emailLabelProperty = value;
            }
        }

        public string EmailTooltipProperty
        {
            get
            {
                return emailTooltipPropertyEdit.Value;
            }
            set
            {
                emailTooltipProperty = value;
            }
        }

        public string EntryCodeLabelProperty
        {
            get
            {
                return entryCodeLabelPropertyEdit.Value;
            }
            set
            {
                entryCodeLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerPropertyEdit.Value;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string MobilePhoneLabelProperty
        {
            get
            {
                return mobilePhoneLabelPropertyEdit.Value;
            }
            set
            {
                mobilePhoneLabelProperty = value;
            }
        }

        public string MobilePhoneTooltipProperty
        {
            get
            {
                return mobilePhoneTooltipPropertyEdit.Value;
            }
            set
            {
                mobilePhoneTooltipProperty = value;
            }
        }

        public string NameLabelProperty
        {
            get
            {
                return nameLabelPropertyEdit.Value;
            }
            set
            {
                nameLabelProperty = value;
            }
        }

        public string PrivateLabelProperty
        {
            get
            {
                return privateLabelPropertyEdit.Value;
            }
            set
            {
                privateLabelProperty = value;
            }
        }

        public string ReceiverTypeLabelProperty
        {
            get
            {
                return receiverTypeLabelPropertyEdit.Value;
            }
            set
            {
                receiverTypeLabelProperty = value;
            }
        }

        public string TelephoneLabelProperty
        {
            get
            {
                return telephoneLabelPropertyEdit.Value;
            }
            set
            {
                telephoneLabelProperty = value;
            }
        }

        public string TelephoneTooltipProperty
        {
            get
            {
                return telephoneTooltipPropertyEdit.Value;
            }
            set
            {
                telephoneTooltipProperty = value;
            }
        }

        public string ZipLabelProperty
        {
            get
            {
                return zipLabelPropertyEdit.Value;
            }
            set
            {
                zipLabelProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(headerPropertyEdit.Value))
            {
                headerPropertyEdit.Value = headerProperty;
            }
            if (string.IsNullOrEmpty(receiverTypeLabelPropertyEdit.Value))
            {
                receiverTypeLabelPropertyEdit.Value = receiverTypeLabelProperty;
            }
            if (string.IsNullOrEmpty(privateLabelPropertyEdit.Value))
            {
                privateLabelPropertyEdit.Value = privateLabelProperty;
            }
            if (string.IsNullOrEmpty(companyLabelPropertyEdit.Value))
            {
                companyLabelPropertyEdit.Value = companyLabelProperty;
            }
            if (string.IsNullOrEmpty(companyNameLabelPropertyEdit.Value))
            {
                companyNameLabelPropertyEdit.Value = companyNameLabelProperty;
            }
            if (string.IsNullOrEmpty(nameLabelPropertyEdit.Value))
            {
                nameLabelPropertyEdit.Value = nameLabelProperty;
            }
            if (string.IsNullOrEmpty(addressLabelPropertyEdit.Value))
            {
                addressLabelPropertyEdit.Value = addressLabelProperty;
            }
            if (string.IsNullOrEmpty(address2LabelPropertyEdit.Value))
            {
                address2LabelPropertyEdit.Value = address2LabelProperty;
            }
            if (string.IsNullOrEmpty(address2TooltipPropertyEdit.Value))
            {
                address2TooltipPropertyEdit.Value = address2TooltipProperty;
            }
            if (string.IsNullOrEmpty(zipLabelPropertyEdit.Value))
            {
                zipLabelPropertyEdit.Value = zipLabelProperty;
            }
            if (string.IsNullOrEmpty(cityPlaceholderPropertyEdit.Value))
            {
                cityPlaceholderPropertyEdit.Value = cityPlaceholderProperty;
            }
            if (string.IsNullOrEmpty(cityLabelPropertyEdit.Value))
            {
                cityLabelPropertyEdit.Value = cityLabelProperty;
            }
            if (string.IsNullOrEmpty(emailLabelPropertyEdit.Value))
            {
                emailLabelPropertyEdit.Value = emailLabelProperty;
            }
            if (string.IsNullOrEmpty(emailTooltipPropertyEdit.Value))
            {
                emailTooltipPropertyEdit.Value = emailTooltipProperty;
            }
            if (string.IsNullOrEmpty(email2LabelPropertyEdit.Value))
            {
                email2LabelPropertyEdit.Value = email2LabelProperty;
            }
            if (string.IsNullOrEmpty(mobilePhoneLabelPropertyEdit.Value))
            {
                mobilePhoneLabelPropertyEdit.Value = mobilePhoneLabelProperty;
            }
            if (string.IsNullOrEmpty(mobilePhoneTooltipPropertyEdit.Value))
            {
                mobilePhoneTooltipPropertyEdit.Value = mobilePhoneTooltipProperty;
            }
            if (string.IsNullOrEmpty(telephoneLabelPropertyEdit.Value))
            {
                telephoneLabelPropertyEdit.Value = telephoneLabelProperty;
            }
            if (string.IsNullOrEmpty(telephoneTooltipPropertyEdit.Value))
            {
                telephoneTooltipPropertyEdit.Value = telephoneTooltipProperty;
            }
            if (string.IsNullOrEmpty(entryCodeLabelPropertyEdit.Value))
            {
                entryCodeLabelPropertyEdit.Value = entryCodeLabelProperty;
            }
        }

        #endregion Methods
    }
}