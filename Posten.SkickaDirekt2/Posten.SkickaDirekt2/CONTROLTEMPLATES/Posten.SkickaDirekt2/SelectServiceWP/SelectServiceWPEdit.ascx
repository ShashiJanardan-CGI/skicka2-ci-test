﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectServiceWPEdit.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.SelectServiceWPEdit" ClassName="SelectServiceWPEdit" %>

<div id="SelectServiceEdit">
    <div class="borders rounded">
        <div class="paddings lightGradient header bottomBorder topRounded">
            <input id="headerPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
        <div class="paddings bottomBorder" id="selectServiceCountry">
            <div class="leftPartEdit">
                <input class="inputEdit" id="countryLabelPropertyEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <textarea class="textareaEdit" rows="3" cols="20" id="countryTooltipPropertyEdit" runat="server" enableviewstate="true" />

                <div class="topPadding">
                    <a>Information Link URL</a>
                    <textarea class="textareaEdit" rows="2" cols="50" id="countryInformationLinkPropertyEdit" runat="server" enableviewstate="true" />
               </div>
            </div>
            <div class="rightPartEdit">
                <select class="field largestField">
                    <option>Sverige</option>
                </select>
            </div>           
            <div class="bothClear"></div>
            
        </div>
        <div class="paddings bottomBorder" id="selectServiceService">
            <div class="leftPartEdit">
                <input class="inputEdit" id="sendServiceLabelPropertyEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <textarea class="textareaEdit" rows="3" cols="20" id="sendServiceTooltipPropertyEdit" runat="server" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="radio" />
                <select id="serviceSelector" class="field largeField">
                    <option>Välj tjänst</option>
                </select>
                <br />
                <input type="radio" checked="checked" />
                <span runat="server" id="chooseServiceLabel"></span>
            </div>
            <div class="bothClear"></div>
        </div>        
        <div class="paddings bottomBorder" id="selectServiceSize">
            <div class="leftPartEdit">
                <input class="inputEdit" id="sizeLabelPropertyEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <textarea class="textareaEdit" rows="3" cols="20" id="sizeTooltipPropertyEdit" runat="server" enableviewstate="true" />
            </div>
            <div class="rightPartEdit selectServiceDims">
                <img id="selectServiceLetterImage" runat="server" visible="true" />
                <input class="radioButtonEdit" type="radio" /><span runat="server" id="envelopeC4"></span>
                <input class="radioButtonEdit" type="radio" /><span runat="server" id="envelopeC5"></span>
                <input class="radioButtonEdit" type="radio" checked="checked" /><span runat="server" id="measureMyself"></span>
            </div>
            <div class="leftPartEdit"></div>
            <div class="rightPartEdit">
                <div class="leftFloat fixedWithSmall">
                    <div>
                        <img id="selectServiceParcelImage" runat="server" visible="true" />&nbsp;
                    </div>
                </div>
                <div class="leftFloat fixedWithMid">
                    <span class="leftMargin"><span runat="server" id="lengthLabel"></span></span>
                    <br />
                    <input type="text" class="field smallFieldEdit" /><span runat="server" id="lengthUnit"></span><br />
                </div>
                <div class="leftFloat leftMargin fixedWithMid">
                    <span class="leftMargin"><span runat="server" id="widthLabel"></span></span>
                    <br />
                    <input type="text" class="field smallFieldEdit" /><span runat="server" id="widthUnit"></span><br />
                </div>
                <div class="leftFloat leftMargin fixedWithMid">
                    <span class="leftMargin"><span runat="server" id="heightLabel"></span></span>
                    <br />
                    <input type="text" class="field smallFieldEdit" /><span runat="server" id="heightUnit"></span><br />
                </div>
                <div class="leftFloat leftMargin fixedWithMid">
                    <span class="leftMargin"><span runat="server" id="diameterLabel"></span></span>
                    <br />
                    <input type="text" class="field smallFieldEdit" /><span runat="server" id="diameterUnit"></span><br />
                </div>
            </div>
            <div class="leftPartEdit"></div>
            <div class="rightPartEdit">
                <br />
                <textarea id="sizeErrorPropertyEdit" rows="2" cols="40" runat="server" enableviewstate="true" />
            </div>
            <div class="bothClear"></div>
        </div>
        <div class="paddings bottomBorder" id="selectServiceWeight">
            <div class="leftPartEdit">
                <input class="inputEdit" id="weightLabelPropertyEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <textarea class="textareaEdit" rows="3" cols="20" id="weightTooltipPropertyEdit" runat="server" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field smallFieldEdit" />
                <input type="radio" checked /><span runat="server" id="weightUnitLow"></span>
                <input type="radio" class="leftMargin" /><span runat="server" id="weightUnitHigh"></span>
            </div>
            <div class="bothClear"></div>
        </div>
        <div class="paddings" id="selectServiceImportant">
            <div class="leftPartEdit">
                <input class="inputEdit" id="serviceFilterLabelPropertyEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <textarea class="textareaEdit" rows="3" cols="20" id="serviceFilterTooltipPropertyEdit" runat="server" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="checkbox" class="selectServiceImportant" />
                <label for="fast">
                    <span>
                        <input class="inputEditNarrow" id="fastButtonPropertyEdit" runat="server" type="text" enableviewstate="true" /></span></label>
                <input type="checkbox" class="selectServiceImportant" />
                <label for="secure">
                    <span>
                        <input class="inputEditNarrow" id="secureButtonPropertyEdit" runat="server" type="text" enableviewstate="true" /></span></label>
                <input type="checkbox" class="selectServiceImportant" />
                <label for="traceable">
                    <span>
                        <input class="inputEditNarrow" id="traceableButtonPropertyEdit" runat="server" type="text" enableviewstate="true" /></span></label>
            </div>
            <div class="bothClear"></div>
        </div>
    </div>
</div>