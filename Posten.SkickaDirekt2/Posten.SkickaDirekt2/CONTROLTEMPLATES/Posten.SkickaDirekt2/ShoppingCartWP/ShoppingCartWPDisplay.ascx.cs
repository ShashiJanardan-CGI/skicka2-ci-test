﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using Microsoft.SharePoint;
    using System;
    using System.Web;
    using System.Web.UI;
    using System.Linq;
    using System.Collections.Generic;
    using Posten.SkickaDirekt2.Resources;

    public partial class ShoppingCartWPDisplay : UserControl
    {
        // TODO: load from configuration        
        private const int CookieDayLength = 5;

        #region Fields

        private string headerProperty;
        private string cartIsEmptyProperty;
        private string confirmHeaderProperty;
        private string confirmQuestionStartProperty;
        private string confirmQuestionEndProperty;

        #endregion Fields

        #region Properties

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string CartIsEmptyProperty
        {
            get
            {
                return cartIsEmptyProperty;
            }
            set
            {
                cartIsEmptyProperty = value;
            }
        }

        public string ConfirmHeaderProperty
        {
            get
            {
                return confirmHeaderProperty;
            }
            set
            {
                confirmHeaderProperty = value;
            }
        }

        public string ConfirmQuestionStartProperty
        {
            get
            {
                return confirmQuestionStartProperty;
            }
            set
            {
                confirmQuestionStartProperty = value;
            }
        }

        public string ConfirmQuestionEndProperty
        {
            get
            {
                return confirmQuestionEndProperty;
            }
            set
            {
                confirmQuestionEndProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.headerPropertyDisplay.InnerText = headerProperty;
            this.cartIsEmptyPropertyDisplay.InnerText = cartIsEmptyProperty;
            this.confirmHeaderPropertyDisplay.InnerText = confirmHeaderProperty;
            this.confirmQuestionStartPropertyDisplay.InnerText = confirmQuestionStartProperty;
            this.confirmQuestionEndPropertyDisplay.InnerText = confirmQuestionEndProperty;
        }

        protected void checkoutButton_ServerClick(object sender, EventArgs e)
        {
            //Add cookie
            addBasketCookie();
            addEShopCookie();

            //Redirect to checkout page
            Response.Redirect(SPContext.Current.Web.Url + Constants.CheckoutPagePath + SPContext.Current.Web.Url);
        }

        private void addBasketCookie()
        {
            string CookieName = "PostenBasketId";
            // silently ignore if not called from inside a web request
            var httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                return;
            }

            // NOTE: cookie is never null since it gets added automatically if it does not extists
            var cookie = httpContext.Response.Cookies[CookieName];
            if (!string.IsNullOrEmpty(basketIDHolder.Value))
            {
                var value = new Guid(basketIDHolder.Value);
                if (value != Guid.Empty)
                {
                    // set the cookie if it has a value
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(CookieDayLength);
                    cookie.Domain = new Uri(SPContext.Current.Web.Url).Host;
                }
                else if (httpContext.Request.Cookies[CookieName] == null)
                {
                    // don't send response to remove cookie if it does not exists
                    httpContext.Response.Cookies.Remove(CookieName);
                }
                else
                {
                    // remove the cookie if the basket does not exists
                    cookie.Expires = DateTime.Now.AddDays(-30);
                }

                // cache the value for the lifetime of this request
                httpContext.Items[CookieName] = value.ToString();
            }
        }

        private void addEShopCookie()
        {
            var CookieName = "eshopUrl";
            // silently ignore if not called from inside a web request
            var httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                return;
            }

            // NOTE: cookie is never null since it gets added automatically if it does not extists
            var cookie = httpContext.Response.Cookies[CookieName];

            // set the cookie if it has a value
            cookie.Value = this.Page.Request.Url.ToString();
            cookie.Expires = DateTime.Now.AddDays(CookieDayLength);
            cookie.Domain = new Uri(SPContext.Current.Web.Url).Host;
            
            // cache the value for the lifetime of this request
            httpContext.Items[CookieName] = this.Page.Request.Url.ToString();

        }

        #endregion Methods
    }
}