﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartWPEdit.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.ShoppingCartWPEdit" %>

<div id="ShoppingCartEdit" class="relativePosition shoppingCart">
    <div class="shoppingCartIcon">
        <div class="shoppingCartIconContent buttonGradient">
            <span></span>
        </div>
    </div>
    <div class="rounded">
        <div class="buttonGradient paddings topRounded header whiteText">
            <input id="headerPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
        <div class="paddings bottomBorders bottomRounded">
            <input id="cartIsEmptyPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
    </div>

    <div class="rounded">
        <div class="buttonGradient paddings topRounded header whiteText">
            <input id="confirmHeaderPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
        <div class="paddings bottomBorders bottomRounded">
            <input id="confirmQuestionStartPropertyEdit" runat="server" type="text" enableviewstate="true" />
            [Artikelnamn]
            <input id="confirmQuestionEndPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
    </div>
</div>