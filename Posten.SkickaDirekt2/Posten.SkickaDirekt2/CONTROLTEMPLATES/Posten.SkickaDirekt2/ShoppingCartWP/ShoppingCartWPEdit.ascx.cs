﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web;
    using System.Web.UI;

    public partial class ShoppingCartWPEdit : UserControl
    {
        #region Fields

        private string headerProperty;
        private string cartIsEmptyProperty;
        private string confirmHeaderProperty;
        private string confirmQuestionStartProperty;
        private string confirmQuestionEndProperty;

        #endregion Fields

        #region Properties

        public string HeaderProperty
        {
            get
            {
                return headerPropertyEdit.Value;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string CartIsEmptyProperty
        {
            get
            {
                return cartIsEmptyPropertyEdit.Value;
            }
            set
            {
                cartIsEmptyProperty = value;
            }
        }

        public string ConfirmHeaderProperty
        {
            get
            {
                return confirmHeaderProperty;
            }
            set
            {
                confirmHeaderProperty = value;
            }
        }

        public string ConfirmQuestionStartProperty
        {
            get
            {
                return confirmQuestionStartProperty;
            }
            set
            {
                confirmQuestionStartProperty = value;
            }
        }

        public string ConfirmQuestionEndProperty
        {
            get
            {
                return confirmQuestionEndProperty;
            }
            set
            {
                confirmQuestionEndProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(headerPropertyEdit.Value))
            {
                headerPropertyEdit.Value = headerProperty;
            }
            if (string.IsNullOrEmpty(cartIsEmptyPropertyEdit.Value))
            {
                cartIsEmptyPropertyEdit.Value = cartIsEmptyProperty;
            }
            if (string.IsNullOrEmpty(confirmHeaderPropertyEdit.Value))
            {
                confirmHeaderPropertyEdit.Value = confirmHeaderProperty;
            }
            if (string.IsNullOrEmpty(confirmQuestionStartPropertyEdit.Value))
            {
                confirmQuestionStartPropertyEdit.Value = confirmQuestionStartProperty;
            }
            if (string.IsNullOrEmpty(confirmQuestionEndPropertyEdit.Value))
            {
                confirmQuestionEndPropertyEdit.Value = confirmQuestionEndProperty;
            }

        }

        #endregion Methods
    }
}