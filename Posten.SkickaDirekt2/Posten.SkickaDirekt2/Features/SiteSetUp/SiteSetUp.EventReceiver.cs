namespace Posten.SkickaDirekt2.Features.SiteSetUp
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Xml;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Publishing;
    using Microsoft.SharePoint.Publishing.WebControls;
    using Microsoft.SharePoint.Utilities;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.Portal.SkickaDirekt2.Resources.Localization;
    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.Utilities;
    using Posten.SkickaDirekt2.WebParts;
    using Microsoft.SharePoint.Administration;
    using System.Web.Configuration;
    using System.Configuration;

    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>
    [Guid("99c5765e-6616-40aa-855f-1bb3a94c9892")]
    public class SiteSetUpEventReceiver : SPFeatureReceiver
    {
        #region Methods

        /// <summary>
        /// on feature activation the start page should be created and set to default page
        /// </summary>
        /// <param name="properties"></param>
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb parent = properties.Feature.Parent as SPWeb;
            uint uiCulture = Convert.ToUInt32(parent.UICulture.LCID);

            using (SPSite site = new SPSite(parent.Url))
            {
                SPWeb web = site.RootWeb;

                PublishingPage page = null;
                PublishingWeb publishingWeb = PublishingWeb.GetPublishingWeb(web);
                page = SetUp_StartPage(web, page, publishingWeb);
                page = SetUp_AboutPage(web, page, publishingWeb, uiCulture);
                page = SetUp_OurServicesPage(web, page, publishingWeb, uiCulture);
                page = SetUp_ConfirmationPage(web, page, publishingWeb, uiCulture);
                page = SetUp_NonSupportedBrowserPage(web, page, publishingWeb, uiCulture);

                //Add style sheets to Style Library
                UploadCSS(web, uiCulture);
            }
        }

        private static void UploadCSS(SPWeb web, uint culture)
        {
            //var listTitle = SPUtility.GetLocalizedString("$Resources:List_StyleLibrary_Title", "SkickaDirekt2Resources", culture);
            SPList documentList = web.Lists[Constants.StyleLibratyTitle];
            SPFolder rootFolder = documentList.RootFolder;

            byte[] documentFile = null;
            var fileName = Constants.CustomCSS;

            using (FileStream fileStream = new FileStream(SPUtility.GetGenericSetupPath(Constants.CSSPath + fileName), FileMode.Open))
            {
                documentFile = new byte[(int)fileStream.Length];
                fileStream.Read(documentFile, 0, (int)fileStream.Length);
                fileStream.Close();
            }

            SPFile file = rootFolder.Files.Add(fileName, documentFile, true);
            file.Update();
            SPListItem documentItem = file.Item;
            documentItem[SPBuiltInFieldId.Title] = fileName;
            documentItem.SystemUpdate();
            documentItem.File.CheckIn("Checked in by system", SPCheckinType.MajorCheckIn);
            
        }

        private static PublishingPage SetUp_NonSupportedBrowserPage(SPWeb web, PublishingPage page, PublishingWeb publishingWeb, uint uiCulture)
        {
            // Create page and set as default
            page = PublishingHelper.CreatePageFromPageLayout(page, publishingWeb, Constants.StartPageLayout, Constants.PageForNonSupportedBrowser, Constants.PageContentTypeID);

            // Set up content web part
            ContentEditorWebPart mainWP = new ContentEditorWebPart();
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement xmlElement = xmlDoc.CreateElement("ContentElement");
            xmlElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.NonSupportedBrowserHtmlPath));
            mainWP.Content = xmlElement;
            PublishingHelper.SetUpWebPart(mainWP, SkickaDirekt2Resources.WebPart_NSBMain_Title);

            // Set up Footer web part
            ContentEditorWebPart footerWP = new ContentEditorWebPart();
            xmlDoc = new XmlDocument();
            xmlElement = xmlDoc.CreateElement("ContentElement");
            xmlElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.FooterHtmlPath));
            footerWP.Content = xmlElement;
            PublishingHelper.SetUpWebPart(footerWP, SkickaDirekt2Resources.WebPart_Footer_Title);

            // Add web parts to page
            PublishingHelper.PageCheckOut(page);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 0, mainWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.FooterWebPartZoneId, 0, footerWP);
            PublishingHelper.PageCheckIn(page, string.Empty);

            page = PublishingHelper.GetPublishingPage(publishingWeb, Constants.PageForNonSupportedBrowser);
            PublishingHelper.PageCheckOut(page);
            page.ListItem[SPBuiltInFieldId.Title] = SPUtility.GetLocalizedString("$Resources:Page_NSBPage_Title", "SkickaDirekt2Resources", uiCulture);//SkickaDirekt2Resources.Page_NSBPage_Title;
            page.ListItem.Update();
            PublishingHelper.PageCheckIn(page, string.Empty);

            return page;
        }

        private static PublishingPage SetUp_AboutPage(SPWeb web, PublishingPage page, PublishingWeb publishingWeb, uint uiCulture)
        {
            // Create page and set as default
            page = PublishingHelper.CreatePageFromPageLayout(page, publishingWeb, Constants.StartPageLayout, Constants.PageForAbout, Constants.PageContentTypeID);

            // Set up Navigation Tabs web part
            NavigationTabsWP navigationTabsWP = new NavigationTabsWP();
            PublishingHelper.SetUpWebPart(navigationTabsWP, SkickaDirekt2Resources.WebPart_NavigationTabs_Title);

            // Set up About web part
            ContentEditorWebPart mainWP = new ContentEditorWebPart();
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement xmlElement = xmlDoc.CreateElement("ContentElement");
            xmlElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.AboutHtmlPath));
            mainWP.Content = xmlElement;
            PublishingHelper.SetUpWebPart(mainWP, SkickaDirekt2Resources.WebPart_AboutMain_Title);

            // Set up Footer web part
            ContentEditorWebPart footerWP = new ContentEditorWebPart();
            xmlDoc = new XmlDocument();
            xmlElement = xmlDoc.CreateElement("ContentElement");
            xmlElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.FooterHtmlPath));
            footerWP.Content = xmlElement;
            PublishingHelper.SetUpWebPart(footerWP, SkickaDirekt2Resources.WebPart_Footer_Title);

            // Add web parts to page
            PublishingHelper.PageCheckOut(page);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 0, navigationTabsWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 1, mainWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.FooterWebPartZoneId, 0, footerWP);
            PublishingHelper.PageCheckIn(page, string.Empty);

            page = PublishingHelper.GetPublishingPage(publishingWeb, Constants.PageForAbout);
            PublishingHelper.PageCheckOut(page);
            page.ListItem[SPBuiltInFieldId.Title] = SPUtility.GetLocalizedString("$Resources:Page_AboutPage_Title", "SkickaDirekt2Resources", uiCulture);//SkickaDirekt2Resources.Page_AboutPage_Title;
            page.ListItem.Update();
            PublishingHelper.PageCheckIn(page, string.Empty);

            return page;
        }

        private static PublishingPage SetUp_OurServicesPage(SPWeb web, PublishingPage page, PublishingWeb publishingWeb, uint uiCulture)
        {
            // Create page and set as default
            page = PublishingHelper.CreatePageFromPageLayout(page, publishingWeb, Constants.StartPageLayout, Constants.PageForOurServices, Constants.PageContentTypeID);

            // Set up Navigation Tabs web part
            NavigationTabsWP navigationTabsWP = new NavigationTabsWP();
            PublishingHelper.SetUpWebPart(navigationTabsWP, SkickaDirekt2Resources.WebPart_NavigationTabs_Title);

            // Set up Services web part
            ContentEditorWebPart mainWP = new ContentEditorWebPart();
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement xmlElement = xmlDoc.CreateElement("ContentElement");
            xmlElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.ServicesHtmlPath));
            mainWP.Content = xmlElement;
            PublishingHelper.SetUpWebPart(mainWP, SkickaDirekt2Resources.WebPart_OurServicesMain_Title);
            PublishingHelper.CloseWebPart(mainWP);

            // Set up Footer web part
            ContentEditorWebPart footerWP = new ContentEditorWebPart();
            xmlDoc = new XmlDocument();
            xmlElement = xmlDoc.CreateElement("ContentElement");
            xmlElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.FooterHtmlPath));
            footerWP.Content = xmlElement;
            PublishingHelper.SetUpWebPart(footerWP, SkickaDirekt2Resources.WebPart_Footer_Title);

            // Add web parts to page
            PublishingHelper.PageCheckOut(page);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 0, navigationTabsWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 1, mainWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.FooterWebPartZoneId, 0, footerWP);
            PublishingHelper.PageCheckIn(page, string.Empty);

            page = PublishingHelper.GetPublishingPage(publishingWeb, Constants.PageForOurServices);
            PublishingHelper.PageCheckOut(page);
            page.ListItem[SPBuiltInFieldId.Title] = SPUtility.GetLocalizedString("$Resources:Page_OurServicesPage_Title", "SkickaDirekt2Resources", uiCulture);
            page.ListItem.Update();
            PublishingHelper.PageCheckIn(page, string.Empty);

            return page;
        }

        /// <summary>
        /// Creates a page as confirmation page with the confirmation results 
        /// </summary>
        /// <param name="web"></param>
        /// <param name="page"></param>
        /// <param name="publishingWeb"></param>
        /// <param name="uiCulture"></param>
        /// <returns></returns>
        private static PublishingPage SetUp_ConfirmationPage(SPWeb web, PublishingPage page, PublishingWeb publishingWeb, uint uiCulture)
        {
            // Create page and set as default
            page = PublishingHelper.CreatePageFromPageLayout(page, publishingWeb, Constants.StartPageLayout, Constants.PageForPaymentConfirmation, Constants.PageContentTypeID);

            // Set up Confirmation web part
            ConfirmationResultsWP confirmationWP = new ConfirmationResultsWP();
            PublishingHelper.SetUpWebPart(confirmationWP, SkickaDirekt2Resources.WebPart_Confirmation_Title);
            
            // Add web parts to page
            PublishingHelper.PageCheckOut(page);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 0, confirmationWP);           
            PublishingHelper.PageCheckIn(page, string.Empty);

            page = PublishingHelper.GetPublishingPage(publishingWeb, Constants.PageForOurServices);
            PublishingHelper.PageCheckOut(page);
            page.ListItem[SPBuiltInFieldId.Title] = SPUtility.GetLocalizedString("$Resources:Page_OurServicesPage_Title", "SkickaDirekt2Resources", uiCulture);
            page.ListItem.Update();
            PublishingHelper.PageCheckIn(page, string.Empty);

            return page;
        }

        /// <summary>
        /// Creates page, sets it as the default page of the site, and adds web parts to it
        /// </summary>
        /// <param name="web"></param>
        /// <param name="page"></param>
        /// <param name="publishingWeb"></param>
        /// <returns></returns>
        private static PublishingPage SetUp_StartPage(SPWeb web, PublishingPage page, PublishingWeb publishingWeb)
        {
            // Create page and set as default
            page = PublishingHelper.CreatePageFromPageLayout(page, publishingWeb, Constants.StartPageLayout, Constants.PageForStart, Constants.PageContentTypeID);
            PublishingHelper.SetPageAsDefault(page, publishingWeb);

            //Information box
            ContentEditorWebPart informationBoxWP = new ContentEditorWebPart();
            XmlDocument document = new XmlDocument();
            XmlElement element = document.CreateElement("ContentElement");
            element.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.MainInfoBoxHtmlPath));
            informationBoxWP.Content = element;
            PublishingHelper.SetUpWebPart(informationBoxWP, SkickaDirekt2Resources.WebPart_InformationBox_Title);
            PublishingHelper.CloseWebPart(informationBoxWP);

            // Set up Select Service web part
            SelectServiceWP selectServiceWP = new SelectServiceWP();
            PublishingHelper.SetUpWebPart(selectServiceWP, SkickaDirekt2Resources.WebPart_SelectService_Title);

            // Set up Navigation Tabs web part
            NavigationTabsWP navigationTabsWP = new NavigationTabsWP();
            PublishingHelper.SetUpWebPart(navigationTabsWP, SkickaDirekt2Resources.WebPart_NavigationTabs_Title);

            // Set up Main Buttons web part
            MainButtonsWP mainButtonsWP = new MainButtonsWP();
            PublishingHelper.SetUpWebPart(mainButtonsWP, SkickaDirekt2Resources.WebPart_MainButtons_Title);

            // Set up Service Result Item web part
            ServicesResultItemWP serviceResultItemWP = new ServicesResultItemWP();
            PublishingHelper.SetUpWebPart(serviceResultItemWP, SkickaDirekt2Resources.WebPart_ServicesResultItem_Title);

            // Set up Cash On Delivery web part
            CustomsDocumentsWP customsDocumentsWP = new CustomsDocumentsWP();
            PublishingHelper.SetUpWebPart(customsDocumentsWP, SkickaDirekt2Resources.WebPart_CustomsDocuments_Title);

            // Set up Cash On Delivery web part
            CashOnDeliveryWP cashOnDeliveryWP = new CashOnDeliveryWP();
            PublishingHelper.SetUpWebPart(cashOnDeliveryWP, SkickaDirekt2Resources.WebPart_CashOnDelivery_Title);

            // Set up Sender Address web part
            SenderAddressWP senderAddressWP = new SenderAddressWP();
            PublishingHelper.SetUpWebPart(senderAddressWP, SkickaDirekt2Resources.WebPart_SenderAddress_Title);

            // Set up Receiver Address web part
            ReceiverAddressWP receiverAddressWP = new ReceiverAddressWP();
            PublishingHelper.SetUpWebPart(receiverAddressWP, SkickaDirekt2Resources.WebPart_ReceiverAddress_Title);

            // Set up Add To Cart web part
            AddToCartWP addToCartWP = new AddToCartWP();
            SPWebApplication webApp = web.Site.WebApplication;
            System.Configuration.Configuration config = WebConfigurationManager.OpenWebConfiguration("/", webApp.Name);
            addToCartWP.ProduceUrlProperty = config.AppSettings.Settings[Constants.CINTProduceUrl].Value;
            addToCartWP.ApplicationUrlProperty = web.Url;            
            addToCartWP.EditUrlProperty = web.Url + Constants.EditUrl;
            addToCartWP.WaybillUrlProperty = web.Url + Constants.WaybillDownloadLink;
            addToCartWP.ConfirmationPageUrlProperty = web.Url + Constants.ConfirmationPageUrl;
            addToCartWP.ConfirmationUrlProperty = web.Url + Constants.ConfirmationDownloadLink;
            PublishingHelper.SetUpWebPart(addToCartWP, SkickaDirekt2Resources.WebPart_AddToCart_Title);

            // Set up Shopping Cart web part
            ShoppingCartWP shoppingCartWP = new ShoppingCartWP();
            PublishingHelper.SetUpWebPart(shoppingCartWP, SkickaDirekt2Resources.WebPart_ShoppingCart_Title);

            //Add Information box to Right zone
            ContentEditorWebPart rightInformationBoxWP = new ContentEditorWebPart();
            XmlDocument informationDoc = new XmlDocument();
            XmlElement infoElement = informationDoc.CreateElement("ContentElement");
            infoElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.InformationBoxHtmlPath)); 
            rightInformationBoxWP.Content = infoElement;
            PublishingHelper.SetUpWebPart(rightInformationBoxWP, SkickaDirekt2Resources.WebPart_InformationBox_Title);   
         
            //Add Media WebPart to the right zone            
            ContentEditorWebPart mediaWP = new ContentEditorWebPart();
            informationDoc = new XmlDocument();
            infoElement = informationDoc.CreateElement("ContentElement");
            infoElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.VideoHtmlPath));
            mediaWP.Content = infoElement;
            PublishingHelper.SetUpWebPart(mediaWP, "Multimedia WP");

            // Set up Footer web part
            ContentEditorWebPart footerWP = new ContentEditorWebPart();
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement xmlElement = xmlDoc.CreateElement("ContentElement");
            xmlElement.InnerText = File.ReadAllText(SPUtility.GetGenericSetupPath(Constants.FooterHtmlPath));
            footerWP.Content = xmlElement;
            PublishingHelper.SetUpWebPart(footerWP, SkickaDirekt2Resources.WebPart_Footer_Title);

            // Add web parts to page
            PublishingHelper.PageCheckOut(page);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 0, navigationTabsWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 1, informationBoxWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 2, selectServiceWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 3, customsDocumentsWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 4, addToCartWP); // Ends up at the bottom
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 5, mainButtonsWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 6, serviceResultItemWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 7, cashOnDeliveryWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 8, senderAddressWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.CenterWebPartZoneId, 9, receiverAddressWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.RightWebPartZoneId, 1, shoppingCartWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.RightWebPartZoneId, 0, rightInformationBoxWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.RightWebPartZoneId, 0, mediaWP);
            PublishingHelper.AddWebPartToPage(web, page.Url.ToString(), string.Empty, Constants.FooterWebPartZoneId, 0, footerWP);
            PublishingHelper.PageCheckIn(page, string.Empty);

            page = PublishingHelper.GetPublishingPage(publishingWeb, Constants.PageForStart);
            PublishingHelper.PageCheckOut(page);
            page.ListItem[SPBuiltInFieldId.Title] = SkickaDirekt2Resources.Page_StartPage_Title;
            //page.ListItem[RF2Resources.Field_IntroductionField_Title] = RF2Resources.Page_SelectTargetGroup_Introduction;
            page.ListItem.Update();
            PublishingHelper.PageCheckIn(page, string.Empty);

            return page;
        }

        #endregion Methods

        #region Other

        // Uncomment the method below to handle the event raised before a feature is deactivated.
        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}
        // Uncomment the method below to handle the event raised after a feature has been installed.
        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}
        // Uncomment the method below to handle the event raised before a feature is uninstalled.
        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}
        // Uncomment the method below to handle the event raised when a feature is upgrading.
        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}

        #endregion Other
    }
}