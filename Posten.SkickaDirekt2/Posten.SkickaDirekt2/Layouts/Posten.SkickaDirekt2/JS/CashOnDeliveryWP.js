﻿/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/

$(document).ready(function () {
    ko.applyBindings(window.skickaCashOnDeliveryVM, document.getElementById('CashOnDeliveryDisplay'));
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- MODEL ---------------------------------------*/
/* -------------------------------------------------------------------------------------*/

var AccountBE = (function () {
    function accountbe(AccountType, AccountNumber, Reference, Amount) {
        this.AccountType = ko.observable(AccountType);
        this.AccountNumber = ko.observable(AccountNumber);
        this.Reference = ko.observable(Reference);
        //CGI CR C34431 START - Replacing "," with "."
        if (Amount != null) {            
            Amount = Amount ? Amount.toLocaleString().replace(",", ".") : Amount;
        }
        //CGI CR C34431 END - Replacing "," with "."       
        this.Amount = ko.observable(Amount);
    }

    return accountbe;
})();

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaCashOnDeliveryVM = (function () {
    var vm = {};

    var GiroTypeEnum = {
        BANKGIRO: 0,
        PLUSGIRO: 1
    };

    /* --------------------------------- Observables ------------------------------------------*/
    vm.AccountType = ko.observable(GiroTypeEnum);
    vm.Account = ko.observable(new AccountBE(GiroTypeEnum.BANKGIRO)).publishOn("Account");
    vm.ValidateAccount = ko.observable(false);

    /* --------------------------------- pub/sub ----------------------------------------------*/
    vm.SelectedProductSuggestion = ko.observable(null).subscribeTo("SelectedProductSuggestion", true);

    ko.postbox.subscribe("IsAddToCartClicked", function () {
        if (vm.SelectedProductSuggestion().ShowCashOnDelivery() || vm.SelectedProductSuggestion().ProductId == '2') {
            EnableValidation();
        }
    });

    ko.postbox.subscribe("ConsignmentToEdit", function (consignment) {
        if (consignment.CODAmount > 0) {
            if (consignment.CODAmount != null) {
                var NoZero = consignment.CODAmount;
                consignment.CODAmount = NoZero ? NoZero.toString().replace(".", ",") : NoZero;
            }
            vm.Account().AccountNumber(consignment.AccountNumber);
            vm.Account().AccountType(consignment.Plusgiro ? GiroTypeEnum.PLUSGIRO : GiroTypeEnum.BANKGIRO);
            vm.Account().Amount(consignment.CODAmount);
            vm.Account().Reference(consignment.PaymentReference);
        }
    });

    /* ----------------------------Validation observables -------------------------------------*/
    vm.AccountValidationErrors = ko.validatedObservable({
        AccountNumber: vm.Account().AccountNumber,
        AccountType: vm.Account().AccountType,
        Amount: vm.Account().Amount,
        Reference: vm.Account().Reference
    });

    /* ------------------------------- Public methods  ----------------------------------------*/

    /* ------------------------------- Private methods  ---------------------------------------*/
    var SetAccountValidation = function () {
        
        vm.Account().AccountNumber.extend({
            required: { onlyIf: function () { return vm.ValidateAccount() && (vm.SelectedProductSuggestion().ShowCashOnDelivery() || vm.SelectedProductSuggestion().ProductId == '2') } },
            bankgiro: { onlyIf: function () { return vm.ValidateAccount() && vm.Account().AccountType() == 0 && (vm.SelectedProductSuggestion().ShowCashOnDelivery() || vm.SelectedProductSuggestion().ProductId == '2'); } },
            plusgiro: { onlyIf: function () { return vm.ValidateAccount() && vm.Account().AccountType() == 1 && (vm.SelectedProductSuggestion().ShowCashOnDelivery() || vm.SelectedProductSuggestion().ProductId == '2'); } }
        });
        vm.Account().Reference.extend({
            required: { onlyIf: function () { return vm.ValidateAccount() && (vm.SelectedProductSuggestion().ShowCashOnDelivery() || vm.SelectedProductSuggestion().ProductId == '2') } },                   
        });
        vm.Account().Amount.extend({
            required: { onlyIf: function () { return vm.ValidateAccount() && (vm.SelectedProductSuggestion().ShowCashOnDelivery() || vm.SelectedProductSuggestion().ProductId == '2') } },
            amountRange: {
                onlyIf: function () { return vm.ValidateAccount() && vm.SelectedProductSuggestion() && (vm.SelectedProductSuggestion().ShowCashOnDelivery() || vm.SelectedProductSuggestion().ProductId == '2') }, params: function () {
                    limit = 0;
                    if (vm.ValidateAccount() && vm.SelectedProductSuggestion() && (vm.SelectedProductSuggestion().ShowCashOnDelivery() || vm.SelectedProductSuggestion().ProductId == '2')) {
                        limit = skickaCashOnDeliveryVM.SelectedProductSuggestion().Product.CodMaxAmount;
                    }
                    return limit;
                } }
    
        });
    };

    var EnableValidation = function () {
        vm.ValidateAccount(true);
        vm.Account().AccountNumber.valueHasMutated();
        vm.Account().AccountType.valueHasMutated();
        vm.Account().Amount.valueHasMutated();
        vm.Account().Reference.valueHasMutated();
    };

   
    var init = (function () {
        SetAccountValidation();
    })();

    /* ------------------------------- Subscriptions ------------------------------------------*/
    vm.AccountValidationErrors.isValid.publishOn("AccountIsValid");

    vm.SelectedProductSuggestion.subscribe(function (productSuggestion) {
        vm.ValidateAccount(false);
    });

    return vm;
})();