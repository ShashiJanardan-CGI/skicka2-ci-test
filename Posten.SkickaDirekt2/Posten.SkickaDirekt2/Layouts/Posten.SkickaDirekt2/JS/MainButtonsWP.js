﻿/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/

$(document).ready(function () {
    $("input[name=senderType]:radio").change(senderType);
    senderType();

    ko.applyBindings(window.skickaMainButtonsVM, document.getElementById('MainButtonsDisplay'));
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaMainButtonsVM = (function () {
    var vm = {};

    /* --------------------------------- pub/sub -----------------------------------------------*/
    vm.SelectedProduct = ko.observable(null).subscribeTo("SelectedProduct");
    vm.IsServiceGuideSelected = ko.observable(false).subscribeTo("IsServiceGuideSelected");
    
    vm.SizeValidationErrorsIsValid = ko.observable().subscribeTo("SizeValidationErrorsIsValid");
    vm.WeightValidationErrorsIsValid = ko.observable().subscribeTo("WeightValidationErrorsIsValid");
    vm.IsWeightRadioValid = ko.observable().subscribeTo("IsWeightRadioValid");
    vm.ServiceError = ko.observable().subscribeTo("ServiceError");

    vm.IsValidSelectService = ko.observable(true).subscribeTo("IsValidSelectService");

    vm.ShowProposalsProgress = ko.observable(false).subscribeTo("ShowProposalsProgress");

    
    /* --------------------------------- Observables ------------------------------------------*/

    /* --------------------------------- Subscriptions ------------------------------------------*/
   

    /* --------------------------------- Public functions -------------------------------------*/
    vm.ResetClick = function () {
        ko.postbox.publish("IsResetClicked");
    };

    vm.ShowProposalsClick = function () {
        skickaSelectServiceVM.IsServiceTypeSwitched(false);
        skickaSelectServiceVM.ServiceError(new ErrorBE(true, false, ""));
        ko.postbox.publish("IsShowProposalsClicked");
    };

    vm.ShowErrorMessageArea = function () {
        try {
            var showArea = false;
            if (!vm.IsValidSelectService()) {
                showArea = true;
            }

            if (vm.ServiceError() && vm.ServiceError().GeneralError) {
                showArea = true;
            }
            return showArea;
        }
        catch (e) {
            console.log(e.message);
            return false;
        }
    };
    return vm;
})();