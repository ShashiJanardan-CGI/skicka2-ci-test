﻿function SessionFormData(tabID)
{
    try{
        //Session Selected Land
        var landObject = new SelectedDestinationSession(skickaSelectServiceVM.SelectedDestination());
        setSessionValue(selectedDestinationKey, JSON.stringify(landObject));

        //Session Selected Product
        var productObject = new SelectedProductSession(skickaSelectServiceVM.SelectedProduct());
        setSessionValue(selectedProductKey, JSON.stringify(productObject));

        //Session Size
        var sizeObject = new SizeSession(skickaSelectServiceVM.CurrentSize());
        setSessionValue(sizeKey, JSON.stringify(sizeObject));

        //Session Weight
        var weightObject = new WeightSession(skickaSelectServiceVM.CurrentWeight());
        setSessionValue(weightKey, JSON.stringify(weightObject));

        //Session Account
        var accountObject = new AccountSession(skickaCashOnDeliveryVM.Account());
        setSessionValue(accountKey, JSON.stringify(accountObject));

        //Session Sender Address
        var senderObject = new AddressSession(skickaSenderVM.SenderAddress());
        setSessionValue(senderAddressKey, JSON.stringify(senderObject));

        //Session Receiver Address
        var receiverObject = new AddressSession(skickaReceiverVM.ReceiverAddress());
        setSessionValue(receiverAddressKey, JSON.stringify(receiverObject));

    }
    catch (e) {
        console.log(e.message);
    }

    //Redirect to destination page
    var redirectionUrl = Tab1UrlProperty;
    if (tabID == 2) {
        redirectionUrl = Tab2UrlProperty;
    }
    else if (tabID == 3) {
        redirectionUrl = Tab3UrlProperty;
    }

    window.location.href = redirectionUrl;
}