﻿$(document).ready(function () {
    console.log("test");
});

function TestService() {
    $.ajax({
        type: "GET",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/TestService",
        dataType: "json",
        success: function (data) {
            console.log(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function ZipToCity(zip) {
    $.ajax({
        type: "GET",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/ZipToCity?zip=" + zip,
        dataType: "json",
        success: function (data) {
            console.log(data.ZipToCityResult);
            return data.ZipToCityResult;
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function FindReceivingPlace(bookingId) {
    $.ajax({
        type: "GET",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/FindReceivingPlace?bookingId=" + bookingId,
        dataType: "json",
        success: function (data) {
            var obj = $.parseJSON(data.FindReceivingPlaceResult);
            console.log(obj);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function GetAvailableOrderLabels() {
    $.ajax({
        type: "GET",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/GetAvailableOrderLabels",
        dataType: "json",
        success: function (data) {
            var obj = $.parseJSON(data.GetAvailableOrderLabelsResult);
            console.log(obj);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function GetConsignment(orderId) {
    $.ajax({
        type: "GET",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/GetConsignment?orderId=" + orderId,
        dataType: "json",
        success: function (data) {
            var obj = $.parseJSON(data.GetConsignmentResult);
            console.log(obj);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function GetItemNotesPDF(bookingId) {
    $.ajax({
        type: "GET",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/GetItemNotesPDF?bookingId=" + bookingId,
        dataType: "json",
        success: function (data) {
            console.log(data.GetItemNotesPDFResult);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function GetConfigurationItem() {
    $.ajax({
        type: "GET",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/GetConfigurationItem",
        dataType: "json",
        success: function (data) {
            var obj = $.parseJSON(data.GetConfigurationItemResult);
            console.log(obj);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function GetValidServicePoints(bookingId, productionPointIds) {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/GetValidServicePoints",
        data: JSON.stringify({ bookingId: bookingId, productionPointIds: productionPointIds.split(";") }),
        processData: false,
        success: function (data) {
            var obj = $.parseJSON(data.GetValidServicePointsResult);
            console.log(obj);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function OrderLabels(prodIds, customerName, contactPerson, email, streetAddress, zip, city, phone, addressCo) {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "http://skickadirekt2.posten.se/_vti_bin/Posten.SkickaDirekt2/transaction.svc/OrderLabels",
        data: JSON.stringify({ prodIds: prodIds.split(";"), customerName: customerName, contactPerson: contactPerson, email: email, streetAddress: streetAddress, zip: zip, city: city, phone: phone, addressCo: addressCo }),
        processData: false,
        success: function (data) {
            console.log(data.OrderLabelsResult);
        },
        error: function (data) {
            console.log(data);
        }
    });
}