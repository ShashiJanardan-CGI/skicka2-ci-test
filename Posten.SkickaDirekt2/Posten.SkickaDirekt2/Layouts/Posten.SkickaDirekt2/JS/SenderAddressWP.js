﻿function senderType(evt) {
    var val = $("input[name=senderType]:checked").val();
    if (val == "company") {
        if ($("#senderCompanyNameDisplay").hasClass("noneDisplay")) {
            $("#senderCompanyNameDisplay").removeClass("noneDisplay");
        }
    } else {
        if (!$("#senderCompanyNameDisplay").hasClass("noneDisplay")) {
            $("#senderCompanyNameDisplay").addClass("noneDisplay");
        }
    }
}

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/

$(document).ready(function () {
    $("input[name=senderType]:radio").change(senderType);
    senderType();

    ko.applyBindings(window.skickaSenderVM, document.getElementById('SenderAddressDisplay'));
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaSenderVM = (function () {
    var vm = {};

    /* --------------------------------- Observables ------------------------------------------*/
    vm.ShowCity = ko.observable();
    vm.SenderAddress = ko.observable(new AddressBE(false)).publishOn("SenderAddress");
    vm.ValidateAddress = ko.observable(false);

    vm.SenderAddressValidationErrors = ko.validatedObservable({
        SenderName: vm.SenderAddress().Name,
        SenderCompanyName: vm.SenderAddress().CompanyName,
        SenderOrganizationNumber: vm.SenderAddress().OrganizationNumber,
        SenderAddress: vm.SenderAddress().AddressField1,
        SenderAddress2: vm.SenderAddress().AddressField2,
        SenderZipCode: vm.SenderAddress().ZipCode,
        SenderCity: vm.SenderAddress().City,
        SenderEmail: vm.SenderAddress().Email,
        SenderEmail2: vm.SenderAddress().Email2,
        SenderEntryCode: vm.SenderAddress().EntryCode
    });

    vm.InvalidZipMsg = ko.observable(false);
    vm.ShowSenderZipProgress = ko.observable(false);

    /* --------------------------------- pub/sub ----------------------------------------------*/
    vm.SelectedProductSuggestion = ko.observable().subscribeTo("SelectedProductSuggestion", true);

    vm.SenderAddressValidationErrors.isValid.publishOn("SenderAddressIsValid");

    ko.postbox.subscribe("IsAddToCartClicked", function () {
        EnableValidation();
    });

    ko.postbox.subscribe("ConsignmentToEdit", function (consignment) {
        var destinationID = consignment.DestinationID;
        var senderObject = extractAddress(consignment.FromAddress, destinationID);
        vm.SenderAddress().AddressField1(senderObject.AddressField1);
        vm.SenderAddress().AddressField2(senderObject.AddressField2);
        vm.SenderAddress().City(senderObject.City);
        vm.SenderAddress().CompanyName(senderObject.CompanyName);
        vm.SenderAddress().Name(senderObject.Name);
        vm.SenderAddress().Email(senderObject.Email);
        vm.SenderAddress().Email2(senderObject.Email);
        vm.SenderAddress().ZipCode(senderObject.ZipCode);
    });

    /* ------------------------------- Public methods  --------------------------------------------*/

    /* ------------------------------- Private methods  --------------------------------------------*/
    var EnableValidation = function () {
        vm.ValidateAddress(true);
        vm.InvalidZipMsg(false);

        vm.SenderAddress().AddressField1.valueHasMutated();
        vm.SenderAddress().AddressField2.valueHasMutated();
        vm.SenderAddress().City.valueHasMutated();
        vm.SenderAddress().CompanyName.valueHasMutated();
        vm.SenderAddress().OrganizationNumber.valueHasMutated();
        vm.SenderAddress().Email.valueHasMutated();
        vm.SenderAddress().Email2.valueHasMutated();
        vm.SenderAddress().Name.valueHasMutated();
        vm.SenderAddress().ZipCode.valueHasMutated();
        vm.SenderAddress().EntryCode.valueHasMutated();
    };

    var SetAddressValidation = function () {
        vm.SenderAddress().Name.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 30 },
            required: { onlyIf: function () { return vm.ValidateAddress() && !vm.SenderAddress().IsCompany(); } }
        });
        vm.SenderAddress().CompanyName.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 30 },
            required: { onlyIf: function () { return vm.ValidateAddress() && vm.SenderAddress().IsCompany(); } }
        });
        vm.SenderAddress().OrganizationNumber.extend({
            required: { onlyIf: function () { return vm.ValidateAddress() && vm.SenderAddress().IsCompany(); } },
            organizationNumber: { onlyIf: function () { return vm.ValidateAddress(); } }
        });
        vm.SenderAddress().AddressField1.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 30 },
            required: { onlyIf: function () { return vm.ValidateAddress() && !vm.SenderAddress().IsCompany(); } }
        });
        vm.SenderAddress().AddressField2.extend({
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 30 }
        });
        vm.SenderAddress().ZipCode.extend({
            required: { onlyIf: function () { return vm.ValidateAddress(); } },
            digit: { onlyIf: function () { return vm.ValidateAddress(); } },
            minLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 5 },
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 5 }
        });
        vm.SenderAddress().Email.extend({
            email: { onlyIf: function () { return vm.ValidateAddress(); } },
            maxLength: { onlyIf: function () { return vm.ValidateAddress(); }, params: 63 },
            required: { onlyIf: function () { return vm.ValidateAddress() && vm.SelectedProductSuggestion().Product.ServiceProperties.SenderEmail == 2; } }
        });
        vm.SenderAddress().Email2.extend({
            required: { onlyIf: function () { return vm.ValidateAddress() && vm.SenderAddress().Email(); } },
            equal: { onlyIf: function () { return vm.ValidateAddress(); }, params: vm.SenderAddress().Email }
        });
        vm.SenderAddress().City.extend({
            required: { onlyIf: function () { return vm.ValidateAddress(); } }
        });
    };

    /* ----------------------------Subscriptions -------------------------------------------------*/
    vm.SelectedProductSuggestion.subscribe(function (productSuggestion) {
        vm.ValidateAddress(false);
    });

    vm.SenderAddress().ZipCode.subscribe(function (zipCode) {
        vm.ShowSenderZipProgress(true);
        if (vm.SenderAddress().ZipCode.isValid()) {
            ZipToCity(zipCode, vm.SenderAddress().City, vm.InvalidZipMsg, vm.ShowSenderZipProgress);
        }
        else {
            vm.SenderAddress().City("");
            vm.InvalidZipMsg(true);
            vm.ShowSenderZipProgress(false);
        }
    }.bind(this));

    var init = (function () {
        SetAddressValidation()
    })()

    return vm;
})();

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Google Tracker js ---------------------------*/
/* -------------------------------------------------------------------------------------*/
$(document).ready(function () {
    try {
        $('#<%=lbAddToCart.ClientID%>').click(function (event) {
            var sender = $("#<%=AddressControl.fromAddressCompany.ClientID %>").val();
            if (sender == null) {
                sender = $("#<%=AddressControl.fromAddressName.ClientID %>").val();
            }
            var email = $('#<%=AddressControl.fromAddressEmail.ClientID%>').val();
            var gac = new gaCookies();
            var VID = gac.getUniqueId();
            ga('send', 'pageview', {
                'dimension1': 'Provide To and From',
                'dimension2': '3',
                'dimension3': '2/1',
                'dimension4': 'Address',
                'dimension5': 'Completed',
                'dimension6': sender,
                'dimension7': email,
                'dimension8': null,
                'dimension9': null,
                'dimension10': VID
            });

            $('#<%=LinkButton1.ClientID%>').click(function (event) {
                var gac = new gaCookies();
                var VID = gac.getUniqueId();
                ga('send', 'pageview', {
                    'dimension1': 'Purchase-Redirect',
                    'dimension2': '4',
                    'dimension3': '3',
                    'dimension4': 'Technical',
                    'dimension5': 'Success',
                    'dimension6': null,
                    'dimension7': null,
                    'dimension8': null,
                    'dimension9': null,
                    'dimension10': VID
                });
            });


            $(window).bind('beforeunload', function (eventObject) {
                if (!inFormOrLink) {
                    var gac = new gaCookies();
                    var VID = gac.getUniqueId();
                    ga('send', 'pageview', {
                        'dimension1': 'Provide To and From',
                        'dimension2': '3',
                        'dimension3': '2/1',
                        'dimension4': 'Address',
                        'dimension5': 'Aborted',
                        'dimension6': null,
                        'dimension7': null,
                        'dimension8': null,
                        'dimension9': null,
                        'dimension10': VID
                    });
                }
                window.setTimeout(function () {
                    // this will execute 1 second later
                }, 1000)
            });
        });
        }
    catch (e) {
        console.log(e.message);
    }
});
