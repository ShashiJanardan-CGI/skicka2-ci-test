﻿function resultItemToggleProperties(link, target) {
    $("#resultMoreProperties_" + target).toggleClass("noneDisplay")
    $(link).toggleClass("resultShowMoreClosed resultShowMoreOpen");
}

function resultItemToggleResults(link) {
    $("#resultMoreSuggestions").toggleClass("noneDisplay")
    $(link).toggleClass("resultShowMoreClosed resultShowMoreOpen");
}

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/

$(document).ready(function () {
    $("input[name=receiverType]:radio").change(receiverType);
    receiverType();

    ko.applyBindings(window.skickaServiceResultVM, document.getElementById('ServicesResultItemDisplay'));
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaServiceResultVM = (function () {
    var vm = {};

    /* --------------------------------- pub/sub ----------------------------------------------*/
    vm.IsShoppingCartEditMode = ko.observable(false).subscribeTo("IsShoppingCartEditMode", true);
    vm.Products = ko.observableArray([]).subscribeTo("Products");
    vm.SelectedProduct = ko.observable().subscribeTo("SelectedProduct", true);

    vm.ProductSuggestions = ko.observable().subscribeTo("ProductSuggestions", true);
    vm.SelectedDestination = ko.observable(null).subscribeTo("SelectedDestination");

    vm.SelectedProductSuggestion = ko.observable().publishOn("SelectedProductSuggestion");
    vm.ProductsScrollTo = ko.observable(false).syncWith("ProductsScrollTo");
    vm.ProductSuggestionsScrollTo = ko.observable(false).syncWith("ProductsScrollTo");

    
    /* --------------------------------- Observables ------------------------------------------*/
    vm.RankedServices = ko.observable(null);

    /* ------------------------------- Public methods  ----------------------------------------*/
    vm.SelectProductSelectionClick = function (productSuggestion) {
        vm.SelectedProductSuggestion(productSuggestion);
        ko.postbox.publish("SelectProductSelectionClicked", productSuggestion);
    };

    vm.ShowAdditionalService = function (productId, destinationEU, additionalServiceCode) {
        var visible = true;
                
        switch (productId) {
            case 2: //7
                visible = (additionalServiceCode != '01');
                
                break;
            case 7: //9 express brev inrikes              
                visible = (additionalServiceCode != 'LX');
                
                break;
            case 12://10                
                visible = (additionalServiceCode != 'RR');
                
                break;
            case 13: //13
                visible = (additionalServiceCode != 'RR');
                
                break;            
        }
               
        return visible;
    };

    vm.ShowAdditionalServicesRow = function (productId) {
        var visible = true;
        switch (productId) {
            case 2: //7
                visible = false;

                break;
            case 7: //9 express brev inrikes              
                visible = false;
        }
        return visible;
    }

    /* ------------------------------- Private methods  ---------------------------------------*/
    // find product definition which matches the suggested product
    var GetMatchingProduct = function (suggestedProduct)
    {
        var matchingProduct = null;
        for (var i = 0, j = vm.Products().length; i < j; i++) {
            if (vm.Products()[i].ProductId == suggestedProduct.ProductId) {
                matchingProduct = vm.Products()[i];
                break;
            }
        };

        return matchingProduct;
    }

    var GetMatchingProductFromId = function (serviceCode) {
        var matchingProduct = null;
        var productCode = null;
        for (var i = 0, j = vm.Products().length; i < j; i++) {
            productCode = vm.Products()[i].Code + (vm.Products()[i].International ? '1' : '0')
            if (productCode == serviceCode) {
                matchingProduct = vm.Products()[i];
                break;
            }
        };

        return matchingProduct;
    }
    /* ------------------------------- Subscriptions ------------------------------------------*/
    vm.ProductSuggestions.subscribe(function (serviceResult) {
        vm.SelectedProductSuggestion(null);
        vm.RankedServices(null);

        if (serviceResult) {
            if (serviceResult.length > 1) {
                var result = new Array();
                ko.utils.arrayForEach(serviceResult, function (serviceObject) {
                    var resultProduct = GetMatchingProduct(serviceObject);

                    // only add the suggested product to the ranked list if it has a matching product definition
                    if (resultProduct) {
                        var priceAndRank = new PriceAndRankBE(
                            serviceObject.ProductId,
                            new PriceBE(
                                    serviceObject.Price.Amount,
                                    serviceObject.Price.Vat,
                                    serviceObject.Price.AmountNoVat,
                                    serviceObject.Price.Currency,
                                    serviceObject.Price.VatCode,//MVU7734 - S
                                    serviceObject.Price.VATAmount//MVU7734 - S
                                ),
                            serviceObject.Rank,
                            resultProduct
                            );

                        result.push(priceAndRank);
                    }
                });

                vm.RankedServices(result);
            }
            else if (serviceResult.length == 1) {
                var serviceObject = serviceResult[0];

                // find product definition which matches the suggested product
                var resultProduct = GetMatchingProduct(serviceObject);

                // only add the suggested product to the ranked list if it has a matching product definition
                if (resultProduct) {
                    var priceAndRank = new PriceAndRankBE(
                            serviceObject.ProductId,
                            new PriceBE(
                                    serviceObject.Price.Amount,
                                    serviceObject.Price.Vat,
                                    serviceObject.Price.AmountNoVat,
                                    serviceObject.Price.Currency,
                                    serviceObject.Price.VatCode, //MVU7734 - S
                                    serviceObject.Price.VATAmount //MVU7734 - S
                                ),
                            serviceObject.Rank,
                            resultProduct
                            );

                    vm.SelectedProductSuggestion(priceAndRank);
                    vm.SelectProductSelectionClick(priceAndRank); //CR C36570
                }
            }
            else if (serviceResult instanceof PriceAndRankBE)
            {
                vm.SelectedProductSuggestion(serviceResult);
            }
        }
    });

    return vm;
})();


