﻿/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/
var cc;
$(document).ready(function () {
    try{
        ko.applyBindings(window.skickaShoppingCartVM, document.getElementById('ShoppingCartDisplay'));

        $("[id$=basketIDHolder]").val(returnLocalValue(basketIdKey));
        retrievePrice();
    }
    catch (e) {
        console.log(e.message);
    }
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaShoppingCartVM = (function () {
    var vm = {};

    /* --------------------------------- pub/sub ----------------------------------------------*/
    vm.ShoppingCartList = ko.observableArray([]).subscribeTo("ShoppingCartList", true);
    var IsShoppingCartEditMode = ko.observable(false).syncWith("IsShoppingCartEditMode");
    vm.CartScrollTo = ko.observable(false).syncWith("CartScrollTo");
    vm.ShoppingCartScrollTo = ko.observable(false).syncWith("CartScrollTo");
    vm.ShoppingCartProgress = ko.observable(false).subscribeTo("ShoppingCartProgress");
    vm.removeItemProgress = ko.observable().publishOn("ShoppingCartProgress");
    vm.ShoppingCartRemoveConfirm = ko.observable(false).syncWith("ShoppingCartRemoveConfirm");
    vm.ShowCheckoutProgress = ko.observable(false).syncWith("ShowCheckoutProgress");
    
    /* --------------------------------- Observables ------------------------------------------*/
    vm.Total = ko.observable(0);

    /* ------------------------------- Public methods  ----------------------------------------*/
    vm.checkoutClick = function () {
        vm.ShowCheckoutProgress(true);
    }

    vm.editItem = function () {
        cc = this;
        ko.postbox.publish("ConsignmentToEdit", this);
        IsShoppingCartEditMode(true);
    }

    vm.confirmRemoveItem = function () {
        $('#masterOverlay').show();
        vm.ShoppingCartRemoveConfirm(this.OrderNumber);
    }

    vm.yesRemoveItem = function () {
        $('#masterOverlay').hide();
        vm.ShoppingCartRemoveConfirm(null);
        vm.removeItemProgress(true);
        var removalResult = vm.removeConsignmentFromItemNotes(vm.removeConsignmentFromCart, this);
        if (skickaAddToCartVM.ConsignmentToEdit() && skickaAddToCartVM.ConsignmentToEdit().OrderNumber === this.OrderNumber) {
            ko.postbox.publish("IsShoppingCartEditMode", false);
            ko.postbox.publish("IsResetClicked");
        }
    }

    vm.noRemoveItem = function () {
        $('#masterOverlay').hide();
        vm.ShoppingCartRemoveConfirm(false);
    }

    vm.removeItem = function () {               
        vm.removeItemProgress(true);
        //Calls to webservice to remove consignment from comerce server and itemnotes        
        var removalResult = vm.removeConsignmentFromItemNotes(vm.removeConsignmentFromCart, this);        
    }

    vm.removeConsignmentFromItemNotes = function(callBack, consignment) {
        $.ajax({
            type: "GET",
            cache: false,
            contentType: "application/json; charset=utf-8",
            async: false,
            url: serverRelativeUrl + servicePath + "/RemoveConsignment?orderNumber=" + consignment.OrderNumber,
            dataType: "json",
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                return callBack(consignment);
            },
            error: function (data) {
                console.log(data);
                vm.removeItemProgress(false);
                return false;
            }
        });
    }

    vm.removeConsignmentFromCart = function (consignment) {
        $.ajax({
            type: "POST",
            cache: true,
            contentType: "application/json; charset=utf-8",
            async: true,
            url: serverRelativeUrl + "/_vti_bin/Posten/Cart/CartTransaction.svc/RemoveService",
            dataType: "json",
            data: JSON.stringify({ serviceOrderNumber: consignment.OrderNumber, basketID: returnLocalValue(basketIdKey) }),
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                var result = data;
                ttt = data;
                removeLocalConsignment(consignment.OrderNumber);
                                
                var priceObject = JSON.parse(data);
                vm.Total(priceObject.TotalString);
                vm.ShoppingCartList.remove(consignment);
                vm.removeItemProgress(false);
                return data;
            },
            error: function (data) {
                console.log("cart removal failure: " + data);
                console.log("removal failure result:" + removalResult);
                console.log("Removal failed - check logs");
                vm.removeItemProgress(false);
                return false;
            }
        });
    }
    
    /* ------------------------------- Private methods  ---------------------------------------*/

    /* ------------------------------- Subscriptions ------------------------------------------*/
    vm.ShoppingCartList.subscribe(function () {
        
    });
    
    
    return vm;
})();

/* ------------------------------- Other functions ------------------------------------------*/

var ttt;






function removeLocalConsignment(orderNumber)
{    
    var localConsignments = returnLocalValue(basketOrderNumberKey); 
    if (localConsignments !== "")
    {        
        var orderNumbers = JSON.parse(localConsignments);     
        if (orderNumbers.indexOf(orderNumber) > -1)
        {            
            orderNumbers = $.grep(orderNumbers, function (value) {         
                return value !== orderNumber;
            });
            
            setLocalValue(basketOrderNumberKey, JSON.stringify(orderNumbers));            
        }        
    }
}

function retrievePrice() {
    var basePrice = "0 kr";
    var basketId = returnLocalValue("basketId");
    if (basketId !== "") {
        try {
            $.ajax({
                type: "POST",
                cache: true,
                contentType: "application/json; charset=utf-8",
                async: true,
                url: serverRelativeUrl + "/_vti_bin/Posten/Cart/CartTransaction.svc/GetBasketPrice",
                dataType: "json",
                data: JSON.stringify({ basketId: basketId }),
                headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
                success: function (data) {
                    if(data !== undefined) {
                        skickaShoppingCartVM.Total(data);
                    }
                    return basePrice;
                },
                error: function (data) {
                    console.log(data);
                    return basePrice;
                }
            });            
        }
        catch (e) {
            console.log(e.message);
            return basePrice;
        }
    }
    
};
