﻿$(document).ready(function () {
    if (!g_disableCheckoutInEditMode) {
        var breakPoint = 960; // This is where the layout collapses
        var topMargin = 20;
        var stickyTop = $('#ShoppingCartDisplay').offset().top;
        var stickyHeight = $('#ShoppingCartDisplay').height();
        var stickyWidth = $('#ShoppingCartDisplay').width();
        var parentHeight = stickyHeight + topMargin;
        $('#ShoppingCartDisplay').parent().height(parentHeight + 'px');
        $('#s4-workspace').scroll(function () {
            if ($(window).width() >= breakPoint) {
                var windowTop = $('#s4-workspace').scrollTop(); // returns number
                if (stickyTop < (windowTop + topMargin) && $('#ShoppingCartDisplay').height() < ($(window).height() - topMargin)) {
                    $('#ShoppingCartDisplay').css({ position: 'fixed', top: topMargin + 'px', width: stickyWidth + 'px' });
                } else {
                    $('#ShoppingCartDisplay').css('position', 'relative');
                }
            } else {
                $('#ShoppingCartDisplay').css('position', 'relative');
            }
        });
    }
});
