﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.SkickaService.Proxy;

    [Serializable]
    [DataContract]
    public class AddressBE
    {
        #region Constructors

        public AddressBE()
        {
        }

        public AddressBE(Party dataAccessObject)
        {
            this.DataAccessObjectToBusinessEntity(dataAccessObject);
        }

        public AddressBE(Posten.Portal.Skicka.SkickaService.Proxy.address dataAccessObject)
        {
            this.DataAccessObjectToBusinessEntity(dataAccessObject);
        }

        public AddressBE(address1 address1)
        {
            this.DataAccessObjectToBusinessEntity(address1);
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public string AddressField1
        {
            get;
            set;
        }

        [DataMember]
        public string AddressField2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public string Country
        {
            get;
            set;
        }

        [DataMember]
        public string Email
        {
            get;
            set;
        }

        [DataMember]
        public string DoorCode
        {
            get;
            set;
        }

        [DataMember]
        public string Mobile
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Phone
        {
            get;
            set;
        }

        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        public Party ToParty()
        {
            Party returnValue = new Party();
            returnValue.name = this.Name;
            returnValue.adress1 = this.AddressField1;
            returnValue.adress2 = this.AddressField2;
            returnValue.city = this.City;
            returnValue.company = this.CompanyName;
            returnValue.country = this.Country;
            returnValue.email = this.Email;
            if (!(string.IsNullOrEmpty(this.Mobile)))
            {
                returnValue.mobile = this.Mobile;
            }
            if (!(string.IsNullOrEmpty(this.Phone)))
            {
                returnValue.phone = this.Phone;
            }
            returnValue.zip = this.ZipCode;

            return returnValue;
        }

        private void DataAccessObjectToBusinessEntity(Party dataAccessObject)
        {
            this.AddressField1 = dataAccessObject.adress1;
            this.AddressField2 = dataAccessObject.adress2;
            this.City = dataAccessObject.city;
            this.CompanyName = dataAccessObject.company;
            this.Country = dataAccessObject.country;
            this.Email = dataAccessObject.email;
            this.Mobile = dataAccessObject.mobile;
            this.Name = dataAccessObject.name;
            this.Phone = dataAccessObject.phone;
            this.ZipCode = dataAccessObject.zip;
        }

        private void DataAccessObjectToBusinessEntity(address1 dataAccessObject)
        {
            this.AddressField1 = dataAccessObject.street;
            this.AddressField2 = dataAccessObject.street2;
            this.City = dataAccessObject.city;
            this.CompanyName = dataAccessObject.company;
            this.Country = dataAccessObject.country;
            this.Email = dataAccessObject.eMail;            
            this.Name = dataAccessObject.fullName;
            this.ZipCode = dataAccessObject.zip;
            this.DoorCode = dataAccessObject.doorCode;            
            this.Mobile = string.Empty;
            if (!(string.IsNullOrEmpty(dataAccessObject.mobile)))
            {
                this.Mobile = dataAccessObject.mobile;
            }

            this.Phone = string.Empty;
            if (!(string.IsNullOrEmpty(dataAccessObject.phone)))
            {
                this.Phone = dataAccessObject.phone;
            }           
        }

        private void DataAccessObjectToBusinessEntity(Posten.Portal.Skicka.SkickaService.Proxy.address dataAccessObject)
        {
            this.AddressField1 = dataAccessObject.street;
            this.AddressField2 = dataAccessObject.street2;
            this.City = dataAccessObject.city;
            this.CompanyName = dataAccessObject.company;
            this.Country = dataAccessObject.country;
            //this.Email = dataAccessObject.email;
            this.Mobile = dataAccessObject.mobile;
            this.Name = dataAccessObject.fullName;
            this.ZipCode = dataAccessObject.zip;
        }

        #endregion Methods
    }
}