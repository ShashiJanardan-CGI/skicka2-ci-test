﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    public class ConfigurationItemBE
    {
        #region Properties

        public string CustomerId
        {
            get;
            set;
        }

        public System.Uri ProduceAddress
        {
            get;
            set;
        }

        public string TaxCountry
        {
            get;
            set;
        }

        #endregion Properties
    }
}