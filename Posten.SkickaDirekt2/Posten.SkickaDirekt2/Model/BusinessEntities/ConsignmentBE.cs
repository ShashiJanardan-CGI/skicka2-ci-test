﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Posten.Portal.Platform.Common.BusinessEntities;
    using Posten.Portal.Skicka.Services.Utils;
    using Posten.Portal.Skicka.SkickaService.Proxy;
    using Posten.SkickaDirekt2.Utilities;

    [Serializable]
    public class ConsignmentBE
    {
        #region Constructors

        public ConsignmentBE()
        {
            this.AdditionalServices = new List<AdditionalServiceBE>();
        }

        public ConsignmentBE(Skicka.SkickaService.Proxy.consignment consignment)
        {
            this.AccountNumber = consignment.postforskottKontoNr;

            if (consignment.additionalServices != null)
            {
                this.AdditionalServices = Array.ConvertAll(
                    consignment.additionalServices,
                    o => this.TranslateToAdditionalServiceBE(o)).ToList();

                this.AdditionalServices = this.AdditionalServices.Where(o => o != null).ToList();
            }

            //Additional cost text
            if (consignment.additionalInfos != null)
            {
                foreach (additionalInfoEntity info in consignment.additionalInfos)
                {
                    this.AdditionalInformation += info.text + "<br/>";
                }
            }

            this.ProductName = consignment.baseServiceName;
            this.ProductCode = consignment.pabloPoductCode;
            this.BaseServiceLeveransId = consignment.baseServiceId;
            this.BookingId = consignment.bookingId;
            this.CODAmount = consignment.postforskottAmount / 100;
            this.DestinationID = consignment.toAddress.country;
            this.Diameter = consignment.diameter;
            this.FromAddress = new AddressBE(consignment.fromAddress);
            this.Height = consignment.height;
            this.Length = consignment.length;
            this.Marketplace = consignment.externalMarketPlace;
            this.MarketplaceBookingID = consignment.externalMarketPlaceBookingId;
            this.PaymentReference = consignment.postforskottPaymentReference;
            this.Plusgiro = consignment.plusgiro;
            this.ServiceCode = consignment.baseServiceId;
            this.ToAddress = new AddressBE(consignment.toAddress);
            this.Weight = consignment.weight;
            this.Width = consignment.width;
            this.Amount = new PriceBE(consignment.totalPrice);
            this.OrderNumber = consignment.orderNumber;
        }

        #endregion Constructors

        #region Properties

        public string AccountNumber
        {
            get;
            set;
        }

        public List<AdditionalServiceBE> AdditionalServices
        {
            get;
            set;
        }

        public PriceBE Amount
        {
            get; set;
        }

        public string BaseServiceLeveransId
        {
            get;
            set;
        }

        public string BookingId
        {
            get;
            set;
        }

        public decimal CODAmount
        {
            get;
            set;
        }

        public string DestinationID
        {
            get;
            set;
        }

        public int Diameter
        {
            get;
            set;
        }

        public AddressBE FromAddress
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public int Length
        {
            get;
            set;
        }

        // external marketplace
        public string Marketplace
        {
            get;
            set;
        }

        public string MarketplaceBookingID
        {
            get;
            set;
        }

        public string PaymentReference
        {
            get;
            set;
        }

        public bool Plusgiro
        {
            get;
            set;
        }

        public string ProductCode
        {
            get; set;
        }

        public string ProductName
        {
            get; set;
        }

        public string ServiceCode
        {
            get;
            set;
        }

        public AddressBE ToAddress
        {
            get;
            set;
        }

        public int Weight
        {
            get;
            set;
        }

        public int Width
        {
            get;
            set;
        }

        public string OrderNumber
        {
            get;
            set;
        }

        public string AdditionalInformation
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private AdditionalServiceBE TranslateToAdditionalServiceBE(additionalService1 additionalService)
        {
            try
            {
                return new AdditionalServiceBE(additionalService);
            }
            catch (Exception ex)
            {
                LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #endregion Methods
    }
}