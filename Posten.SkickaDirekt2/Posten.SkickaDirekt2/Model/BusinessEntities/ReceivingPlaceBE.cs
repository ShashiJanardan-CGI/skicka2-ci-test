﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class ReceivingPlaceBE
    {
        #region Properties

        public string Address
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public List<string> OpenTimes
        {
            get;
            set;
        }

        public string Postnummer
        {
            get;
            set;
        }

        public string Postort
        {
            get;
            set;
        }

        public string ProductionPointId
        {
            get;
            set;
        }

        public List<string> StoppingTimes
        {
            get;
            set;
        }

        #endregion Properties
    }
}