﻿using Posten.Portal.Platform.Common.Container;
using Posten.Portal.Skicka.Services.Logging;
using Posten.Portal.Skicka.Services.SkickaService;
using Posten.Portal.Skicka.Services.Utils;
using Posten.SkickaDirekt2.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Posten.Portal.SkickaDirekt2.Modules.HttpHandlers
{
    public class PdfHttpHandler : IHttpHandler
    {
        #region Members
        private static LogController logger;
        private string transactionLogID;
        #endregion Members

        #region Constructors

        public PdfHttpHandler()
        {
            try
            {
                logger = new LogController();
            }
            catch (Exception e)
            {
                var message = e.Message + e.StackTrace;
            }
        }

        #endregion Constructors

        #region Properties

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

         public string TransactionLogID
        {
            get { return transactionLogID; }
            set { transactionLogID = value; }
        }
        #endregion Properties

        #region Methods

        public void ProcessRequest(HttpContext context)
        {
            StringBuilder getPdfLogInfo = new StringBuilder();
            string bookingNr = context.Request[Constants.Request_bookingId_key];
            
            try{
                 LogUtility.RetrieveLogHeaders(context, out this.transactionLogID);
                logger.LogMessage(this.transactionLogID, "Start:ProcessRequestFor PDF_HTTP_Handler");

            // Note: Gmail can't handle links to pdf with content-disposition attachment, so these will not have the querystring saveFile=yes.
                bool showOpenOrSaveDialog = context.Request[Constants.Request_saveFile_key] != null && context.Request[Constants.Request_saveFile_key] == "yes";

            getPdfLogInfo.AppendLine("Processing request for PDF with bookingId: " + bookingNr);

            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            string path = request.Path.ToLower();

            byte[] bytes = GetPdf(bookingNr);

            if (bytes != null)
            {
                getPdfLogInfo.AppendLine("We've got a 'PDF' with byte[]-size: " + bytes.Length);
            }
            else
            {
                getPdfLogInfo.AppendLine("The GetPdf-method resulted in null");
            }

            if (bytes != null)
            {
                string downloadFileName = string.Format("Waybill_{0}.pdf", bookingNr.ToString());
                response.Clear();
                response.ContentType = "application/pdf";
                if (showOpenOrSaveDialog)
                {
                    // Force an "Open or Save file"-dialog in web browser.
                    response.AddHeader("Content-disposition", "Attachment;filename=" + downloadFileName);
                }
                else
                {
                    response.AddHeader("Content-disposition", "filename=" + downloadFileName);
                }

                response.Flush();
                response.BinaryWrite(bytes);
                response.Flush();
                response.End();
            }
            else
            {                
                response.Write("Något gick fel. Försök igen senare");
            }
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                string errmess = "<div style='background-color:#feeeee;margin-top:20px;margin-bottom:10px;color:#cd0104;border-top:#ce0004 5px solid;font-weight:bold;padding:5px 15px 10px 15px;font-family:Verdana,Arial,Helvetica,sans-serif;font-weight:bold;font-size:14px;color:#031e50;'>Något har blivit fel. Var god kontakta Kundtjänst på telefon 020 - 23 22 21</div>";
                string errmess2 = "<div style='background-color:#feeeee;margin-top:20px;margin-bottom:10px;color:#cd0104;border-top:#ce0004 5px solid;font-weight:bold;padding:5px 15px 10px 15px;font-family:Verdana,Arial,Helvetica,sans-serif;font-weight:bold;font-size:14px;color:#031e50;'>" + ex.Message + "</div>";
                context.Response.Write(errmess);
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:ProcessRequestFor PDF_HTTP_Handler");
            }
        }

        private byte[] GetPdf(string bookingId)
        {
            byte[] itemNotesPDF = null;
            long actualItemId = 0;

             try
            {
                logger.LogMessage(this.transactionLogID, "Start:GetPdf- bookingId: " + bookingId); 
            long.TryParse(bookingId, out actualItemId);

            ISkickaService skickaService = new SkickaServiceAgent();
            itemNotesPDF = skickaService.GetItemNotesPDF(bookingId);
            }
             catch (Exception e)
             {
                 logger.LogError(this.transactionLogID, e);
                 throw;
             }
             finally
             {
                 logger.LogMessage(this.transactionLogID, "End:GetPdf- bookingId: " + bookingId);
             }
            return itemNotesPDF;
        }

        #endregion Methods
    }
}
