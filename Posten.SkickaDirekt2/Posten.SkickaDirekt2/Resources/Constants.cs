﻿namespace Posten.SkickaDirekt2.Resources
{
    #region Enumerations

    public enum ConfigurationType
    {
        DEV,
        RIT,
        PROD
    }

    #endregion Enumerations

    internal class Constants
    {
        #region Fields

        public const string CampaignID = "CampaignID";
        public const string CatchLoggerMessage = "{0}" + " " + "{1}";
        public const string CenterWebPartZoneId = "CenterColumn";
        public const string CharactersAllowedInAddressesFormatRegExp = @"[^{0}åäö\w\sa-z]";
        public const string CharactersAllowedInAddressesRegExp = @"[^åäö\w\sa-z]";
        public const string ConfigOwner = "SkickaDirekt2";
        public const string CSSRelativePath = "/_Layouts/Posten.SkickaDirekt2/Css/";
        public const string DigitRegExp = @"\d";
        public const string EmailRegExp = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        public const string FeatureProperty_WebConfigMode = "WebConfigMode";
        public const string FeatureProperty_WebConfigTemplateFile = "WebConfigTemplateFile";
        public const string CINTUserName = "username";
        public const string CINTPassword = "password";
        public const string CartWebSealUrl = "CartWebSealUrl";
        public const string SiteUrl = "ApplicationUrl";
        public const string CINTProduceUrl = "ProduceUrl";
        
        public const string FooterWebPartZoneId = "Footer";       
        public const string LogApplicationName = "SkickaDirekt2";
        public const string LogCategoryDefault = "SkickaDirekt2";
        public const string LogCategoryVerbose = "SkickaDirekt2 VERBOSE";
        public const int LogEntryEventId = 3000;
        public const string LoggerMessage = "{0}" + " " + "{1}" + ",\r\n" + "{2}";
        public const string LogInnerException = "Inner Exception";
        public const string LogUnhandledException = "Unable to HandleException in LogHelper ";

        public const string CustomCSS = "skickadirekt.css";
        public const string StyleLibratyTitle = "Formatbibliotek";

        // Urls - Paths
        public const string MasterPageUrl = "/_catalogs/masterpage/SkickaDirekt2_v4.master";
        public const string NewLine = "\r\n\t";
        public const string CountryInformationLink = "http://services3.posten.se/ptm/ptm_do.jsp?country=*&action=searchCountry";
        public const string CheckoutPagePath = "/checkout/_layouts/posten/cart/2.0/checkout.aspx?returnUrl=";
        public const string ConfirmationDownloadLink = "/checkout/skicka.confirmation?bookingid=${bookingId}";
        public const string WaybillDownloadLink = "/checkout/skicka.waybill?bookingId=${bookingId}";
        public const string ApplicationUrl = "";
        public const string ConfirmationPageUrl = "/checkout/_layouts/Posten/Cart/2.0/Confirmation.aspx";
        public const string EditUrl = "?returnUrl=";
        public const string ProduceUrl = "http://sitgw.cint.postdk.net:6300/soap/Common/ProduceItemNotesService_v0200";
        public const string CSSPath =  "template/layouts/posten.skickadirekt2/CSS/";
        public const string TrackAndTraceLink = "<font>F&ouml;rs&auml;ndelseid:<a href='http://www.postnord.se/sv/verktyg/sok/Sidor/spara-brev-paket-och-pall.aspx?search=${consignmentNr}' target='_blank'>${consignmentNr}</a></font>";

        //Regex
        public const string OnlyDigitsRegExp = @"[^\d0-9-]";
        public const string OrganizationNumberRegExp = @"[\d0-9]{6}-[\d0-9]{4}|[\d0-9]{6} [\d0-9]{4}|[\d0-9]{10}";
        public const string ParRegExp = @"(?<RemoveAllButDigits>[^\d0-9-])";
        public const string ParRegexpEnd = "]";
        public const string ParRegexpStart = "[";
        public const string ParseParParad = @"[^\d0-9-+<> ]";
        public const string pathXml = "template/layouts/Posten.SkickaDirekt2/XML/Config";
        public const string PhoneRegExp = @"^((\d{2,4}[\s-]{1})|(\d{2,3}))(((\d{2,3}[\s]{1}){2,3}(\d{2,3}))|(\d{5,9}))$";
        public const string PostalCodeRegExp = @"^\d+$";
        public const string ZipCodeRegExp = @"^\d{5}$";
        public const string SpaceRegExp = @"\s";

        // Content Type IDs
        public const string PageContentTypeID = "0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF39";
        public const string PageForAbout = "About.aspx";
        public const string PageForOurServices = "Services.aspx";
        public const string PageForPaymentConfirmation = "Confirm.aspx";
        public const string PageForNonSupportedBrowser = "NonSupportedBrowser.aspx";

        // Pages
        public const string PageForStart = "Start.aspx";
        public const string ParBlockListSeparator = ",";
        public const string SemicolonDelimiter = ";";
        public const string SourceTemplateFilename_DEV = "templateWeb.config";
        public const string SourceTemplateFilename_PROD = "templateWeb_PROD";
        public const string SourceTemplateFilename_RIT = "templateWeb_RIT";
        public const string SourceWebConfigFilename = "sourceWeb.config";        
        public const string Request_saveFile_key = "saveFile";
        public const string Request_bookingId_key = "bookingid";

        // PageLayouts
        public const string StartPageLayout = "SkickaDirekt2_StartPage.aspx";
        public const string TransactionLogID = "TransactionLogID";
        public const string UserID = "UserID";
        public const string V4MasterPageUrl = "/_catalogs/masterpage/v4.master";
        public const string WebPartCategory = "SkickaDirekt2 Properties";
        public const string RightWebPartZoneId = "RightColumn";

        public const string MainInfoBoxHtmlPath = "template/layouts/Posten.SkickaDirekt2/HTML/InfoBox.html";
        public const string InformationBoxHtmlPath = "template/layouts/Posten.SkickaDirekt2/HTML/Information.html";
        public const string FooterHtmlPath = "template/layouts/Posten.SkickaDirekt2/HTML/Footer.html";
        public const string VideoHtmlPath = "template/layouts/Posten.SkickaDirekt2/HTML/Video.html";
        public const string ServicesHtmlPath = "template/layouts/Posten.SkickaDirekt2/HTML/Services.html";
        public const string AboutHtmlPath = "template/layouts/Posten.SkickaDirekt2/HTML/About.html";
        public const string NonSupportedBrowserHtmlPath = "template/layouts/Posten.SkickaDirekt2/HTML/NonSupportedBrowser.html";
                
        #endregion Fields
    }
}