﻿namespace Posten.Portal.Skicka.Services.SkickaService
{
    using System;
    using System.Collections.Generic;
    using System.Web;

    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.PVT;
    using Posten.SkickaDirekt2.Utilities;

    public class SkickaServiceStub : ISkickaService
    {
        #region Fields

        static List<ConsignmentBE> Consignments = new List<ConsignmentBE>();

        private HttpContext context = null;

        #endregion Fields

        #region Methods

        public ReceivingPlaceBE FindReceivingPlace(string bookingId)
        {
            ReceivingPlaceBE rp = new ReceivingPlaceBE()
            {
                Address = "Address",
                Name = "Name",
                OpenTimes = new List<string>() { "OpenTime", "OpenTime" },
                Postnummer = "Postnummer",
                Postort = "Postort",
                ProductionPointId = "ProductionPointId",
                StoppingTimes = new List<string>() { "StoppingTime", "StoppingTime" }
            };

            return rp;
        }

        public List<OrderLabelBE> GetAvailableOrderLabels()
        {
            List<OrderLabelBE> olbe = new List<OrderLabelBE>();
            olbe.Add(new OrderLabelBE()
            {
                CustomerNumber = "CustomerNumber",
                LabelType = "LabelType",
                Name = "Name",
                ProdId = "ProdId",
                Quantity = "Quantity"
            });

            return olbe;
        }

        public ConfigurationItemBE GetConfigurationItem()
        {
            return new ConfigurationItemBE()
            {
                CustomerId = "22",
                ProduceAddress = new Uri(@"http://www.capgemini.com"),
                TaxCountry = "Sverige"
            };
        }

        public ConsignmentBE GetConsignment(string orderId)
        {
            ConsignmentBE result = null;
            foreach (ConsignmentBE consignment in Consignments)
            {
                if (consignment.BookingId == orderId)
                {
                    result = consignment;
                    break;
                }
            }

            return result;
        }

        public List<ConsignmentBE> GetConsignments(string bookningId)
        {
            return Consignments;
        }

        public byte[] GetItemNotesPDF(string bookningId)
        {
            byte[] notes = { new byte(), new byte() };
            return notes;
        }

        public List<ReceivingPlaceBE> GetValidServicePoints(string bookingId, string[] productionPointIds)
        {
            List<ReceivingPlaceBE> validServicePoints = new List<ReceivingPlaceBE>();
            validServicePoints.Add(new ReceivingPlaceBE());
            return validServicePoints;
        }

        public string GetVersion()
        {
            return "SkickaServiceStub";
        }

        public InformPutInCartResponseBE InformPutInCart(
            string orderNumber,
            ValidationResponseBE validatedService,
            string serviceName,
            string serviceCode,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            string codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,
            string receiverDestinationId)
        {
            ConsignmentBE consignment = new ConsignmentBE()
            {
                AdditionalServices = validatedService.AdditionalServices,
                Amount = validatedService.Price,
                BaseServiceLeveransId = "BaseServiceLeveransId",
                BookingId = orderNumber,
                DestinationID = receiverDestinationId,
                Diameter = size.Diameter,
                FromAddress = fromAddress,
                Height = size.Height,
                Length = size.Length,
                Marketplace = validatedService.Marketplace,
                MarketplaceBookingID = validatedService.MarketplaceBookingId,
                ProductCode = validatedService.ProductCode,
                ProductName = validatedService.BaseService.Name,
                ServiceCode = validatedService.BaseService.AdditionalServiceCode,
                ToAddress = toAddress,
                Weight = weight,
                Width = size.Width
            };

            double amount = UIHelper.ReturnDouble(codAmount);
            if (amount > 0)
            {
                consignment.Plusgiro = plusgiro;
                consignment.AccountNumber = postforskottKontoNr;
                consignment.PaymentReference = postforskottPaymentReference;
                consignment.CODAmount = Convert.ToInt64(amount); ;
            }

            Consignments.Add(consignment);

            return new InformPutInCartResponseBE()
            {
                AdditionalInfo = new List<InfoBE>(),
                Status = "OK"
            };
        }

        public InformPutInCartResponseBE InformUpdateInCart(
            string productCode,
            string baseServiceId,
            int length,
            int width,
            int height,
            int weight,
            List<AdditionalServiceBE> additionalServices,
            string valueAmount,
            string content,
            string codAmount,
            int diameter,
            string destinationId,
            AddressBE fromAddress,
            AddressBE toAddress,
            int kolliNrSeriesType,
            string orderNumber,
            string passwordCollecting,
            string postforskottClearingNr,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottName,
            string postforskottPaymentReference,
            PriceBE price,
            PriceBE baseServicePrice,
            string baseServiceName,
            string baseServiceOrderRowNumber,
            string baseServiceSapProductId,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details)
        {
            throw new NotImplementedException();
        }

        public InformPutInCartResponseBE InformUpdateInCart(
            ValidationResponseBE validatedService,
            string oldOrderNumber,
            string newOrderNumber,
            string productCode,
            string baseServiceId,
            int length,
            int width,
            int height,
            int weight,
            List<AdditionalServiceBE> additionalServices,
            double valueAmount,
            string content,
            string codAmount,
            int diameter,
            string destinationId,
            AddressBE fromAddress,
            AddressBE toAddress,
            int kolliNrSeriesType,
            string passwordCollecting,
            string postforskottClearingNr,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottName,
            string postforskottPaymentReference,
            PriceBE price,
            PriceBE baseServicePrice,
            string baseServiceName,
            string baseServiceOrderRowNumber,
            string baseServiceSapProductId,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details)
        {
            throw new NotImplementedException();
        }

        public bool RemoveConsignment(string orderNumber) {
            throw new NotImplementedException();
        }

        public InformPutInCartResponseBE InformUpdateInCart(
            string oldOrderNumber,
            string newOrderNumber,
            string baseServiceOrderRowNumber,
            string serviceName,
            string serviceCode,
            ValidationResponseBE validatedService,
            ServicePropertiesBE serviceCombination,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            string codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details, string receiverDestinationId)
        {
            throw new NotImplementedException();
        }

        public void Initialize(HttpContext context)
        {
            this.context = context;
        }

        public string OrderLabels(string[] prodIds, string customerName, string contactPerson, string email, string streetAddress, string zip, string city, string phone, string addressCo)
        {
            return "OK";
        }

        public string ReturnWithColon(string intime)
        {
            string outtime = string.Empty;
            return outtime;
        }

        #endregion Methods
    }
}