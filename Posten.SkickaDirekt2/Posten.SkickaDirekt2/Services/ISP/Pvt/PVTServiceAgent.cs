﻿namespace Posten.Portal.Skicka.Services.PVT
{
    using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
    using Posten.Portal.Platform.Common.Services;
    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.Logging;
    using Posten.Portal.Skicka.Services.Utils;
    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Net;
    using System.ServiceModel;
    using System.Web;
    using System.Web.Script.Serialization;

    public class PVTServiceAgent : ServiceAgentBase, IPVTService
    {
        #region Fields

        private static LogController logger;

        private PvtServiceBeanClient pvtClient;
        private string transactionLogID;

        #endregion Fields

        #region Constructors

        public PVTServiceAgent()
        {
            try
            {
                this.pvtClient = (PvtServiceBeanClient)ConfigureServiceClient<PvtServiceBeanClient, PvtServiceBean>();
                logger = new LogController();
            }
            catch (Exception exception)
            {
                var message = exception.Message + exception.StackTrace;
            }
        }

        public PVTServiceAgent(HttpContext context)
        {
            try
            {
                this.pvtClient = new PvtServiceBeanClient();
                this.pvtClient.ClientCredentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings[Constants.CINTUserName];
                this.pvtClient.ClientCredentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings[Constants.CINTPassword];

                LogUtility.RetrieveLogHeaders(context, out this.transactionLogID);
                logger = new LogController();
            }
            catch (Exception exception)
            {
                var message = exception.Message + exception.StackTrace;
            }
        }

        #endregion Constructors

        #region Properties

        public string TransactionLogID
        {
            get { return transactionLogID; }
            set { transactionLogID = value; }
        }

        #endregion Properties

        #region Methods


        
        public DestinationsBE GetDestinations()
        {
            DestinationsBE destinations = new DestinationsBE();
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetDestinations");                  
                DestinationResponse response = new DestinationResponse();
                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    response = this.pvtClient.listDestinations();
                }
                destinations = new DestinationsBE(response);                
            }
            catch (Exception ex)
            {                
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetDestinations");
            }
            return destinations;
        }

        
        public PriceAndRanksBE GetPreferredProduct(long productId, string destinationID, SizeBE measurements, int weight)
        {
            PriceAndRanksBE result = null;
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetPreferredProduct:productId=" + productId + ",destinationID=" + destinationID + ",weight=" + weight);                  
                PriceAndRankResponse response = new PriceAndRankResponse();                               
                ParcelSize size = TranslateSizeBEtoParcelSize(measurements);

                preferredProductRequest request = new preferredProductRequest();
                request.externalMarketPlace = ConstantsAndEnums.SkickaMarketPlaceID;
                request.productIdSpecified = true;
                request.productId = productId;
                request.destinationId = destinationID;
                request.size = size;
                request.weightSpecified = true;
                request.weight = weight;

                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    response = this.pvtClient.getPreferredProduct(request);
                }
                result = new PriceAndRanksBE(response);                
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetPreferredProduct:productId=" + productId + ",destinationID=" + destinationID + ",weight=" + weight);  
            }
            return result;
        }

        
        public ProductsBE GetProducts(string countryISO2Code)
        {
            ProductsBE products = null;
            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetProducts:countryISO2Code=" + countryISO2Code); 
                
                productsRequest request = new productsRequest();                
                request.destinationId = countryISO2Code;
                request.externalMarketPlace = ConstantsAndEnums.SkickaMarketPlaceID;

                ServicesResponse result = new ServicesResponse();
                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    result = this.pvtClient.getProducts(request);
                }
                products = new ProductsBE(result);
                
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetProducts:countryISO2Code=" + countryISO2Code); 
            }
            return products;
        }

        
        public PriceAndRanksBE GetProductSuggestions(string countryISO2Code, SizeBE measurements, int weight, ConstantsAndEnums.Characteristics[] filters)
        {
            PriceAndRanksBE productSuggestions = null;

            try
            {
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:GetProductSuggestions:countryISO2Code=" + countryISO2Code + ",weight=" + weight);

                ParcelSize size = TranslateSizeBEtoParcelSize(measurements);
                characteristics[] characteristics = Array.ConvertAll(
                    filters,
                    s => (characteristics)Enum.Parse(typeof(characteristics), Enum.GetName(typeof(ConstantsAndEnums.Characteristics), s)));
                
                productSuggestionsRequest request = new productSuggestionsRequest();                
                request.externalMarketPlace = ConstantsAndEnums.SkickaMarketPlaceID;
                request.destinationId = countryISO2Code;
                request.size = size;
                request.weightSpecified = true;
                request.weight = weight;
                request.filters = characteristics;

                PriceAndRankResponse result = new PriceAndRankResponse();
                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    result = this.pvtClient.getProductSuggestions(request);
                }
                productSuggestions = new PriceAndRanksBE(result);                
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:GetProductSuggestions:countryISO2Code=" + countryISO2Code + ",weight=" + weight);
            }
            return productSuggestions;
        }

        public string GetVersion()
        {
            string version = "Version not set";
            try
            {
                logger.LogMessage("Start:GetVersion");
                version = this.pvtClient.getVersion();
            }
            catch (Exception ex)
            {
                logger.LogError(ex);

                if (ExceptionPolicy.HandleException(ex, "SkickaExceptionPolicy"))
                {
                    throw;
                }
            }
            finally {
                logger.LogMessage("End:GetVersion");
            }

            return version;
        }

        public string TestService()
        {
            try
            {
                return "Agent";
            }
            catch (Exception ex)
            {
                Posten.Portal.Skicka.Services.Utils.LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return ex.ToString();
            }
        }

        
        public ValidationResponseBE ValidateItem(SizeBE size, int weight, List<string> additionalServiceIDs, AccountBE account, string countryISOCode, long selectedServiceID, AddressBE sender, AddressBE receiver)
        {
            orderRequest request = new orderRequest();
            ValidationResponseBE validationResponse = new ValidationResponseBE();
            itemResponse response = new itemResponse();
            try
            {               
                var correlationId = logger.LogMessage(this.transactionLogID, "Start:ValidateItem:weight=" + weight + ",countryISOCode=" + countryISOCode + ",selectedServiceID=" + selectedServiceID);

                InitiateValidateItemRequest(this.transactionLogID, size, weight, additionalServiceIDs, account, countryISOCode, selectedServiceID, sender, receiver, request);
                
                using (new OperationContextScope(this.pvtClient.InnerChannel))
                {
                    LogUtility.AddTransactionLogIDHeader(this.transactionLogID, correlationId);                    
                    response = this.pvtClient.validateItem(request);
                }

                if (response.status == responseStatus.SUCCESS)
                {
                    //Initiate validation response                    
                    InitiateValidationResponse(this.transactionLogID, response, validationResponse);
                }
                else
                {
                    validationResponse.Valid = false;
                    validationResponse.StatusMsg = response.statusMsg;

                    logger.LogMessage(this.transactionLogID, "Result:ValidateItem - INVALID INPUT - " + response.statusMsg); 
                }                
            }
            catch (Exception ex)
            {
                logger.LogError(this.transactionLogID, ex);
                throw;
            }
            finally
            {
                logger.LogMessage(this.transactionLogID, "End:ValidateItem:weight=" + weight + ",countryISOCode=" + countryISOCode + ",selectedServiceID=" + selectedServiceID);
            }
            return validationResponse;
        }

        
        public string ZipToCity(string zipCode)
        {
            zipCode = zipCode.Replace(" ", string.Empty).Trim();
            string jsonString = string.Empty;
            string returnValue = false.ToString();
            string keyword = "postalcity";
            try
            {
                logger.LogMessage(this.transactionLogID, "Start:ZipToCity:zipCode=" + zipCode);
                string url = ConfigurationManager.AppSettings[ConstantsAndEnums.SkickaZipCodeServiceUrlAppKey];
                WebRequest request = HttpWebRequest.Create(url + zipCode);
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        jsonString = reader.ReadToEnd();
                    }
                }
                if (!string.IsNullOrEmpty(jsonString))
                {
                    var zipAndCityDictionary = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(jsonString);
                    if (zipAndCityDictionary != null && zipAndCityDictionary.ContainsKey(keyword))
                    {
                        returnValue = zipAndCityDictionary[keyword];
                    }
                }
                
            }
            catch (Exception e)
            {
                logger.LogError(this.transactionLogID, e);
                if (e.Message.Contains("Unable to connect to the remote server"))
                {
                    returnValue = "fail";
                }
                else
                {
                    returnValue = "Invalid";
                }
            }
            finally {
                logger.LogMessage(this.transactionLogID, "End:ZipToCity:zipCode=" + zipCode);
            }
           
            return returnValue;
        }

        private static void InitiateValidateItemRequest(string transactionLogID, SizeBE size, int weight, List<string> additionalServiceIDs, AccountBE account, string countryISOCode, long selectedServiceID, AddressBE sender, AddressBE receiver, orderRequest request)
        {
            request.externalMarketPlace = ConstantsAndEnums.SkickaMarketPlaceID;
            request.order = new Order();
            try
            {
                var correlationId = logger.LogMessage(transactionLogID, "Start:InitiateValidateItemRequest:weight=" + weight + ",countryISOCode=" + countryISOCode + ",selectedServiceID=" + selectedServiceID);
                if (additionalServiceIDs != null && additionalServiceIDs.Count > 0)
                { //This section is only provided to request order object if additional services have been selected
                    request.order.additionalServiceIds = additionalServiceIDs.ToArray();
                }

                if (additionalServiceIDs.Contains("01") || selectedServiceID.Equals(2))//Postforkott - COD: cash on delivery
                {
                    CodInformation codInfo = new CodInformation();
                    codInfo.accountNo = account.AccountNumber;
                    codInfo.amount = UIHelper.ReturnDouble(account.Amount);
                    codInfo.amountSpecified = true;
                    codInfo.reference = account.Reference;
                    codInfo.giroType = account.ReturnAccountGiroType();
                    codInfo.giroTypeSpecified = true;

                    request.order.codInformation = codInfo;
                }

                request.order.destinationId = countryISOCode;
                //request.order.marketPlaceId = ConstantsAndEnums.SkickaMarketPlaceID;
                request.order.productId = selectedServiceID;
                request.order.productIdSpecified = true;
                request.order.receiver = receiver.ToParty();
                request.order.sender = sender.ToParty();
                request.order.size = size.BusinessEntityToDataAccessObject();
                request.order.weight = weight;
                request.order.weightSpecified = true;
            }
            catch (Exception e)
            {
                logger.LogError(transactionLogID, e);
                throw;
            }
            finally {
                logger.LogMessage(transactionLogID, "End:InitiateValidateItemRequest:weight=" + weight + ",countryISOCode=" + countryISOCode + ",selectedServiceID=" + selectedServiceID);
            }
        }

        private static void InitiateValidationResponse(string transactionLogID, itemResponse response, ValidationResponseBE validationResponse)
        {
            try
            {
                logger.LogMessage(transactionLogID, "Start: InitiateValidationResponse");
                validationResponse.Valid = true;
                validationResponse.SapId = response.item.baseService.sapProductId;
                validationResponse.ReferenceNumberSerialType = response.item.referenceNumberSerialType;
                validationResponse.StatusMsg = response.statusMsg;
                validationResponse.ProductCode = response.item.baseService.serviceId;//response.item.compoundPvtId;
                validationResponse.CompanyCode = response.item.baseService.companyCode;

                validationResponse.Price = new PriceBE();
                if (response.item.totalPrice != null)
                {
                    validationResponse.Price.Amount = response.item.totalPrice.amount;
                    validationResponse.Price.AmountNoVat = response.item.totalPrice.amountNoVat;
                    validationResponse.Price.VATAmount = response.item.totalPrice.vatAmount;//MVU7734 - S
                    validationResponse.Price.Currency = response.item.totalPrice.currency;
                    validationResponse.Price.VATPercentage = (decimal)response.item.totalPrice.vat;
                    validationResponse.Price.VatCode = response.item.totalPrice.vatCode; //MVU7734 - S
                }

                validationResponse.BaseServicePrice = new PriceBE();
                if (response.item.baseService.totalPrice != null) //baseService is an order item
                {
                    validationResponse.BaseServicePrice.Amount = response.item.baseService.totalPrice.amount;
                    validationResponse.BaseServicePrice.AmountNoVat = response.item.baseService.totalPrice.amountNoVat;
                    validationResponse.BaseServicePrice.VATAmount = response.item.baseService.totalPrice.vatAmount;//MVU7734 - S
                    validationResponse.BaseServicePrice.Currency = response.item.baseService.totalPrice.currency;
                    validationResponse.BaseServicePrice.VATPercentage = (decimal)response.item.baseService.totalPrice.vat;
                    validationResponse.BaseServicePrice.VatCode = response.item.baseService.totalPrice.vatCode; //MVU7734 - S
                }

                if (response.item.deliveryTime != null)
                {
                    validationResponse.DeliveryInfo = new DeliveryInfoBE();
                    validationResponse.DeliveryInfo.Delayed = response.item.deliveryTime.delayed;
                    validationResponse.DeliveryInfo.DeliveryDay = response.item.deliveryTime.deliveryDay;
                    validationResponse.DeliveryInfo.DeliveryHour = response.item.deliveryTime.deliveryHour;
                }

                //Base Service
                validationResponse.BaseService = new AdditionalServiceBE();
                validationResponse.BaseService.AdditionalServiceCode = response.item.baseService.serviceId;
                validationResponse.BaseService.ServiceKey = response.item.baseService.serviceId;//response.item.compoundPvtId;
                validationResponse.BaseService.Name = response.item.baseService.name;
                validationResponse.BaseService.SapId = response.item.baseService.sapProductId;
                validationResponse.BaseService.CompanyCode = response.item.baseService.companyCode;
                validationResponse.BaseService.Price = new PriceBE();
                validationResponse.BaseService.Price.Currency = response.item.baseService.price.currency;
                validationResponse.BaseService.Price.Amount = response.item.baseService.price.amount;
                validationResponse.BaseService.Price.AmountNoVat = response.item.baseService.price.amountNoVat;
                validationResponse.BaseService.Price.VATAmount = response.item.baseService.price.vatAmount; //MVU7734 - S
                validationResponse.BaseService.Price.VATPercentage = (decimal)response.item.baseService.price.vat;
                validationResponse.BaseService.Price.VatCode = response.item.baseService.price.vatCode; //MVU7734 - S
                validationResponse.BaseService.PvtId = response.item.baseService.pvtId;

                validationResponse.AdditionalServices = new List<AdditionalServiceBE>();
                if (response.item.additionalServices != null)
                {
                    foreach (LineItem additionalService in response.item.additionalServices)
                    {
                        AdditionalServiceBE additionalServiceBE = new AdditionalServiceBE();
                        additionalServiceBE.AdditionalServiceCode = additionalService.serviceId;
                        additionalServiceBE.ServiceKey = response.item.baseService.serviceId; //additionalService.serviceId; // response.item.baseService.pvtId;//response.item.code;
                        additionalServiceBE.Name = additionalService.name;
                        additionalServiceBE.SapId = additionalService.sapProductId;
                        additionalServiceBE.CompanyCode = additionalService.companyCode;
                        additionalServiceBE.Price = new PriceBE();
                        additionalServiceBE.Price.Currency = additionalService.price.currency;
                        additionalServiceBE.Price.Amount = additionalService.price.amount;
                        additionalServiceBE.Price.AmountNoVat = additionalService.price.amountNoVat;
                        additionalServiceBE.Price.VATAmount = additionalService.price.vatAmount;//MVU7734 - S
                        additionalServiceBE.Price.VATPercentage = (decimal)additionalService.price.vat;
                        additionalServiceBE.Price.VatCode = additionalService.price.vatCode; //MVU7734 - S
                        additionalServiceBE.PvtId = additionalService.pvtId;

                        validationResponse.AdditionalServices.Add(additionalServiceBE);
                    }
                }

                if (response.responseInfos != null)
                {
                    validationResponse.AdditionalInfo = new List<InfoBE>();
                    foreach (responseInfo info in response.responseInfos)
                    {
                        InfoBE infoBe = new InfoBE();
                        //infoBe.Attribute = info.description;
                        infoBe.Code = info.code.ToString();
                        infoBe.Value = info.description;
                        validationResponse.AdditionalInfo.Add(infoBe);
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError(transactionLogID, e);
                throw;
            }
            finally
            {
                logger.LogMessage(transactionLogID, "End: InitiateValidationResponse");
            }
        }

        private static CodInformation TranslateAccountBEtoCodInformation(string transactionLogID, AccountBE account)
        {
            CodInformation translatedObject = null;
            try
            {
                logger.LogMessage(transactionLogID, "Start: TranslateAccountBEtoCodInformation");
                translatedObject = new CodInformation()
                {
                    accountNo = account.AccountNumber,

                    amountSpecified = true,
                    amount = UIHelper.ReturnDouble(account.Amount),

                    giroTypeSpecified = true,
                    giroType = (i2GiroType)Enum.Parse(typeof(i2GiroType), Enum.GetName(typeof(AccountBE.GiroTypeEnum), account.AccountType)),

                    reference = account.Reference,
                };
            }
            catch (Exception e)
            {
                logger.LogError(transactionLogID, e);
                throw;
            }
            finally
            {
                logger.LogMessage(transactionLogID, "End: TranslateAccountBEtoCodInformation");
            }

            return translatedObject;
        }
                
        private static ParcelSize TranslateSizeBEtoParcelSize(SizeBE sizeBE)
        {
            ParcelSize translatedSize = new ParcelSize();
            try
            {
                translatedSize.diameter = sizeBE.Diameter;
                translatedSize.height = sizeBE.Height;
                translatedSize.length = sizeBE.Length;
                translatedSize.width = sizeBE.Width;
            }
            catch (Exception e)
            {
                logger.LogError(e);
            }

            return translatedSize;
        }

        #endregion Methods

       
    }
}