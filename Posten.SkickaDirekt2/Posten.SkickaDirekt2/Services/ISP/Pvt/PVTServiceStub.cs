﻿namespace Posten.Portal.Skicka.Services.PVT
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.Utils;
    using System.Web;

    //public class PVTServiceStub : IPVTService
    //{
    //    #region Fields

    //    public Dictionary<long, List<AdditionalServiceBE>> ProductAdditionalServicesDictionary = new Dictionary<long, List<AdditionalServiceBE>>();
    //    public List<ProductDefinitionBE> ProductDefinitions = new List<ProductDefinitionBE>();

    //    #endregion Fields

    //    #region Constructors

    //    public PVTServiceStub()
    //    {
    //        // Initialize ProductDefinitions
    //        List<AdditionalServiceBE> productOneAdditionalServices = new List<AdditionalServiceBE>()
    //            {
    //                new AdditionalServiceBE() { AdditionalServiceCode = "a", Name = "Personlig utlämning", CompanyCode = "pvtA", Price = new PriceBE() { Amount = 15, VATAmount = 6, AmountNoVat = 10, Currency = "SEK" } },
    //                new AdditionalServiceBE() { AdditionalServiceCode = "01", Name = "Postförskott", CompanyCode = "pvtB", Price = new PriceBE() { Amount = 25, VATAmount = 6, AmountNoVat = 20, Currency = "SEK", VATPercentage = 25.0M } },
    //                new AdditionalServiceBE() { AdditionalServiceCode = "c", Name = "Kvittens via mail", CompanyCode = "pvtC", Price = new PriceBE() { Amount = 35, VATAmount = 6, AmountNoVat = 30, Currency = "SEK" } },
    //                new AdditionalServiceBE() { AdditionalServiceCode = "d", Name = "Personlig utlämning", CompanyCode = "pvtD", Price = new PriceBE() { Amount = 45, VATAmount = 6, AmountNoVat = 40, Currency = "SEK" } }
    //            };

    //        ProductDefinitionBE productOne =
    //            new ProductDefinitionBE()
    //               {
    //                   ProductId = 1,
    //                   Code = "AP",
    //                   Name = "Skicka lätt",
    //                   WeightIntervals = new int[] { 100, 200, 500, 1740 },
    //                   MinWeight = 100,
    //                   MaxWeight = 1740,
    //                   MinSize = new ParcelSizeBE() { Length = 140, RollLength = 100, Width = 90 },
    //                   MaxSize = new ParcelSizeBE() { Length = 600, RollLength = 900 },
    //                   AdditionalServices = productOneAdditionalServices.ToArray(),
    //                   Price = new PriceBE() { Amount = 200, AmountNoVat = 188, Currency = "SEK", VATAmount = 6 },
    //                   Description = "<ul><li>A property</li><li>Getting there</li></ul>",
    //                   AdditionalInfoText = this.GetDummyAdditionalInfoTexts(),
    //                   Properties = new ServicePropertiesBE()
    //                   {
    //                       DoorCode = ServicePropertiesBE.ShowValue.HIDE,
    //                       FilterCOD = true,
    //                       FilterDelivery = true,
    //                       FilterExpress = true,
    //                       FilterTrace = true,
    //                       FilterValue = true,
    //                       LetterAllowed = true,
    //                       MailNotification = ServicePropertiesBE.ShowValue.REQUIRED,
    //                       PhoneNotification = ServicePropertiesBE.ShowValue.HIDE,
    //                       SMSNotification = ServicePropertiesBE.ShowValue.SHOW,
    //                       Vat = true
    //                   },
    //                   International = false
    //               };

    //        List<AdditionalServiceBE> productTwoAdditionalServices = new List<AdditionalServiceBE>()
    //            {
    //                new AdditionalServiceBE() { AdditionalServiceCode = "a", Name = "Personlig utlämning", CompanyCode = "pvtA", Price = new PriceBE() { Amount = 15, VATAmount = 6, AmountNoVat = 10, Currency = "SEK" } },
    //            };

    //        ProductDefinitionBE productTwo =
    //            new ProductDefinitionBE()
    //                {
    //                    ProductId = 2,
    //                    Code = "TEST",
    //                    Name = "TEST",
    //                    WeightIntervals = new int[] { 100, 200, 500, 1740, 4000, 6000 },
    //                    MinWeight = 100,
    //                    MaxWeight = 6000,
    //                    MinSize = new ParcelSizeBE() { Length = 140, RollLength = 140, Width = 90 },
    //                    MaxSize = new ParcelSizeBE() { Length = 600, RollLength = 600 },
    //                    AdditionalServices = productTwoAdditionalServices.ToArray(),
    //                    Documents = new ServiceDocumentBE[]
    //                        {
    //                            new ServiceDocumentBE() {
    //                                DocumentCategory = ServiceDocumentBE.ServiceDocumentCategoryEnum.TOLL_DOCUMENT,
    //                                DocumentId = 1,
    //                                DocumentType = ServiceDocumentBE.ServiceDocumentTypeEnum.PDF,
    //                                Name = "Långt bort",
    //                                Url = "www.dn.se"
    //                            },

    //                            new ServiceDocumentBE() {
    //                                DocumentCategory = ServiceDocumentBE.ServiceDocumentCategoryEnum.TOLL_DOCUMENT,
    //                                DocumentId = 2,
    //                                DocumentType = ServiceDocumentBE.ServiceDocumentTypeEnum.PDF,
    //                                Name = "Väldigt långt bort",
    //                                Url = "www.svd.se"
    //                            }
    //                        },
    //                    Price = new PriceBE() { Amount = 100, AmountNoVat = 94, Currency = "SEK", VATAmount = 6 },
    //                    Description = "<strong>Lorem Ipsum</strong> är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.",
    //                    AdditionalInfoText = this.GetDummyAdditionalInfoTexts(),
    //                    Properties = new ServicePropertiesBE()
    //                    {
    //                        DoorCode = ServicePropertiesBE.ShowValue.REQUIRED,
    //                        FilterCOD = true,
    //                        FilterDelivery = true,
    //                        FilterExpress = true,
    //                        FilterTrace = true,
    //                        FilterValue = true,
    //                        LetterAllowed = false,
    //                        MailNotification = ServicePropertiesBE.ShowValue.REQUIRED,
    //                        PhoneNotification = ServicePropertiesBE.ShowValue.REQUIRED,
    //                        SMSNotification = ServicePropertiesBE.ShowValue.REQUIRED,
    //                        Vat = true
    //                    },
    //                    International = true
    //                };

    //        List<AdditionalServiceBE> productThreeAdditionalServices = new List<AdditionalServiceBE>()
    //            {
    //                    new AdditionalServiceBE() { AdditionalServiceCode = "a", Name = "Personlig utlämning", CompanyCode = "pvtA", Price = new PriceBE() { Amount = 45, VATAmount = 6, AmountNoVat = 40, Currency = "SEK" } },
    //            };

    //        ProductDefinitionBE productThree = new ProductDefinitionBE()
    //        {
    //            ProductId = 3,
    //            Code = "EE",
    //            Name = "EMS - International Express",
    //            WeightIntervals = new int[] { 100, 200, 500, 1740, 4000, 6000 },
    //            MinWeight = 100,
    //            MaxWeight = 6000,
    //            MinSize = new ParcelSizeBE() { Length = 40, RollLength = 40, Width = 50 },
    //            MaxSize = new ParcelSizeBE() { Length = 100, RollLength = 100 },
    //            AdditionalServices = productThreeAdditionalServices.ToArray(),
    //            Documents = new ServiceDocumentBE[]
    //                {
    //                    new ServiceDocumentBE() {
    //                        DocumentCategory = ServiceDocumentBE.ServiceDocumentCategoryEnum.TOLL_DOCUMENT,
    //                        DocumentId = 1,
    //                        DocumentType = ServiceDocumentBE.ServiceDocumentTypeEnum.PDF,
    //                        Name = "Långt bort",
    //                        Url = "www.dn.se"
    //                    },

    //                    new ServiceDocumentBE() {
    //                        DocumentCategory = ServiceDocumentBE.ServiceDocumentCategoryEnum.TOLL_DOCUMENT,
    //                        DocumentId = 2,
    //                        DocumentType = ServiceDocumentBE.ServiceDocumentTypeEnum.PDF,
    //                        Name = "Väldigt långt bort",
    //                        Url = "www.svd.se"
    //                    }
    //                },
    //            Price = new PriceBE() { Amount = 100, AmountNoVat = 94, Currency = "SEK", VATAmount = 6 },
    //            Description = "<strong>Lorem Ipsum</strong> är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog att antal bokstäver och blandade dem för att göra ett provexemplar av en bok. Lorem ipsum har inte bara överlevt fem århundraden, utan även övergången till elektronisk typografi utan större förändringar. Det blev allmänt känt på 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.",
    //            AdditionalInfoText = this.GetDummyAdditionalInfoTexts(),
    //            Properties = new ServicePropertiesBE()
    //            {
    //                DoorCode = ServicePropertiesBE.ShowValue.HIDE,
    //                FilterCOD = true,
    //                FilterDelivery = true,
    //                FilterExpress = true,
    //                FilterTrace = true,
    //                FilterValue = true,
    //                LetterAllowed = true,
    //                MailNotification = ServicePropertiesBE.ShowValue.HIDE,
    //                PhoneNotification = ServicePropertiesBE.ShowValue.REQUIRED,
    //                SMSNotification = ServicePropertiesBE.ShowValue.HIDE,
    //                Vat = true
    //            },
    //            International = true
    //        };

    //        // Do not add items if already in list/dictionary
    //        if (ProductDefinitions.Find(o => o.ProductId == productOne.ProductId) == null)
    //        {
    //            ProductDefinitions.Add(productOne);
    //            ProductAdditionalServicesDictionary.Add(productOne.ProductId, productOneAdditionalServices);
    //        }

    //        if (ProductDefinitions.Find(o => o.ProductId == productTwo.ProductId) == null)
    //        {
    //            ProductDefinitions.Add(productTwo);
    //            ProductAdditionalServicesDictionary.Add(productTwo.ProductId, productTwoAdditionalServices);
    //        }

    //        if (ProductDefinitions.Find(o => o.ProductId == productThree.ProductId) == null)
    //        {
    //            ProductDefinitions.Add(productThree);
    //            ProductAdditionalServicesDictionary.Add(productThree.ProductId, productThreeAdditionalServices);
    //        }
    //    }

    //    #endregion Constructors

    //    #region Methods

    //    public DestinationsBE GetDestinations(HttpContext context)
    //    {
    //        DestinationsBE destinations = new DestinationsBE();
    //        destinations.Status = BaseBE.ResponseStatus.SUCCESS;

    //        destinations.Destinations = new DestinationBE[] {
    //            new DestinationBE()
    //                {
    //                    Name = "Sverige",
    //                    DestinationID = "Sverige",
    //                    ISO = "SE",
    //                    EU = true,
    //                    Europe = true,
    //                    Information = "This is some random information for Sverige",
    //                    ZipCodeDigits = 5,
    //                    ZipCodeDigitsSpecified = true
    //                },
    //            new DestinationBE()
    //                {
    //                    Name = "Norge",
    //                    DestinationID = "Norge",
    //                    ISO = "NOR",
    //                    EU = false,
    //                    Europe = true,
    //                    Information = "This is some random information for Norge",
    //                    ZipCodeDigits = 4,
    //                    ZipCodeDigitsSpecified = true
    //                },
    //            new DestinationBE()
    //                {
    //                    Name = "Italien",
    //                    DestinationID = "Italien",
    //                    ISO = "ITA",
    //                    EU = true,
    //                    Europe = true,
    //                    Information = "This is some random information for Italien",
    //                    ZipCodeDigits = 0,
    //                    ZipCodeDigitsSpecified = false
    //                },
    //            new DestinationBE()
    //                {
    //                    Name = "Gambia",
    //                    DestinationID = "Gambia",
    //                    ISO = "LBSTR",
    //                    EU = false,
    //                    Europe = false,
    //                    Information = "This is some random information for Gambia",
    //                    ZipCodeDigits = 0,
    //                    ZipCodeDigitsSpecified = false
    //                },
    //            new DestinationBE()
    //                {
    //                    Name = "General Failure",
    //                    DestinationID = "genfail",
    //                    ISO = "genfail",
    //                    EU = false,
    //                    Europe = false,
    //                    Information = "This country fails loading services with a general failure",
    //                    ZipCodeDigits = 0,
    //                    ZipCodeDigitsSpecified = false
    //                },
    //            new DestinationBE()
    //                {
    //                    Name = "Long response",
    //                    DestinationID = "longresp",
    //                    ISO = "longresp",
    //                    EU = false,
    //                    Europe = false,
    //                    Information = "This country will make the services loading take a long time",
    //                    ZipCodeDigits = 0,
    //                    ZipCodeDigitsSpecified = false
    //                }
    //        };

    //        return destinations;
    //    }

    //    public PriceAndRanksBE GetPreferredProduct(HttpContext context, long productId, SizeBE measurements, int weight)
    //    {
    //        PriceAndRanksBE result = new PriceAndRanksBE()
    //        {
    //            Status = BaseBE.ResponseStatus.SUCCESS,
    //            StatusMsg = string.Empty,
    //            ProductSuggestions = new PriceAndRankBE[]
    //            {
    //                new PriceAndRankBE()
    //                {
    //                    Price = new PriceBE()
    //                        {
    //                            Amount = 100,
    //                            AmountNoVat = 90,
    //                            Currency = "SEK",
    //                            VATAmount = 10
    //                        },
    //                    ProductId = productId,
    //                    Rank = 1
    //                }
    //            }
    //        };

    //        return result;
    //    }

    //    public ProductsBE GetProducts(HttpContext context, string countryCode)
    //    {
    //        if (countryCode == "genfail")
    //        {
    //            var errorResult = new ProductsBE()
    //            {
    //                Status = BaseBE.ResponseStatus.DB_ERROR,
    //                StatusMsg = "General failure",
    //                ResponseInfos = new ResponseInfoBE[]
    //                    {
    //                        new ResponseInfoBE() {
    //                            Code = 23,
    //                            Description = "Techinal ISP error!"
    //                        }
    //                    }
    //            };

    //            return errorResult;
    //        }

    //        if (countryCode == "longresp")
    //        {
    //            System.Threading.Thread.Sleep(2000);
    //        }

    //        var result = new ProductsBE()
    //        {
    //            Status = BaseBE.ResponseStatus.SUCCESS,
    //            StatusMsg = string.Empty,
    //            Products = new ProductDefinitionBE[] {
    //                ProductDefinitions[0],
    //                ProductDefinitions[1]
    //            }
    //        };

    //        if (countryCode != "Sverige")
    //        {
    //            var products = new List<ProductDefinitionBE>(result.Products);
    //            products.Add(ProductDefinitions[2]);
    //            result.Products = products.ToArray();
    //        }

    //        return result;
    //    }

    //    public PriceAndRanksBE GetProductSuggestions(HttpContext context, string countryCode, SizeBE measurements, int weight, ConstantsAndEnums.Characteristics[] filters)
    //    {
    //        PriceAndRanksBE result = new PriceAndRanksBE()
    //        {
    //            Status = BaseBE.ResponseStatus.SUCCESS,
    //            StatusMsg = string.Empty,
    //            ProductSuggestions = new PriceAndRankBE[]
    //                {
    //                    new PriceAndRankBE()
    //                    {
    //                        ProductId = ProductDefinitions[0].ProductId,
    //                        Price = ProductDefinitions[0].Price,
    //                        Rank = 1
    //                    },
    //                    new PriceAndRankBE()
    //                    {
    //                        ProductId = ProductDefinitions[1].ProductId,
    //                        Price = ProductDefinitions[1].Price,
    //                        Rank = 1
    //                    },
    //                    new PriceAndRankBE()
    //                    {
    //                        ProductId = ProductDefinitions[2].ProductId,
    //                        Price = ProductDefinitions[2].Price,
    //                        Rank = 2
    //                    }
    //                }
    //        };

    //        return result;
    //    }

    //    public string GetVersion()
    //    {
    //        return "Stub Version";
    //    }

    //    public string TestService()
    //    {
    //        return "Stub";
    //    }

    //    //public AccountValidationResponseBE GetAccountValidationResponse(string accountNumber, string reference, string type)
    //    //{
    //    //    AccountValidationResponseBE validAccountResponse = new AccountValidationResponseBE();
    //    //    return validAccountResponse;
    //    //}
    //    public ValidationResponseBE ValidateItem(HttpContext context,
    //        SizeBE size,
    //        int weight,
    //        List<string> additionalServiceIDs,
    //        AccountBE account,
    //        string countryISOCode,
    //        long selectedServiceID,
    //        AddressBE sender,
    //        AddressBE receiver)
    //    {
    //        // find all matching additional services for the service specified using selectedServiceID
    //        List<AdditionalServiceBE> additionalServices = new List<AdditionalServiceBE>();
    //        ProductDefinitionBE productDefinition = ProductDefinitions.Find(o => o.ProductId == selectedServiceID);
    //        foreach (string additionalServiceId in additionalServiceIDs)
    //        {
    //            foreach (AdditionalServiceBE additionalService in productDefinition.AdditionalServices)
    //            {
    //                if (additionalServiceId == additionalService.AdditionalServiceCode)
    //                {
    //                    additionalServices.Add(additionalService);
    //                    break;
    //                }
    //            }
    //        }

    //        // calculate price
    //        PriceBE price = productDefinition.Price;
    //        foreach (AdditionalServiceBE additionalService in additionalServices)
    //        {
    //            price.Amount += additionalService.Price.Amount;
    //            price.AmountNoVat += additionalService.Price.AmountNoVat;
    //        }

    //        // return mocked response item
    //        return new ValidationResponseBE()
    //        {
    //            AdditionalInfo = new List<InfoBE>(),
    //            AdditionalServices = additionalServices,
    //            BaseService = new AdditionalServiceBE()
    //            {
    //                AdditionalServiceCode = productDefinition.Code,
    //                Description = productDefinition.Description,
    //                Name = productDefinition.Name,
    //                Price = productDefinition.Price,
    //                ProductCode = productDefinition.Code
    //            },
    //            BaseServicePrice = productDefinition.Price,
    //            CompanyCode = string.Empty,
    //            DeliveryInfo = new DeliveryInfoBE(),
    //            Marketplace = "posten.se",
    //            MarketplaceBookingId = "1",
    //            Price = price,
    //            ProductCode = "PVT compound id",
    //            ReferenceNumberSerialType = 0,
    //            SapId = string.Empty,
    //            StatusMsg = string.Empty,
    //            Valid = true
    //        };
    //    }

    //    public string ZipToCity(HttpContext context, string zip)
    //    {
    //        return "Stockholm";
    //    }

    //    private static double CalculatePriceForAdditionalServices(HttpContext context, List<AdditionalServiceBE> additionalServices, double price)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    private AdditionalInfoTextBE GetDummyAdditionalInfoTexts(HttpContext context)
    //    {
    //        AdditionalInfoTextBE additionalInfoText = new AdditionalInfoTextBE()
    //        {
    //            Delivery = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.DELIVERY, LabelName = "Leverans:", Text = "Hämta hos ombud" },
    //            DeliveryTime = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.DELIVERY_TIME, LabelName = "Leveranstid:", Text = "Nästa vardag" },
    //            GoodsValue = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.GOODS_VALUE, LabelName = "Varuvärde:", Text = "" },
    //            IdRequired = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.ID_REQUIRED, LabelName = "Legitimerad utlämning", Text = "Ja" },
    //            MaxWeight = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.MAX_WEIGHT, LabelName = "Maxvikt:", Text = "30 kg" },
    //            Receiver = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.RECEIVER, LabelName = "Skicka till:", Text = "Första bästa" },
    //            Submission = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.SUBMISSION, LabelName = "Inlämning:", Text = "Ombud" },
    //            Traceable = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.TRACEABLE, LabelName = "Spårbart:", Text = "Ja" },
    //            Size = new ServiceTextBE() { LabelKey = ServiceTextBE.ServiceTextLabelEnum.SIZE, LabelName = "Storlek:", Text = "Minimimått: 14 x 9 cm. Maximimått: 60 cm, Bredd+längd+tjocklek 90 cm." }
    //        };

    //        return additionalInfoText;
    //    }

    //    #endregion Methods
    //}
}