﻿namespace Posten.Portal.Skicka.Services.Utils
{
    using System;
    using System.Collections.Specialized;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.Text;
    using System.Web;
    using System.Xml;
    using System.Xml.Serialization;

    using Microsoft.Practices.EnterpriseLibrary.Logging;

    using Posten.Portal.Platform.Common.Logging;
    using Posten.SkickaDirekt2.Resources;

    public class LogUtility
    {
        #region Methods

        /// <summary>
        /// Transaction Log ID
        /// </summary>
        /// <param name="logId"></param>
        public static void AddTransactionLogIDHeader(string logId, string correlationId)
        {
            HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                requestMessage.Headers[ConstantsAndEnums.TransactionLogID] = logId;
                requestMessage.Headers[ConstantsAndEnums.CorrelationID] = correlationId;
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;
       }


        [DllImport("advapi32.dll")]
        public static extern uint EventActivityIdControl(uint controlCode, ref Guid activityId);

        /// <summary>
        /// Exception logging utillity wrapper for calling the common posten logger. Recursively adds all inner exceptions.
        /// </summary>
        /// <param name="ex">Exception to be logged</param>
        /// <param name="declaringType">The calling class</param>
        /// <param name="callingMethodName">Name of method where exception is thrown</param>
        /// <param name="additionalOptionalMessage">Additional message (optional) to send to logger</param>
        public static string LogCriticalException(Exception ex, Type declaringType, string callingMethodName, params string[] additionalOptionalMessages)
        {
            var correlationID = GetCorrelationId();
            if (IsCriticalLoggingEnabled())
            {
                StringBuilder additonalMessageText = new StringBuilder();
                if (additionalOptionalMessages != null && additionalOptionalMessages.Length > 0)
                {
                    foreach (string additionalOptionalMessage in additionalOptionalMessages)
                    {
                        additonalMessageText.Append(additionalOptionalMessage + Environment.NewLine);
                    }
                }

                string errorText = string.Format("CorrlelationID: {1}{0}", Environment.NewLine, correlationID);
                errorText += additonalMessageText.ToString() + ex.Message + Environment.NewLine + ex.StackTrace;

                if (ex.InnerException != null)
                {
                    errorText += Environment.NewLine + Environment.NewLine + GetRecursiveErrorTrace(ex.InnerException);                    
                }

            }

            return correlationID;
        }

        /// <summary>
        /// Information logging wrapper for calling the posten common logger. Writes a serializable object to the log.
        /// </summary>
        /// <typeparam name="T">Type of the object which will be serialized to the log as xml</typeparam>
        /// <param name="objectToBeLogged">The object which will be serialized to the log as xml</param>
        /// <param name="declaringType">The calling class</param>
        /// <param name="callingMethodName">The calling method</param>
        public static string LogVerbose<T>(T objectToBeLogged, Type declaringType, string callingMethodName, params string[] additionalOptionalMessages)
        {
            var correlationID = GetCorrelationId();
            if (IsVerboseLoggingEnabled())
            {
                if (!typeof(T).IsSerializable)
                {
                    throw new Exception(string.Format("Object ({0}) is not serializable", typeof(T).FullName));
                }

                StringBuilder additonalMessageText = new StringBuilder();
                if (additionalOptionalMessages != null && additionalOptionalMessages.Length > 0)
                {
                    foreach (string additionalOptionalMessage in additionalOptionalMessages)
                    {
                        additonalMessageText.Append(additionalOptionalMessage + Environment.NewLine);
                    }
                }

                string logText = additonalMessageText.ToString();

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                StringWriter stringWriter = new StringWriter();
                XmlWriter xmlWriter = XmlWriter.Create(stringWriter);
                xmlSerializer.Serialize(xmlWriter, objectToBeLogged);

                logText += string.Format("CorrlelationID: {1}{0}{2}", Environment.NewLine, correlationID, stringWriter.ToString());

            }

            return correlationID;
        }

        /// <summary>
        /// Retrieves a log ID HTTP Header fromthe current context of an incoming ajax request
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns></returns>
        public static string RetrieveAjaxHeaderValue(HttpContext currentContext, string key)
        {
            var value = string.Empty;
            try
            {
                if (currentContext != null)
                {
                    NameValueCollection headerList = currentContext.Request.Headers;
                    value = headerList.Get("X-" + key);
                }
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logIDContainer"></param>        
        public static void RetrieveLogHeaders(HttpContext context, out string logIDContainer)
        {
            logIDContainer = RetrieveAjaxHeaderValue(context, ConstantsAndEnums.TransactionLogID);
        }

        public static string GetCorrelationId()
        {
            const uint EVENT_ACTIVITY_CTRL_GET_ID = 1;

            Guid g = Guid.Empty;
            EventActivityIdControl(EVENT_ACTIVITY_CTRL_GET_ID, ref g);

            return g.ToString();
        }

        private static string GetRecursiveErrorTrace(Exception ex)
        {
            if (ex.InnerException != null)
            {
                return ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + Environment.NewLine + GetRecursiveErrorTrace(ex.InnerException);
            }

            return ex.Message + Environment.NewLine + ex.StackTrace;
        }

        private static bool IsCriticalLoggingEnabled()
        {
            LogEntry logEntry = new LogEntry();
            logEntry.Severity = System.Diagnostics.TraceEventType.Critical;

            return true;//Logger.ShouldLog(logEntry);
        }

        private static bool IsVerboseLoggingEnabled()
        {
            LogEntry logEntry = new LogEntry();
            logEntry.Severity = System.Diagnostics.TraceEventType.Verbose;

            return true;//Logger.ShouldLog(logEntry);
        }

        #endregion Methods
    }
}