﻿namespace Posten.SkickaDirekt2.Utilities
{
    using System.Web.UI;
    using System.Web.UI.HtmlControls;

    using Microsoft.SharePoint;

    using Posten.SkickaDirekt2.Resources;

    public static class UIHelper
    {
        #region Methods

        public static void RegisterCSSInPageHeader(Page page, string cssFileName)
        {
            HtmlLink cssLink = new HtmlLink();
            cssLink.Attributes["rel"] = "stylesheet";
            cssLink.Attributes["type"] = "text/css";
            cssLink.Href = SPContext.Current.Site.Url + Constants.CSSRelativePath + cssFileName;
            page.Header.Controls.Add(cssLink);
        }

        public static long ReturnLong(string longString)
        {
            long returnValue;
            long.TryParse(longString, out returnValue);

            return returnValue;
        }

        public static double ReturnDouble(string doubleString)
        {
            double returnValue = 0;
            if (!string.IsNullOrEmpty(doubleString))
            {
                double.TryParse(doubleString.Trim().Replace(" ", "").Replace(".", ","), out returnValue);
            }

            return returnValue;
        }

        #endregion Methods
    }
}