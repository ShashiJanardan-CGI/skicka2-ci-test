﻿namespace Posten.Portal.SkickaDirekt2.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml.Linq;

    using Microsoft.SharePoint.Utilities;

    public static class XMLHelper
    {
        #region Methods

        //private static LogController logger = new LogController();
        public static XDocument ReturnXMLSource(string sourcePath)
        {
            string configFilePath = SPUtility.GetGenericSetupPath(sourcePath);
            XDocument source = XDocument.Load(configFilePath);
            return source;
        }

        /// <summary>
        /// Extension method for system.xml.linq
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static object ToNonAnonymousList<T>(this List<T> list, Type t)
        {
            try
            {
                //define system Type representing List of objects of T type:
                var genericType = typeof(List<>).MakeGenericType(t);

                //create an object instance of defined type:
                var l = Activator.CreateInstance(genericType);

                //get method Add from from the list:
                MethodInfo addMethod = l.GetType().GetMethod("Add");

                //loop through the calling list:
                foreach (T item in list)
                {
                    //convert each object of the list into T object
                    //by calling extension ToType<T>()
                    //Add this object to newly created list:
                    addMethod.Invoke(l, new object[] { item.ToType(t) });
                }

                //return List of T objects:
                return l;
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
                return null;
            }
        }

        /// <summary>
        /// Extension method to system.xml.linq
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object ToType<T>(this object obj, T type)
        {
            //create instance of T type object:
            var tmp = Activator.CreateInstance(Type.GetType(type.ToString()));

            //loop through the properties of the object you want to covert:
            foreach (PropertyInfo pi in obj.GetType().GetProperties())
            {
                try
                {
                    //get the value of property and try
                    //to assign it to the property of T type object:
                    tmp.GetType().GetProperty(pi.Name).SetValue(tmp, pi.GetValue(obj, null), null);
                }
                catch (Exception exception)
                {
                    //logger.LogError(exception);
                }
            }
            //return the T type object:
            return tmp;
        }

        #endregion Methods
    }
}