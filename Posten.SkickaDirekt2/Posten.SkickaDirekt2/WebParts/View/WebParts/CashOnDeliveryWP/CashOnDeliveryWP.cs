﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class CashOnDeliveryWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/CashOnDeliveryWP/CashOnDeliveryWPUserControl.ascx";

        private string accountNumberLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CashOnDelivery_AccountNumberLabelProperty") as string;
        private string accountTypeLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CashOnDelivery_AccountTypeLabel") as string;
        private string amountLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CashOnDelivery_AmountLabelProperty") as string;
        private string bankGiroLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CashOnDelivery_BankGiroLabelProperty") as string;
        private string headerProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CashOnDelivery_Header") as string;
        private string postalGiroLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CashOnDelivery_PostalGiroLabelProperty") as string;
        private string referenceLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CashOnDelivery_ReferenceLabelProperty") as string;
        private string referencePlaceholderProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CashOnDelivery_ReferencePlaceholderProperty") as string;

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("Account Number Label:"),
        WebDescription("Account Number field label"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AccountNumberLabelProperty
        {
            get
            {
                return accountNumberLabelProperty;
            }
            set
            {
                accountNumberLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Account Type Label:"),
        WebDescription("Account Type section label"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AccountTypeLabelProperty
        {
            get
            {
                return accountTypeLabelProperty;
            }
            set
            {
                accountTypeLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Amount Label:"),
        WebDescription("Amount field label"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AmountLabelProperty
        {
            get
            {
                return amountLabelProperty;
            }
            set
            {
                amountLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Bank Giro Label:"),
        WebDescription("Bank Giro radio button label"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string BankGiroLabelProperty
        {
            get
            {
                return bankGiroLabelProperty;
            }
            set
            {
                bankGiroLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Header:"),
        WebDescription("Cash On Delivery Header"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Postal Giro Label:"),
        WebDescription("Postal Giro radio button label"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string PostalGiroLabelProperty
        {
            get
            {
                return postalGiroLabelProperty;
            }
            set
            {
                postalGiroLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Reference Label:"),
        WebDescription("Reference field label"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ReferenceLabelProperty
        {
            get
            {
                return referenceLabelProperty;
            }
            set
            {
                referenceLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Reference Placeholder:"),
        WebDescription("Reference field placeholder"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ReferencePlaceholderProperty
        {
            get
            {
                return referencePlaceholderProperty;
            }
            set
            {
                referencePlaceholderProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                CashOnDeliveryWP webPart = (CashOnDeliveryWP)manager.WebParts[this.ID];

                webPart.HeaderProperty = this.headerProperty;
                webPart.AccountTypeLabelProperty = this.accountTypeLabelProperty;
                webPart.BankGiroLabelProperty = this.bankGiroLabelProperty;
                webPart.PostalGiroLabelProperty = this.postalGiroLabelProperty;
                webPart.accountNumberLabelProperty = this.accountNumberLabelProperty;
                webPart.referenceLabelProperty = this.referenceLabelProperty;
                webPart.referencePlaceholderProperty = this.referencePlaceholderProperty;
                webPart.amountLabelProperty = this.amountLabelProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            CashOnDeliveryWPUserControl control = (CashOnDeliveryWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}