﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class CustomsDocumentsWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/CustomsDocumentsWP/CustomsDocumentsWPUserControl.ascx";

        private string aboutLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CustomDocuments_AboutLabel") as string;
        private string headerProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CustomDocuments_Header") as string;
        private string customsDocumentsInfoLinkProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_CustomsDocuments_InfoURL") as string;

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("About Label:"),
        WebDescription("Label on About link"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AboutLabelProperty
        {
            get
            {
                return aboutLabelProperty;
            }
            set
            {
                aboutLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Header:"),
        WebDescription("Custom Documents Header"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Info Link:"),
        WebDescription("Custom Documents Info Link URL"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CustomsDocumentsInfoLinkProperty
        {
            get
            {
                return customsDocumentsInfoLinkProperty;
            }
            set
            {
                customsDocumentsInfoLinkProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                CustomsDocumentsWP webPart = (CustomsDocumentsWP)manager.WebParts[this.ID];

                webPart.HeaderProperty = this.headerProperty;
                webPart.AboutLabelProperty = this.aboutLabelProperty;
                webPart.CustomsDocumentsInfoLinkProperty = this.customsDocumentsInfoLinkProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            CustomsDocumentsWPUserControl control = (CustomsDocumentsWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}