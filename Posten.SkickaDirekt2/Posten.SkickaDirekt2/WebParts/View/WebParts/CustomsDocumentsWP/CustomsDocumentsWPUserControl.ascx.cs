﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class CustomsDocumentsWPUserControl : UserControl
    {
        #region Fields

        private string aboutLabelProperty;
        private string headerProperty;
        private string customsDocumentsInfoLinkProperty;

        #endregion Fields

        #region Properties

        public string AboutLabelProperty
        {
            get
            {
                return aboutLabelProperty;
            }
            set
            {
                aboutLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string CustomsDocumentsInfoLinkProperty
        {
            get
            {
                return customsDocumentsInfoLinkProperty;
            }
            set
            {
                customsDocumentsInfoLinkProperty = value;
            }
        }

        public CustomsDocumentsWP ParentWebPart
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "CustomsDocumentsWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((CustomsDocumentsWPDisplay)this.displayControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((CustomsDocumentsWPDisplay)this.displayControl).AboutLabelProperty = this.ParentWebPart.AboutLabelProperty;
                ((CustomsDocumentsWPDisplay)this.displayControl).CustomsDocumentsInfoLinkProperty = this.ParentWebPart.CustomsDocumentsInfoLinkProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((CustomsDocumentsWPEdit)this.editControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((CustomsDocumentsWPEdit)this.editControl).AboutLabelProperty = this.ParentWebPart.AboutLabelProperty;
                ((CustomsDocumentsWPEdit)this.editControl).CustomsDocumentsInfoLinkProperty = this.ParentWebPart.CustomsDocumentsInfoLinkProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.HeaderProperty = ((CustomsDocumentsWPEdit)this.editControl).HeaderProperty;
            this.ParentWebPart.AboutLabelProperty = ((CustomsDocumentsWPEdit)this.editControl).AboutLabelProperty;
            this.ParentWebPart.CustomsDocumentsInfoLinkProperty = ((CustomsDocumentsWPEdit)this.editControl).CustomsDocumentsInfoLinkProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}