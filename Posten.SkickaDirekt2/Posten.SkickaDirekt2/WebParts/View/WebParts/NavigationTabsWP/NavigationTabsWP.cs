﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class NavigationTabsWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/NavigationTabsWP/NavigationTabsWPUserControl.ascx";

        private string tab1TitleProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_Tab1Title") as string;
        private string tab1UrlProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_Tab1Url") as string;
        private string tab2TitleProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_Tab2Title") as string;
        private string tab2UrlProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_Tab2Url") as string;
        private string tab3TitleProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_Tab3Title") as string;
        private string tab3UrlProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_NavigationTabs_Tab3Url") as string;

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("Tab 1 title:"),
        WebDescription("Title of the first navigation tab from left"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Tab1TitleProperty
        {
            get
            {
                return tab1TitleProperty;
            }
            set
            {
                tab1TitleProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Tab 1 url:"),
        WebDescription("Url of the first navigation tab from left"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Tab1UrlProperty
        {
            get
            {
                return tab1UrlProperty;
            }
            set
            {
                tab1UrlProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Tab 2 title:"),
        WebDescription("Title of the second navigation tab from left"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Tab2TitleProperty
        {
            get
            {
                return tab2TitleProperty;
            }
            set
            {
                tab2TitleProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Tab 2 url:"),
        WebDescription("Url of the second navigation tab from left"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Tab2UrlProperty
        {
            get
            {
                return tab2UrlProperty;
            }
            set
            {
                tab2UrlProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Tab 3 title:"),
        WebDescription("Title of the third navigation tab from left"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Tab3TitleProperty
        {
            get
            {
                return tab3TitleProperty;
            }
            set
            {
                tab3TitleProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Tab 3 url:"),
        WebDescription("Url of the third navigation tab from left"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Tab3UrlProperty
        {
            get
            {
                return tab3UrlProperty;
            }
            set
            {
                tab3UrlProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                NavigationTabsWP webPart = (NavigationTabsWP)manager.WebParts[this.ID];

                webPart.Tab1TitleProperty = this.tab1TitleProperty;
                webPart.Tab2TitleProperty = this.tab2TitleProperty;
                webPart.Tab3TitleProperty = this.tab3TitleProperty;
                webPart.Tab1UrlProperty = this.tab1UrlProperty;
                webPart.Tab2UrlProperty = this.tab2UrlProperty;
                webPart.Tab3UrlProperty = this.tab3UrlProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            NavigationTabsWPUserControl control = (NavigationTabsWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}