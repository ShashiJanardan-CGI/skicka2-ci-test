﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CustomControls" TagName="ReceiverAddressWPDisplay" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/ReceiverAddressWP/ReceiverAddressWPDisplay.ascx" %>
<%@ Register TagPrefix="CustomControls" TagName="ReceiverAddressWPEdit" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/ReceiverAddressWP/ReceiverAddressWPEdit.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReceiverAddressWPUserControl.ascx.cs" Inherits="Posten.SkickaDirekt2.WebParts.UserControls.ReceiverAddressWPUserControl" %>

<div id="ReceiverAddressWPDisplayPanel" runat="server">
    <CustomControls:ReceiverAddressWPDisplay ID="displayControl" runat="server"></CustomControls:ReceiverAddressWPDisplay>
</div>

<div id="ReceiverAddressWPEditPanel" runat="server">
    <CustomControls:ReceiverAddressWPEdit ID="editControl" runat="server"></CustomControls:ReceiverAddressWPEdit>
</div>

<div id="savePanel" runat="server">
    <input id="saveButton" runat="server" type="button" value="Spara" />
</div>