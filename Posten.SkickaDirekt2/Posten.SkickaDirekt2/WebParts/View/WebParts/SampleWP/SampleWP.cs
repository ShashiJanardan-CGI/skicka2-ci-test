﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.WebParts.Presenter;
    using Posten.SkickaDirekt2.WebParts.UserControls;
    using Posten.SkickaDirekt2.WebParts.View.Interfaces;

    [ToolboxItemAttribute(false)]
    public class SampleWP : System.Web.UI.WebControls.WebParts.WebPart, ISampleWP
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/SampleWP/SampleWPUserControl.ascx";

        private SampleWPPresenter presenter;
        private string sampleProperty = "Default Value"; //To be read from resource file

        #endregion Fields

        #region Constructors

        public SampleWP()
        {
            this.presenter = new SampleWPPresenter(this);
        }

        #endregion Constructors

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("Sample Property:"),
        WebDescription("Sample Property"),
        Category("SkickaDirekt2 Properties"),
        Personalizable(PersonalizationScope.Shared)]
        public string SampleProperty
        {
            get
            {
                return sampleProperty;
            }
            set
            {
                sampleProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                SampleWP webPart = (SampleWP)manager.WebParts[this.ID];
                webPart.SampleProperty = this.sampleProperty;
                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            SampleWPUserControl control = (SampleWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}