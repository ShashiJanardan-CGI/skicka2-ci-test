﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CustomControls" TagName="SampleWPDisplay" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/SampleWP/SampleWPDisplay.ascx" %>
<%@ Register TagPrefix="CustomControls" TagName="SampleWPEdit" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/SampleWP/SampleWPEdit.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SampleWPUserControl.ascx.cs" Inherits="Posten.SkickaDirekt2.WebParts.UserControls.SampleWPUserControl" %>

<%--<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/SkickaDirekt2-Core.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>--%>

<SharePoint:ScriptLink runat="server" ID="sampleWP" Name="/Posten.SkickaDirekt2/JS/SampleWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="SampleWPDisplayPanel" runat="server">
    <CustomControls:SampleWPDisplay ID="displayControl" runat="server"></CustomControls:SampleWPDisplay>
</div>

<div id="SampleWPEditPanel" runat="server">
    <CustomControls:SampleWPEdit ID="editControl" runat="server"></CustomControls:SampleWPEdit>
</div>

<div id="savePanel" runat="server">
    <input id="saveButton" runat="server" type="button" value="Spara" />
</div>