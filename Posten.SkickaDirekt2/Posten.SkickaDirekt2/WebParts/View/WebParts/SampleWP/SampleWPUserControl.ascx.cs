﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class SampleWPUserControl : UserControl
    {
        #region Fields

        private string sampleProperty;

        #endregion Fields

        #region Properties

        public SampleWP ParentWebPart
        {
            get;
            set;
        }

        public string SampleProperty
        {
            get
            {
                return sampleProperty;
            }
            set
            {
                sampleProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            //Register webpart css
            UIHelper.RegisterCSSInPageHeader(this.Page, "SampleWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                this.displayControl.Visible = true;
                ((SampleWPDisplay)this.displayControl).SampleProperty = this.ParentWebPart.SampleProperty;

                this.saveButton.Visible = false;
                this.editControl.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                this.editControl.Visible = true;
                ((SampleWPEdit)this.editControl).SampleProperty = this.ParentWebPart.SampleProperty;

                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.SampleProperty = ((SampleWPEdit)this.editControl).SampleProperty;
            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}