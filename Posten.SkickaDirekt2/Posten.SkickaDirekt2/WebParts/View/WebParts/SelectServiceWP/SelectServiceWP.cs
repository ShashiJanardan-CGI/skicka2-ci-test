﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class SelectServiceWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/SelectServiceWP/SelectServiceWPUserControl.ascx";

        private string countryInformationProperty = Constants.CountryInformationLink;
        private string countryLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_Country") as string;
        private string countryTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_CountryTooltip") as string;
        private string diameterMaxProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_DiameterMax") as string;
        private string diameterMinProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_DiameterMin") as string;
        private string fastButtonProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_FastButton") as string;
        private string headerProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_Header") as string;
        private string heightMaxProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_HeightMax") as string;
        private string heightMinProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_HeightMin") as string;
        private string lengthMaxProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_LengthMax") as string;
        private string secureButtonProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SecureButton") as string;
        private string sendServiceLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SendService") as string;
        private string sendServiceTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SendServiceTooltip") as string;
        private string serviceFilterLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_ServicetFilter") as string;
        private string serviceFilterTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_ServicetFilterTooltip") as string;
        private string sizeErrorProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeError") as string;
        private string sizeLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_Size") as string;
        private string sizeTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeTooltip") as string;
        private string traceableButtonProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_TraceableButton") as string;
        private string weightLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_Weight") as string;
        private string weightTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_WeightTooltip") as string;
        private string widthMaxProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_WidthMax") as string;
        private string widthMinProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_WidthMin") as string;

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("Country label:"),
        WebDescription("Label on coutry drop down."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CountryLabelProperty
        {
            get
            {
                return countryLabelProperty;
            }
            set
            {
                countryLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Country tooltip:"),
        WebDescription("Tooltip for country section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CountryTooltipProperty
        {
            get
            {
                return countryTooltipProperty;
            }
            set
            {
                countryTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Country Information Link:"),
        WebDescription("Link to the page with more information about selected country."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CountryInformationProperty
        {
            get
            {
                return countryInformationProperty;
            }
            set
            {
                countryInformationProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Fast delivery button:"),
        WebDescription("Text to be shown on button for fast delivery"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string FastButtonProperty
        {
            get
            {
                return fastButtonProperty;
            }
            set
            {
                fastButtonProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Header:"),
        WebDescription("Select Service Header"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Secure delivery button:"),
        WebDescription("Text to be shown on button for secure delivery"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SecureButtonProperty
        {
            get
            {
                return secureButtonProperty;
            }
            set
            {
                secureButtonProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Select service label:"),
        WebDescription("Label on select service."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SendServiceLabelProperty
        {
            get
            {
                return sendServiceLabelProperty;
            }
            set
            {
                sendServiceLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Select service tooltip:"),
        WebDescription("Tooltip on select service."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SendServiceTooltipProperty
        {
            get
            {
                return sendServiceTooltipProperty;
            }
            set
            {
                sendServiceTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Service filter label:"),
        WebDescription("Label on service filter section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ServiceFilterLabelProperty
        {
            get
            {
                return serviceFilterLabelProperty;
            }
            set
            {
                serviceFilterLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Service filter tooltip:"),
        WebDescription("Tooltip on service filter section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ServiceFilterTooltipProperty
        {
            get
            {
                return serviceFilterTooltipProperty;
            }
            set
            {
                serviceFilterTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Size error:"),
        WebDescription("Error to show when size of C4/C5 doesn't match selected service"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SizeErrorProperty
        {
            get
            {
                return sizeErrorProperty;
            }
            set
            {
                sizeErrorProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Size label:"),
        WebDescription("Label on size section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SizeLabelProperty
        {
            get
            {
                return sizeLabelProperty;
            }
            set
            {
                sizeLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Size tooltip:"),
        WebDescription("Tooltip on size section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SizeTooltipProperty
        {
            get
            {
                return sizeTooltipProperty;
            }
            set
            {
                sizeTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Traceable delivery button:"),
        WebDescription("Text to be shown on button for traceable delivery"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string TraceableButtonProperty
        {
            get
            {
                return traceableButtonProperty;
            }
            set
            {
                traceableButtonProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Weight label:"),
        WebDescription("Label on weight section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string WeightLabelProperty
        {
            get
            {
                return weightLabelProperty;
            }
            set
            {
                weightLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Weight tooltip:"),
        WebDescription("Tooltip on weight section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string WeightTooltipProperty
        {
            get
            {
                return weightTooltipProperty;
            }
            set
            {
                weightTooltipProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                SelectServiceWP webPart = (SelectServiceWP)manager.WebParts[this.ID];

                webPart.HeaderProperty = this.headerProperty;
                webPart.CountryInformationProperty = this.countryInformationProperty;
                webPart.CountryLabelProperty = this.countryLabelProperty;
                webPart.SendServiceLabelProperty = this.sendServiceLabelProperty;
                webPart.SizeLabelProperty = this.sizeLabelProperty;
                webPart.WeightLabelProperty = this.weightLabelProperty;
                webPart.ServiceFilterLabelProperty = this.serviceFilterLabelProperty;
                webPart.CountryTooltipProperty = this.countryTooltipProperty;
                webPart.SendServiceTooltipProperty = this.sendServiceTooltipProperty;
                webPart.SizeTooltipProperty = this.sizeTooltipProperty;
                webPart.WeightTooltipProperty = this.weightTooltipProperty;
                webPart.ServiceFilterTooltipProperty = this.serviceFilterTooltipProperty;
                webPart.FastButtonProperty = this.fastButtonProperty;
                webPart.SecureButtonProperty = this.secureButtonProperty;
                webPart.TraceableButtonProperty = this.traceableButtonProperty;
                webPart.SizeErrorProperty = this.sizeErrorProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            SelectServiceWPUserControl control = (SelectServiceWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods

        #region Other

        //[WebBrowsable(true),
        //WebDisplayName("Width min value:"),
        //WebDescription("Min value for the Width field"),
        //Category(Constants.WebPartCategory),
        //Personalizable(PersonalizationScope.Shared)]
        //public string WidthMinProperty
        //{
        //    get
        //    {
        //        return widthMinProperty;
        //    }
        //    set
        //    {
        //        widthMinProperty = value;
        //    }
        //}
        //[WebBrowsable(true),
        //WebDisplayName("Height min value:"),
        //WebDescription("Min value for the Height field"),
        //Category(Constants.WebPartCategory),
        //Personalizable(PersonalizationScope.Shared)]
        //public string HeightMinProperty
        //{
        //    get
        //    {
        //        return heightMinProperty;
        //    }
        //    set
        //    {
        //        heightMinProperty = value;
        //    }
        //}
        //[WebBrowsable(true),
        //WebDisplayName("Diameter min value:"),
        //WebDescription("Min value for the Diameter field"),
        //Category(Constants.WebPartCategory),
        //Personalizable(PersonalizationScope.Shared)]
        //public string DiameterMinProperty
        //{
        //    get
        //    {
        //        return diameterMinProperty;
        //    }
        //    set
        //    {
        //        diameterMinProperty = value;
        //    }
        //}
        //[WebBrowsable(true),
        //WebDisplayName("Length max value:"),
        //WebDescription("Max value for the Length field"),
        //Category(Constants.WebPartCategory),
        //Personalizable(PersonalizationScope.Shared)]
        //public string LengthMaxProperty
        //{
        //    get
        //    {
        //        return lengthMaxProperty;
        //    }
        //    set
        //    {
        //        lengthMaxProperty = value;
        //    }
        //}
        //[WebBrowsable(true),
        //WebDisplayName("Width max value:"),
        //WebDescription("Max value for the Width field"),
        //Category(Constants.WebPartCategory),
        //Personalizable(PersonalizationScope.Shared)]
        //public string WidthMaxProperty
        //{
        //    get
        //    {
        //        return widthMaxProperty;
        //    }
        //    set
        //    {
        //        widthMaxProperty = value;
        //    }
        //}
        //[WebBrowsable(true),
        //WebDisplayName("Height max value:"),
        //WebDescription("Max value for the Height field"),
        //Category(Constants.WebPartCategory),
        //Personalizable(PersonalizationScope.Shared)]
        //public string HeightMaxProperty
        //{
        //    get
        //    {
        //        return heightMaxProperty;
        //    }
        //    set
        //    {
        //        heightMaxProperty = value;
        //    }
        //}
        //[WebBrowsable(true),
        //WebDisplayName("Diameter max value:"),
        //WebDescription("Max value for the Diameter field"),
        //Category(Constants.WebPartCategory),
        //Personalizable(PersonalizationScope.Shared)]
        //public string DiameterMaxProperty
        //{
        //    get
        //    {
        //        return diameterMaxProperty;
        //    }
        //    set
        //    {
        //        diameterMaxProperty = value;
        //    }
        //}

        #endregion Other
    }
}