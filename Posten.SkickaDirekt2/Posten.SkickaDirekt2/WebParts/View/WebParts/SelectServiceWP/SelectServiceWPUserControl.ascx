﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CustomControls" TagName="SelectServiceWPDisplay" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/SelectServiceWP/SelectServiceWPDisplay.ascx" %>
<%@ Register TagPrefix="CustomControls" TagName="SelectServiceWPEdit" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/SelectServiceWP/SelectServiceWPEdit.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectServiceWPUserControl.ascx.cs" Inherits="Posten.SkickaDirekt2.WebParts.UserControls.SelectServiceWPUserControl" %>

<%--<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/SelectServiceWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>--%>

<div id="SelectServiceWPDisplayPanel" runat="server">
    <CustomControls:SelectServiceWPDisplay ID="displayControl" runat="server"></CustomControls:SelectServiceWPDisplay>
</div>

<div id="SelectServiceWPEditPanel" runat="server">
    <CustomControls:SelectServiceWPEdit ID="editControl" runat="server"></CustomControls:SelectServiceWPEdit>
</div>

<div id="savePanel" runat="server">
    <input id="saveButton" runat="server" type="button" value="Spara" />
</div>