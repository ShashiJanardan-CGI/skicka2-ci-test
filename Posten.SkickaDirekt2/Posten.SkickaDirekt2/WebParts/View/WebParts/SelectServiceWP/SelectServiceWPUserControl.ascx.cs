﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class SelectServiceWPUserControl : UserControl
    {
        #region Fields

        private string countryInformationProperty;
        private string countryLabelProperty;
        private string countryTooltipProperty;
        private string fastButtonProperty;
        private string headerProperty;
        private string secureButtonProperty;
        private string sendServiceLabelProperty;
        private string sendServiceTooltipProperty;
        private string serviceFilterLabelProperty;
        private string serviceFilterTooltipProperty;
        private string sizeErrorProperty;
        private string sizeLabelProperty;
        private string sizeTooltipProperty;
        private string traceableButtonProperty;
        private string weightLabelProperty;
        private string weightTooltipProperty;

        #endregion Fields

        #region Properties
        public string CountryInformationProperty
        {
            get
            {
                return countryInformationProperty;
            }
            set
            {
                countryInformationProperty = value;
            }
        }

        public string CountryLabelProperty
        {
            get
            {
                return countryLabelProperty;
            }
            set
            {
                countryLabelProperty = value;
            }
        }

        public string CountryTooltipProperty
        {
            get
            {
                return countryTooltipProperty;
            }
            set
            {
                countryTooltipProperty = value;
            }
        }

        public string FastButtonProperty
        {
            get
            {
                return fastButtonProperty;
            }
            set
            {
                fastButtonProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public SelectServiceWP ParentWebPart
        {
            get;
            set;
        }

        public string SecureButtonProperty
        {
            get
            {
                return secureButtonProperty;
            }
            set
            {
                secureButtonProperty = value;
            }
        }

        public string SendServiceLabelProperty
        {
            get
            {
                return sendServiceLabelProperty;
            }
            set
            {
                sendServiceLabelProperty = value;
            }
        }

        public string SendServiceTooltipProperty
        {
            get
            {
                return sendServiceTooltipProperty;
            }
            set
            {
                sendServiceTooltipProperty = value;
            }
        }

        public string ServiceFilterLabelProperty
        {
            get
            {
                return serviceFilterLabelProperty;
            }
            set
            {
                serviceFilterLabelProperty = value;
            }
        }

        public string ServiceFilterTooltipProperty
        {
            get
            {
                return serviceFilterTooltipProperty;
            }
            set
            {
                serviceFilterTooltipProperty = value;
            }
        }

        public string SizeErrorProperty
        {
            get
            {
                return sizeErrorProperty;
            }
            set
            {
                sizeErrorProperty = value;
            }
        }

        public string SizeLabelProperty
        {
            get
            {
                return sizeLabelProperty;
            }
            set
            {
                sizeLabelProperty = value;
            }
        }

        public string SizeTooltipProperty
        {
            get
            {
                return sizeTooltipProperty;
            }
            set
            {
                sizeTooltipProperty = value;
            }
        }

        public string TraceableButtonProperty
        {
            get
            {
                return traceableButtonProperty;
            }
            set
            {
                traceableButtonProperty = value;
            }
        }

        public string WeightLabelProperty
        {
            get
            {
                return weightLabelProperty;
            }
            set
            {
                weightLabelProperty = value;
            }
        }

        public string WeightTooltipProperty
        {
            get
            {
                return weightTooltipProperty;
            }
            set
            {
                weightTooltipProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "SelectServiceWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((SelectServiceWPDisplay)this.displayControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((SelectServiceWPDisplay)this.displayControl).CountryInformationProperty = this.ParentWebPart.CountryInformationProperty;
                ((SelectServiceWPDisplay)this.displayControl).CountryLabelProperty = this.ParentWebPart.CountryLabelProperty;
                ((SelectServiceWPDisplay)this.displayControl).SendServiceLabelProperty = this.ParentWebPart.SendServiceLabelProperty;
                ((SelectServiceWPDisplay)this.displayControl).SizeLabelProperty = this.ParentWebPart.SizeLabelProperty;
                ((SelectServiceWPDisplay)this.displayControl).WeightLabelProperty = this.ParentWebPart.WeightLabelProperty;
                ((SelectServiceWPDisplay)this.displayControl).ServiceFilterLabelProperty = this.ParentWebPart.ServiceFilterLabelProperty;
                ((SelectServiceWPDisplay)this.displayControl).CountryTooltipProperty = this.ParentWebPart.CountryTooltipProperty;
                ((SelectServiceWPDisplay)this.displayControl).CountryInformationProperty = this.ParentWebPart.CountryInformationProperty;
                ((SelectServiceWPDisplay)this.displayControl).SendServiceTooltipProperty = this.ParentWebPart.SendServiceTooltipProperty;
                ((SelectServiceWPDisplay)this.displayControl).ServiceFilterTooltipProperty = this.ParentWebPart.ServiceFilterTooltipProperty;
                ((SelectServiceWPDisplay)this.displayControl).SizeTooltipProperty = this.ParentWebPart.SizeTooltipProperty;
                ((SelectServiceWPDisplay)this.displayControl).WeightTooltipProperty = this.ParentWebPart.WeightTooltipProperty;
                ((SelectServiceWPDisplay)this.displayControl).FastButtonProperty = this.ParentWebPart.FastButtonProperty;
                ((SelectServiceWPDisplay)this.displayControl).SecureButtonProperty = this.ParentWebPart.SecureButtonProperty;
                ((SelectServiceWPDisplay)this.displayControl).TraceableButtonProperty = this.ParentWebPart.TraceableButtonProperty;
                ((SelectServiceWPDisplay)this.displayControl).SizeErrorProperty = this.ParentWebPart.SizeErrorProperty;
                //((SelectServiceWPDisplay)this.displayControl).LengthMaxProperty = this.ParentWebPart.LengthMaxProperty;
                //((SelectServiceWPDisplay)this.displayControl).WidthMinProperty = this.ParentWebPart.WidthMinProperty;
                //((SelectServiceWPDisplay)this.displayControl).WidthMaxProperty = this.ParentWebPart.WidthMaxProperty;
                //((SelectServiceWPDisplay)this.displayControl).HeightMinProperty = this.ParentWebPart.HeightMinProperty;
                //((SelectServiceWPDisplay)this.displayControl).HeightMaxProperty = this.ParentWebPart.HeightMaxProperty;
                //((SelectServiceWPDisplay)this.displayControl).DiameterMinProperty = this.ParentWebPart.DiameterMinProperty;
                //((SelectServiceWPDisplay)this.displayControl).DiameterMaxProperty = this.ParentWebPart.DiameterMaxProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((SelectServiceWPEdit)this.editControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((SelectServiceWPEdit)this.editControl).CountryInformationLinkProperty = this.ParentWebPart.CountryInformationProperty;
                ((SelectServiceWPEdit)this.editControl).CountryLabelProperty = this.ParentWebPart.CountryLabelProperty;
                ((SelectServiceWPEdit)this.editControl).SendServiceLabelProperty = this.ParentWebPart.SendServiceLabelProperty;
                ((SelectServiceWPEdit)this.editControl).SizeLabelProperty = this.ParentWebPart.SizeLabelProperty;
                ((SelectServiceWPEdit)this.editControl).WeightLabelProperty = this.ParentWebPart.WeightLabelProperty;
                ((SelectServiceWPEdit)this.editControl).ServiceFilterLabelProperty = this.ParentWebPart.ServiceFilterLabelProperty;
                ((SelectServiceWPEdit)this.editControl).CountryTooltipProperty = this.ParentWebPart.CountryTooltipProperty;
                ((SelectServiceWPEdit)this.editControl).SendServiceTooltipProperty = this.ParentWebPart.SendServiceTooltipProperty;
                ((SelectServiceWPEdit)this.editControl).SizeTooltipProperty = this.ParentWebPart.SizeTooltipProperty;
                ((SelectServiceWPEdit)this.editControl).WeightTooltipProperty = this.ParentWebPart.WeightTooltipProperty;
                ((SelectServiceWPEdit)this.editControl).ServiceFilterTooltipProperty = this.ParentWebPart.ServiceFilterTooltipProperty;
                ((SelectServiceWPEdit)this.editControl).FastButtonProperty = this.ParentWebPart.FastButtonProperty;
                ((SelectServiceWPEdit)this.editControl).SecureButtonProperty = this.ParentWebPart.SecureButtonProperty;
                ((SelectServiceWPEdit)this.editControl).TraceableButtonProperty = this.ParentWebPart.TraceableButtonProperty;
                ((SelectServiceWPEdit)this.editControl).SizeErrorProperty = this.ParentWebPart.SizeErrorProperty;
                //((SelectServiceWPEdit)this.editControl).LengthMaxProperty = this.ParentWebPart.LengthMaxProperty;
                //((SelectServiceWPEdit)this.editControl).WidthMinProperty = this.ParentWebPart.WidthMinProperty;
                //((SelectServiceWPEdit)this.editControl).WidthMaxProperty = this.ParentWebPart.WidthMaxProperty;
                //((SelectServiceWPEdit)this.editControl).HeightMinProperty = this.ParentWebPart.HeightMinProperty;
                //((SelectServiceWPEdit)this.editControl).HeightMaxProperty = this.ParentWebPart.HeightMaxProperty;
                //((SelectServiceWPEdit)this.editControl).DiameterMinProperty = this.ParentWebPart.DiameterMinProperty;
                //((SelectServiceWPEdit)this.editControl).DiameterMaxProperty = this.ParentWebPart.DiameterMaxProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.HeaderProperty = ((SelectServiceWPEdit)this.editControl).HeaderProperty;
            this.ParentWebPart.CountryInformationProperty = ((SelectServiceWPEdit)this.editControl).CountryInformationLinkProperty;
            this.ParentWebPart.CountryLabelProperty = ((SelectServiceWPEdit)this.editControl).CountryLabelProperty;
            this.ParentWebPart.SendServiceLabelProperty = ((SelectServiceWPEdit)this.editControl).SendServiceLabelProperty;
            this.ParentWebPart.SizeLabelProperty = ((SelectServiceWPEdit)this.editControl).SizeLabelProperty;
            this.ParentWebPart.WeightLabelProperty = ((SelectServiceWPEdit)this.editControl).WeightLabelProperty;
            this.ParentWebPart.ServiceFilterLabelProperty = ((SelectServiceWPEdit)this.editControl).ServiceFilterLabelProperty;
            this.ParentWebPart.CountryTooltipProperty = ((SelectServiceWPEdit)this.editControl).CountryTooltipProperty;
            this.ParentWebPart.SendServiceTooltipProperty = ((SelectServiceWPEdit)this.editControl).SendServiceTooltipProperty;
            this.ParentWebPart.SizeTooltipProperty = ((SelectServiceWPEdit)this.editControl).SizeTooltipProperty;
            this.ParentWebPart.WeightTooltipProperty = ((SelectServiceWPEdit)this.editControl).WeightTooltipProperty;
            this.ParentWebPart.ServiceFilterTooltipProperty = ((SelectServiceWPEdit)this.editControl).ServiceFilterTooltipProperty;
            this.ParentWebPart.FastButtonProperty = ((SelectServiceWPEdit)this.editControl).FastButtonProperty;
            this.ParentWebPart.SecureButtonProperty = ((SelectServiceWPEdit)this.editControl).SecureButtonProperty;
            this.ParentWebPart.TraceableButtonProperty = ((SelectServiceWPEdit)this.editControl).TraceableButtonProperty;
            this.ParentWebPart.SizeErrorProperty = ((SelectServiceWPEdit)this.editControl).SizeErrorProperty;
            //this.ParentWebPart.LengthMaxProperty = ((SelectServiceWPEdit)this.editControl).LengthMaxProperty;
            //this.ParentWebPart.WidthMinProperty = ((SelectServiceWPEdit)this.editControl).WidthMinProperty;
            //this.ParentWebPart.WidthMaxProperty = ((SelectServiceWPEdit)this.editControl).WidthMaxProperty;
            //this.ParentWebPart.HeightMinProperty = ((SelectServiceWPEdit)this.editControl).HeightMinProperty;
            //this.ParentWebPart.HeightMaxProperty = ((SelectServiceWPEdit)this.editControl).HeightMaxProperty;
            //this.ParentWebPart.DiameterMinProperty = ((SelectServiceWPEdit)this.editControl).DiameterMinProperty;
            //this.ParentWebPart.DiameterMaxProperty = ((SelectServiceWPEdit)this.editControl).DiameterMaxProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods

        #region Other

        //private string widthMinProperty;
        //private string heightMinProperty;
        //private string diameterMinProperty;
        //private string lengthMaxProperty;
        //private string widthMaxProperty;
        //private string heightMaxProperty;
        //private string diameterMaxProperty;
        //public string WidthMinProperty
        //{
        //    get
        //    {
        //        return widthMinProperty;
        //    }
        //    set
        //    {
        //        widthMinProperty = value;
        //    }
        //}
        //public string HeightMinProperty
        //{
        //    get
        //    {
        //        return heightMinProperty;
        //    }
        //    set
        //    {
        //        heightMinProperty = value;
        //    }
        //}
        //public string DiameterMinProperty
        //{
        //    get
        //    {
        //        return diameterMinProperty;
        //    }
        //    set
        //    {
        //        diameterMinProperty = value;
        //    }
        //}
        //public string LengthMaxProperty
        //{
        //    get
        //    {
        //        return lengthMaxProperty;
        //    }
        //    set
        //    {
        //        lengthMaxProperty = value;
        //    }
        //}
        //public string WidthMaxProperty
        //{
        //    get
        //    {
        //        return widthMaxProperty;
        //    }
        //    set
        //    {
        //        widthMaxProperty = value;
        //    }
        //}
        //public string HeightMaxProperty
        //{
        //    get
        //    {
        //        return heightMaxProperty;
        //    }
        //    set
        //    {
        //        heightMaxProperty = value;
        //    }
        //}
        //public string DiameterMaxProperty
        //{
        //    get
        //    {
        //        return diameterMaxProperty;
        //    }
        //    set
        //    {
        //        diameterMaxProperty = value;
        //    }
        //}

        #endregion Other
    }
}