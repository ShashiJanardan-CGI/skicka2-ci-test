﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CustomControls" TagName="SenderAddressWPDisplay" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/SenderAddressWP/SenderAddressWPDisplay.ascx" %>
<%@ Register TagPrefix="CustomControls" TagName="SenderAddressWPEdit" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/SenderAddressWP/SenderAddressWPEdit.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SenderAddressWPUserControl.ascx.cs" Inherits="Posten.SkickaDirekt2.WebParts.UserControls.SenderAddressWPUserControl" %>

<div id="SenderAddressWPDisplayPanel" runat="server">
    <CustomControls:SenderAddressWPDisplay ID="displayControl" runat="server"></CustomControls:SenderAddressWPDisplay>
</div>

<div id="SenderAddressWPEditPanel" runat="server">
    <CustomControls:SenderAddressWPEdit ID="editControl" runat="server"></CustomControls:SenderAddressWPEdit>
</div>

<div id="savePanel" runat="server">
    <input id="saveButton" runat="server" type="button" value="Spara" />
</div>