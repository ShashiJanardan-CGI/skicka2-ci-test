﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class SenderAddressWPUserControl : UserControl
    {
        #region Fields

        private string address2LabelProperty;
        private string address2TooltipProperty;
        private string addressLabelProperty;
        private string cityLabelProperty;
        private string cityPlaceholderProperty;
        private string companyLabelProperty;
        private string companyNameLabelProperty;
        private string email2LabelProperty;
        private string emailLabelProperty;
        private string emailTooltipProperty;
        private string headerProperty;
        private string mobilePhoneLabelProperty;
        private string mobilePhoneTooltipProperty;
        private string nameLabelProperty;
        private string organizationNumberLabelProperty;
        private string privateLabelProperty;
        private string senderTypeLabelProperty;
        private string zipLabelProperty;

        #endregion Fields

        #region Properties

        public string Address2LabelProperty
        {
            get
            {
                return address2LabelProperty;
            }
            set
            {
                address2LabelProperty = value;
            }
        }

        public string Address2TooltipProperty
        {
            get
            {
                return address2TooltipProperty;
            }
            set
            {
                address2TooltipProperty = value;
            }
        }

        public string AddressLabelProperty
        {
            get
            {
                return addressLabelProperty;
            }
            set
            {
                addressLabelProperty = value;
            }
        }

        public string CityLabelProperty
        {
            get
            {
                return cityLabelProperty;
            }
            set
            {
                cityLabelProperty = value;
            }
        }

        public string CityPlaceholderProperty
        {
            get
            {
                return cityPlaceholderProperty;
            }
            set
            {
                cityPlaceholderProperty = value;
            }
        }

        public string CompanyLabelProperty
        {
            get
            {
                return companyLabelProperty;
            }
            set
            {
                companyLabelProperty = value;
            }
        }

        public string CompanyNameLabelProperty
        {
            get
            {
                return companyNameLabelProperty;
            }
            set
            {
                companyNameLabelProperty = value;
            }
        }

        public string Email2LabelProperty
        {
            get
            {
                return email2LabelProperty;
            }
            set
            {
                email2LabelProperty = value;
            }
        }

        public string Email2TooltipProperty
        {
            get
            {
                return emailTooltipProperty;
            }
            set
            {
                emailTooltipProperty = value;
            }
        }

        public string EmailLabelProperty
        {
            get
            {
                return emailLabelProperty;
            }
            set
            {
                emailLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string MobilePhoneLabelProperty
        {
            get
            {
                return mobilePhoneLabelProperty;
            }
            set
            {
                mobilePhoneLabelProperty = value;
            }
        }

        public string MobilePhoneTooltipProperty
        {
            get
            {
                return mobilePhoneTooltipProperty;
            }
            set
            {
                mobilePhoneTooltipProperty = value;
            }
        }

        public string NameLabelProperty
        {
            get
            {
                return nameLabelProperty;
            }
            set
            {
                nameLabelProperty = value;
            }
        }

        public string OrganizationNumberLabelProperty
        {
            get
            {
                return organizationNumberLabelProperty;
            }
            set
            {
                organizationNumberLabelProperty = value;
            }
        }

        public SenderAddressWP ParentWebPart
        {
            get;
            set;
        }

        public string PrivateLabelProperty
        {
            get
            {
                return privateLabelProperty;
            }
            set
            {
                privateLabelProperty = value;
            }
        }

        public string SenderTypeLabelProperty
        {
            get
            {
                return senderTypeLabelProperty;
            }
            set
            {
                senderTypeLabelProperty = value;
            }
        }

        public string ZipLabelProperty
        {
            get
            {
                return zipLabelProperty;
            }
            set
            {
                zipLabelProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "SenderAddressWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((SenderAddressWPDisplay)this.displayControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((SenderAddressWPDisplay)this.displayControl).CompanyNameLabelProperty = this.ParentWebPart.CompanyNameLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).OrganizationNumberLabelProperty = this.ParentWebPart.OrganizationNumberLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).NameLabelProperty = this.ParentWebPart.NameLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).AddressLabelProperty = this.ParentWebPart.AddressLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).Address2LabelProperty = this.ParentWebPart.Address2LabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).ZipLabelProperty = this.ParentWebPart.ZipLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).EmailLabelProperty = this.ParentWebPart.EmailLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).Email2LabelProperty = this.ParentWebPart.Email2LabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).SenderTypeLabelProperty = this.ParentWebPart.SenderTypeLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).PrivateLabelProperty = this.ParentWebPart.PrivateLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).CompanyLabelProperty = this.ParentWebPart.CompanyLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).CityPlaceholderProperty = this.ParentWebPart.CityPlaceholderProperty;
                ((SenderAddressWPDisplay)this.displayControl).CityLabelProperty = this.ParentWebPart.CityLabelProperty;
                ((SenderAddressWPDisplay)this.displayControl).Address2TooltipProperty = this.ParentWebPart.Address2TooltipProperty;
                ((SenderAddressWPDisplay)this.displayControl).EmailTooltipProperty = this.ParentWebPart.EmailTooltipProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((SenderAddressWPEdit)this.editControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((SenderAddressWPEdit)this.editControl).CompanyNameLabelProperty = this.ParentWebPart.CompanyNameLabelProperty;
                ((SenderAddressWPEdit)this.editControl).NameLabelProperty = this.ParentWebPart.NameLabelProperty;
                ((SenderAddressWPEdit)this.editControl).OrganizationNumberLabelProperty = this.ParentWebPart.OrganizationNumberLabelProperty;
                ((SenderAddressWPEdit)this.editControl).AddressLabelProperty = this.ParentWebPart.AddressLabelProperty;
                ((SenderAddressWPEdit)this.editControl).Address2LabelProperty = this.ParentWebPart.Address2LabelProperty;
                ((SenderAddressWPEdit)this.editControl).ZipLabelProperty = this.ParentWebPart.ZipLabelProperty;
                ((SenderAddressWPEdit)this.editControl).EmailLabelProperty = this.ParentWebPart.EmailLabelProperty;
                ((SenderAddressWPEdit)this.editControl).Email2LabelProperty = this.ParentWebPart.Email2LabelProperty;
                ((SenderAddressWPEdit)this.editControl).SenderTypeLabelProperty = this.ParentWebPart.SenderTypeLabelProperty;
                ((SenderAddressWPEdit)this.editControl).PrivateLabelProperty = this.ParentWebPart.PrivateLabelProperty;
                ((SenderAddressWPEdit)this.editControl).CompanyLabelProperty = this.ParentWebPart.CompanyLabelProperty;
                ((SenderAddressWPEdit)this.editControl).CityPlaceholderProperty = this.ParentWebPart.CityPlaceholderProperty;
                ((SenderAddressWPEdit)this.editControl).CityLabelProperty = this.ParentWebPart.CityLabelProperty;
                ((SenderAddressWPEdit)this.editControl).Address2TooltipProperty = this.ParentWebPart.Address2TooltipProperty;
                ((SenderAddressWPEdit)this.editControl).EmailTooltipProperty = this.ParentWebPart.EmailTooltipProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.HeaderProperty = ((SenderAddressWPEdit)this.editControl).HeaderProperty;
            this.ParentWebPart.CompanyNameLabelProperty = ((SenderAddressWPEdit)this.editControl).CompanyNameLabelProperty;
            this.ParentWebPart.OrganizationNumberLabelProperty = ((SenderAddressWPEdit)this.editControl).OrganizationNumberLabelProperty;
            this.ParentWebPart.NameLabelProperty = ((SenderAddressWPEdit)this.editControl).NameLabelProperty;
            this.ParentWebPart.AddressLabelProperty = ((SenderAddressWPEdit)this.editControl).AddressLabelProperty;
            this.ParentWebPart.Address2LabelProperty = ((SenderAddressWPEdit)this.editControl).Address2LabelProperty;
            this.ParentWebPart.ZipLabelProperty = ((SenderAddressWPEdit)this.editControl).ZipLabelProperty;
            this.ParentWebPart.EmailLabelProperty = ((SenderAddressWPEdit)this.editControl).EmailLabelProperty;
            this.ParentWebPart.Email2LabelProperty = ((SenderAddressWPEdit)this.editControl).Email2LabelProperty;
            this.ParentWebPart.SenderTypeLabelProperty = ((SenderAddressWPEdit)this.editControl).SenderTypeLabelProperty;
            this.ParentWebPart.PrivateLabelProperty = ((SenderAddressWPEdit)this.editControl).PrivateLabelProperty;
            this.ParentWebPart.CompanyLabelProperty = ((SenderAddressWPEdit)this.editControl).CompanyLabelProperty;
            this.ParentWebPart.CityPlaceholderProperty = ((SenderAddressWPEdit)this.editControl).CityPlaceholderProperty;
            this.ParentWebPart.CityLabelProperty = ((SenderAddressWPEdit)this.editControl).CityLabelProperty;
            this.ParentWebPart.Address2TooltipProperty = ((SenderAddressWPEdit)this.editControl).Address2TooltipProperty;
            this.ParentWebPart.EmailTooltipProperty = ((SenderAddressWPEdit)this.editControl).EmailTooltipProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}