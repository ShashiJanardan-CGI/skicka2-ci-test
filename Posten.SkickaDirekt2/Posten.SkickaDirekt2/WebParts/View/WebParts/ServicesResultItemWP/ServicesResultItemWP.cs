﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class ServicesResultItemWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/ServicesResultItemWP/ServicesResultItemWPUserControl.ascx";

        private string additionsHeaderProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ServicesResultItem_AdditionsHeader") as string;
        private string additionsTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ServicesResultItem_AdditionsTooltip") as string;
        private string buyOnlineProperty = string.Empty;//HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ServicesResultItem_BuyOnline") as string;
        private string headerProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ServicesResultItem_Header") as string;
        private string morePropertiesLinkProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ServicesResultItem_MoreProperties") as string;
        private string moreProposalsLinkProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ServicesResultItem_MoreProposals") as string;
        private string wayBillProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ServicesResultItem_WayBill") as string;
        private string vatProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_inclVAT") as string;
        private string vatFreeProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_noVAT") as string;

        #endregion Fields

        #region Properties
         [WebBrowsable(true),
        WebDisplayName("Non-EU VAT Text:"),
        WebDescription("Non-EU VAT Text:"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string VatFreeProperty
        {
            get
            {
                return vatFreeProperty;
            }
            set
            {
                vatFreeProperty = value;
            }
        }

         [WebBrowsable(true),
        WebDisplayName("EU VAT Text:"),
        WebDescription("EU VAT Text:"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string VatProperty
        {
            get
            {
                return vatProperty;
            }
            set
            {
                vatProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Additional services header:"),
        WebDescription("Header for the additional services section"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AdditionsHeaderProperty
        {
            get
            {
                return additionsHeaderProperty;
            }
            set
            {
                additionsHeaderProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Additional services tooltip:"),
        WebDescription("Tooltip for the additional services section"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AdditionsTooltipProperty
        {
            get
            {
                return additionsTooltipProperty;
            }
            set
            {
                additionsTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Buy online:"),
        WebDescription("Text for services only available online"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string BuyOnlineProperty
        {
            get
            {
                return buyOnlineProperty;
            }
            set
            {
                buyOnlineProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Header:"),
        WebDescription("Service Result Item Header"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("More properties:"),
        WebDescription("Text on link to show more properties"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string MorePropertiesLinkProperty
        {
            get
            {
                return morePropertiesLinkProperty;
            }
            set
            {
                morePropertiesLinkProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("More proposals:"),
        WebDescription("Text on link to show more proposals"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string MoreProposalsLinkProperty
        {
            get
            {
                return moreProposalsLinkProperty;
            }
            set
            {
                moreProposalsLinkProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Shipping note button:"),
        WebDescription("Text for button to choose shipping note"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string WayBillProperty
        {
            get
            {
                return wayBillProperty;
            }
            set
            {
                wayBillProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                ServicesResultItemWP webPart = (ServicesResultItemWP)manager.WebParts[this.ID];

                webPart.HeaderProperty = this.headerProperty;
                webPart.MoreProposalsLinkProperty = this.moreProposalsLinkProperty;
                webPart.BuyOnlineProperty = this.buyOnlineProperty;
                webPart.WayBillProperty = this.wayBillProperty;
                webPart.MorePropertiesLinkProperty = this.morePropertiesLinkProperty;
                webPart.AdditionsHeaderProperty = this.additionsHeaderProperty;
                webPart.AdditionsTooltipProperty = this.additionsTooltipProperty;
                webPart.VatFreeProperty = this.vatFreeProperty;
                webPart.VatProperty = this.VatProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            ServicesResultItemWPUserControl control = (ServicesResultItemWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}