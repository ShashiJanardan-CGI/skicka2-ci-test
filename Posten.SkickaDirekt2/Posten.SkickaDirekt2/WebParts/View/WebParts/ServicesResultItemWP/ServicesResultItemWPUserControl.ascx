﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CustomControls" TagName="ServicesResultItemWPDisplay" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/ServicesResultItemWP/ServicesResultItemWPDisplay.ascx" %>
<%@ Register TagPrefix="CustomControls" TagName="ServicesResultItemWPEdit" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/ServicesResultItemWP/ServicesResultItemWPEdit.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicesResultItemWPUserControl.ascx.cs" Inherits="Posten.SkickaDirekt2.WebParts.UserControls.ServicesResultItemWPUserControl" %>

<%--<SharePoint:ScriptLink runat="server" ID="ServicesResultItemJS" Name="/Posten.SkickaDirekt2/JS/ServicesResultItemWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>--%>

<div id="ServicesResultItemWPDisplayPanel" runat="server">
    <CustomControls:ServicesResultItemWPDisplay ID="displayControl" runat="server" ></CustomControls:ServicesResultItemWPDisplay>
</div>

<div id="ServicesResultItemWPEditPanel" runat="server">
    <CustomControls:ServicesResultItemWPEdit ID="editControl" runat="server"></CustomControls:ServicesResultItemWPEdit>
</div>

<div id="savePanel" runat="server">
    <input id="saveButton" runat="server" type="button" value="Spara" />
</div>