﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class ShoppingCartWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/ShoppingCartWP/ShoppingCartWPUserControl.ascx";

        private string headerProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ShoppingCart_Header") as string;
        private string cartIsEmptyProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ShoppingCart_CartIsEmpty") as string;
        private string confirmHeaderProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ShoppingCart_ConfirmHeader") as string;
        private string confirmQuestionStartProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ShoppingCart_ConfirmQuestionStart") as string;
        private string confirmQuestionEndProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_ShoppingCart_ConfirmQuestionEnd") as string;

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("Header:"),
        WebDescription("Shopping Cart Header"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Empty Cart:"),
        WebDescription("Empty Cart Message"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CartIsEmptyProperty
        {
            get
            {
                return cartIsEmptyProperty;
            }
            set
            {
                cartIsEmptyProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Confirm Header:"),
        WebDescription("Confirm Dialog Header"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ConfirmHeaderProperty
        {
            get
            {
                return confirmHeaderProperty;
            }
            set
            {
                confirmHeaderProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Confirm Question Start:"),
        WebDescription("First part of confirm question"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ConfirmQuestionStartProperty
        {
            get
            {
                return confirmQuestionStartProperty;
            }
            set
            {
                confirmQuestionStartProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Confirm Question End:"),
        WebDescription("Last part of confirm question"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ConfirmQuestionEndProperty
        {
            get
            {
                return confirmQuestionEndProperty;
            }
            set
            {
                confirmQuestionEndProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                ShoppingCartWP webPart = (ShoppingCartWP)manager.WebParts[this.ID];

                webPart.HeaderProperty = this.headerProperty;
                webPart.CartIsEmptyProperty = this.cartIsEmptyProperty;
                webPart.ConfirmHeaderProperty = this.confirmHeaderProperty;
                webPart.ConfirmQuestionStartProperty = this.confirmQuestionStartProperty;
                webPart.ConfirmQuestionEndProperty = this.confirmQuestionEndProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }
            
        protected override void CreateChildControls()
        {
            ShoppingCartWPUserControl control = (ShoppingCartWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}