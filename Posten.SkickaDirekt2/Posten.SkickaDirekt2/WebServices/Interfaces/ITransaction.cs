﻿namespace Posten.Portal.SkickaDirekt2.WebServices.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;

    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.Utils;

    [ServiceContract]
    [ServiceKnownType(typeof(ServiceOrderItem))]
    [ServiceKnownType(typeof(AddressBE))]
    [ServiceKnownType(typeof(PriceBE))]
    [ServiceKnownType(typeof(AdditionalServiceBE))]
    [ServiceKnownType(typeof(DeliveryInfoBE))]
    [ServiceKnownType(typeof(ValidationResponseBE))]
    [ServiceKnownType(typeof(AdditionalServiceBE))]
    [ServiceKnownType(typeof(ServicePropertiesBE))]
    [ServiceKnownType(typeof(SizeBE))]
    [ServiceKnownType(typeof(AccountBE))]
    [ServiceKnownType(typeof(itemResponse))]
    public interface ITransactionService
    {
        #region Methods

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/AddOrderToCart", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        void AddOrderToCart(
            string basketOrderNumber,
            ValidationResponseBE validationResponse,
            string serviceName,
            string serviceCode,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            string codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,
            string receiverDestinationId);

        [OperationContract]
        [WebGet(UriTemplate = "/FindReceivingPlace?bookingId={bookingId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string FindReceivingPlace(string bookingId);

        [OperationContract]
        [WebGet(UriTemplate = "/GetAvailableOrderLabels", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetAvailableOrderLabels();

        [OperationContract]
        [WebGet(UriTemplate = "/GetConfigurationItem", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetConfigurationItem();

        [OperationContract]
        [WebGet(UriTemplate = "/GetConsignment?orderId={orderId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetConsignment(string orderId);

        [OperationContract]
        [WebGet(UriTemplate = "/GetConsignments?bookingId={bookingId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetConsignments(string bookingId);

        [OperationContract]
        [WebGet(UriTemplate = "/GetData?value={value}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetData(int value);

        [OperationContract]
        [WebGet(UriTemplate = "/GetDestinations", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetDestinations();

        [OperationContract]
        [WebGet(UriTemplate = "/GetItemNotesPDF?bookingId={bookingId}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        IEnumerable<byte> GetItemNotesPDF(string bookingId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/GetPreferredProduct", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetPreferredProduct(long productId, string destinationID, SizeBE measurements, int weight);

        [OperationContract]
        [WebGet(UriTemplate = "/GetProducts?countryCode={countryCode}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetProducts(string countryCode);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/GetProductSuggestions", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetProductSuggestions(string countryCode, SizeBE measurements, int weight, ConstantsAndEnums.Characteristics[] filters);

        [OperationContract]
        [WebGet(UriTemplate = "/GetTransactionLogID", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetTransactionLogID();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/GetValidServicePoints", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetValidServicePoints(string bookingId, IEnumerable<string> productionPointIds);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/OrderLabels", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string OrderLabels(IEnumerable<string> prodIds, string customerName, string contactPerson, string email, string streetAddress, string zip, string city, string phone, string addressCo);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/ReturnConsignment", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ReturnConsignment(string orderNumber);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/ReturnConsignments", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ReturnConsignments(List<string> orderNumbers);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/UpdateOrderInCart", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        void UpdateOrderInCart(
            string basketOrderNumber,
            ValidationResponseBE validationResponse,
            ServicePropertiesBE serviceCombination,
            string serviceName,
            string serviceCode,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            string codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,string receiverDestinationId, string updateOrderId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/ValidateItem", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ValidateItem(SizeBE size, int weight, IEnumerable<string> additionalServiceIDs, AccountBE account, string countryCode, long selectedServiceID, AddressBE sender, AddressBE receiver);

        [OperationContract]
        [WebGet(UriTemplate = "/ZipToCity?zip={zip}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ZipToCity(string zip);

        [OperationContract]
        [WebGet(UriTemplate = "/RemoveConsignment?orderNumber={orderNumber}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool RemoveConsignment(string orderNumber);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "/AddItem", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]       
        string AddItem(SizeBE size, int weight, IEnumerable<string> additionalServiceIDs, AccountBE account, string countryCode, long selectedServiceID, AddressBE sender, AddressBE receiver,
           ServiceOrderItem serviceOrderItemSettings);

        #endregion Methods
    }
}