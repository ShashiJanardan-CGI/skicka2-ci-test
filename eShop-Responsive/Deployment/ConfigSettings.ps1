param ([string]$environment)

function Set-ConfigAppSetting
	($PathToConfig=$(throw 'Configuration file is required'),
		 [string]$Key = $(throw 'No Key Specified'), 
		 [string]$Value = $(throw 'No Value Specified'))
{	
		
		$doc = $PathToConfig
		
		$node = $doc.configuration.SelectSingleNode("appSettings/add[@key='$Key']")
		if($node)
		{
			$node.value = $Value
		}
	
}

function Set-ConfigEndPoint
	($PathToConfig=$(throw 'Configuration file is required'),
		 [string]$Name = $(throw 'No Name Specified'), 
		 [string]$Address = $(throw 'No Address Specified'))
{	
	$doc = $PathToConfig
		$node = $doc.configuration."system.serviceModel".client.SelectSingleNode("endpoint[@name='$Name']")
		if($node)
		{
			$node.address = $Address
		}
	
} 

function Get-ConfigSettings
([string]$SourcePath=$(throw 'Source file is required'),
	[string]$Environment=$(throw 'Environment Name is required'))
{
	if (Test-Path $SourcePath)
	{
		Write-Host "Loading Source XML"
		$xmldata = new-object "System.Xml.XmlDocument"		
		$xmldata.LoadXml((get-content $SourcePath))		
		
		$webConfig = $xmldata.SelectSingleNode("root/$Environment").getATTRIBUte("webconfigpath")
		
		Write-Host "Loading Web.Config XML"
		$webconfigxml = new-object "System.Xml.XmlDocument"		
		$webconfigxml.LoadXml((get-content $webConfig))		
		
		$settingxmlNode = $xmldata.SelectNodes("root/$Environment/AppSettings/setting")
		foreach($setting in $settingxmlNode)
		{	
			Write-Host "Updating appsettings: " $setting.key
			Set-ConfigAppSetting $webconfigxml $setting.key $setting.value
		}
		
		$endpointxmlNode = $xmldata.SelectNodes("root/$Environment/ServiceEndpoint/endpoint")
		foreach($sep in $endpointxmlNode)
		{			
			Write-Host "Updating service endpoint: " $sep.name
			Set-ConfigEndPoint $webconfigxml $sep.name $sep.url
		}
		
		$webconfigxml.Save($webConfig)
	}
}
$sourceXML = "source.xml"

Get-ConfigSettings $sourceXML $environment