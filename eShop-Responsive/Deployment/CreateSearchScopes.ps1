Write-Host "Loading Powershell addin ..." -nonewline
Add-PSSnapin Microsoft.SharePoint.PowerShell
Write-host "done"

#read variables from EshopManagedProperties.xml
Write-Host "Loading Source XML"
$xmldata = new-object "System.Xml.XmlDocument"		
$xmldata.LoadXml((get-content "EshopManagedProperties.xml"))		
$SearchAppnode = $xmldata.SelectSingleNode("SearchApplication")
$searchapp = Get-SPEnterpriseSearchServiceApplication $SearchAppnode.Name
$searchconfigurationnode = $xmldata.SelectSingleNode("SearchApplication/SearchConfiguration")
$ServiceContextUrl = $searchconfigurationnode.Url
$ServiceApplicationProxyGroupName = $searchconfigurationnode.ServiceApplicationProxyGroup

function CreateScope([string]$name, [string] $description, $url, $propertyrules)
{
	write-host "Creating Search scope:" $name
	#delete existing
	
	
	$newscope = Get-SPEnterpriseSearchQueryScope -Identity $name -SearchApplication $searchapp -Url $url -ErrorAction silentlycontinue
	if($newscope -ne $null)
	{
		$newscope | Remove-SPEnterpriseSearchQueryScope
	}
	
	$newscope = New-SPEnterpriseSearchQueryScope -Name $name -Description $description -SearchApplication $searchapp -DisplayInAdminUI $true -OwningSiteUrl $url
	#Create Rules
	foreach($rule in $propertyrules)
	{
		New-SPEnterpriseSearchQueryScopeRule -RuleType PropertyQuery -ManagedProperty $rule.propertyname -PropertyValue $rule.propertyfilter -FilterBehavior Require -URL $url -scope $newscope -SearchApplication $searchapp
	}
}

foreach($scope in $xmldata.SelectNodes("SearchApplication/ContentSources/ContentSource"))
{
	write-host $scope.Name
	write-host $scope.Description
	write-host $scope.url
	#write-host $scope.SelectNodes("Rules/Property")
	CreateScope $scope.Name $scope.Description $scope.url $scope.SelectNodes("Rules/Property")
}
