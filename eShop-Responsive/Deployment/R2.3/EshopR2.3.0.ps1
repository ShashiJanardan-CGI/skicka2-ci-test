param ([string]$environment)

Write-host "getting cart configuration list"
$environment = $environment;

try
{
	$web = get-spweb $environment
}
catch
{ 
	$web = $null
}

$list = $web.Lists["Cart Config"]

$itemtoadd= $list.items | where {$_.title -eq "Cart Max Items"}
if ($itemtoadd -eq $null)
{
	Write-host "Setting for maxservice does not exist. Adding it to the list:" 
	$itemtoadd=$list.items.add()
	$itemtoadd["Title"] = "Cart Max Items"
	$itemtoadd["PostenCartValue"] = 3
	$itemtoadd.update()
Write-host "Setting for maxservice added" 
}

$itemtoadd= $list.items | where {$_.title -eq "Blocked SSN"}
if ($itemtoadd -eq $null)
{
	Write-host "Setting for blocked ssn does not exist. Adding it to the list:" 
	$itemtoadd=$list.items.add()
	$itemtoadd["Title"] = "Blocked SSN"
	$itemtoadd["PostenCartValue"] = ""
	$itemtoadd.update()
Write-host "Setting for blocked ssn added" 
}


