param ([string]$environment)

Write-host "getting cart configuration list"
$environment = $environment;

try
{
	$web = get-spweb $environment
}
catch
{ 
	$web = $null
}

$list = $web.Lists["Cart Config"]

$itemtoadd= $list.items | where {$_.title -eq "SERVICESWITHDELIVERYREQUIRED"}
if ($itemtoadd -eq $null)
{
	Write-host "Setting for servicewith delivery required does not exist. Adding it to the list:" 
	$itemtoadd=$list.items.add()
	$itemtoadd["Title"] = "SERVICESWITHDELIVERYREQUIRED"
	$itemtoadd["PostenCartValue"] = ""
	$itemtoadd.update()
Write-host "Setting for servicewith delivery required added" 
}

$itemtoadd= $list.items | where {$_.title -eq "IsVatIncludedInDisplayedPrice"}
if ($itemtoadd -eq $null)
{
	Write-host "Setting for VatDisplayOption does not exist. Adding it to the list:" 
	$itemtoadd=$list.items.add()
	$itemtoadd["Title"] = "IsVatIncludedInDisplayedPrice"
	$itemtoadd["PostenCartValue"] = "Yes"
	$itemtoadd.update()
Write-host "Setting for VatDisplayOption added" 
}