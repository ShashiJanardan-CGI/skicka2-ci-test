param
(
	[Parameter(Position=0)]
	[string] $InputFile = "Deployment-Full.xml"
)

if ((Get-PSSnapin | where {$_.Name -eq "Microsoft.SharePoint.PowerShell"}) -eq $null) {
	Write-Host "Loading SharePoint Powershell snapin..." -NoNewline
	Add-PSSnapin Microsoft.SharePoint.PowerShell;
	Write-Host "DONE."
}

[xml]$xml = (Get-Content $InputFile)
$environment = $xml.Deployment.Webapplication.Url


try
{
	$web = get-spweb $environment
}
catch
{ 
	$web = $null
}

$list = $web.Lists["Cart Config"]

$itemtoadd= $list.items | where {$_.title -eq "SERVICESWITHDELIVERYREQUIRED"}
if ($itemtoadd -eq $null)
{
	Write-host "Setting for servicewith delivery required does not exist. Adding it to the list:" 
	$itemtoadd=$list.items.add()
	$itemtoadd["Title"] = "SERVICESWITHDELIVERYREQUIRED"
	$itemtoadd["PostenCartValue"] = ""
	$itemtoadd.update()
Write-host "Setting for servicewith delivery required added" 
}