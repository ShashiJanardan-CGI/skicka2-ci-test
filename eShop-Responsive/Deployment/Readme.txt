Prepare farm
-------------

1. Recommendation: Install Swedish, Norwegian, Danish and Finish language pack
2. Recommendation: Patch SP2010 with Feb2011 CU
3. Run SharePoint 2010 Products Configuration Wizard to finalize installation of language pack and the CU Patch 
4. Reboot server
5. Run AutoSPInstallerLaunch.bat to install necessary service applications (not fully tested, 27/4)
6. Reboot server


Uninstall solutions
--------------------

1. Run Uninstall-All.cmd to remove the web application at http://<computername>/ and all packages from farm


Deploy solutions
-----------------

1. Run build.bat to build the packages
2. Run Deployment.cmd to create a Web Application at http://<computername>/ and to deploy packages, create sites and activate features

Note: After this you should be able to redeploy the solutions from Visual Studio and mannuly activate features

There are different deployment xml-files depending on what type of enviorement should be deployed.
- Developement: Deployment-Dev.xml, will deploy minimal enviorement with common componentes and basic site collection
- Samint: Deployment-Full.xml, will deploy Cart, eShop and My Pages in different language variations


Re-deploy solutions
--------------------

1. Uninstall solutions
2. Deploy solutions

