param ([string]$url,[string]$title)


Add-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue 


function UpdateWebTitle([string]$url,[string]$title)
{

    try
    {          
        
		$spWeb = Get-SPWeb $url
		$resource = $spWeb.TitleResource;
		$culturetoupdate = new-object System.Globalization.CultureInfo([int]1053)
		
		$resource.SetValueForUICulture($culturetoupdate, $title)
		
		Write-Host ("original title:" + $spweb.Title )
		
		$spweb.Update()
        $spWeb.Dispose()                   
        Write-Host -BackgroundColor DarkGreen -ForegroundColor White ("Web Title Updated: " + $url);      
    }    
    catch      
    {            
        Write-Host -BackgroundColor DarkRed -ForegroundColor White ("Failed:" + $url + ". Error details : " + $_)      
    }
}
UpdateWebTitle $url  $title