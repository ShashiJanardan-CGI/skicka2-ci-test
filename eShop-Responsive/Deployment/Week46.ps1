param ([string]$environment)

Write-host "Checking if subsite for the new shopping cart has been created"
$environment = $environment + "/2.0";
Write-host "Checking if subsite for the new shopping cart has been created"
try
{
	$web = get-spweb $environment
}
catch
{ 
	$web = null
}

if ($web -eq $null)
{
	Write-host "Subsite does not exist. Creating:" + $environment;
	New-SpWeb -url $environment -Name "Cart 2.0" -Language "1053" -Template "CMSPUBLISHING#0"
	Write-host "Setting Masterpage"
	$web = Get-SpWeb $environment
	$masterpageurl = $web.ServerRelativeUrl.TrimEnd('/') + "/_catalogs/masterpage/Posten.master"
	$web.MasterUrl  = $masterpageurl
	$web.update()
	Write-host "Masterpage has been updated"
}
Write-host "Subsite Creation for 2.0 has been completed."