
param ($url, $filename, $propertyname, $propertyvalue, $typename)


Add-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue 


function UpdateWebPartProperty([string]$url,[string]$filename,[string]$propertyname,[string]$propertyvalue,[string] $TypeName)
{

    try
    {          
        
		$spWeb = Get-SPWeb $url
		$page = $spweb.GetFile($filename)
		
		Write-Host ("file status:" + $page.Checkouttype)
		if ($page.Checkouttype -ne [Microsoft.SharePoint.SPFile+SPCheckOutType]::None)
		{
			$page.undocheckout()
		}
		
		$page.checkout()
		$wpm = $page.GetLimitedWebpartManager([System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
		foreach($webpart in $wpm.webparts)
		{
			if([string]::Compare($TypeName,$webpart.webbrowsableobject.GetType().FullName, $true) -eq 0)
			{
				Write-Host ("Original propertyvalue:" + $webpart.$propertyname)
				
				$webpart.$propertyname =$propertyvalue
				$wpm.savechanges($webpart)
				Write-Host ("Updated propertyvalue:" + $propertyvalue)
			}
		}
		$page.checkin("")
		$page.publish("")
		
		
        Write-Host -BackgroundColor DarkGreen -ForegroundColor White ("Web Part Property Updated: " + $url);      
    }    
    catch      
    {            
        Write-Host -BackgroundColor DarkRed -ForegroundColor White ("Failed:" + $url + ". Error details : " + $_)      
    }
}

UpdateWebPartProperty $url $filename $propertyname $propertyvalue $typename