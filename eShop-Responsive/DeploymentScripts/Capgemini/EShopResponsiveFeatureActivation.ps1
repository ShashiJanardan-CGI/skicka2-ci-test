.\LoadConfig.ps1

$SiteUrl = $settings["siteUrl"]
$SiteAdministrator = $settings["siteAdministrator"]
$SiteTitle = $settings["siteTitle"]
$SiteLanguage = $settings["language"]
$WebApplicationUrl = $settings["webApplicationUrl"]

#------ EShop-Responsive ------#
echo "----------------------------------------------------------------------------"
echo "Activating Feature: Posten Cart WebConfig 2.0"
echo "----------------------------------------------------------------------------"
#Enable-SPFeature 9ee5711b-8b8b-4559-b373-9732a815a12b -url $SiteUrl 

echo "----------------------------------------------------------------------------"
echo "Activating Feature: Posten Cart 2.0 Common"
echo "----------------------------------------------------------------------------"
Enable-SPFeature 62435a25-17e3-4e36-b284-778ff013dfc4 -url $SiteUrl 

echo "----------------------------------------------------------------------------"
echo "Activating Feature: Posten Cart 2.0 Infrastructure"
echo "----------------------------------------------------------------------------"
Enable-SPFeature d8179d6f-2f9c-48fd-b35a-4631cadcd5b6 -url $SiteUrl 