# Get Start Time
$startDTM = (Get-Date)

.\LoadConfig.ps1
. .\Functions.ps1

$webAppName = $settings["webApplicationName"]
$WebApplicationUrl = $settings["webApplicationUrl"]

Write-Host "*******Cleaning Skicka2 Packages from the farm *****************"
Reset-IISOnServers
RetractSolution "Posten.Portal.Cart.Presentation.wsp"
RetractSolution "Posten.Portal.Cart.Core.wsp"

Reset-IISOnServers $True


Write-Host "*******Deploying Skicka2 Packages on the farm *****************"
Reset-IISOnServers
DeploySolution "Posten.Portal.Cart.Core.wsp" $webAppName
DeploySolution "Posten.Portal.Cart.Presentation.wsp" $webAppName
Reset-IISOnServers $True

echo "----------------------------------------------------------------------------"
echo "Activating Feature: Posten Cart 2.0 Web.config Settings"
echo "----------------------------------------------------------------------------"
Enable-SPFeature 9ee5711b-8b8b-4559-b373-9732a815a12b -url $WebApplicationUrl


Write-Host "*******Setting up Skicka2 Site*****************"
.\EShopResponsiveFeatureActivation.ps1

