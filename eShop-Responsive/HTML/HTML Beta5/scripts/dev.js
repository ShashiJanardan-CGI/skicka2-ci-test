
/**************************************************************************************
***** Development scripts                                                        ******
**************************************************************************************/

function loadModule(source, target, after) {
	$.ajax({
		url: "modules/" + source + ".html",
		success: function(data) {
			if(after)
				$(target).after(data);
			else
				$(target).append(data);
		},
		async: false
	});
}

function loadTemplate(tpl) {
	$('#Body .mid > .box').detach();
	
	if(tpl == "4_1") {
		$('#Body .mid').append('<div class="box colspan-4" id="box-1"></div>');
		$('#Body .mid').append('<div class="box" id="box-2"></div>');
	}
	if(tpl == "1_4") {
		$('#Body .mid').append('<div class="box" id="box-1"></div>');
		$('#Body .mid').append('<div class="box colspan-4" id="box-2"></div>');
	}
	else if(tpl == "1_3_1") {
		$('#Body .mid').append('<div class="box" id="box-1"></div>');
		$('#Body .mid').append('<div class="box colspan-3" id="box-2"></div>');
		$('#Body .mid').append('<div class="box" id="box-3"></div>');
	}
	else if(tpl == "5") {
		$('#Body .mid').append('<div class="box colspan-5" id="box-1"></div>');
	}
	else if(tpl == "all") {
		// Columns: 4-1
		$('#Body .mid').append('<div class="box colspan-5"></div>');
		$('#Body .mid > .box:last-child').append('<div id="box-1" class="box colspan-4"></div>');
		$('#Body .mid > .box:last-child').append('<div id="box-2" class="box"></div>');
		
		// Columns: 3-2
		$('#Body .mid').append('<div class="box colspan-5"></div>');
		$('#Body .mid > .box:last-child').append('<div id="box-3" class="box colspan-3"></div>');
		$('#Body .mid > .box:last-child').append('<div id="box-4" class="box colspan-2"></div>');
		
		
		// Columns: 5
		$('#Body .mid').append('<div id="box-5" class="box colspan-5"></div>');
	}
}
	
function getRequest() {
	var vars = [], hash;
	var hashes = (window.location.href.indexOf('?') < 0) ? "" : window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	
	for(var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1].replace("#", "");
	}
	
	return vars;
}

jQuery(function($) {
	// Parse request vars to js vars.
	var url = getRequest();
	
	var loc  = url["loc"];
	var tpl  = url["tpl"];
	var menu = url["menu"];
	var page = url["page"];
	var mod  = url["mod"];
	var webshop = true;
	
	// Select site theme, .com if omitted.
	if(loc == "se")			$('head').append('<link href="styles/se.css"  rel="stylesheet" type="text/css" />');
	else if(loc == "dk")	$('head').append('<link href="styles/dk.css"  rel="stylesheet" type="text/css" />');
	else if(loc == "no")	$('head').append('<link href="styles/no.css"  rel="stylesheet" type="text/css" />');
	else if(loc == "fi")	$('head').append('<link href="styles/fi.css"  rel="stylesheet" type="text/css" />');
	else					$('head').append('<link href="styles/com.css" rel="stylesheet" type="text/css" />');
	
	// Always load the Global Menu and the Footer
	loadModule("global_menu", ".b1");
	loadModule("footer", "#Body", true);
	
	// Select which box template to use, none if omitted.
	loadTemplate(tpl)
	
	if(mod == "info_alert") {
		loadTemplate("4_1")
		loadModule("info_alert", "#box-1");
		
		return
	}
	
	// Select a page setup to use.
	switch (page) {
		case "start" :
			loadTemplate("4_1");
			loadModule("main_campaign_module", "#box-1");
			loadModule("center_console_menu", "#box-1");
			loadModule("bundle_offer", "#box-1");
			loadModule("quick_finder", "#box-2");
			loadModule("iframe", "#box-1");
			break;
		case "article" :
			menu = 'true';
			loadTemplate("1_3_1");
			loadModule("sub_menu", "#box-1");
			loadModule("main_campaign_module", "#box-2");
			loadModule("article", "#box-2");
			loadModule("related_content_files", "#box-2");
			loadModule("web_assistant", "#box-3");
			loadModule("quick_finder", "#box-3");
			break;
		case "product" :
			menu = 'true';
			loadTemplate("1_3_1");
			loadModule("sub_menu", "#box-1");
			loadModule("main_campaign_module", "#box-2");
			loadModule("article", "#box-2");
			loadModule("related_content_files", "#box-2");
			loadModule("related_products", "#box-2");
			loadModule("web_assistant", "#box-3");
			loadModule("quick_finder", "#box-3");
			break;
		case "sitemap" :
			menu = 'true';
			loadTemplate("1_4");
			loadModule("site_map_page", "#box-2");
			break;
		case "403" :
			menu = 'true';
			loadTemplate("1_3_1");
			loadModule("sub_menu", "#box-1");
			loadModule("403", "#box-2");
			loadModule("web_assistant", "#box-3");
			loadModule("contact_support", "#box-3");
			break;
		case "webshop" :
			webshop = 'true';
			menu = 'true';
			loadTemplate("1_3_1");
			loadModule("webshop_products", "#box-2");
			break;
		default :
			
			webshop = 'true';
			menu = 'true';
			loadTemplate("all");
			
			// Columns: 4-1
			loadModule("main_campaign_module", "#box-1");
			loadModule("center_console_menu", "#box-1");
			loadModule("bundle_offer", "#box-1");
			loadModule("info_alert", "#box-1");
			loadModule("campaign_e", "#box-1");
			loadModule("iframe", "#box-1");;
			loadModule("site_map_page", "#box-1");
			loadModule("sub_menu", "#box-2");
			loadModule("quick_finder", "#box-2");
			loadModule("faq_module", "#box-2");
			loadModule("search_result_docs", "#box-2");
			loadModule("campaign_a", "#box-2");
			
			// Columns: 3-2
			loadModule("related_content_links", "#box-3");
			loadModule("related_content_mixed", "#box-3");
			loadModule("related_content_files", "#box-3");
			loadModule("related_products", "#box-3");
			loadModule("iframe", "#box-3");
			loadModule("bundle_offer", "#box-3");
			loadModule("call_to_action", "#box-3");
			loadModule("faq_module", "#box-3");
			loadModule("all_services_page", "#box-3");
			loadModule("faq_page", "#box-3");
			loadModule("search_result_page", "#box-3")
			loadModule("campaign_b", "#box-3");
			
			loadModule("iframe", "#box-4");
			loadModule("faq_module", "#box-4");
			
			// Columns: 5
			loadModule("iframe", "#box-5");
			
			/*
			loadTemplate("1_3_1");
			
			loadModule("sub_menu", "#box-1");
			loadModule("main_campaign_module", "#box-1");
			loadModule("related_content", "#box-1");
			loadModule("article", "#box-1");
			loadModule("quick_finder", "#box-3");
			*/
	
		
	}
	
/*	if(mod != "") {
		loadTemplate("4_1");
		loadModule(mod, "#box-1");
	}*/
	
	// Display Main Menu if menu is set to true, false if omitted.
	if(menu == 'true')
		loadModule("main_menu", "#Header", true);
	
	if(webshop == 'true')
		loadModule("webshop_global", ".b2", false);
	
	// Load JavaScript
	$.getScript("scripts/core.js")
});