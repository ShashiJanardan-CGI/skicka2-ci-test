﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class AccountInfo : IAccountInfo
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string EmailAddress { get; set; }

        public string MobileNumber { get; set; }

        public Address Address { get; set; }

        public string CareOfAddress { get; set; }

        public string CompanyCode { get; set; }

        public string PersonalCodeNumber { get; set; }

        public string SmsNumber { get; set; }

        public string State { get; set; }

        public string StateChangeReason { get; set; }

        public string StateChangeTime { get; set; }

        public string StateChangedBy { get; set; }

        public string Type { get; set; }

        public string Password { get; set; }
    }
}
