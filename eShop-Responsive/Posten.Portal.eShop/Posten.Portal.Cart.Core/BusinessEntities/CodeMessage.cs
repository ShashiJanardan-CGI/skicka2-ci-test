﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class CodeMessage : ICodeMessage
    {
        public int Code { get; set; }

        public string Message{ get; set;}
    }
}
