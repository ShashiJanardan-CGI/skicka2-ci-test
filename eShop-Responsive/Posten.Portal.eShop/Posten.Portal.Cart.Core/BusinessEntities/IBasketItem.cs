﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IBasketItem
    {
        string BasketId { get; set; }

        BasketStatus BasketStatus { get; set; }

        IBillingAccountInfo BillingAccountInfo { get; set; }

        string BookingId { get; set; }

        IDeliveryAccountInfo DeliveryAccountInfo { get; set; }

        ICollection<IOrderSummaryItem> OrderSummaryItems { get; set; }

        DateTime PaymentDate { get; set; }

        string PaymentMethod { get; set; }

        Uri ReceiptUrl { get; set; }

        decimal Total { get; set; }

        decimal TotalExclTax { get; set; }

        string TotalExclTaxString { get; set; }

        string TotalString { get; set; }

        decimal TotalTax { get; set; }

        string TotalTaxString { get; set; }

        long TransactionId { get; set; }

        string TransactionState { get; set; }
    }
}
