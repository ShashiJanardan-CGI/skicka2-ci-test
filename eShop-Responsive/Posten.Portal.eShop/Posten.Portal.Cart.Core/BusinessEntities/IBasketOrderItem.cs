﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IBasketOrderItem
    {
        string BasketId { get; set; }

        string OrderId { get; set; }       
    }
}
