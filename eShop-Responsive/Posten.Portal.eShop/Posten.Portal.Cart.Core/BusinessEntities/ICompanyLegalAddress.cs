﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface ICompanyLegalAddress : ILegalAddress
    {
        string CompanyName { get; set; }
    }
}
