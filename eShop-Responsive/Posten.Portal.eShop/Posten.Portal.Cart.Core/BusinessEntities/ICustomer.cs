﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface ICustomer
    {
        string IpAddress { get; set;}
        string SocialSecurityNumber { get; set; }
        string Phone { get; set; }
        string Email { get; set;}
        string ReferenceText { get; set; }
    }
}
