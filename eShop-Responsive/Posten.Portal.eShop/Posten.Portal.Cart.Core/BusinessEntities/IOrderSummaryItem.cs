﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IOrderSummaryItem
    {
        DateTime Created { get; set; }

        string CurrencyCode { get; set; }

        string Details { get; set; }

        string FreeTextField1 { get; set; }

        string FreeTextField2 { get; set; }

        decimal LinePrice { get; set; }

        string LinePriceString { get; set; }

        string LineTaxString { get; set; }

        decimal ListPrice { get; set; }

        string Name { get; set; }

        Uri OrderItemImageUrl { get; set; }
        
        OrderItemType OrderItemType { get; set; }

        string OrderNumber { get; set; }

        int Quantity { get; set; }

        decimal Vat { get; set; }
    }
}
