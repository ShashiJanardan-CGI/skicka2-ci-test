﻿using System;
using System.Collections.Generic;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IPaymentMethodResponse
    {
        long id { get; set; }
        string name { get; set; }
        string description { get; set; }
        PaymentMethodType type { get; }
    }
}
