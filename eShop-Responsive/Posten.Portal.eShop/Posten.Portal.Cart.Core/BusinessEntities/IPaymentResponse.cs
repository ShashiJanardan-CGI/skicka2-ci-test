﻿using System;
using System.Collections.Generic;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IPaymentResponse
    {
        Uri PaymentUrl { get; set; }

        long TransactionId { get; set; }

        ICollection<IDeliveryIdMapping> DeliveryIdMappings { get; set; }
    }
}
