﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IPaymentResult
    {
        long TransactionId { get; set; }

        string Receipt { get; set; }

        Uri ReceiptUrl { get; set; }

        long TransactionState { get; set; }
    }
}
