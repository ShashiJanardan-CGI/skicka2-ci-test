﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IProductOrderItem
    {
        string ArticleId { get; set; }

        string ArticleName { get; set; }

        decimal ArticlePrice { get; set; }

        string BasketId { get; set; }

        string CurrencyCode { get; set; }

        string OrderNumber { get; set; }

        int Quantity { get; set; }

        decimal Vat { get; set; }

        decimal VatPercentage { get; set; }

        decimal ArticlePricewithVAT { get; }

        decimal Total { get; }

        decimal TotalExcludingVat { get; }

        string Taxcode { get; set; }
    }
}
