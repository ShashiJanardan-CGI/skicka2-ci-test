﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Posten.Portal.Cart.Services.Proxy.ServiceBasketService;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IServiceOrderItem
    {
        string ApplicationId { get; set; }

        string ApplicationName { get; set; }

        string BasketId { get; set; }

        Uri ConfirmationUrl { get; set; }

        string CurrencyCode { get; set; }

        Uri CustomApplicationLink { get; set; }

        string CustomApplicationLinkText { get; set; }

        string Details { get; set; }

        string EditUrl { get; set; }

        string FreeTextField1 { get; set; }

        string FreeTextField2 { get; set; }

        string Name { get; set; }

        string OrderNumber { get; set; }

        Uri ProduceUrl { get; set; }

        Uri ServiceImageUrl { get; set; }

        ICollection<IServiceOrderLineItem> ServiceOrderLines { get; set; }

        decimal SubTotal { get; set; }

        decimal Vat { get; set; }

        decimal Total { get;}
    }
}
