﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IServiceOrderLineItem
    {
        string ArticleId { get; set; }

        string ArticleName { get; set; }

        decimal ArticlePrice { get; set; }

        string CompanyCode { get; set; }

        string CustomerId { get; set; }

        string FootnoteText { get; set; }

        int OrderLineNumber { get; set; }

        decimal Quantity { get; set; }

        string TaxCountry { get; set; }

        string VatCode { get; set; }

        decimal Total { get; set; }

        decimal Vat { get; set; }

        decimal VatPercentage { get; set; }
    }
}
