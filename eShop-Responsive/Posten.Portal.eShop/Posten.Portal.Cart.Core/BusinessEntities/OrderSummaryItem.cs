﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class OrderSummaryItem : IOrderSummaryItem
    {
        public string OrderNumber { get; set; }

        public DateTime Created { get; set; }

        public string CurrencyCode { get; set; }

        public string Details { get; set; }

        public string FreeTextField1 { get; set; }

        public string FreeTextField2 { get; set; }

        public decimal LinePrice { get; set; }

        public string LinePriceString { get; set; }

        public string LineTaxString { get; set; }

        public decimal ListPrice { get; set; }

        public string Name { get; set; }

        public Uri OrderItemImageUrl { get; set; }

        public OrderItemType OrderItemType { get; set; }

        public int Quantity { get; set; }

        public decimal Vat { get; set; }

        
    }
}
