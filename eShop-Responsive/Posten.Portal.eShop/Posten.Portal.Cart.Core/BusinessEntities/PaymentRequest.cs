﻿using System;
using System.Collections.Generic;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class PaymentRequest : PaymentRequestBase, IPaymentRequest
    {
        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public string CustomerZip { get; set; }

        public string CustomerCity { get; set; }

        public string CustomerAtt { get; set; }

        public string CustomerCountry { get; set; }

        public string CustomerVatNo { get; set; }

        public string UserId { get; set; }

        public string Currency { get; set; }

        public string Language { get; set; }

        public string TransferText { get; set; }
    }
}
