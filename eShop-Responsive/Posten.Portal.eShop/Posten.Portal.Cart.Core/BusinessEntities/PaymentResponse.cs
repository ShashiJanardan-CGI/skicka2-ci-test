﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class PaymentResponse : IPaymentResponse
    {
        public Uri PaymentUrl { get; set; }

        public long TransactionId { get; set; }

        public ICollection<IDeliveryIdMapping> DeliveryIdMappings { get; set; }
    }
}
