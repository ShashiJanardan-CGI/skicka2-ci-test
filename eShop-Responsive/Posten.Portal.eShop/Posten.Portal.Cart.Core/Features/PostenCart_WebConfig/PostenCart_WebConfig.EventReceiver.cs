using System;
using System.Runtime.InteropServices;
using Microsoft.SharePoint;
using Posten.Portal.Platform.Common.Utilities;

namespace Posten.Portal.Cart.EventReceivers
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("1357b66f-0138-4240-9145-0d0e3f77690f")]
    public class WebConfigEventReceiver : WebConfigEngine
    {
        protected override string OwnerModif
        {
            get { return "Posten.Portal.Cart"; }
        }

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            AddAppSetting("CartZipCodeServiceUrl", "http://pgm.posten.se/pgm/resources/notis/postalcode/");
            AddAppSetting("PaymentServiceClientId", "22");
            AddAppSetting("CookieDomain", "posten.se");
//            AddAppSetting("CartWebSealUrl", "http://skicka.posten.se/service");
//            AddAppSetting("CartWebSealUrl", "http://skickadirekt2.posten.se/service");
            AddAppSetting("CartWebSealUrl", "http://sit1-skicka2.ipostnord.com/service");
            AddAppSetting("TemporaryActivationUrl", "http://pseteam5.utm.posten.se/beppo/createaccount.aspx?step=3");
            AddAppSetting("CartCheckoutOverrideUrl", "http://skicka.posten.se/service/2.0/_layouts/Posten/Cart/2.0/Checkout.aspx");
            AddAppSetting("CartInvoiceCustomerId", "20605928");
            AddAppSetting("CartInvoiceVatCode", "U1");
            AddAppSetting("CartConfirmationUrls", "Skicka,http://skicka.posten.se/Skicka/skicka.confirmation?BookingId={0};Ebrev Webb,https://ebrevwebbkontant.posten.se/ebrev/EbrevFiles/OrderConfirmationPage.html");

            AddUnityMapping(typeof(Services.IBasketService), typeof(Services.BasketServiceAgent), typeof(Services.BasketServiceStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.BasketService.IBasketService), "http://localhost:8777/BasketService.svc", DEFAULT_WCF_BUFFER_SIZE, true);

            // This is done in the web.config configuration scripts
            //AddUnityMapping(typeof(Services.IItemNotesServiceBean), typeof(Services.ItemNotesServiceBeanAgent), typeof(Services.ItemNotesServiceBeanStub), ServiceMapping.Agent);
            //AddWebServiceMapping(typeof(Services.Proxy.ItemNotesServiceBean.ItemNotesServiceBean), "http://localhost:8777/ItemNotesServiceBean.svc", DEFAULT_WCF_BUFFER_SIZE, true);

            AddUnityMapping(typeof(Services.IProductBasketService), typeof(Services.ProductBasketAgent), typeof(Services.ProductBasketStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.ProductBasketService.IProductBasket), "http://localhost:8777/ProductBasket.svc", DEFAULT_WCF_BUFFER_SIZE, true);

            AddUnityMapping(typeof(Services.IServiceBasketService), typeof(Services.ServiceBasketAgent), typeof(Services.ServiceBasketStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.ServiceBasketService.IServiceBasket), "http://localhost:8777/ServiceBasket.svc", DEFAULT_WCF_BUFFER_SIZE, true);

            AddUnityMapping(typeof(Services.IPaymentService), typeof(Services.PaymentServiceAgent), typeof(Services.PaymentServiceStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.PaymentService.PaymentServiceEndpoint), "http://10.127.4.49:8080/PaymentsolutionService/PaymentService", DEFAULT_WCF_BUFFER_SIZE, true);
            this.AddXml("configuration/system.serviceModel/bindings/basicHttpBinding/binding[@name='PaymentServiceEndpointBinding'][@maxBufferSize='10240000'][@maxReceivedMessageSize='10240000'][@sendTimeout='00:03:00'][@receiveTimeout='00:03:00']", @"<readerQuotas maxStringContentLength=""1024000""/>");

            AddUnityMapping(typeof(Services.IPostalCodeService), typeof(Services.PostalCodeServiceAgent), typeof(Services.PostalCodeServiceAgent), ServiceMapping.Agent);
                        
            AddUnityMapping(typeof(Services.IAccountService), typeof(Services.AccountServiceAgent), typeof(Services.AccountServiceStub), ServiceMapping.Stub);
            AddUnityMapping(typeof(Services.ICartSettingsService), typeof(Services.CartSettingsAgent), typeof(Services.CartSettingsStub), ServiceMapping.Agent);

            AddUnityMapping(typeof(Services.IUserManagementService), typeof(Services.UserManagementServiceAgent),typeof(Services.UserManagementStub), ServiceMapping.Agent);
            AddWebServiceMapping(typeof(Services.Proxy.UserManagementService.UserManagementServiceEndpoint), "http://10.127.4.49:8080/UserManagementService/UserManagementService", DEFAULT_WCF_BUFFER_SIZE, true);

            this.AddXml("configuration/loggingConfiguration/listeners", @"<add name=""CartVerboseTrace"" 	fileName=""c:\portallogs\cart-trace.txt"" type=""Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.RollingFlatFileTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"" listenerDataType=""Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.RollingFlatFileTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging, Version=5.0.414.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"" filter=""All"" formatter=""Text Formatter"" header=""----------------------------------------"" footer=""----------------------------------------"" rollFileExistsBehavior=""Increment"" rollInterval=""Day"" rollSizeKB=""0"" timeStampPattern=""yyyy-MM-dd"" traceOutputOptions=""Timestamp"" />");
            this.AddXml("configuration/loggingConfiguration/categorySources", @"<add name=""CartBusiness"" switchValue=""Verbose""><listeners><add name=""CartVerboseTrace"" /></listeners></add>");

            AddExpressionBuilder("CartCode", typeof(Presentation.CartCodeExpressionBuilder));
            
            // Apply changes
            ApplyChangesInWebConfig(properties);
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            // Roll-back web.config changes
            RemoveChangesInWebConfig(properties);
        }

        public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        {
        }
    }
}
