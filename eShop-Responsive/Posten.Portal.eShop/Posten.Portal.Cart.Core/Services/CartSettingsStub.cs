﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class CartSettingsStub : ICartSettingsService
    {
        #region Methods

        public bool AddBlockedSSn(string ssn)
        {
            throw new NotImplementedException();
        }

        public BusinessEntities.CartSettingItems GetItems()
        {
            throw new NotImplementedException();
        }

        public bool RemoveBlockedSSn(string ssn)
        {
            throw new NotImplementedException();
        }

        public bool RemoveBlockService(string id)
        {
            throw new NotImplementedException();
        }

        public bool SaveBlockService(string id, string name)
        {
            throw new NotImplementedException();
        }

        public bool SaveItems(BusinessEntities.CartSettingItems items)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}
