﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Posten.Portal.Cart.BusinessEntities;

    public interface IAccountService
    {
        #region Methods

        /// <summary>
        /// This will Create a user account in the Access project and also on the Commerce Server.
        /// </summary>
        /// <param name="account">the information of the account to be created.</param>
        /// <returns>a boolean which says if the operation is a success or not.</returns>
        bool CreateAccount(IAccountInfo account);

        /// <summary>
        /// This will return true if there is an account which already exists with this email.
        /// </summary>
        /// <param name="email">the email address to check if there is an existing account</param>
        /// <returns>a boolean which says if an account with supplied email exists</returns>
        bool DoesEmailAccountExist(string email);

        /// <summary>
        /// returns the Account information if the email and password matches.
        /// </summary>
        /// <param name="email">the email of the account</param>
        /// <param name="password">the password of the account</param>
        /// <returns>the account information which belongs to the email. If nothing is returned, the password is incorrect</returns>
        IAccountInfo GetAccountInfo(string email, string password);

        /// <summary>
        /// returns the Account information of the current user
        /// </summary>
        /// <param name="username">name of the current user</param>
        /// <returns></returns>
        IAccountInfo GetCurrentUser(string username);

        #endregion Methods
    }
}