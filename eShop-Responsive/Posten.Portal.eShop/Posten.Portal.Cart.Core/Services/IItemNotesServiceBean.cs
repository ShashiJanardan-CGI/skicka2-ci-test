﻿namespace Posten.Portal.Cart.Services
{
    using Posten.Portal.Cart.Services.Proxy.ItemNotesServiceBean;

    public interface IItemNotesServiceBean
    {
        #region Methods

        void InformPaymentComplete(string[] orderNumbers, status status);

        void InformPaymentPending(string buyerEMail, string[] orderNumbers, string transactionId);

        #endregion Methods
    }
}