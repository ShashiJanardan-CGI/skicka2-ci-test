﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;

    public interface IProductBasketService
    {
        #region Methods

        bool AddProductOrder(ref string basketId, IProductOrderItem productOrderItem, ref string orderNumber);

        IProductOrderItem GetProductOrderItem(string basketId, string productOrderNumber);

        ICollection<IProductOrderItem> GetProductOrders(string basketId);

        ICollection<IProductOrderItem> GetProductOrdersByBookingId(string bookingId);

        ICollection<IProductOrderItem> GetProductOrdersByTransactionId(long transactionId);

        bool RemoveProductOrder(string basketId, string productOrderNumber);

        string UpdateProductOrder(string basketId, IProductOrderItem productOrderItem);

        #endregion Methods
    }
}