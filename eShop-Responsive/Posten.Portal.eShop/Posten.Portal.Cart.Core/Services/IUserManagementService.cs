﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Posten.Portal.Cart.BusinessEntities;

    public interface IUserManagementService
    {
        #region Methods

        /// <summary>
        /// Verify email address.
        /// </summary>
        /// <param name="email"></param>
        bool checkEmailAddress(string email);

        /// <summary>
        /// Get the account information of the email address.
        /// </summary>
        /// <param name="email">email address of the current user</param>
        /// <returns></returns>
        IAccountInfo GetAccount(string email);

        /// <summary>
        /// Save the account information.
        /// </summary>
        /// <param name="account"></param>
        ICollection<ICodeMessage> SaveAccount(IAccountInfo account);

        /// <summary>
        /// Updates user information.
        /// </summary>
        /// <param name="account">account information of user</param>
        /// <returns></returns>
        ICollection<ICodeMessage> UpdateAccount(IAccountInfo account);

        #endregion Methods
    }
}