﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;
    using System.Collections.ObjectModel;

    public class PaymentServiceStub : IPaymentService
    {
        #region Methods

        public IDirectPaymentRequest DirectPayment(IDirectPaymentRequest paymentrequest)
        {
            throw new NotImplementedException();
        }

        public IPaymentResult FinalisePayment(long transactionId, bool receiptAsPdf, bool receiptAsUrl, Uri responseUrl)
        {
            throw new NotImplementedException();
        }

        public ICollection<ILegalAddress> GetLegalAddresses(string Ip, string SSNo)
        {
            Collection<ILegalAddress> addresses = new Collection<ILegalAddress>();

            LegalAddress address1 = null;
            address1.City = "Solna";
            address1.Country = "SE";
            address1.FirstName = "Greger";
            address1.LastName = "Hermelin";
            address1.PostalCode = "16930";
            address1.StreetAddress = "Virebergsvägen 14";

            LegalAddress address2 = null;
            address2.City = "Stockholm";
            address2.Country = "SE";
            address2.FirstName = "Greger";
            address2.LastName = "Hermelin";
            address2.PostalCode = "12345";
            address2.StreetAddress = "Hemvärnsgatan 117";

            addresses.Add(address1);
            addresses.Add(address2);
            return addresses;
        }

        public ICollection<IPaymentMethodResponse> GetPaymentMethods()
        {
            throw new NotImplementedException();
        }

        public IPaymentResult GetPaymentResult(long transactionId, bool receiptAsPdf, bool receiptAsUrl)
        {
            throw new NotImplementedException();
        }

        public IPaymentResponse InitialisePayment(IPaymentRequest paymentRequest, Uri responseUrl, Uri cancelUrl)
        {
            throw new NotImplementedException();
        }

        IPaymentResponse IPaymentService.DirectPayment(IDirectPaymentRequest paymentrequest)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}