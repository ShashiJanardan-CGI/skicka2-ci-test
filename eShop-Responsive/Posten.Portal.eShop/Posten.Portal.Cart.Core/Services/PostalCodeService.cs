﻿namespace Posten.Portal.Cart.Services
{
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.ServiceModel.Web;

    using Posten.Portal.Cart.BusinessEntities;

    [ServiceContract]
    public interface IPostalCodeService
    {
        #region Methods

        [OperationContract]
        [WebGet(UriTemplate = "{country}/{postalCode}", ResponseFormat = WebMessageFormat.Json, BodyStyle= WebMessageBodyStyle.Wrapped)]
        PostalCodeInfo GetPostalCode(string country, string postalCode);

        #endregion Methods
    }

    public class PostalCodeServiceAgent : IPostalCodeService
    {
        #region Fields

        private IPostalCodeService serviceClientInstance;

        #endregion Fields

        #region Properties

        private IPostalCodeService ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    var factory = new WebChannelFactory<IPostalCodeService>();
                    factory.Endpoint.Address  = new EndpointAddress("http://pgm.posten.se/pgm/resources/notis/postalcode/");
                    factory.Endpoint.Behaviors.Add(new WebHttpBehavior());
                    this.serviceClientInstance = factory.CreateChannel();
                }

                return this.serviceClientInstance;
            }
        }

        #endregion Properties

        #region Methods

        public PostalCodeInfo GetPostalCode(string country, string postalCode)
        {
            PostalCodeInfo result = new PostalCodeInfo();
            //var currentUser = Utilities.Utils.ImpersonateUser();
            result = this.ServiceClient.GetPostalCode(country, postalCode);
            //Utilities.Utils.RevertImpersonation(currentUser);
            return result;
        }

        #endregion Methods
    }
}