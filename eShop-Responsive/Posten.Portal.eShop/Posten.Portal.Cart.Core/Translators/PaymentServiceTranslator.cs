﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Posten.Portal.Cart.BusinessEntities;

namespace Posten.Portal.Cart.Translators
{
    using Posten.Portal.Cart.Utilities;

    public static class PaymentServiceTranslator
    {
        internal static Services.Proxy.PaymentService.PaymentRequest ToProxyEntity(this IPaymentRequest item)
        {
            return new Services.Proxy.PaymentService.PaymentRequest
            {
                clientId = item.ClientId,
                paymentMethod = item.PaymentMethod,
                totalAmount = item.TotalAmount,
                totalAmountNoTax = item.TotalAmountNoTax,
                totalTax = item.TotalTax,
                captureNow = item.CaptureNow,
                articles = item.Articles.ToProxyEntityArray(),
                customerName = item.CustomerName,
                customerAddress = item.CustomerAddress,
                customerZip = item.CustomerZip,
                customerCity = item.CustomerCity,
                customerAtt = item.CustomerAtt,
                customerCountry = item.CustomerCountry,
                customerVatNo = item.CustomerVatNo,
                userId = item.UserId,
                currency = item.Currency,
                language = item.Language,
                transferText = item.TransferText
            };
        }

        internal static Services.Proxy.PaymentService.Article ToProxyEntity(this IPaymentArticle item)
        {
            return new Services.Proxy.PaymentService.Article
            {
                articleId = item.ArticleId,
                articleName = item.ArticleName,
                quantity = item.Quantity,
                articlePrice = item.ArticlePrice,
                totalPrice = item.TotalPrice,
                totalPriceVatIncluded = item.TotalPriceVatIncluded,
                tax = item.Tax,
                orderNo = item.OrderNumber,
                companyCode = item.CompanyCode,
                taxRate = item.TaxRate,
                taxCountry = item.TaxCountry,
                customerId = item.CustomerId,
                quantityDecimals = item.QuantityDecimals,
                articleDescription = item.ArticleDescription,
                freetext = item.Freetext,
                footnoteText = item.FootnoteText,
                referenceText = item.ReferenceText,
                taxCode = item.TaxCode
            };
        }

        internal static IPaymentResponse ToBusinessEntity(this Services.Proxy.PaymentService.InitialisePaymentResponse item)
        {
            return new PaymentResponse
            {
                PaymentUrl = item.paymentUrl.ToUri(),
                TransactionId = item.paymentTransactionId,
                DeliveryIdMappings = item.deliveryIdMappings.ToBusinessEntityCollection()
            };
        }

        internal static IDeliveryIdMapping ToBusinessEntity(this Services.Proxy.PaymentService.deliveryIdMapping item)
        {
            return new DeliveryIdMapping
            {
                DeliveryId = item.deliveryId,
                RowNumber = item.rowNumber
            };
        }

        internal static IPaymentResult ToBusinessEntity(this Services.Proxy.PaymentService.FinalisePaymentResponse item)
        {
            return new PaymentResult
            {
                TransactionId = item.paymentTransactionId,
                Receipt = item.receipt,
                ReceiptUrl = item.receiptUrl.ToUri(),
                TransactionState = item.transactionState
            };
        }

        internal static IPaymentResult ToBusinessEntity(this Services.Proxy.PaymentService.GetPaymentResultResponse item)
        {
            return new PaymentResult
            {
                TransactionId = 0,
                Receipt = item.receipt,
                ReceiptUrl = item.receiptUrl.ToUri(),
                TransactionState = item.transactionState
            };
        }


        internal static IPaymentMethodResponse ToBusinessEntity(this Services.Proxy.PaymentService.PaymentMethod item)
        {
            return new PaymentMethodResult
            {
                description = item.description,
                id = item.id,
                name = item.name
            };
        }

        internal static ICollection<IPaymentMethodResponse> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.PaymentService.PaymentMethod> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ILegalAddress ToBusinessEntity(this Services.Proxy.PaymentService.address item)
        {
            if (item.companyName == null)
            {
                return new PrivateLegalAddress
                {
                    City = item.city,
                    Country = item.country,
                    FirstName = item.firstName,
                    LastName = item.lastName,
                    StreetAddress = item.street,
                    PostalCode = item.zipCode
                };
            }
            
            return new CompanyLegalAddress 
            {
                City = item.city,
                CompanyName = item.companyName,
                Country = item.country,
                StreetAddress = item.street,
                PostalCode = item.zipCode
            };
        }

        internal static ICollection<ILegalAddress> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.PaymentService.address> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }


        internal static Services.Proxy.PaymentService.Article[] ToProxyEntityArray(this IEnumerable<IPaymentArticle> items)
        {
            return items.Select(item => item.ToProxyEntity()).ToArray();
        }

        internal static ICollection<IDeliveryIdMapping> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.PaymentService.deliveryIdMapping> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static IEnumerable<IPaymentArticle> ToPaymentArticle(this IServiceOrderItem item)
        {

            //Article Price and Total has no VAT. We will need to add them.
            var results = from lineitem in item.ServiceOrderLines
                          select (IPaymentArticle)new PaymentArticle()
                            {
                                ArticleDescription = "",
                                ArticleId = lineitem.ArticleId,
                                ArticleName = lineitem.ArticleName,
                                ArticlePrice = Convert.ToInt64((lineitem.ArticlePrice) * 100m),
                                CompanyCode = lineitem.CompanyCode,
                                CustomerId = lineitem.CustomerId,
                                FootnoteText = lineitem.FootnoteText,
                                Freetext = null,
                                OrderNumber = item.OrderNumber,
                                Quantity = Convert.ToInt64(lineitem.Quantity),
                                QuantityDecimals = 0,
                                ReferenceText = null,
                                Tax = Convert.ToInt64(lineitem.Vat * 100m),
                                TaxCountry = lineitem.TaxCountry,
                                TaxRate = Convert.ToInt32(lineitem.VatPercentage* 10000),
                                TotalPrice = Convert.ToInt64(Math.Round((lineitem.Total) * 100m)),
                                TotalPriceVatIncluded = Convert.ToInt64(lineitem.Vat * 100m) + Convert.ToInt64(Math.Round((lineitem.Total) * 100m)), 
                                TaxCode = lineitem.VatCode
                                

                            };
            return results.ToList<IPaymentArticle>();
        }

        internal static IPaymentArticle ToPaymentArticle(this IProductOrderItem item)
        {
            var returnvalue = new PaymentArticle
            {
                ArticleDescription = item.ArticleName,
                ArticleId = item.ArticleId,
                ArticleName = item.ArticleName,
                ArticlePrice = Convert.ToInt64(Math.Round(item.ArticlePrice * 100m)),
                CompanyCode = "F092",
                CustomerId = "11052892",
                FootnoteText = string.Empty,
                Freetext = string.Empty,
                OrderNumber = item.OrderNumber,
                Quantity = item.Quantity,
                QuantityDecimals = 0,
                ReferenceText = string.Empty,
                Tax = Convert.ToInt64(Math.Round(item.Vat * 100m)),
                TaxCountry = "SE",
                TaxRate = Convert.ToInt32(Math.Round(item.VatPercentage * 10000m)),
                TotalPrice = Convert.ToInt64(Math.Round(item.TotalExcludingVat * 100m)),
                TotalPriceVatIncluded = Convert.ToInt64(Math.Round(item.Total * 100m))
                
            };
            
            //set total with tax
            returnvalue.TotalPriceVatIncluded = returnvalue.TotalPrice + returnvalue.Tax;

            return returnvalue;
        }

        public static IPaymentRequest ToPaymentRequest(this BasketContext basketContext, PaymentMethodType paymentMethod)
        {
            var basket = basketContext.BasketItem;

            PaymentRequest paymentRequest = new PaymentRequest();

            paymentRequest.Articles = basketContext.Articles;
            paymentRequest.CaptureNow = true;
            paymentRequest.ClientId = Utils.ToLong(System.Configuration.ConfigurationManager.AppSettings["PaymentServiceClientId"]);
            paymentRequest.Currency = "SEK"; // from articles
            paymentRequest.CustomerAddress = basket.BillingAccountInfo.AddressLine;
            paymentRequest.CustomerAtt = basket.BillingAccountInfo.Name + " " + basket.BillingAccountInfo.LastName;
            paymentRequest.CustomerName = basket.BillingAccountInfo.Organization;
            paymentRequest.CustomerCity = basket.BillingAccountInfo.City;
            paymentRequest.CustomerCountry = basket.BillingAccountInfo.CountryCode;
            paymentRequest.CustomerVatNo = string.Empty; // from BillingInfo
            paymentRequest.CustomerZip = basket.BillingAccountInfo.PostalCode;
            paymentRequest.Language = "sv"; // from UICulture
            paymentRequest.PaymentMethod = (long)paymentMethod;
            paymentRequest.TotalAmount = Convert.ToInt64(Math.Round(basket.Total * 100m));
            paymentRequest.TotalAmountNoTax = Convert.ToInt64(Math.Round(basket.TotalExclTax * 100m));
            paymentRequest.TotalTax = paymentRequest.TotalAmount - paymentRequest.TotalAmountNoTax;
            paymentRequest.TransferText = string.Empty;
            paymentRequest.UserId = "10";

            return paymentRequest;
        }

        internal static Services.Proxy.PaymentService.directPaymentRequest ToProxyEntity(this IDirectPaymentRequest directpaymentrequest)
        {
            return new Services.Proxy.PaymentService.directPaymentRequest() { 
                articles= directpaymentrequest.Articles.ToProxyEntityArray(),
                billingAddress = directpaymentrequest.LegalAddress.ToProxyEntity(),
                legalAddress= directpaymentrequest.LegalAddress.ToProxyEntity(),
                captureNow=true,
                clientId=directpaymentrequest.ClientId,
                currency=directpaymentrequest.Currency,
                customer= directpaymentrequest.Customer.ToProxyEntity(directpaymentrequest), 
                language= directpaymentrequest.Language, 
                paymentMethod= directpaymentrequest.PaymentMethod, 
                totalAmount = directpaymentrequest.TotalAmount, 
                totalAmountNoTax= directpaymentrequest.TotalAmountNoTax , 
                totalTax= directpaymentrequest.TotalTax 
            };
        }

        internal static Services.Proxy.PaymentService.customer ToProxyEntity(this ICustomer customer, IDirectPaymentRequest request)
        {

            if (request.LegalAddress.Type  == LegalAddressType.Company)
            {
                return new Services.Proxy.PaymentService.customer()
                {
                    ip = customer.IpAddress,
                    email = customer.Email,
                    socialSecNo = customer.SocialSecurityNumber,
                    phone = customer.Phone,
                    reference=( (ICompanyLegalAddress) request.LegalAddress).CompanyName
                };
            }
            return new Services.Proxy.PaymentService.customer()
            {
                ip = customer.IpAddress,
                email = customer.Email,
                socialSecNo = customer.SocialSecurityNumber,
                phone = customer.Phone,
            };
        }


        internal static Services.Proxy.PaymentService.address ToProxyEntity(this ILegalAddress address)
        {

            if (address.Type == LegalAddressType.Private)
            { 
                var privateaddress = (IPrivateLegalAddress) address;

                return new Services.Proxy.PaymentService.address()
                {
                    city = address.City,
                    country = address.Country,
                    zipCode = address.PostalCode,
                    street = address.StreetAddress,
                    lastName = privateaddress.LastName,
                    firstName = privateaddress.FirstName
                };
            }
            else
            {
                var companyaddress = (ICompanyLegalAddress)address;

                return new Services.Proxy.PaymentService.address()
                {
                    city = address.City,
                    country = address.Country,
                    zipCode = address.PostalCode,
                    street = address.StreetAddress,
                    companyName = companyaddress.CompanyName,
                    firstName=address.FirstName,
                    lastName= address.LastName
                };
            }
        }



        public static IDirectPaymentRequest ToDirectPaymentRequest(this BasketContext basketContext, PaymentMethodType paymentMethod)
        {
            var basket = basketContext.BasketItem;

            DirectPaymentRequest paymentRequest = new DirectPaymentRequest();



            paymentRequest.Articles = basketContext.Articles;
            paymentRequest.CaptureNow = true;
            paymentRequest.ClientId = Utils.ToLong(System.Configuration.ConfigurationManager.AppSettings["PaymentServiceClientId"]);
            paymentRequest.Currency = "SEK"; // from articles
            paymentRequest.Language = "sv"; // from UICulture
            paymentRequest.PaymentMethod = (long)paymentMethod;
            paymentRequest.TotalAmount = Convert.ToInt64(Math.Round(basket.Total * 100m));
            paymentRequest.TotalAmountNoTax = Convert.ToInt64(Math.Round(basket.TotalExclTax * 100m));
            paymentRequest.TotalTax = paymentRequest.TotalAmount - paymentRequest.TotalAmountNoTax;

            

            return paymentRequest;
        }

        internal static IPaymentResponse ToDirectPaymentResponse(this Services.Proxy.PaymentService.DirectPaymentResponse response)
        {
            return new PaymentResponse
            {
                PaymentUrl = response.paymentUrl.ToUri(),
                TransactionId = response.paymentTransactionId,
                DeliveryIdMappings = response.deliveryIdMappings.ToBusinessEntityCollection()
            };
        }

        internal static Services.Proxy.PaymentService.address ToProxyEntity(this IAddress address)
        {
            return new Services.Proxy.PaymentService.address()
            {
                city = address.City,
                country = address.Country,
                street = address.StreetAddress,
                zipCode = address.PostalCode
            };
        }
    }
}
