﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Posten.Portal.Cart.Utilities;

namespace Posten.Portal.Cart.Translators
{
    public static class ScriptBasketTranslator
    {
        public static IScriptBasket ToScriptBasket(this BasketContext context)
        {
            var basket = new ScriptBasket();
            basket.BasketId = context.BasketId;
            basket.ValueIncludingVat = context.BasketItem.Total.ToString("C");
            basket.ValueExcludingVat = context.BasketItem.TotalExclTax.ToString("C");

            var orderItems = new List<IScriptBasketOrderItem>();

            foreach (var item in context.ProductOrderItems)
            {
                orderItems.Add(new ScriptBasketProductOrderItem
                                   {
                                       OrderNumber = item.OrderNumber,
                                       Name = item.ArticleName,
                                       Description = item.ArticleId,
                                       Price = item.Total.ToString("C"),
                                       Quantity = item.Quantity
                                   });
            }

            basket.OrderItems = orderItems.ToArray();
            return basket;
        }
    }
}
