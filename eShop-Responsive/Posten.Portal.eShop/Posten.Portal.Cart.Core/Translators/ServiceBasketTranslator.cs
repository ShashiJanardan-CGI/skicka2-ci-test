﻿using System.Collections.Generic;
using System.Linq;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Utilities;
using ProxyServiceOrderItem = Posten.Portal.Cart.Services.Proxy.ServiceBasketService.ServiceOrderItem;
using ProxyServiceOrderLineItem = Posten.Portal.Cart.Services.Proxy.ServiceBasketService.ServiceOrderLineItem;

namespace Posten.Portal.Cart.Translators
{
    internal static class ServiceBasketTranslator
    {
        internal static ProxyServiceOrderLineItem ToProxyEntity(this IServiceOrderLineItem item)
        {
            return new ProxyServiceOrderLineItem
            {
                ArticleId = Utils.Trim(item.ArticleId),
                ArticleName = Utils.Trim(item.ArticleName), 
                ArticlePrice = item.ArticlePrice, 
                CompanyCode = item.CompanyCode, 
                CustomerId = item.CustomerId,
                FootnoteText = Utils.Trim(item.FootnoteText), 
                OrderLineNo = item.OrderLineNumber, 
                Quantity = item.Quantity, 
                TaxCountry = item.TaxCountry, 
                Total = item.Total, 
                Vat = item.Vat, 
                VatPercentage = item.VatPercentage,
                MICode=item.VatCode
            };
        }

        internal static IServiceOrderLineItem ToBusinessEntity(this ProxyServiceOrderLineItem item)
        {
            return new ServiceOrderLineItem
            {
                ArticleId = Utils.Trim(item.ArticleId), 
                ArticleName = Utils.Trim(item.ArticleName), 
                ArticlePrice = item.ArticlePrice, 
                CompanyCode = item.CompanyCode, 
                CustomerId = item.CustomerId, 
                FootnoteText = Utils.Trim(item.FootnoteText), 
                OrderLineNumber = item.OrderLineNo, 
                Quantity = item.Quantity, 
                TaxCountry = item.TaxCountry, 
                Total = item.Total, 
                Vat = item.Vat, 
                VatPercentage = item.VatPercentage,
                VatCode=item.MICode
            };
        }

        internal static ProxyServiceOrderItem ToProxyEntity(this IServiceOrderItem item)
        {
            return new ProxyServiceOrderItem
            {
                ApplicationId = Utils.Trim(item.ApplicationId), 
                ApplicationName = Utils.Trim(item.ApplicationName), 
                BasketId = item.BasketId, 
                ConfirmationUrl = item.ConfirmationUrl.ToSafeString(),
                CurrencyCode = item.CurrencyCode,
                CustomApplicationLink = item.CustomApplicationLink.ToSafeString(), 
                CustomApplicationLinkText = Utils.Trim(item.CustomApplicationLinkText), 
                Details = Utils.Trim(item.Details), 
                EditUrl = item.EditUrl, 
                FreeTextField1 = Utils.Trim(item.FreeTextField1), 
                FreeTextField2 = Utils.Trim(item.FreeTextField2), 
                Name = Utils.Trim(item.Name), 
                OrderNo = item.OrderNumber,
                ProduceUrl = item.ProduceUrl.ToSafeString() , 
                ServiceImageUrl = item.ServiceImageUrl.ToSafeString(), 
                ServiceOrderLines = item.ServiceOrderLines.ToProxyEntityArray(), 
                SubTotal = item.SubTotal, 
                Vat = item.Vat
            };
        }

        internal static IServiceOrderItem ToBusinessEntity(this ProxyServiceOrderItem item)
        {
            return new ServiceOrderItem
            {
                ApplicationId = Utils.Trim(item.ApplicationId), 
                ApplicationName = Utils.Trim(item.ApplicationName), 
                BasketId = item.BasketId, 
                ConfirmationUrl = item.ConfirmationUrl.ToUri(), 
                CurrencyCode = item.CurrencyCode, 
                CustomApplicationLink = item.CustomApplicationLink.ToUri(), 
                CustomApplicationLinkText = Utils.Trim(item.CustomApplicationLinkText), 
                Details = Utils.Trim(item.Details), 
                EditUrl = item.EditUrl, 
                FreeTextField1 = Utils.Trim(item.FreeTextField1), 
                FreeTextField2 = Utils.Trim(item.FreeTextField2), 
                Name = Utils.Trim(item.Name), 
                OrderNumber = item.OrderNo, 
                ProduceUrl = item.ProduceUrl.ToUri(), 
                ServiceImageUrl = item.ServiceImageUrl.ToUri(), 
                ServiceOrderLines = item.ServiceOrderLines.ToBusinessEntityCollection(), 
                SubTotal = item.SubTotal, 
                Vat = item.Vat
            };
        }

        internal static ProxyServiceOrderLineItem[] ToProxyEntityArray(this IEnumerable<IServiceOrderLineItem> items)
        {
            return items.Select(item => item.ToProxyEntity()).ToArray();
        }

        internal static ICollection<IServiceOrderLineItem> ToBusinessEntityCollection(this IEnumerable<ProxyServiceOrderLineItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }

        internal static ProxyServiceOrderItem[] ToProxyEntityArray(this IEnumerable<IServiceOrderItem> items)
        {
            return items.Select(item => item.ToProxyEntity()).ToArray();
        }

        internal static ICollection<IServiceOrderItem> ToBusinessEntityCollection(this IEnumerable<ProxyServiceOrderItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        }
    }
}
