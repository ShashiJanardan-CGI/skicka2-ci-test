﻿using System;
using System.Collections.Generic;
using System.Linq;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Services;
using Posten.Portal.Cart.Utilities;
using Posten.Portal.Platform.Common.Container;

namespace Posten.Portal.Cart
{
    // Implements the BasketService functionality
    public partial class BasketContext
    {
        private IBasketItem basketItem;
        private IBasketService basketService;

        public decimal TotalValue
        {
            get { return this.BasketItem.Total; }
        }

        public decimal TotalVatValue
        {
            get { return this.BasketItem.TotalTax; }
        }

        public int NumberOfOrderLines
        {
            get { return this.BasketItem.OrderSummaryItems.Count; }
        }

        public int NumberOfArticle
        {
            get
            {
                var total = 0;
                if (this.BasketItem.OrderSummaryItems != null)
                {
                    foreach (var item in this.BasketItem.OrderSummaryItems)
                    {
                        if (item.Quantity>0 )
                            total += item.Quantity;
                        else
                        {
                            total++;
                        }
                    }
                }
                return total;
            }
        }

        public IBasketItem BasketItem
        {
            get
            {
                this.EnsureBasket();

                // TODO: remove 
                if (this.IsEmpty)
                {
                    this.basketItem = new Posten.Portal.Cart.Services.BasketServiceStub().GetBasketItemByBookingId("BookingId");
                }

                return this.basketItem;
            }
        }

        private IBasketService BasketService
        {
            get
            {
                if (this.basketService == null)
                {
                    this.basketService = new BasketServiceAgent();//IoC.Resolve<IBasketService>();
                }

                return this.basketService;
            }
        }

        public bool AssignBillingInfo(IBillingAccountInfo billingInfo)
        {
            Utils.CheckNullValue("billingInfo", billingInfo);
            this.EnsureBasket();

            if (this.IsEmpty)
            {
                // cannot set billing address on an empty basket
                return false;
            }

            // TODO: handle exception
            var result = this.BasketService.AssignBillingAccount(this.BasketId.ToString(), billingInfo);

            this.InvalidateCache();

            return result;
        }

        public bool AssignDeliveryInfo(IDeliveryAccountInfo deliveryInfo)
        {
            Utils.CheckNullValue("deliveryInfo", deliveryInfo);
            this.EnsureBasket();

            if (this.IsEmpty)
            {
                // cannot set billing address on an empty basket
                return false;
            }

            // TODO: handle exception
            var result = this.BasketService.AssignDeliveryAccount(this.BasketId.ToString(), deliveryInfo);

            this.InvalidateCache();

            return result;
        }

        internal ICollection<IPaymentMethod> GetAvailablePayments()
        {
            this.EnsureBasket();

            if (this.IsEmpty)
            {
                return new List<IPaymentMethod>();
            }

            // TODO: handle exception
            var result = this.BasketService.GetAvailablePayments(this.BasketId.ToString());

            return result;
        }

        public bool PrepareForCheckout(int version)
        {
            this.EnsureBasket();

            if (this.IsEmpty || this.IsCurrent)
            {
                // cannot checkout an empty or the current basket
                return false;
            }

            // TODO: handle exception
            var result = this.BasketService.PrepareForCheckout(this.BasketId.ToString(), version);

            this.InvalidateCache();

            return result;
        }

        public IBasketItem RecalculateVAT(string countrycode, string postalcode)
        {
            Utils.CheckNullValue("countrycode", countrycode);
            Utils.CheckNullValue("postalcode", postalcode);
            this.EnsureBasket();

            if (this.IsEmpty) { 
                return new BasketItem();
            }

            var result = this.BasketService.RecalculateVAT(this.BasketId.ToString(), countrycode, postalcode);

            return result;
        }

        public BasketContext Reload()
        {
            this.InvalidateCache();
            this.EnsureBasket();
            return this;
        }

        private void InvalidateCache()
        {

            this.basketItem = null;
            this.productOrderItems = null;
            this.serviceOrderItems = null;
        }

        public static void ReloadCurrent()
        {
            //Remove Key from HttpCache
            System.Web.HttpContext.Current.Items[ItemsCurrentKey] = null;
        }

        private void EnsureBasket()
        {
            if (this.basketItem != null)
            {
                return;
            }

            // Try to load an existing basket if basketId is set
            if (this.BasketId != Guid.Empty)
            {
                try
                {
                    // If not using current get the real status.
                    if (!this.IsCurrent )
                        this.basketItem = this.BasketService.GetBasketItem(this.BasketId.ToString());

                    //For current (used in the shopping cart) use Get
                    else
                        this.basketItem = this.BasketService.GetResolvedBasketItem(this.BasketId.ToString());
                    
                    BasketCookie.CurrentBasketId = this.basketId = this.basketItem.BasketId.ToGuid();
                
                }
                catch
                {
                    // TODO: only catch BasketNotFoundException
                    if (!this.IsCurrent)
                    {
                        throw;
                    }
                }
            }

           
        }

        public ICollection<IPaymentArticle> Articles
        {
            get { return BasketService.GetBasketArticles(this.BasketId.ToString()); }
        }
    }
}
