﻿using System;
using System.Configuration;
using System.Collections.Generic;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Services;
using Posten.Portal.Cart.Utilities;
using Posten.Portal.Platform.Common.Container;
using System.Linq;

namespace Posten.Portal.Cart
{
    // Implements the ServiceBasket methods
    public partial class BasketContext
    {
        private IServiceBasketService serviceBasket;
        private ICollection<IServiceOrderItem> serviceOrderItems;
        private IServiceOrderItem invoiceFee;
        private const string _InvoiceArticleId = "500000312";
        private const string _InvoiceServiceApplicationID = "HandlingFee";
        private const string _InvoiceCompanyCode = "F091";


        private string _InvoiceCustomerId
        {
            get { return ConfigurationManager.AppSettings["CartInvoiceCustomerId"]; }
        }

        private string _InvoiceVatCode
        {
            get { return ConfigurationManager.AppSettings["CartInvoiceVatCode"]; }
        }

        public ICollection<IServiceOrderItem> ServiceOrderItems
        {
            get
            {
                this.GetAllServiceOrderItems();
                return this.serviceOrderItems;
            }
        }

        private void GetAllServiceOrderItems()
        {
            if (this.serviceOrderItems == null)
            {
                this.EnsureBasket();

                // only call the web-service if the basket exists
                var allitems = this.IsEmpty ? new List<IServiceOrderItem>() : this.ServiceBasket.GetServiceOrders(this.BasketId.ToString());
                this.serviceOrderItems =
                    (from items in allitems where items.ApplicationId != _InvoiceServiceApplicationID select items).
                        ToList();

                //set invoice if it is there.
                this.invoiceFee = (from items in allitems where items.ApplicationId == _InvoiceServiceApplicationID select items).SingleOrDefault();
            }
        }

        public ICollection<IServiceOrderItem> ServiceOrderItemsWithFees
        {
            get
            {
                var returnvalue = new List<IServiceOrderItem>();
                //Use the property ServiceOrderItems to Query everything.
                this.GetAllServiceOrderItems();

                returnvalue.AddRange(serviceOrderItems);

                if (invoiceFee != null)
                    returnvalue.Add(invoiceFee);

                return returnvalue;
            }
        }

        private IServiceBasketService ServiceBasket
        {
            get
            {
                if (this.serviceBasket == null)
                {
                    this.serviceBasket = new ServiceBasketAgent();// IoC.Resolve<IServiceBasketService>();
                }

                return this.serviceBasket;
            }
        }

        /// <summary>
        /// Adds a service order to the basket
        /// </summary>
        /// <param name="serviceOrder">The service order that is to be added</param>
        /// <returns>The orderNumber the service order recived</returns>
        /// <exception cref="ArgumentNullException">Thrown when serviceOrder is null</exception>
        /// <exception cref="BasketException">Thrown when the service order cannot be added because the basket status is not InProgress</exception>
        public BasketOrderItem AddServiceOrder(IServiceOrderItem serviceOrder)
        {
            return AddServiceOrder(serviceOrder, true);
        }

        /// <summary>
        /// Adds a service order to the basket
        /// </summary>
        /// <param name="serviceOrder">The service order that is to be added</param>
        /// <returns>The orderNumber the service order recived</returns>
        /// <exception cref="ArgumentNullException">Thrown when serviceOrder is null</exception>
        /// <exception cref="BasketException">Thrown when the service order cannot be added because the basket status is not InProgress</exception>
        public BasketOrderItem AddServiceOrder(IServiceOrderItem serviceOrder, bool willrememberurl)
        {
            BasketOrderItem basketOrderItem = new BasketOrderItem();

            Utils.CheckNullValue("serviceOrder", serviceOrder);
            this.EnsureBasket();

            string newBasketId = this.BasketId.ToString();
            string orderNumber = string.Empty;

            // TODO: handle exception
            var result = this.ServiceBasket.AddServiceOrder(ref newBasketId, serviceOrder, ref orderNumber);

            // always refresh the cookie when we add new products to the basket
            BasketCookie.CurrentBasketId = this.basketId = newBasketId.ToGuid();
            if (willrememberurl)
                Utilities.Utils.DropCookie("eshopUrl", System.Web.HttpContext.Current.Request.UrlReferrer.ToSafeString());
            this.InvalidateCache();

            basketOrderItem.OrderId = orderNumber;
            basketOrderItem.BasketId = BasketCookie.CurrentBasketId.ToString();

            return basketOrderItem;
        }

        /// <summary>
        /// Gets the <see cref="IServiceOrderItem"/> with the specified <paramref name="orderNumber"/> from the basket.
        /// </summary>
        /// <param name="orderNumber">The order number of the service order to get.</param>
        /// <returns>The service order with the specified <paramref name="orderNumber"/>, if found; otherwise, null.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="orderNumber"/> is null.</exception>
        public IServiceOrderItem GetServiceOrder(string orderNumber)
        {
            Utils.CheckNullValue("orderNumber", orderNumber);
            this.EnsureBasket();

            if (this.IsEmpty)
            {
                return null;
            }

            // TODO: handle exception
            return this.ServiceBasket.GetServiceOrderItem(this.BasketId.ToString(), orderNumber);
        }

        /// <summary>
        /// Updates an existing service order (<paramref name="orderNumber"/>) in the basket.
        /// The original order will be removed and a new order with a new order number will be added to the basket.
        /// </summary>
        /// <param name="orderNumber">The order number of the order that is to be updated</param>
        /// <param name="serviceOrder">The updated service order item</param>
        /// <returns>The orderNumber the new service order recived</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="orderNumber"/> is null or empty</exception>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="serviceOrder"/> is null</exception>
        public string UpdateServiceOrder(string orderNumber, IServiceOrderItem serviceOrder)
        {
            Utils.CheckNullValue("orderNumber", orderNumber);
            Utils.CheckNullValue("serviceOrder", serviceOrder);
            this.EnsureBasket();

            // remove from method parameter?
            serviceOrder.OrderNumber = orderNumber;

            // TODO: handle exception
            var result = this.ServiceBasket.UpdateServiceOrder(this.BasketId.ToString(), serviceOrder);

            this.InvalidateCache();

            return result;
        }

        /// <summary>
        /// Removes a service order from the basket
        /// </summary>
        /// <param name="orderNumber">The order number that should be removed</param>
        /// <returns>True if the order was successfully removed or does not exists in the basket</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="orderNumber"/> is null or empty</exception>
        public bool RemoveServiceOrder(string orderNumber)
        {
            Utils.CheckNullValue("orderNumber", orderNumber);
            this.EnsureBasket();

            if (this.IsEmpty)
            {
                return true;
            }

            var result = this.ServiceBasket.RemoveServiceOrder(this.BasketId.ToString(), orderNumber);

            this.InvalidateCache();

            return result;
        }

        public void AddInvoiceFee()
        {
            this.EnsureBasket();

            var orderline = new ServiceOrderLineItem()
            {
                ArticleId = BasketContext._InvoiceArticleId,
                ArticlePrice = 23.2m,
                Vat = 5.8m,
                VatPercentage = 0.25m,
                ArticleName = Utils.GetResourceString("INVOICEFEE_NAME"),
                OrderLineNumber = 1,
                Quantity = 1,
                Total = 23.2m,
                TaxCountry = "SE",
                CompanyCode = _InvoiceCompanyCode,
                CustomerId = _InvoiceCustomerId, 
                VatCode=_InvoiceVatCode 
            };
            var orderlines = new List<IServiceOrderLineItem>();

            orderlines.Add(orderline);


            IServiceOrderItem invoicefee = new ServiceOrderItem()
            {
                ApplicationId = _InvoiceServiceApplicationID,
                ApplicationName = _InvoiceServiceApplicationID,
                BasketId = this.BasketId.ToString(),
                CurrencyCode = "SEK",
                Name = Utils.GetResourceString("INVOICEFEE_NAME"),
                ServiceOrderLines = orderlines,
                SubTotal = 23.2m,
                Vat = 5.8m
            };


            //check if invoice has already been added.
            if (InvoiceFee == null)
            {
                invoiceFee = invoicefee;
                AddServiceOrder(invoicefee,false);
            }
        }

        public void RemoveInvoiceFee()
        {
            this.EnsureBasket();

            //check if invoice has already been added.
            if (InvoiceFee != null)
                this.RemoveServiceOrder(InvoiceFee.OrderNumber);

            invoiceFee = null;
        }

        public IServiceOrderItem InvoiceFee
        {
            get
            {
                this.GetAllServiceOrderItems();
                return invoiceFee;
            }
        }
    }
}
