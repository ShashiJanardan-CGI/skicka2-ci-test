﻿using System;
using System.Web;
using Posten.Portal.Cart.Utilities;

namespace Posten.Portal.Cart
{
    // Implements the core functionality
    public partial class BasketContext
    {
        private Guid basketId;

        protected BasketContext(Guid basketId)
        {
            this.basketId = basketId;
        }

        protected BasketContext(string bookingid)
        {
//#if DEBUG
//            var basket = new Posten.Portal.Cart.Services.BasketServiceStub().GetBasketItemByBookingId(bookingid);
//            this.basketId = new Guid();
//#else
            var basket = this.BasketService.GetBasketItemByBookingId(bookingid);
            this.basketId = new Guid(basket.BasketId);
//#endif
        }

        public Guid BasketId
        {
            get { return this.basketId; }
        }

        public bool IsEmpty
        {
            get { return this.BasketId == Guid.Empty; }
        }

        public bool IsCurrent
        {
            get
            {
                // BasketContext can only be used in a web request
                var httpContext = HttpContext.Current;
                if (httpContext == null)
                {
                    return false;
                }

                // check if this is the current basket
                return this == httpContext.Items[ItemsCurrentKey];
            }
        }

        public bool IsAuthenticated {
            get 
            {
                return HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }
    }

    // Implements the singelton functionality
    public partial class BasketContext
    {
        private const string ItemsBaseKey = "BasketContext";
        private const string ItemsCurrentKey = "BasketContext_Current";

        private static readonly object lockObject = new object();
        public static string BasketID { get; set;}
        public static BasketContext Current
        {
            get
            {
                Guid currentBasketID;
                // BasketContext can only be used in a web request
                var httpContext = HttpContext.Current;
                if (httpContext == null)
                {
                    return null;
                }

                var basketContext = httpContext.Items[ItemsCurrentKey] as BasketContext;
                if (basketContext == null)
                {
                    lock (lockObject)
                    {
                        if (string.IsNullOrEmpty(BasketID))
                        {
                            currentBasketID = BasketCookie.CurrentBasketId;
                        }
                        else
                        {
                            currentBasketID = new Guid(BasketID);
                            BasketID = null;
                        }
                        basketContext = GetContext(currentBasketID);
                        httpContext.Items[ItemsCurrentKey] = basketContext;
                    }
                }
                    
                return basketContext;
            }
        }


        public static BasketContext GetContext(Guid basketId)
        {
            // BasketContext can only be used in a web request
            var httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                return null;
            }

            var itemsKey = GetItemsKey(basketId);

            var basketContext = httpContext.Items[itemsKey] as BasketContext;
            if (basketContext == null)
            {
                basketContext = new BasketContext(basketId);
                httpContext.Items[itemsKey] = basketContext;
            }

            return basketContext;
        }

        public static BasketContext GetContext(string bookingID)
        {
            // BasketContext can only be used in a web request
            var httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                return null;
            }

            var itemsKey = ItemsBaseKey + "_" + bookingID;

            var basketContext = httpContext.Items[itemsKey] as BasketContext;
            if (basketContext == null)
            {
                basketContext = new BasketContext(bookingID);
                httpContext.Items[itemsKey] = basketContext;
            }

            return basketContext;
        }

        private static string GetItemsKey(Guid basketId)
        {
            return ItemsBaseKey + "_" + basketId;
        }
    }
}
