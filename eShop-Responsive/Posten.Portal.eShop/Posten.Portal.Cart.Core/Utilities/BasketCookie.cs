﻿using System;
using System.Configuration;
using System.Web;
using Posten.Portal.Cart.Translators;

namespace Posten.Portal.Cart.Utilities
{
    internal static class BasketCookie
    {
        // TODO: load from configuration
        private const string CookieName = "PostenBasketId";
        private const int CookieDayLength = 5;

        internal static Guid CurrentBasketId
        {
            get
            {
                var httpContext = HttpContext.Current;
                if (httpContext != null)
                {
                    // return the cached value if it exists
                    var cachedGuid = httpContext.Items[CookieName] as string;
                    if (!string.IsNullOrEmpty(cachedGuid))
                    {
                        return cachedGuid.ToGuid();
                    }

                    // get cookie
                    var cookie = httpContext.Request.Cookies[CookieName];
                    if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
                    {
                        var basketId = cookie.Value.ToGuid();

                        // update the expiration date each time we use the basket-id
                        CurrentBasketId = basketId;

                        return basketId;
                    }
                }

                return Guid.Empty;
            }

            set
            {
                // silently ignore if not called from inside a web request
                var httpContext = HttpContext.Current;
                if (httpContext == null)
                {
                    return;
                }

                // NOTE: cookie is never null since it gets added automatically if it does not extists
                var cookie = httpContext.Response.Cookies[CookieName];

                if (value != Guid.Empty)
                {
                    // set the cookie if it has a value
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(CookieDayLength);
                    cookie.Domain = (ConfigurationManager.AppSettings["CookieDomain"] == null) ? string.Empty : ConfigurationManager.AppSettings["CookieDomain"].ToString();
                }
                else if (httpContext.Request.Cookies[CookieName] == null)
                {
                    // don't send response to remove cookie if it does not exists
                    httpContext.Response.Cookies.Remove(CookieName);
                }
                else
                {
                    // remove the cookie if the basket does not exists
                    cookie.Expires = DateTime.Now.AddDays(-30);
                }

                // cache the value for the lifetime of this request
                httpContext.Items[CookieName] = value.ToString();
            }
        }
    }
}

