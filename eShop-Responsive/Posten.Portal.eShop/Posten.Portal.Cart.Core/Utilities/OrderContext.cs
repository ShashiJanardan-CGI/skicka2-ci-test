﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Services;
using Posten.Portal.Platform.Common.Container;

namespace Posten.Portal.Cart
{
    public class OrderContext
    {
        private const string ItemsBaseKey = "OrderContext";
        private IBasketItem basketItem;
        private ICollection<IProductOrderItem> productOrderItems;
        private ICollection<IServiceOrderItem> serviceOrderItems;
        private BasketContext basketcontext;


        public decimal InvoiceFee {
            get { return basketcontext.InvoiceFee == null ? 0 : basketcontext.InvoiceFee.Total; }
        }

        private OrderContext(string bookingId)
        {
            basketcontext = BasketContext.GetContext(bookingId);
            this.basketItem = basketcontext.BasketItem;
            this.BookingId = this.basketItem.BookingId;
            this.ReciptUrl = this.BasketItem.ReceiptUrl;
        }

        public string BookingId { get; private set; }

        public Uri ReciptUrl { get; private set; }

        public decimal TotalValue
        {
            get { return this.BasketItem.Total; }
        }

        public decimal TotalVatValue
        {
            get { return this.BasketItem.TotalTax; }
        }

        public ICollection<IProductOrderItem> ProductOrderItems
        {
            get
            {
                var BasketContext = Cart.BasketContext.GetContext(this.BookingId);

                if (this.productOrderItems == null)
                {
                    // only call the web-service if the basket exists
                    this.productOrderItems = BasketContext.ProductOrderItems;
                }

                return this.productOrderItems;
            }
        }

        public ICollection<IServiceOrderItem> ServiceOrderItems
        {
            get
             {
                if (this.serviceOrderItems == null)
                {
                    // only call the web-service if the basket exists
                    this.serviceOrderItems = this.basketcontext.ServiceOrderItems;
                }

                return this.serviceOrderItems;
            }
        }

        public IBasketItem BasketItem
        {
            get
            {
                return this.basketItem;
            }
        }

        public static OrderContext GetContext(string bookingId)
        {
            // OrderContext can only be used in a web request
            var httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                return null;
            }

            var itemsKey = GetItemsKey(bookingId);

            var orderContext = httpContext.Items[itemsKey] as OrderContext;
            if (orderContext == null)
            {
                // TODO: validate bookingId
                orderContext = new OrderContext(bookingId);
                httpContext.Items[itemsKey] = orderContext;
            }

            return orderContext;
        }

        private static string GetItemsKey(string bookingId)
        {
            return ItemsBaseKey + "_" + bookingId;
        }
    }
}
