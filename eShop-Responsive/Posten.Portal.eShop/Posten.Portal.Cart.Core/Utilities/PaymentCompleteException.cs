﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Posten.Portal.Cart.Utilities
{
    public class PaymentCompleteException : Exception, ISerializable
    {
        public PaymentCompleteException()
        {
        }

        public PaymentCompleteException(string message) : base(message)
        {            
        }

        public PaymentCompleteException(string message, Exception inner) : base(message, inner)
        {
        }
            
        protected PaymentCompleteException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
