﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Posten.Portal.Cart.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Import Namespace="Posten.Portal.Cart.BusinessEntities" %>
<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI.WebControls" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckoutBasket.ascx.cs" Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.Legacy.CheckoutBakset" %>

<% if (!this.Model.IsEmpty) { %>

    <ul class="cart-items cart-items-last">
    <p class="message empty-message">
     Din varukorg är tom</p>
    <div class="products">
    <% if (this.Model.ProductOrderItems.Count > 0) {%>
        <li ><h3>Produkter</h3></li>
        <% foreach (IProductOrderItem item in this.Model.ProductOrderItems) { %>
        <li class="product">
            <div class="info">
                <h4><%= item.ArticleName %></h4>
                <p class="description">Art # <%= item.ArticleId %></p>
            </div>
            <div class="price-container">
                <div class="price-container-buttons basket-commonstyles">
                    <a class="button-delete button white" name="0" data-orderno="<%= item.OrderNumber %>" rel="#basket_modal_delete_confirm" href="#" title="Ta bort">
                    	<img src="<%= this.GetImagePath("icon-delete.gif") %>" alt="Ta bort" /><span>Ta bort</span>
					</a>
                </div>
                <div class="price-container-price">
                    <div class="h4title"><%= Utils.FormatCurrency(item.Total, item.CurrencyCode) %></div>
                    <div class="vat">(varav moms <%= Utils.FormatCurrency(item.Vat, item.CurrencyCode) %>)</div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </li>
        <% } %>
    <% } %>
    </div>
    <div class="services">
    <% if (this.Model.ServiceOrderItems.Count > 0) { string title = string.Empty; %>
        <% foreach (IServiceOrderItem item in this.Model.ServiceOrderItems) { %>
        <% if (title != item.ApplicationName) { title = item.ApplicationName; %>
        <li class="service-label"><h3><%= title %></h3></li>
        <% } %>
        <li class="product">
            <% if (!string.IsNullOrEmpty(Utils.ToSafeString( item.ServiceImageUrl))) { %>
                <img class="product-image" src="<%= item.ServiceImageUrl.ToString() %>" alt="<%= item.Name%>" />
            <% } %>
            <div class="info">
                <h4><%= item.Name %></h4>
                <p class="description"><%= item.FreeTextField1 %><br /><%= item.FreeTextField2 %></p>
            </div>
            <div class="price-container">
                <div class="price-container-buttons basket-commonstyles">
                    <% if (!string.IsNullOrEmpty(item.EditUrl)) { %>
                        <a class="button-edit button white" data-orderno="<%= item.OrderNumber %>" href="<%= this.GetEditUrl(item) %>" title="&Auml;ndra">
                            <img src="<%= this.GetImagePath("icon-edit.gif") %>" alt="&Auml;ndra" /><span>&Auml;ndra</span>
                        </a>
                    <% } %>
                    <a class="button-delete button white" name="1" data-orderno="<%= item.OrderNumber %>" rel="#basket_modal_delete_confirm" href="#" title="Ta bort">
                        <img src="<%= this.GetImagePath("icon-delete.gif") %>" alt="Ta bort" /><span>Ta bort</span>
                    </a>
                </div>
                <div class="price-container-price">
                    <div class="h4title"><%= Utils.FormatCurrency(item.Total, item.CurrencyCode) %></div>
                    <div class="vat">(varav moms <%= Utils.FormatCurrency(item.Vat, item.CurrencyCode) %>)</div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </li>
        <% } %>
    <% } %>
    </div>
    </ul>
<% } %>

<% if (this.Model.IsEmpty) { %>
    <p class="message">Din varukorg är tom</p>
<% } %>

<div id="payment-sum">
    <div class="payment-sum-price-container">
        <h2>Att betala</h2>
        <h4 class="title"><%= Utils.FormatCurrency(this.Model.TotalValue, "SEK") %></h4>
        <div class="vat">(varav moms <%= Utils.FormatCurrency(Model.TotalVatValue, "SEK") %>)</div>
    </div>
    <div class="clear"></div>
</div>
