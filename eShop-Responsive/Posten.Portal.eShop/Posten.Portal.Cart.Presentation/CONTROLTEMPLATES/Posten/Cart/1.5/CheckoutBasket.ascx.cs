﻿using System;
using System.Web.UI;
using Posten.Portal.Cart.Utilities;

namespace Posten.Portal.Cart.Presentation.ControlTemplates.Legacy
{
    using Posten.Portal.Cart.BusinessEntities;

    public partial class CheckoutBakset : UserControl
    {
        protected BasketContext Model
        {
            get { return BasketContext.Current; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected string GetImagePath(string file)
        {
            return "/_layouts/Posten/Cart/1.5/Images/" + file;
        }

        protected string GetEditUrl(IServiceOrderItem serviceOrder)
        {
            return string.Format(serviceOrder.EditUrl, serviceOrder.OrderNumber, Server.UrlEncode(this.Request.Url.ToWebSealUrl().ToString()));
        }
    }
}
