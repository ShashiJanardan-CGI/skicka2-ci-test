﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Posten.Portal.Cart.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Import Namespace="Posten.Portal.Cart.BusinessEntities" %>
<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.WebControls" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LegacyConfirmation.ascx.cs"
    Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.Legacy.LegacyConfirmation" %>



<% if (this.OrderContext != null)
   { %>
<h1>
    Bekr&auml;ftelse</h1>
<div class="confirmation-info-top">
</div>
<div class="confirmation-info">
    <div class="confirmation-image">
    </div>
    <div class="inner">
        <h2 style="margin-bottom: 6px; margin-top: 0px; padding: 0px; background-color: #dff7df;">
            Tack f&ouml;r din best&auml;llning!</h2>
        <div style="float: left">
            <strong>Ditt best&auml;llningsnummer &auml;r:
                <label>
                    <%= HttpUtility.HtmlEncode(this.OrderContext.BookingId) %></label></strong><br />
            En bekr&auml;ftelse på din beställning har skickats till din e-postadress, <a href="<%= HttpUtility.HtmlAttributeEncode("/_layouts/Posten/Cart/common/Receipt.ashx?BookingId=" + this.OrderContext.BookingId) %>">
                ladda ner kvittot</a>
            f&ouml;r utskrift.
        </div>
    </div>
    <div class="clearfix">
    </div>
</div>
<% if (this.OrderContext.ProductOrderItems.Count > 0)
   { %>
<div class="basket-styles">
    <h2 style="margin-top: 20px;">
        Din beställning</h2>
    <ul class="cart-items cart-items-last">
        <li>
            <h3>Produkter</h3>
        </li>
        <% foreach (IProductOrderItem item in this.OrderContext.ProductOrderItems)
           { %>
        <li class="product">
            <div class="info">
                <h4>
                    <%= item.ArticleName %></h4>
                <p class="description">
                    Art #
                    <%= item.ArticleId %></p>
            </div>
            <div class="price-container">
                <div class="price-container-price">
                    <div class="h4title">
                        <%= Utils.FormatCurrency(item.Total, item.CurrencyCode) %></div>
                    <div class="vat">
                        (varav moms
                        <%= Utils.FormatCurrency(item.Vat, item.CurrencyCode) %>)</div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
        </li>
        <% } %>
        <li>
            <h3>Tjänster</h3>
        </li>
        <% foreach (IServiceOrderItem item in this.OrderContext.ServiceOrderItems)
           { %>
        <li class="product">
            <div class="info">
                <h4>
                    <%= item.Name %></h4>
                <p class="description">
                    <%= item.FreeTextField1 %><br />
                    <%= item.FreeTextField2 %></p>
            </div>
            <div class="price-container">
                <div class="price-container-price">
                    <div class="h4title">
                        <%= Utils.FormatCurrency(item.Total, item.CurrencyCode) %></div>
                    <div class="vat">
                        (varav moms
                        <%= Utils.FormatCurrency(item.Vat, item.CurrencyCode) %>)</div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
        </li>
        <% } %>
    </ul>
    <div id="payment-sum">
        <div class="payment-sum-price-container">
            <h2 style="margin-top: 0; background: transparent;">
                Att betala</h2>
            <h4 class="title">
                <%= Utils.FormatCurrency(this.OrderContext.TotalValue, "SEK") %></h4>
            <div class="vat">
                (varav moms
                <%= Utils.FormatCurrency(OrderContext.TotalVatValue, "SEK") %>)</div>
        </div>
        <div class="clear">
        </div>
    </div>
</div>
<% } %>
<asp:ListView ID="ServiceOrders" runat="server">
    <LayoutTemplate>
        <div class="confirmation-remote">
            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="dynamic-data" data-url="<%# ((Uri) Eval("ConfirmationUrl")).PathAndQuery %>">
            <h1><img class="spinner" src="/_layouts/images/posten/cart/spinner.gif" alt="loading"  /> Laddar</h1>
        </div>
    </ItemTemplate>
</asp:ListView>
<% }
   else
   { %>
<h1>
    Bekr&auml;ftelse kan ej visas</h1>
<% } %>
<div class="confirmation-information">
    <h2>
        Information</h2>
    <p>
        Din best&auml;llning &auml;r mottagen och blir nu bekr&auml;ftad. Orderbekr&auml;ftelsen
        skickas till den e-postadress som du angett. Har du inte f&aring;tt en bekr&auml;ftelse
        inom tv&aring; timmar v&auml;nligen kontakta <a href="http://www.posten.se/kontakta_oss">
            Posten kundtj&auml;nst</a>.</p>
</div>
<div class="rights-of-regrets">
    <h2>
        &Aring;ngerr&auml;tt/Retur</h2>
    <p>
        Varor och tj&auml;nster kan returneras inom 14 dagar. Observera att du inte kan
        &aring;ngra k&ouml;pet av en tj&auml;nst efter att tj&auml;nstens utf&ouml;rande
        har p&aring;b&ouml;rjats. När en tj&auml;nst anses p&aring;b&ouml;rjad framg&aring;r
        av de <a href="<%= HttpUtility.HtmlAttributeEncode(this.TermsOfServiceUrl) %>" target="_blank">
            villkor</a> som g&auml;ller för tj&auml;nsten. L&auml;s mer om &aring;ngerr&auml;tt/retur
        i <a href="<%= HttpUtility.HtmlAttributeEncode(this.TermsOfSaleUrl) %>" target="_blank">
            k&ouml;pvillkoren</a>.
    </p>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        $('.confirmation-remote .dynamic-data').each(function () {
            var $this = $(this); $this.load($this.data('url'));
        });
    });
</script>
