﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniBasket.ascx.cs" Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.Legacy.MiniBasket" %>

<script type="text/javascript">
    var g_ServerRelativeUrl = '<asp:Literal ID="ServerRelativeUrl" runat="server" />';
</script>
<link href="/_layouts/Posten/Cart/1.5/Styles/minibasket.css" type="text/css" rel="stylesheet" />
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common.css" type="text/css" rel="stylesheet" />
<!--[if IE 6]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie6.css" type="text/css" rel="stylesheet" />
<![endif]-->
<!--[if IE]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie.css" type="text/css" rel="stylesheet" />
<![endif]-->
<div id="kundkorgen_minibasket"></div>
<script src="/_layouts/Posten/Cart/1.5/Scripts/jquery.tools.min.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/1.5/Scripts/jquery-avensia-plugins.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/1.5/Scripts/basket-common.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/1.5/Scripts/minibasket.js" type="text/javascript"></script>


<%--

function UpdateCart(basket)
{
}

$(function () {
    PostenMiniBasket.initialize(UpdateMiniBasket);
    PostenMiniBasket.update();
});

API:
PostenMiniBasket.initialize(updateFunction)
PostenMiniBasket.update()
//PostenMiniBasket.IsEmpty()
//PostenMiniBasket.AddProduct(productId, quantity)
PostenMiniBasket.UpdateOrderItem(orderNumber, quantity)
PostenMiniBasket.RemoveOrderItem(orderNumber)
PostenMiniBasket.EmptyBasket()


C#
---
BasketContext : IScriptBasket
 
 Public Properties:
 - Guid BasketId (get)
 - decimal TotalValueIncludingVAT (get, private set), should be session cached and updated on changes in basket
 - decimal TotalValueExcludingVAT (get, private set), should be session cached and updated on changes in basket
 - int NumberOfOrderItems (get, private set), should be session cached and updated on changes in basket
 - IServiceOrder[] ServiceOrderItems (get)
 - IProductOrder[] ProductOrderItems (get)

 Service methods:
 - string AddServiceOrder(IServiceOrderItem order), returns orderNumber
 - IServiceOrderItem GetServiceOrder(string orderNumber), returns service order
 - string UpdateServiceOrder(string orderNumber, ServiceOrderItem order), returns new orderNumber
 - bool RemoveServiceOrder(string orderNumber), returns 

 Product methods:
 - string AddProductOrder(IProductOrderItem order), returns orderNumber
 - IProductOrderItem GetProductOrder(string orderNumber), returns product order
 - string UpdateProductOrder(string orderNumber, ProductOrderItem order), returns new orderNumber
 - bool RemoveProductOrder(string orderNumber), returns 

 Static Properties:
 - BasketContext Current (get), returns a basket context based on the cookie basket id

 Static methods:
 - GetContext(Guid basketId), returns a basket context with the current 


Script WCF Service

 Methods
 - IScriptBasket GetBasket()
 //- IScriptBasket AddProduct(string productId, int quantity)
 - IScriptBasket UpdateOrderItem(string orderNumber, int quantity)
 - IScriptBasket RemoveOrderItem(string orderNumber)
 - IScriptBasket EmptyBasket()

--%>