﻿namespace Posten.Portal.Cart.Presentation.ControlTemplates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.UI;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebControls;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Services;
    using Posten.Portal.Platform.Common.Container;

    public abstract class CartConfigurationBase : UserControl
    {
        #region Fields

        ICartSettingsService service = IoC.Resolve<ICartSettingsService>();

        #endregion Fields

        #region Properties

        protected abstract string DefaultPaymentValue
        {
            get;
        }

        protected CartSettingItems GetItems
        {
            get
            {
                return service.GetItems();
            }
        }

        protected abstract string InvoicePaymentValue
        {
            get;
        }

        protected abstract bool ShowSaveMessage
        {
            set;
        }

        #endregion Properties

        #region Methods

        protected void Save_Settings(object sender, EventArgs e)
        {
            var item = new CartSettingItems();
            item.defaultpayment = this.DefaultPaymentValue;
            item.invoiceoption = this.InvoicePaymentValue;

            //save default payment type
            var result = service.SaveItems(item);

            if (result)
            {this.ShowSaveMessage = true;}
        }

        #endregion Methods
    }
}