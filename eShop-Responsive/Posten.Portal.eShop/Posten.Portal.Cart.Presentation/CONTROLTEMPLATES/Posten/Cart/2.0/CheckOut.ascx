﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>
<%@ Register TagPrefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls"
    Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Register TagPrefix="uc" TagName="EditCart" Src="~/_CONTROLTEMPLATES/Posten/Cart/2.0/EditCart.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckOut.ascx.cs" Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.CheckOut" %>
<script src="/_layouts/Posten/Cart/Presentation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Presentation/js/common/jquery.cookie.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Presentation/js/jquery.ezpz_tooltip.min.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Presentation/js/jqModal.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Common/Scripts/jquery.tmpl.min.js" type="text/javascript"></script>
<div>
    <asp:Literal ID="ltrlErrorMessage" runat="server"></asp:Literal></div>
<div id="deliveryhelp-content-1">
    <div class="arrowtooltip">
    </div>
    <div class="helpcontent">
        <%= Utils.GetResourceString("HelpText_DeliveryInfo")%>
    </div>
</div>
<div id="promo-content">
    <div class="arrowtooltip">
    </div>
    <div class="helpcontent">
        <%= Utils.GetResourceString("HelpText_PromoInfo")%>
    </div>
</div>
<div>
    <div class="checkoutcontrol">
        <div class="colspan-2 webpartzone">
            <div id="LeftColumnZone">
                <div>
                    <div class="module common ">
                        <div class="edge top">
                            <div class="left border">
                            </div>
                            <div class="right border">
                            </div>
                        </div>
                        <div class="header common">
                            <span>
                                <asp:Literal runat="server" ID="CustomerDataLabel" Text='<%$CartCode: Utils.GetResourceString("CheckOut_CustomerData_Label")%>' />
                            </span>
                        </div>
                        <div class="content common" style="overflow: hidden;">
                            <div style="width: 100%;">
                                <div class="vsummary">
                                    <div class="errorsummary">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="email-sec">
                                    <div class="email-lbl">
                                        <label for="<%= txtEmail.ClientID %>">
                                            <asp:Literal runat="server" ID="YourEmailLabel" Text='<%$CartCode: Utils.GetResourceString("Your_Email_Label")%>' />:&nbsp;*
                                        </label>
                                    </div>
                                    <div class="email-tbox">
                                        <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="required email enablestate"></asp:TextBox>
                                    </div>
                                    
                                </div>
                                 
                                <div id="divPassword" class="password" style="display: none;">
                                    <div class="password-lbl">
                                        <asp:Literal runat="server" ID="PasswordLabel" Text='<%$CartCode: Utils.GetResourceString("Password_Label")%>' />:&nbsp;*
                                    </div>
                                    <div class="password-tbox">
                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                    </div>
                                    <div class="btnlogin" style="float: right">
                                        <span>
                                            <asp:HyperLink ID="btnLogIn" runat="server" Text='<%$CartCode: Utils.GetResourceString("Login_Label")%>' />
                                        </span>
                                    </div>
                                </div>
                                <div class="rbutton">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Text='<%$CartCode: Utils.GetResourceString("Private_Person_Label")%>' Value="private"
                                            class="private">
                                        </asp:ListItem>
                                        <asp:ListItem Text='<%$CartCode: Utils.GetResourceString("Business_Option_Label")%>' Value="company"
                                            class="company">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                    <div class="disclaimer emailinfo">
                                        <asp:Literal runat="server" ID="Literal7" Text='<%$CartCode: Utils.GetResourceString("Email_Info_Text")%>' />
                                    </div>
                                </div>
                               
                                <div class="custdetails">
                                    <div class="custdetails-lbls">
                                        <div id="ssno-lbl" style="display: none">
                                            <label for="<%= txtCivicCompNo  %>">
                                                <asp:Literal runat="server" ID="Literal5" Text='<%$CartCode: Utils.GetResourceString("Civic_Label")%>' />:</label>
                                        </div>
                                        <div>
                                            <label for="<%= txtFirstName.ClientID  %>">
                                                <asp:Literal runat="server" ID="FirstNameLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_FirstName_Label")%>' />:&nbsp;</label>
                                        </div>
                                        <div>
                                            <label for="<%= txtLastName.ClientID  %>">
                                                <asp:Literal runat="server" ID="SurNameLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_SurName_Label")%>' />:&nbsp;</label>
                                        </div>
                                        <div>
                                            <label for="<%= txtAddress.ClientID  %>">
                                                <asp:Literal runat="server" ID="AddressLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_Address_Label")%>' />:&nbsp;</label>
                                        </div>
                                        <div>
                                            <label for="<%= txtPostalCode.ClientID  %>">
                                                <asp:Literal runat="server" ID="PostalLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_PostalCode_Label")%>' />:&nbsp;</label>
                                        </div>
                                        <div>
                                            <label for="<%= txtCity.ClientID  %>">
                                                <asp:Literal runat="server" ID="CityLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_City_Label")%>' />:
                                                </label>
                                        </div>
                                        <div>
                                            <label for="<%= ddlstCountry.ClientID  %>">
                                                <asp:Literal runat="server" ID="CountryLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_Country_Label")%>' />:&nbsp;</label>
                                        </div>
                                        <div>
                                            <label for="<%= txtMobileNumber.ClientID  %>">
                                                <asp:Literal runat="server" ID="MobileNoLabel1" Text='<%$CartCode: Utils.GetResourceString("Mobile_Number_Label")%>' />:</label>
                                        </div>
                                    </div>
                                    <div class="custdetails-tboxes">
                                        <div id="ssno-tbox" style="display: none">
                                            <asp:TextBox ID="txtCivicCompNo" runat="server" Text="" CssClass="civiccompno_id enablestate"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtFirstName" runat="server" Text="" CssClass="firstname  enablestate"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtLastName" runat="server" Text="" CssClass="lastname  enablestate"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="address  enablestate"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtPostalCode" runat="server" CssClass="postalcode  enablestate"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="city  enablestate"></asp:TextBox>
                                        </div>
                                        <div>
                                            <!--<asp:ListItem Value="SE">Sverige</asp:ListItem>
                                                <asp:ListItem Value="DK">Danmark</asp:ListItem>
                                                <asp:ListItem Value="NO">Norge</asp:ListItem>
                                                <asp:ListItem Value="DE">Tyskland</asp:ListItem>-->
                                            <asp:DropDownList ID="ddlstCountry" runat="server" CssClass="country ">
                                                <asp:ListItem Value="AF" Text='<%$CartCode: Utils.GetResourceString("AF")%>'></asp:ListItem>
                                                <asp:ListItem Value="AL" Text='<%$CartCode: Utils.GetResourceString("AL")%>'></asp:ListItem>
                                                <asp:ListItem Value="DZ" Text='<%$CartCode: Utils.GetResourceString("DZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="AS" Text='<%$CartCode: Utils.GetResourceString("AS")%>'></asp:ListItem>
                                                <asp:ListItem Value="AD" Text='<%$CartCode: Utils.GetResourceString("AD")%>'></asp:ListItem>
                                                <asp:ListItem Value="AO" Text='<%$CartCode: Utils.GetResourceString("AO")%>'></asp:ListItem>
                                                <asp:ListItem Value="AI" Text='<%$CartCode: Utils.GetResourceString("AI")%>'></asp:ListItem>
                                                <asp:ListItem Value="AG" Text='<%$CartCode: Utils.GetResourceString("AG")%>'></asp:ListItem>
                                                <asp:ListItem Value="AR" Text='<%$CartCode: Utils.GetResourceString("AR")%>'></asp:ListItem>
                                                <asp:ListItem Value="AM" Text='<%$CartCode: Utils.GetResourceString("AM")%>'></asp:ListItem>
                                                <asp:ListItem Value="AW" Text='<%$CartCode: Utils.GetResourceString("AW")%>'></asp:ListItem>
                                                <asp:ListItem Value="AU" Text='<%$CartCode: Utils.GetResourceString("AU")%>'></asp:ListItem>
                                                <asp:ListItem Value="AZ" Text='<%$CartCode: Utils.GetResourceString("AZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="BS" Text='<%$CartCode: Utils.GetResourceString("BS")%>'></asp:ListItem>
                                                <asp:ListItem Value="BH" Text='<%$CartCode: Utils.GetResourceString("BH")%>'></asp:ListItem>
                                                <asp:ListItem Value="BD" Text='<%$CartCode: Utils.GetResourceString("BD")%>'></asp:ListItem>
                                                <asp:ListItem Value="BB" Text='<%$CartCode: Utils.GetResourceString("BB")%>'></asp:ListItem>
                                                <asp:ListItem Value="BE" Text='<%$CartCode: Utils.GetResourceString("BE")%>'></asp:ListItem>
                                                <asp:ListItem Value="BZ" Text='<%$CartCode: Utils.GetResourceString("BZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="BJ" Text='<%$CartCode: Utils.GetResourceString("BJ")%>'></asp:ListItem>
                                                <asp:ListItem Value="BM" Text='<%$CartCode: Utils.GetResourceString("BM")%>'></asp:ListItem>
                                                <asp:ListItem Value="BT" Text='<%$CartCode: Utils.GetResourceString("BT")%>'></asp:ListItem>
                                                <asp:ListItem Value="BO" Text='<%$CartCode: Utils.GetResourceString("BO")%>'></asp:ListItem>
                                                <asp:ListItem Value="BA" Text='<%$CartCode: Utils.GetResourceString("BA")%>'></asp:ListItem>
                                                <asp:ListItem Value="BW" Text='<%$CartCode: Utils.GetResourceString("BW")%>'></asp:ListItem>
                                                <asp:ListItem Value="BR" Text='<%$CartCode: Utils.GetResourceString("BR")%>'></asp:ListItem>
                                                <asp:ListItem Value="BN" Text='<%$CartCode: Utils.GetResourceString("BN")%>'></asp:ListItem>
                                                <asp:ListItem Value="BG" Text='<%$CartCode: Utils.GetResourceString("BG")%>'></asp:ListItem>
                                                <asp:ListItem Value="BF" Text='<%$CartCode: Utils.GetResourceString("BF")%>'></asp:ListItem>
                                                <asp:ListItem Value="BI" Text='<%$CartCode: Utils.GetResourceString("BI")%>'></asp:ListItem>
                                                <asp:ListItem Value="KY" Text='<%$CartCode: Utils.GetResourceString("KY")%>'></asp:ListItem>
                                                <asp:ListItem Value="CF" Text='<%$CartCode: Utils.GetResourceString("CF")%>'></asp:ListItem>
                                                <asp:ListItem Value="CL" Text='<%$CartCode: Utils.GetResourceString("CL")%>'></asp:ListItem>
                                                <asp:ListItem Value="CO" Text='<%$CartCode: Utils.GetResourceString("CO")%>'></asp:ListItem>
                                                <asp:ListItem Value="CK" Text='<%$CartCode: Utils.GetResourceString("CK")%>'></asp:ListItem>
                                                <asp:ListItem Value="CR" Text='<%$CartCode: Utils.GetResourceString("CR")%>'></asp:ListItem>
                                                <asp:ListItem Value="CY" Text='<%$CartCode: Utils.GetResourceString("CY")%>'></asp:ListItem>
                                                <asp:ListItem Value="DK" Text='<%$CartCode: Utils.GetResourceString("DK")%>'></asp:ListItem>
                                                <asp:ListItem Value="DJ" Text='<%$CartCode: Utils.GetResourceString("DJ")%>'></asp:ListItem>
                                                <asp:ListItem Value="CX" Text='<%$CartCode: Utils.GetResourceString("CX")%>'></asp:ListItem>
                                                <asp:ListItem Value="CC" Text='<%$CartCode: Utils.GetResourceString("CC")%>'></asp:ListItem>
                                                <asp:ListItem Value="EC" Text='<%$CartCode: Utils.GetResourceString("EC")%>'></asp:ListItem>
                                                <asp:ListItem Value="EG" Text='<%$CartCode: Utils.GetResourceString("EG")%>'></asp:ListItem>
                                                <asp:ListItem Value="GQ" Text='<%$CartCode: Utils.GetResourceString("GQ")%>'></asp:ListItem>
                                                <asp:ListItem Value="SV" Text='<%$CartCode: Utils.GetResourceString("SV")%>'></asp:ListItem>
                                                <asp:ListItem Value="CI" Text='<%$CartCode: Utils.GetResourceString("CI")%>'></asp:ListItem>
                                                <asp:ListItem Value="ER" Text='<%$CartCode: Utils.GetResourceString("ER")%>'></asp:ListItem>
                                                <asp:ListItem Value="EE" Text='<%$CartCode: Utils.GetResourceString("EE")%>'></asp:ListItem>
                                                <asp:ListItem Value="ET" Text='<%$CartCode: Utils.GetResourceString("ET")%>'></asp:ListItem>
                                                <asp:ListItem Value="FK" Text='<%$CartCode: Utils.GetResourceString("FK")%>'></asp:ListItem>
                                                <asp:ListItem Value="FJ" Text='<%$CartCode: Utils.GetResourceString("FJ")%>'></asp:ListItem>
                                                <asp:ListItem Value="PH" Text='<%$CartCode: Utils.GetResourceString("PH")%>'></asp:ListItem>
                                                <asp:ListItem Value="FI" Text='<%$CartCode: Utils.GetResourceString("FI")%>'></asp:ListItem>
                                                <asp:ListItem Value="AE" Text='<%$CartCode: Utils.GetResourceString("AE")%>'></asp:ListItem>
                                                <asp:ListItem Value="FR" Text='<%$CartCode: Utils.GetResourceString("FR")%>'></asp:ListItem>
                                                <asp:ListItem Value="GF" Text='<%$CartCode: Utils.GetResourceString("GF")%>'></asp:ListItem>
                                                <asp:ListItem Value="PF" Text='<%$CartCode: Utils.GetResourceString("PF")%>'></asp:ListItem>
                                                <asp:ListItem Value="GA" Text='<%$CartCode: Utils.GetResourceString("GA")%>'></asp:ListItem>
                                                <asp:ListItem Value="GM" Text='<%$CartCode: Utils.GetResourceString("GM")%>'></asp:ListItem>
                                                <asp:ListItem Value="GE" Text='<%$CartCode: Utils.GetResourceString("GE")%>'></asp:ListItem>
                                                <asp:ListItem Value="GH" Text='<%$CartCode: Utils.GetResourceString("GH")%>'></asp:ListItem>
                                                <asp:ListItem Value="GI" Text='<%$CartCode: Utils.GetResourceString("GI")%>'></asp:ListItem>
                                                <asp:ListItem Value="GR" Text='<%$CartCode: Utils.GetResourceString("GR")%>'></asp:ListItem>
                                                <asp:ListItem Value="GD" Text='<%$CartCode: Utils.GetResourceString("GD")%>'></asp:ListItem>
                                                <asp:ListItem Value="GP" Text='<%$CartCode: Utils.GetResourceString("GP")%>'></asp:ListItem>
                                                <asp:ListItem Value="GU" Text='<%$CartCode: Utils.GetResourceString("GU")%>'></asp:ListItem>
                                                <asp:ListItem Value="GT" Text='<%$CartCode: Utils.GetResourceString("GT")%>'></asp:ListItem>
                                                <asp:ListItem Value="GN" Text='<%$CartCode: Utils.GetResourceString("GN")%>'></asp:ListItem>
                                                <asp:ListItem Value="GW" Text='<%$CartCode: Utils.GetResourceString("GW")%>'></asp:ListItem>
                                                <asp:ListItem Value="GY" Text='<%$CartCode: Utils.GetResourceString("GY")%>'></asp:ListItem>
                                                <asp:ListItem Value="HT" Text='<%$CartCode: Utils.GetResourceString("HT")%>'></asp:ListItem>
                                                <asp:ListItem Value="HN" Text='<%$CartCode: Utils.GetResourceString("HN")%>'></asp:ListItem>
                                                <asp:ListItem Value="HK" Text='<%$CartCode: Utils.GetResourceString("HK")%>'></asp:ListItem>
                                                <asp:ListItem Value="IN" Text='<%$CartCode: Utils.GetResourceString("IN")%>'></asp:ListItem>
                                                <asp:ListItem Value="ID" Text='<%$CartCode: Utils.GetResourceString("ID")%>'></asp:ListItem>
                                                <asp:ListItem Value="IQ" Text='<%$CartCode: Utils.GetResourceString("IQ")%>'></asp:ListItem>
                                                <asp:ListItem Value="IR" Text='<%$CartCode: Utils.GetResourceString("IR")%>'></asp:ListItem>
                                                <asp:ListItem Value="IE" Text='<%$CartCode: Utils.GetResourceString("IE")%>'></asp:ListItem>
                                                <asp:ListItem Value="IS" Text='<%$CartCode: Utils.GetResourceString("IS")%>'></asp:ListItem>
                                                <asp:ListItem Value="IL" Text='<%$CartCode: Utils.GetResourceString("IL")%>'></asp:ListItem>
                                                <asp:ListItem Value="IT" Text='<%$CartCode: Utils.GetResourceString("IT")%>'></asp:ListItem>
                                                <asp:ListItem Value="JM" Text='<%$CartCode: Utils.GetResourceString("JM")%>'></asp:ListItem>
                                                <asp:ListItem Value="JP" Text='<%$CartCode: Utils.GetResourceString("JP")%>'></asp:ListItem>
                                                <asp:ListItem Value="YE" Text='<%$CartCode: Utils.GetResourceString("YE")%>'></asp:ListItem>
                                                <asp:ListItem Value="JO" Text='<%$CartCode: Utils.GetResourceString("JO")%>'></asp:ListItem>
                                                <asp:ListItem Value="VG" Text='<%$CartCode: Utils.GetResourceString("VG")%>'></asp:ListItem>
                                                <asp:ListItem Value="VI" Text='<%$CartCode: Utils.GetResourceString("VI")%>'></asp:ListItem>
                                                <asp:ListItem Value="KH" Text='<%$CartCode: Utils.GetResourceString("KH")%>'></asp:ListItem>
                                                <asp:ListItem Value="CM" Text='<%$CartCode: Utils.GetResourceString("CM")%>'></asp:ListItem>
                                                <asp:ListItem Value="CA" Text='<%$CartCode: Utils.GetResourceString("CA")%>'></asp:ListItem>
                                                <asp:ListItem Value="CV" Text='<%$CartCode: Utils.GetResourceString("CV")%>'></asp:ListItem>
                                                <asp:ListItem Value="KZ" Text='<%$CartCode: Utils.GetResourceString("KZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="KE" Text='<%$CartCode: Utils.GetResourceString("KE")%>'></asp:ListItem>
                                                <asp:ListItem Value="CN" Text='<%$CartCode: Utils.GetResourceString("CN")%>'></asp:ListItem>
                                                <asp:ListItem Value="KG" Text='<%$CartCode: Utils.GetResourceString("KG")%>'></asp:ListItem>
                                                <asp:ListItem Value="KI" Text='<%$CartCode: Utils.GetResourceString("KI")%>'></asp:ListItem>
                                                <asp:ListItem Value="KM" Text='<%$CartCode: Utils.GetResourceString("KM")%>'></asp:ListItem>
                                                <asp:ListItem Value="CG" Text='<%$CartCode: Utils.GetResourceString("CG")%>'></asp:ListItem>
                                                <asp:ListItem Value="HR" Text='<%$CartCode: Utils.GetResourceString("HR")%>'></asp:ListItem>
                                                <asp:ListItem Value="CU" Text='<%$CartCode: Utils.GetResourceString("CU")%>'></asp:ListItem>
                                                <asp:ListItem Value="KW" Text='<%$CartCode: Utils.GetResourceString("KW")%>'></asp:ListItem>
                                                <asp:ListItem Value="LA" Text='<%$CartCode: Utils.GetResourceString("LA")%>'></asp:ListItem>
                                                <asp:ListItem Value="LS" Text='<%$CartCode: Utils.GetResourceString("LS")%>'></asp:ListItem>
                                                <asp:ListItem Value="LV" Text='<%$CartCode: Utils.GetResourceString("LV")%>'></asp:ListItem>
                                                <asp:ListItem Value="LB" Text='<%$CartCode: Utils.GetResourceString("LB")%>'></asp:ListItem>
                                                <asp:ListItem Value="LR" Text='<%$CartCode: Utils.GetResourceString("LR")%>'></asp:ListItem>
                                                <asp:ListItem Value="LY" Text='<%$CartCode: Utils.GetResourceString("LY")%>'></asp:ListItem>
                                                <asp:ListItem Value="LI" Text='<%$CartCode: Utils.GetResourceString("LI")%>'></asp:ListItem>
                                                <asp:ListItem Value="LT" Text='<%$CartCode: Utils.GetResourceString("LT")%>'></asp:ListItem>
                                                <asp:ListItem Value="LU" Text='<%$CartCode: Utils.GetResourceString("LU")%>'></asp:ListItem>
                                                <asp:ListItem Value="MO" Text='<%$CartCode: Utils.GetResourceString("MO")%>'></asp:ListItem>
                                                <asp:ListItem Value="MG" Text='<%$CartCode: Utils.GetResourceString("MG")%>'></asp:ListItem>
                                                <asp:ListItem Value="MK" Text='<%$CartCode: Utils.GetResourceString("MK")%>'></asp:ListItem>
                                                <asp:ListItem Value="MW" Text='<%$CartCode: Utils.GetResourceString("MW")%>'></asp:ListItem>
                                                <asp:ListItem Value="MY" Text='<%$CartCode: Utils.GetResourceString("MY")%>'></asp:ListItem>
                                                <asp:ListItem Value="MV" Text='<%$CartCode: Utils.GetResourceString("MV")%>'></asp:ListItem>
                                                <asp:ListItem Value="ML" Text='<%$CartCode: Utils.GetResourceString("ML")%>'></asp:ListItem>
                                                <asp:ListItem Value="MT" Text='<%$CartCode: Utils.GetResourceString("MT")%>'></asp:ListItem>
                                                <asp:ListItem Value="MA" Text='<%$CartCode: Utils.GetResourceString("MA")%>'></asp:ListItem>
                                                <asp:ListItem Value="MH" Text='<%$CartCode: Utils.GetResourceString("MH")%>'></asp:ListItem>
                                                <asp:ListItem Value="MQ" Text='<%$CartCode: Utils.GetResourceString("MQ")%>'></asp:ListItem>
                                                <asp:ListItem Value="MR" Text='<%$CartCode: Utils.GetResourceString("MR")%>'></asp:ListItem>
                                                <asp:ListItem Value="MU" Text='<%$CartCode: Utils.GetResourceString("MU")%>'></asp:ListItem>
                                                <asp:ListItem Value="YT" Text='<%$CartCode: Utils.GetResourceString("YT")%>'></asp:ListItem>
                                                <asp:ListItem Value="MX" Text='<%$CartCode: Utils.GetResourceString("MX")%>'></asp:ListItem>
                                                <asp:ListItem Value="FM" Text='<%$CartCode: Utils.GetResourceString("FM")%>'></asp:ListItem>
                                                <asp:ListItem Value="MZ" Text='<%$CartCode: Utils.GetResourceString("MZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="MD" Text='<%$CartCode: Utils.GetResourceString("MD")%>'></asp:ListItem>
                                                <asp:ListItem Value="MC" Text='<%$CartCode: Utils.GetResourceString("MC")%>'></asp:ListItem>
                                                <asp:ListItem Value="MN" Text='<%$CartCode: Utils.GetResourceString("MN")%>'></asp:ListItem>
                                                <asp:ListItem Value="ME" Text='<%$CartCode: Utils.GetResourceString("ME")%>'></asp:ListItem>
                                                <asp:ListItem Value="MS" Text='<%$CartCode: Utils.GetResourceString("MS")%>'></asp:ListItem>
                                                <asp:ListItem Value="MM" Text='<%$CartCode: Utils.GetResourceString("MM")%>'></asp:ListItem>
                                                <asp:ListItem Value="NA" Text='<%$CartCode: Utils.GetResourceString("NA")%>'></asp:ListItem>
                                                <asp:ListItem Value="NR" Text='<%$CartCode: Utils.GetResourceString("NR")%>'></asp:ListItem>
                                                <asp:ListItem Value="NL" Text='<%$CartCode: Utils.GetResourceString("NL")%>'></asp:ListItem>
                                                <asp:ListItem Value="AN" Text='<%$CartCode: Utils.GetResourceString("AN")%>'></asp:ListItem>
                                                <asp:ListItem Value="NP" Text='<%$CartCode: Utils.GetResourceString("NP")%>'></asp:ListItem>
                                                <asp:ListItem Value="NI" Text='<%$CartCode: Utils.GetResourceString("NI")%>'></asp:ListItem>
                                                <asp:ListItem Value="NE" Text='<%$CartCode: Utils.GetResourceString("NE")%>'></asp:ListItem>
                                                <asp:ListItem Value="NG" Text='<%$CartCode: Utils.GetResourceString("NG")%>'></asp:ListItem>
                                                <asp:ListItem Value="KP" Text='<%$CartCode: Utils.GetResourceString("KP")%>'></asp:ListItem>
                                                <asp:ListItem Value="MP" Text='<%$CartCode: Utils.GetResourceString("MP")%>'></asp:ListItem>
                                                <asp:ListItem Value="NO" Text='<%$CartCode: Utils.GetResourceString("NO")%>'></asp:ListItem>
                                                <asp:ListItem Value="NC" Text='<%$CartCode: Utils.GetResourceString("NC")%>'></asp:ListItem>
                                                <asp:ListItem Value="NZ" Text='<%$CartCode: Utils.GetResourceString("NZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="OM" Text='<%$CartCode: Utils.GetResourceString("OM")%>'></asp:ListItem>
                                                <asp:ListItem Value="AT" Text='<%$CartCode: Utils.GetResourceString("AT")%>'></asp:ListItem>
                                                <asp:ListItem Value="TP" Text='<%$CartCode: Utils.GetResourceString("TP")%>'></asp:ListItem>
                                                <asp:ListItem Value="PK" Text='<%$CartCode: Utils.GetResourceString("PK")%>'></asp:ListItem>
                                                <asp:ListItem Value="PW" Text='<%$CartCode: Utils.GetResourceString("PW")%>'></asp:ListItem>
                                                <asp:ListItem Value="PA" Text='<%$CartCode: Utils.GetResourceString("PA")%>'></asp:ListItem>
                                                <asp:ListItem Value="PG" Text='<%$CartCode: Utils.GetResourceString("PG")%>'></asp:ListItem>
                                                <asp:ListItem Value="PY" Text='<%$CartCode: Utils.GetResourceString("PY")%>'></asp:ListItem>
                                                <asp:ListItem Value="PE" Text='<%$CartCode: Utils.GetResourceString("PE")%>'></asp:ListItem>
                                                <asp:ListItem Value="PN" Text='<%$CartCode: Utils.GetResourceString("PN")%>'></asp:ListItem>
                                                <asp:ListItem Value="PL" Text='<%$CartCode: Utils.GetResourceString("PL")%>'></asp:ListItem>
                                                <asp:ListItem Value="PT" Text='<%$CartCode: Utils.GetResourceString("PT")%>'></asp:ListItem>
                                                <asp:ListItem Value="PR" Text='<%$CartCode: Utils.GetResourceString("PR")%>'></asp:ListItem>
                                                <asp:ListItem Value="QA" Text='<%$CartCode: Utils.GetResourceString("QA")%>'></asp:ListItem>
                                                <asp:ListItem Value="RE" Text='<%$CartCode: Utils.GetResourceString("RE")%>'></asp:ListItem>
                                                <asp:ListItem Value="RO" Text='<%$CartCode: Utils.GetResourceString("RO")%>'></asp:ListItem>
                                                <asp:ListItem Value="RW" Text='<%$CartCode: Utils.GetResourceString("RW")%>'></asp:ListItem>
                                                <asp:ListItem Value="RU" Text='<%$CartCode: Utils.GetResourceString("RU")%>'></asp:ListItem>
                                                <asp:ListItem Value="SH" Text='<%$CartCode: Utils.GetResourceString("SH")%>'></asp:ListItem>
                                                <asp:ListItem Value="KN" Text='<%$CartCode: Utils.GetResourceString("KN")%>'></asp:ListItem>
                                                <asp:ListItem Value="LC" Text='<%$CartCode: Utils.GetResourceString("LC")%>'></asp:ListItem>
                                                <asp:ListItem Value="PM" Text='<%$CartCode: Utils.GetResourceString("PM")%>'></asp:ListItem>
                                                <asp:ListItem Value="VC" Text='<%$CartCode: Utils.GetResourceString("VC")%>'></asp:ListItem>
                                                <asp:ListItem Value="SB" Text='<%$CartCode: Utils.GetResourceString("SB")%>'></asp:ListItem>
                                                <asp:ListItem Value="WS" Text='<%$CartCode: Utils.GetResourceString("WS")%>'></asp:ListItem>
                                                <asp:ListItem Value="SM" Text='<%$CartCode: Utils.GetResourceString("SM")%>'></asp:ListItem>
                                                <asp:ListItem Value="ST" Text='<%$CartCode: Utils.GetResourceString("ST")%>'></asp:ListItem>
                                                <asp:ListItem Value="SA" Text='<%$CartCode: Utils.GetResourceString("SA")%>'></asp:ListItem>
                                                <asp:ListItem Value="CH" Text='<%$CartCode: Utils.GetResourceString("CH")%>'></asp:ListItem>
                                                <asp:ListItem Value="SN" Text='<%$CartCode: Utils.GetResourceString("SN")%>'></asp:ListItem>
                                                <asp:ListItem Value="RS" Text='<%$CartCode: Utils.GetResourceString("RS")%>'></asp:ListItem>
                                                <asp:ListItem Value="SC" Text='<%$CartCode: Utils.GetResourceString("SC")%>'></asp:ListItem>
                                                <asp:ListItem Value="SL" Text='<%$CartCode: Utils.GetResourceString("SL")%>'></asp:ListItem>
                                                <asp:ListItem Value="SG" Text='<%$CartCode: Utils.GetResourceString("SG")%>'></asp:ListItem>
                                                <asp:ListItem Value="SK" Text='<%$CartCode: Utils.GetResourceString("SK")%>'></asp:ListItem>
                                                <asp:ListItem Value="SI" Text='<%$CartCode: Utils.GetResourceString("SI")%>'></asp:ListItem>
                                                <asp:ListItem Value="SO" Text='<%$CartCode: Utils.GetResourceString("SO")%>'></asp:ListItem>
                                                <asp:ListItem Value="ES" Text='<%$CartCode: Utils.GetResourceString("ES")%>'></asp:ListItem>
                                                <asp:ListItem Value="LK" Text='<%$CartCode: Utils.GetResourceString("LK")%>'></asp:ListItem>
                                                <asp:ListItem Value="UK" Text='<%$CartCode: Utils.GetResourceString("UK")%>'></asp:ListItem>
                                                <asp:ListItem Value="SD" Text='<%$CartCode: Utils.GetResourceString("SD")%>'></asp:ListItem>
                                                <asp:ListItem Value="SR" Text='<%$CartCode: Utils.GetResourceString("SR")%>'></asp:ListItem>
                                                <asp:ListItem Value="SE" Text='<%$CartCode: Utils.GetResourceString("SE")%>' Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="SZ" Text='<%$CartCode: Utils.GetResourceString("SZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="ZA" Text='<%$CartCode: Utils.GetResourceString("ZA")%>'></asp:ListItem>
                                                <asp:ListItem Value="KR" Text='<%$CartCode: Utils.GetResourceString("KR")%>'></asp:ListItem>
                                                <asp:ListItem Value="SY" Text='<%$CartCode: Utils.GetResourceString("SY")%>'></asp:ListItem>
                                                <asp:ListItem Value="TJ" Text='<%$CartCode: Utils.GetResourceString("TJ")%>'></asp:ListItem>
                                                <asp:ListItem Value="TW" Text='<%$CartCode: Utils.GetResourceString("TW")%>'></asp:ListItem>
                                                <asp:ListItem Value="TZ" Text='<%$CartCode: Utils.GetResourceString("TZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="TD" Text='<%$CartCode: Utils.GetResourceString("TD")%>'></asp:ListItem>
                                                <asp:ListItem Value="TH" Text='<%$CartCode: Utils.GetResourceString("TH")%>'></asp:ListItem>
                                                <asp:ListItem Value="CZ" Text='<%$CartCode: Utils.GetResourceString("CZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="TG" Text='<%$CartCode: Utils.GetResourceString("TG")%>'></asp:ListItem>
                                                <asp:ListItem Value="TO" Text='<%$CartCode: Utils.GetResourceString("TO")%>'></asp:ListItem>
                                                <asp:ListItem Value="TT" Text='<%$CartCode: Utils.GetResourceString("TT")%>'></asp:ListItem>
                                                <asp:ListItem Value="TN" Text='<%$CartCode: Utils.GetResourceString("TN")%>'></asp:ListItem>
                                                <asp:ListItem Value="TR" Text='<%$CartCode: Utils.GetResourceString("TR")%>'></asp:ListItem>
                                                <asp:ListItem Value="TM" Text='<%$CartCode: Utils.GetResourceString("TM")%>'></asp:ListItem>
                                                <asp:ListItem Value="TC" Text='<%$CartCode: Utils.GetResourceString("TC")%>'></asp:ListItem>
                                                <asp:ListItem Value="TV" Text='<%$CartCode: Utils.GetResourceString("TV")%>'></asp:ListItem>
                                                <asp:ListItem Value="DE" Text='<%$CartCode: Utils.GetResourceString("DE")%>'></asp:ListItem>
                                                <asp:ListItem Value="UG" Text='<%$CartCode: Utils.GetResourceString("UG")%>'></asp:ListItem>
                                                <asp:ListItem Value="UA" Text='<%$CartCode: Utils.GetResourceString("UA")%>'></asp:ListItem>
                                                <asp:ListItem Value="HU" Text='<%$CartCode: Utils.GetResourceString("HU")%>'></asp:ListItem>
                                                <asp:ListItem Value="UY" Text='<%$CartCode: Utils.GetResourceString("UY")%>'></asp:ListItem>
                                                <asp:ListItem Value="US" Text='<%$CartCode: Utils.GetResourceString("US")%>'></asp:ListItem>
                                                <asp:ListItem Value="UZ" Text='<%$CartCode: Utils.GetResourceString("UZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="VU" Text='<%$CartCode: Utils.GetResourceString("VU")%>'></asp:ListItem>
                                                <asp:ListItem Value="VA" Text='<%$CartCode: Utils.GetResourceString("VA")%>'></asp:ListItem>
                                                <asp:ListItem Value="VE" Text='<%$CartCode: Utils.GetResourceString("VE")%>'></asp:ListItem>
                                                <asp:ListItem Value="VN" Text='<%$CartCode: Utils.GetResourceString("VN")%>'></asp:ListItem>
                                                <asp:ListItem Value="BY" Text='<%$CartCode: Utils.GetResourceString("BY")%>'></asp:ListItem>
                                                <asp:ListItem Value="WF" Text='<%$CartCode: Utils.GetResourceString("WF")%>'></asp:ListItem>
                                                <asp:ListItem Value="ZM" Text='<%$CartCode: Utils.GetResourceString("ZM")%>'></asp:ListItem>
                                                <asp:ListItem Value="ZW" Text='<%$CartCode: Utils.GetResourceString("ZW")%>'></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtMobileNumber" CssClass="mobileidno  enablestate" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="disclaimer">
                                        <asp:Literal runat="server" ID="MobileDisclaimerLabel" Text='<%$CartCode: Utils.GetResourceString("Mobile_Disclaimer_Label")%>' />
                                    </div>
                                </div>
                                <div class="save-section">
                                    <div id="NewPassword" class="pword" runat="server">
                                        <div class="pword-lbl">
                                            <asp:Literal runat="server" ID="NewPasswordLabel" Text='<%$CartCode: Utils.GetResourceString("Enter_Password_Label")%>' />:
                                        </div>
                                        <div class="pword-tbox">
                                            <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="save-info-lbl">
                                        <div>
                                            <asp:Literal runat="server" ID="SaveOptionLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_Save_Option_Label")%>' /></div>
                                    </div>
                                    <div class="save-details">
                                        <PostenWebControls:PostenButton ID="btnSave" runat="server" Text='<%$CartCode: Utils.GetResourceString("Save_Label")%>'
                                            CssClass="btn-Save validatesavedetails" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  3049 - [Posten] The "I want to subscribe to Postens Newsletter" section should be removed.
                        <div class="content common">
                            <div class="delivery-n-subscribe">
                                <div class="subscribe">
                                    <asp:CheckBox ID="chkSubscription" runat="server" Text='<%$CartCode: Utils.GetResourceString("Subscribe_NewsLetter_Label")%>' />
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="footer common">
                        </div>
                        <div class="edge bottom">
                            <div class="left border">
                            </div>
                            <div class="right border">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="colspan-2 webpartzone">
            <div id="MiddleColumnZone">
                <div id="MiddleColumnContent">
                    <div>
                        <div class="module common ">
                            <div class="edge top">
                                <div class="left border">
                                </div>
                                <div class="right border">
                                </div>
                            </div>
                            <div class="header common">
                                <span>
                                    <asp:Literal runat="server" ID="PaymentTypeLabel" Text='<%$CartCode: Utils.GetResourceString("CheckOut_PaymentType_Label")%>' />
                                </span>
                            </div>
                            <div class="content common">
                                <div class="vsummary">
                                    <div class="secondcolumnerrorsummary">
                                        <span><%= Utils.GetResourceString("2nd_column_validation_summary") %></span>
                                    </div>
                                </div>
                                <div class="pay-type-lbl">
                                    <asp:Literal runat="server" ID="PayTypeLabel" Text='<%$CartCode: Utils.GetResourceString("Pay_Type_Label")%>' />
                                    <a id="deliveryhelp-target-1" class="delivery-img" type="text"></a>
                                </div>
                                <div class="inv-rbutton payment-order" id="InvoicePayment" runat="server" visible="false">
                                    <asp:RadioButton GroupName="GroupPaymentTypes" ID="radioButtonInvoicePayment" runat="server" />
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/_layouts/images/Posten/Cart/Klarna_icon.png"
                                        ImageAlign="Middle" />
                                    <br />
                                    <asp:Label ID="LoginDescription" runat="server" Text='<%$CartCode: Utils.GetResourceString("Invoice_Required_Login_Text") %>'
                                        Visible="false" CssClass="login-label" />
                                    <div id="invoicepaymentmessage" style="display: none" class="login-label">
                                    </div>
                                    <div id="divFaktura" class="invoice" style="display: none;">
                                        <div class="inv-lbl">
                                            <label for="<%= txtSocialSecNo.ClientID  %>" id="civiclabelid">
                                                <b>
                                                    <asp:Literal runat="server" ID="CivicLabel" Text='<%$CartCode: Utils.GetResourceString("Civic_Label")%>' />:&nbsp;*</b></label>
                                        </div>
                                        <div class="inv-tbox">
                                            <asp:TextBox ID="txtSocialSecNo" CssClass="SocialSecNoTextBox enablestate" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="hidCompanyName" runat="server" />
                                        </div>
                                        <div id="inv-ssn-instruction">
                                            <asp:Literal runat="server" ID="Literal4" Text='<%$CartCode: Utils.GetResourceString("Invoice_SSN_Instruction_Label")%>' />
                                        </div>
                                        <div class="inv-btn">
                                            <PostenWebControls:PostenButton ID="btnGetAddress" CssClass="getLegalAddressButton"
                                                runat="server" Text='<%$CartCode: Utils.GetResourceString("Download_Data_Label")%>' />
                                        </div>
                                        <div class="addressSection scrollable" style="display: none">
                                            <div class="scrollbar">
                                                <div class="track">
                                                    <div class="thumb">
                                                        <div class="end"></div>
                                                    </div>
                                                </div>
                                            </div>
			                                <div class="viewport">
			                                    <div class="overview">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="instruction">
                                            <div id="shippingLabelHidden" class="instruction-lbl" style="display: none">
                                                <asp:Literal runat="server" ID="ShipToAddressLabel" Text='<%$CartCode: Utils.GetResourceString("Ship_To_Label")%>' />:
                                            </div>
                                            <div id="shippingLabelShow" class="instruction-lbl">
                                            </div>
                                            <div class="intsruction-desc">
                                                <asp:Literal runat="server" ID="InvoiceInstructionLabel" Text="&nbsp;" />
                                            </div>
                                        </div>
                                        <div id="contact_heading">
                                           <strong><asp:Literal runat="server" ID="Literal6" Text='<%$CartCode: Utils.GetResourceString("Contact_Label")%>' /></strong>
                                            <div id="contact_heading_line"></div>
                                        </div>
                                        <div id="ifname" class="mobile">
                                            <div class="mobile-lbl">
                                                <label for="<%= InvoiceFirstNameTextBox.ClientID  %>">
                                                    <asp:Literal runat="server" ID="Literal2" Text='<%$CartCode: Utils.GetResourceString("Customer_FirstName_Label")%>' />:&nbsp;*</label>
                                            </div>
                                            <div class="mobile-tbox">
                                                <asp:TextBox ID="InvoiceFirstNameTextBox" CssClass="invoicefname_tbox enablestate" runat="server" />
                                            </div>
                                        </div>
                                        <div id="ilname" class="mobile">
                                            <div class="mobile-lbl">
                                                <label for="<%= InvoiceLastNameTextBox.ClientID  %>">
                                                    <asp:Literal runat="server" ID="Literal3" Text='<%$CartCode: Utils.GetResourceString("Customer_SurName_Label")%>' />:&nbsp;*</label>
                                            </div>
                                            <div class="mobile-tbox">
                                                <asp:TextBox ID="InvoiceLastNameTextBox" CssClass="invoicelname_tbox enablestate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="mobile">
                                            <div class="mobile-lbl">
                                                <label for="<%= InvoiceMobileNoTextBox.ClientID  %>">
                                                    <asp:Literal runat="server" ID="MobileNoLabel" Text='<%$CartCode: Utils.GetResourceString("Mobile_Number_Label")%>' />:&nbsp;*</label>
                                            </div>
                                            <div class="mobile-tbox">
                                                <asp:TextBox ID="InvoiceMobileNoTextBox" CssClass="invoicemobile_tbox enablestate" runat="server" />
                                            </div>
                                            <div class="mobile-desc">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="payment-order" id="OnlinePayment" runat="server" visible="false">
                                    <asp:RadioButton GroupName="GroupPaymentTypes" ID="radioButtonOnlinePayment" runat="server" />&nbsp;
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/_layouts/images/Posten/Cart/u136.png"
                                        ImageAlign="Middle" /><br />
                                    <asp:Label ID="OnlinePaymentMessageLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("Invoice_Required_Login_Text") %>'
                                        Visible="false" CssClass="login-label" />
                                </div>
                                <div class="payment-order" id="InternetPayment" runat="server" visible="false">
                                    <asp:RadioButton GroupName="GroupPaymentTypes" ID="radioButtonInternetPayment" runat="server" /><br />
                                    <asp:Label ID="InternetPaymentMessageLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("Invoice_Required_Login_Text") %>'
                                        Visible="false" CssClass="login-label" />
                                </div>
                            </div>
                            <div class="content common" style="display: none;">
                                <div class="pay-type-lbl">
                                    <asp:Literal runat="server" ID="Literal1" Text='<%$CartCode: Utils.GetResourceString("Shipping_Address_Label")%>' />
                                </div>
                                <div class="sameadd-rbutton">
                                    <asp:RadioButton ID="chkSameBillingAddress" GroupName="sendtoaddress" runat="server"
                                        Text='<%$CartCode: Utils.GetResourceString("Same_As_Billing_Address_Label")%>'
                                        Checked="true" />
                                </div>
                                <div class="newadd-rbutton">
                                    <asp:RadioButton ID="chkNewDeliveryAddress" GroupName="sendtoaddress" runat="server"
                                        Text='<%$CartCode: Utils.GetResourceString("New_Address_Option_Label")%>' Checked="false" />
                                </div>
                                <div id="divNewAddress" class="newadd">
                                    <div class="newadd-lbls">
                                        <div>
                                            <asp:Literal runat="server" ID="NewCompanyNameLabel" Text='<%$CartCode: Utils.GetResourceString("Company_Name_Label")%>' />:&nbsp;</div>
                                        <div>
                                            <label for="<%= NewFirstNameTxt.ClientID %>">
                                                <asp:Literal runat="server" ID="NewFirstNameLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_FirstName_Label")%>' />:&nbsp;*</label></div>
                                        <div>
                                            <label for="<%= NewSurNameTxt.ClientID %>">
                                                <asp:Literal runat="server" ID="NewSurNameLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_SurName_Label")%>' />:&nbsp;*</label></div>
                                        <div>
                                            <label for="<%= NewAddressTxt.ClientID %>">
                                                <asp:Literal runat="server" ID="NewAddressLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_Address_Label")%>' />:&nbsp;*</label></div>
                                        <div>
                                            <label for="<%= NewPostalTxt.ClientID %>">
                                                <asp:Literal runat="server" ID="NewPostalLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_PostalCode_Label")%>' />:&nbsp;*</label></div>
                                        <div>
                                            <label for="<%= NewCityTxt.ClientID %>">
                                                <asp:Literal runat="server" ID="NewCityLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_City_Label")%>' />:&nbsp;*</label></div>
                                        <div>
                                            <label for="<%= NewCountryTxt.ClientID %>">
                                                <asp:Literal runat="server" ID="NewCountryLabel" Text='<%$CartCode: Utils.GetResourceString("Customer_Country_Label")%>' />:&nbsp;*</label></div>
                                        <div>
                                            <asp:Literal runat="server" ID="NewMobileNoLabel" Text='<%$CartCode: Utils.GetResourceString("Mobile_Number_Label")%>' />:</div>
                                    </div>
                                    <div class="newadd-tboxes">
                                        <div>
                                            <asp:TextBox ID="NewCompanyNameTxt" CssClass="new-company enablestate" runat="server"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="NewFirstNameTxt" CssClass="new-firstname enablestate" runat="server"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="NewSurNameTxt" CssClass="new-lastname enablestate" runat="server"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="NewAddressTxt" CssClass="new-address enablestate" runat="server"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="NewPostalTxt" CssClass="new-postal enablestate" runat="server"></asp:TextBox>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="NewCityTxt" CssClass="new-city enablestate" runat="server"></asp:TextBox>
                                        </div>
                                        <div>
                                            <!--<asp:ListItem Selected="True" Value="SE">Sverige</asp:ListItem>
                                            <asp:ListItem Value="DK">Danmark</asp:ListItem>
                                            <asp:ListItem Value="NO">Norge</asp:ListItem>
                                            <asp:ListItem Value="DE">Tyskland</asp:ListItem>-->
                                            <asp:DropDownList ID="NewCountryTxt" CssClass="new-country" runat="server">
                                                <asp:ListItem Value="AF" Text='<%$CartCode: Utils.GetResourceString("AF")%>'></asp:ListItem>
                                                <asp:ListItem Value="AL" Text='<%$CartCode: Utils.GetResourceString("AL")%>'></asp:ListItem>
                                                <asp:ListItem Value="DZ" Text='<%$CartCode: Utils.GetResourceString("DZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="AS" Text='<%$CartCode: Utils.GetResourceString("AS")%>'></asp:ListItem>
                                                <asp:ListItem Value="AD" Text='<%$CartCode: Utils.GetResourceString("AD")%>'></asp:ListItem>
                                                <asp:ListItem Value="AO" Text='<%$CartCode: Utils.GetResourceString("AO")%>'></asp:ListItem>
                                                <asp:ListItem Value="AI" Text='<%$CartCode: Utils.GetResourceString("AI")%>'></asp:ListItem>
                                                <asp:ListItem Value="AG" Text='<%$CartCode: Utils.GetResourceString("AG")%>'></asp:ListItem>
                                                <asp:ListItem Value="AR" Text='<%$CartCode: Utils.GetResourceString("AR")%>'></asp:ListItem>
                                                <asp:ListItem Value="AM" Text='<%$CartCode: Utils.GetResourceString("AM")%>'></asp:ListItem>
                                                <asp:ListItem Value="AW" Text='<%$CartCode: Utils.GetResourceString("AW")%>'></asp:ListItem>
                                                <asp:ListItem Value="AU" Text='<%$CartCode: Utils.GetResourceString("AU")%>'></asp:ListItem>
                                                <asp:ListItem Value="AZ" Text='<%$CartCode: Utils.GetResourceString("AZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="BS" Text='<%$CartCode: Utils.GetResourceString("BS")%>'></asp:ListItem>
                                                <asp:ListItem Value="BH" Text='<%$CartCode: Utils.GetResourceString("BH")%>'></asp:ListItem>
                                                <asp:ListItem Value="BD" Text='<%$CartCode: Utils.GetResourceString("BD")%>'></asp:ListItem>
                                                <asp:ListItem Value="BB" Text='<%$CartCode: Utils.GetResourceString("BB")%>'></asp:ListItem>
                                                <asp:ListItem Value="BE" Text='<%$CartCode: Utils.GetResourceString("BE")%>'></asp:ListItem>
                                                <asp:ListItem Value="BZ" Text='<%$CartCode: Utils.GetResourceString("BZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="BJ" Text='<%$CartCode: Utils.GetResourceString("BJ")%>'></asp:ListItem>
                                                <asp:ListItem Value="BM" Text='<%$CartCode: Utils.GetResourceString("BM")%>'></asp:ListItem>
                                                <asp:ListItem Value="BT" Text='<%$CartCode: Utils.GetResourceString("BT")%>'></asp:ListItem>
                                                <asp:ListItem Value="BO" Text='<%$CartCode: Utils.GetResourceString("BO")%>'></asp:ListItem>
                                                <asp:ListItem Value="BA" Text='<%$CartCode: Utils.GetResourceString("BA")%>'></asp:ListItem>
                                                <asp:ListItem Value="BW" Text='<%$CartCode: Utils.GetResourceString("BW")%>'></asp:ListItem>
                                                <asp:ListItem Value="BR" Text='<%$CartCode: Utils.GetResourceString("BR")%>'></asp:ListItem>
                                                <asp:ListItem Value="BN" Text='<%$CartCode: Utils.GetResourceString("BN")%>'></asp:ListItem>
                                                <asp:ListItem Value="BG" Text='<%$CartCode: Utils.GetResourceString("BG")%>'></asp:ListItem>
                                                <asp:ListItem Value="BF" Text='<%$CartCode: Utils.GetResourceString("BF")%>'></asp:ListItem>
                                                <asp:ListItem Value="BI" Text='<%$CartCode: Utils.GetResourceString("BI")%>'></asp:ListItem>
                                                <asp:ListItem Value="KY" Text='<%$CartCode: Utils.GetResourceString("KY")%>'></asp:ListItem>
                                                <asp:ListItem Value="CF" Text='<%$CartCode: Utils.GetResourceString("CF")%>'></asp:ListItem>
                                                <asp:ListItem Value="CL" Text='<%$CartCode: Utils.GetResourceString("CL")%>'></asp:ListItem>
                                                <asp:ListItem Value="CO" Text='<%$CartCode: Utils.GetResourceString("CO")%>'></asp:ListItem>
                                                <asp:ListItem Value="CK" Text='<%$CartCode: Utils.GetResourceString("CK")%>'></asp:ListItem>
                                                <asp:ListItem Value="CR" Text='<%$CartCode: Utils.GetResourceString("CR")%>'></asp:ListItem>
                                                <asp:ListItem Value="CY" Text='<%$CartCode: Utils.GetResourceString("CY")%>'></asp:ListItem>
                                                <asp:ListItem Value="DK" Text='<%$CartCode: Utils.GetResourceString("DK")%>'></asp:ListItem>
                                                <asp:ListItem Value="DJ" Text='<%$CartCode: Utils.GetResourceString("DJ")%>'></asp:ListItem>
                                                <asp:ListItem Value="CX" Text='<%$CartCode: Utils.GetResourceString("CX")%>'></asp:ListItem>
                                                <asp:ListItem Value="CC" Text='<%$CartCode: Utils.GetResourceString("CC")%>'></asp:ListItem>
                                                <asp:ListItem Value="EC" Text='<%$CartCode: Utils.GetResourceString("EC")%>'></asp:ListItem>
                                                <asp:ListItem Value="EG" Text='<%$CartCode: Utils.GetResourceString("EG")%>'></asp:ListItem>
                                                <asp:ListItem Value="GQ" Text='<%$CartCode: Utils.GetResourceString("GQ")%>'></asp:ListItem>
                                                <asp:ListItem Value="SV" Text='<%$CartCode: Utils.GetResourceString("SV")%>'></asp:ListItem>
                                                <asp:ListItem Value="CI" Text='<%$CartCode: Utils.GetResourceString("CI")%>'></asp:ListItem>
                                                <asp:ListItem Value="ER" Text='<%$CartCode: Utils.GetResourceString("ER")%>'></asp:ListItem>
                                                <asp:ListItem Value="EE" Text='<%$CartCode: Utils.GetResourceString("EE")%>'></asp:ListItem>
                                                <asp:ListItem Value="ET" Text='<%$CartCode: Utils.GetResourceString("ET")%>'></asp:ListItem>
                                                <asp:ListItem Value="FK" Text='<%$CartCode: Utils.GetResourceString("FK")%>'></asp:ListItem>
                                                <asp:ListItem Value="FJ" Text='<%$CartCode: Utils.GetResourceString("FJ")%>'></asp:ListItem>
                                                <asp:ListItem Value="PH" Text='<%$CartCode: Utils.GetResourceString("PH")%>'></asp:ListItem>
                                                <asp:ListItem Value="FI" Text='<%$CartCode: Utils.GetResourceString("FI")%>'></asp:ListItem>
                                                <asp:ListItem Value="AE" Text='<%$CartCode: Utils.GetResourceString("AE")%>'></asp:ListItem>
                                                <asp:ListItem Value="FR" Text='<%$CartCode: Utils.GetResourceString("FR")%>'></asp:ListItem>
                                                <asp:ListItem Value="GF" Text='<%$CartCode: Utils.GetResourceString("GF")%>'></asp:ListItem>
                                                <asp:ListItem Value="PF" Text='<%$CartCode: Utils.GetResourceString("PF")%>'></asp:ListItem>
                                                <asp:ListItem Value="GA" Text='<%$CartCode: Utils.GetResourceString("GA")%>'></asp:ListItem>
                                                <asp:ListItem Value="GM" Text='<%$CartCode: Utils.GetResourceString("GM")%>'></asp:ListItem>
                                                <asp:ListItem Value="GE" Text='<%$CartCode: Utils.GetResourceString("GE")%>'></asp:ListItem>
                                                <asp:ListItem Value="GH" Text='<%$CartCode: Utils.GetResourceString("GH")%>'></asp:ListItem>
                                                <asp:ListItem Value="GI" Text='<%$CartCode: Utils.GetResourceString("GI")%>'></asp:ListItem>
                                                <asp:ListItem Value="GR" Text='<%$CartCode: Utils.GetResourceString("GR")%>'></asp:ListItem>
                                                <asp:ListItem Value="GD" Text='<%$CartCode: Utils.GetResourceString("GD")%>'></asp:ListItem>
                                                <asp:ListItem Value="GP" Text='<%$CartCode: Utils.GetResourceString("GP")%>'></asp:ListItem>
                                                <asp:ListItem Value="GU" Text='<%$CartCode: Utils.GetResourceString("GU")%>'></asp:ListItem>
                                                <asp:ListItem Value="GT" Text='<%$CartCode: Utils.GetResourceString("GT")%>'></asp:ListItem>
                                                <asp:ListItem Value="GN" Text='<%$CartCode: Utils.GetResourceString("GN")%>'></asp:ListItem>
                                                <asp:ListItem Value="GW" Text='<%$CartCode: Utils.GetResourceString("GW")%>'></asp:ListItem>
                                                <asp:ListItem Value="GY" Text='<%$CartCode: Utils.GetResourceString("GY")%>'></asp:ListItem>
                                                <asp:ListItem Value="HT" Text='<%$CartCode: Utils.GetResourceString("HT")%>'></asp:ListItem>
                                                <asp:ListItem Value="HN" Text='<%$CartCode: Utils.GetResourceString("HN")%>'></asp:ListItem>
                                                <asp:ListItem Value="HK" Text='<%$CartCode: Utils.GetResourceString("HK")%>'></asp:ListItem>
                                                <asp:ListItem Value="IN" Text='<%$CartCode: Utils.GetResourceString("IN")%>'></asp:ListItem>
                                                <asp:ListItem Value="ID" Text='<%$CartCode: Utils.GetResourceString("ID")%>'></asp:ListItem>
                                                <asp:ListItem Value="IQ" Text='<%$CartCode: Utils.GetResourceString("IQ")%>'></asp:ListItem>
                                                <asp:ListItem Value="IR" Text='<%$CartCode: Utils.GetResourceString("IR")%>'></asp:ListItem>
                                                <asp:ListItem Value="IE" Text='<%$CartCode: Utils.GetResourceString("IE")%>'></asp:ListItem>
                                                <asp:ListItem Value="IS" Text='<%$CartCode: Utils.GetResourceString("IS")%>'></asp:ListItem>
                                                <asp:ListItem Value="IL" Text='<%$CartCode: Utils.GetResourceString("IL")%>'></asp:ListItem>
                                                <asp:ListItem Value="IT" Text='<%$CartCode: Utils.GetResourceString("IT")%>'></asp:ListItem>
                                                <asp:ListItem Value="JM" Text='<%$CartCode: Utils.GetResourceString("JM")%>'></asp:ListItem>
                                                <asp:ListItem Value="JP" Text='<%$CartCode: Utils.GetResourceString("JP")%>'></asp:ListItem>
                                                <asp:ListItem Value="YE" Text='<%$CartCode: Utils.GetResourceString("YE")%>'></asp:ListItem>
                                                <asp:ListItem Value="JO" Text='<%$CartCode: Utils.GetResourceString("JO")%>'></asp:ListItem>
                                                <asp:ListItem Value="VG" Text='<%$CartCode: Utils.GetResourceString("VG")%>'></asp:ListItem>
                                                <asp:ListItem Value="VI" Text='<%$CartCode: Utils.GetResourceString("VI")%>'></asp:ListItem>
                                                <asp:ListItem Value="KH" Text='<%$CartCode: Utils.GetResourceString("KH")%>'></asp:ListItem>
                                                <asp:ListItem Value="CM" Text='<%$CartCode: Utils.GetResourceString("CM")%>'></asp:ListItem>
                                                <asp:ListItem Value="CA" Text='<%$CartCode: Utils.GetResourceString("CA")%>'></asp:ListItem>
                                                <asp:ListItem Value="CV" Text='<%$CartCode: Utils.GetResourceString("CV")%>'></asp:ListItem>
                                                <asp:ListItem Value="KZ" Text='<%$CartCode: Utils.GetResourceString("KZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="KE" Text='<%$CartCode: Utils.GetResourceString("KE")%>'></asp:ListItem>
                                                <asp:ListItem Value="CN" Text='<%$CartCode: Utils.GetResourceString("CN")%>'></asp:ListItem>
                                                <asp:ListItem Value="KG" Text='<%$CartCode: Utils.GetResourceString("KG")%>'></asp:ListItem>
                                                <asp:ListItem Value="KI" Text='<%$CartCode: Utils.GetResourceString("KI")%>'></asp:ListItem>
                                                <asp:ListItem Value="KM" Text='<%$CartCode: Utils.GetResourceString("KM")%>'></asp:ListItem>
                                                <asp:ListItem Value="CG" Text='<%$CartCode: Utils.GetResourceString("CG")%>'></asp:ListItem>
                                                <asp:ListItem Value="HR" Text='<%$CartCode: Utils.GetResourceString("HR")%>'></asp:ListItem>
                                                <asp:ListItem Value="CU" Text='<%$CartCode: Utils.GetResourceString("CU")%>'></asp:ListItem>
                                                <asp:ListItem Value="KW" Text='<%$CartCode: Utils.GetResourceString("KW")%>'></asp:ListItem>
                                                <asp:ListItem Value="LA" Text='<%$CartCode: Utils.GetResourceString("LA")%>'></asp:ListItem>
                                                <asp:ListItem Value="LS" Text='<%$CartCode: Utils.GetResourceString("LS")%>'></asp:ListItem>
                                                <asp:ListItem Value="LV" Text='<%$CartCode: Utils.GetResourceString("LV")%>'></asp:ListItem>
                                                <asp:ListItem Value="LB" Text='<%$CartCode: Utils.GetResourceString("LB")%>'></asp:ListItem>
                                                <asp:ListItem Value="LR" Text='<%$CartCode: Utils.GetResourceString("LR")%>'></asp:ListItem>
                                                <asp:ListItem Value="LY" Text='<%$CartCode: Utils.GetResourceString("LY")%>'></asp:ListItem>
                                                <asp:ListItem Value="LI" Text='<%$CartCode: Utils.GetResourceString("LI")%>'></asp:ListItem>
                                                <asp:ListItem Value="LT" Text='<%$CartCode: Utils.GetResourceString("LT")%>'></asp:ListItem>
                                                <asp:ListItem Value="LU" Text='<%$CartCode: Utils.GetResourceString("LU")%>'></asp:ListItem>
                                                <asp:ListItem Value="MO" Text='<%$CartCode: Utils.GetResourceString("MO")%>'></asp:ListItem>
                                                <asp:ListItem Value="MG" Text='<%$CartCode: Utils.GetResourceString("MG")%>'></asp:ListItem>
                                                <asp:ListItem Value="MK" Text='<%$CartCode: Utils.GetResourceString("MK")%>'></asp:ListItem>
                                                <asp:ListItem Value="MW" Text='<%$CartCode: Utils.GetResourceString("MW")%>'></asp:ListItem>
                                                <asp:ListItem Value="MY" Text='<%$CartCode: Utils.GetResourceString("MY")%>'></asp:ListItem>
                                                <asp:ListItem Value="MV" Text='<%$CartCode: Utils.GetResourceString("MV")%>'></asp:ListItem>
                                                <asp:ListItem Value="ML" Text='<%$CartCode: Utils.GetResourceString("ML")%>'></asp:ListItem>
                                                <asp:ListItem Value="MT" Text='<%$CartCode: Utils.GetResourceString("MT")%>'></asp:ListItem>
                                                <asp:ListItem Value="MA" Text='<%$CartCode: Utils.GetResourceString("MA")%>'></asp:ListItem>
                                                <asp:ListItem Value="MH" Text='<%$CartCode: Utils.GetResourceString("MH")%>'></asp:ListItem>
                                                <asp:ListItem Value="MQ" Text='<%$CartCode: Utils.GetResourceString("MQ")%>'></asp:ListItem>
                                                <asp:ListItem Value="MR" Text='<%$CartCode: Utils.GetResourceString("MR")%>'></asp:ListItem>
                                                <asp:ListItem Value="MU" Text='<%$CartCode: Utils.GetResourceString("MU")%>'></asp:ListItem>
                                                <asp:ListItem Value="YT" Text='<%$CartCode: Utils.GetResourceString("YT")%>'></asp:ListItem>
                                                <asp:ListItem Value="MX" Text='<%$CartCode: Utils.GetResourceString("MX")%>'></asp:ListItem>
                                                <asp:ListItem Value="FM" Text='<%$CartCode: Utils.GetResourceString("FM")%>'></asp:ListItem>
                                                <asp:ListItem Value="MZ" Text='<%$CartCode: Utils.GetResourceString("MZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="MD" Text='<%$CartCode: Utils.GetResourceString("MD")%>'></asp:ListItem>
                                                <asp:ListItem Value="MC" Text='<%$CartCode: Utils.GetResourceString("MC")%>'></asp:ListItem>
                                                <asp:ListItem Value="MN" Text='<%$CartCode: Utils.GetResourceString("MN")%>'></asp:ListItem>
                                                <asp:ListItem Value="ME" Text='<%$CartCode: Utils.GetResourceString("ME")%>'></asp:ListItem>
                                                <asp:ListItem Value="MS" Text='<%$CartCode: Utils.GetResourceString("MS")%>'></asp:ListItem>
                                                <asp:ListItem Value="MM" Text='<%$CartCode: Utils.GetResourceString("MM")%>'></asp:ListItem>
                                                <asp:ListItem Value="NA" Text='<%$CartCode: Utils.GetResourceString("NA")%>'></asp:ListItem>
                                                <asp:ListItem Value="NR" Text='<%$CartCode: Utils.GetResourceString("NR")%>'></asp:ListItem>
                                                <asp:ListItem Value="NL" Text='<%$CartCode: Utils.GetResourceString("NL")%>'></asp:ListItem>
                                                <asp:ListItem Value="AN" Text='<%$CartCode: Utils.GetResourceString("AN")%>'></asp:ListItem>
                                                <asp:ListItem Value="NP" Text='<%$CartCode: Utils.GetResourceString("NP")%>'></asp:ListItem>
                                                <asp:ListItem Value="NI" Text='<%$CartCode: Utils.GetResourceString("NI")%>'></asp:ListItem>
                                                <asp:ListItem Value="NE" Text='<%$CartCode: Utils.GetResourceString("NE")%>'></asp:ListItem>
                                                <asp:ListItem Value="NG" Text='<%$CartCode: Utils.GetResourceString("NG")%>'></asp:ListItem>
                                                <asp:ListItem Value="KP" Text='<%$CartCode: Utils.GetResourceString("KP")%>'></asp:ListItem>
                                                <asp:ListItem Value="MP" Text='<%$CartCode: Utils.GetResourceString("MP")%>'></asp:ListItem>
                                                <asp:ListItem Value="NO" Text='<%$CartCode: Utils.GetResourceString("NO")%>'></asp:ListItem>
                                                <asp:ListItem Value="NC" Text='<%$CartCode: Utils.GetResourceString("NC")%>'></asp:ListItem>
                                                <asp:ListItem Value="NZ" Text='<%$CartCode: Utils.GetResourceString("NZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="OM" Text='<%$CartCode: Utils.GetResourceString("OM")%>'></asp:ListItem>
                                                <asp:ListItem Value="AT" Text='<%$CartCode: Utils.GetResourceString("AT")%>'></asp:ListItem>
                                                <asp:ListItem Value="TP" Text='<%$CartCode: Utils.GetResourceString("TP")%>'></asp:ListItem>
                                                <asp:ListItem Value="PK" Text='<%$CartCode: Utils.GetResourceString("PK")%>'></asp:ListItem>
                                                <asp:ListItem Value="PW" Text='<%$CartCode: Utils.GetResourceString("PW")%>'></asp:ListItem>
                                                <asp:ListItem Value="PA" Text='<%$CartCode: Utils.GetResourceString("PA")%>'></asp:ListItem>
                                                <asp:ListItem Value="PG" Text='<%$CartCode: Utils.GetResourceString("PG")%>'></asp:ListItem>
                                                <asp:ListItem Value="PY" Text='<%$CartCode: Utils.GetResourceString("PY")%>'></asp:ListItem>
                                                <asp:ListItem Value="PE" Text='<%$CartCode: Utils.GetResourceString("PE")%>'></asp:ListItem>
                                                <asp:ListItem Value="PN" Text='<%$CartCode: Utils.GetResourceString("PN")%>'></asp:ListItem>
                                                <asp:ListItem Value="PL" Text='<%$CartCode: Utils.GetResourceString("PL")%>'></asp:ListItem>
                                                <asp:ListItem Value="PT" Text='<%$CartCode: Utils.GetResourceString("PT")%>'></asp:ListItem>
                                                <asp:ListItem Value="PR" Text='<%$CartCode: Utils.GetResourceString("PR")%>'></asp:ListItem>
                                                <asp:ListItem Value="QA" Text='<%$CartCode: Utils.GetResourceString("QA")%>'></asp:ListItem>
                                                <asp:ListItem Value="RE" Text='<%$CartCode: Utils.GetResourceString("RE")%>'></asp:ListItem>
                                                <asp:ListItem Value="RO" Text='<%$CartCode: Utils.GetResourceString("RO")%>'></asp:ListItem>
                                                <asp:ListItem Value="RW" Text='<%$CartCode: Utils.GetResourceString("RW")%>'></asp:ListItem>
                                                <asp:ListItem Value="RU" Text='<%$CartCode: Utils.GetResourceString("RU")%>'></asp:ListItem>
                                                <asp:ListItem Value="SH" Text='<%$CartCode: Utils.GetResourceString("SH")%>'></asp:ListItem>
                                                <asp:ListItem Value="KN" Text='<%$CartCode: Utils.GetResourceString("KN")%>'></asp:ListItem>
                                                <asp:ListItem Value="LC" Text='<%$CartCode: Utils.GetResourceString("LC")%>'></asp:ListItem>
                                                <asp:ListItem Value="PM" Text='<%$CartCode: Utils.GetResourceString("PM")%>'></asp:ListItem>
                                                <asp:ListItem Value="VC" Text='<%$CartCode: Utils.GetResourceString("VC")%>'></asp:ListItem>
                                                <asp:ListItem Value="SB" Text='<%$CartCode: Utils.GetResourceString("SB")%>'></asp:ListItem>
                                                <asp:ListItem Value="WS" Text='<%$CartCode: Utils.GetResourceString("WS")%>'></asp:ListItem>
                                                <asp:ListItem Value="SM" Text='<%$CartCode: Utils.GetResourceString("SM")%>'></asp:ListItem>
                                                <asp:ListItem Value="ST" Text='<%$CartCode: Utils.GetResourceString("ST")%>'></asp:ListItem>
                                                <asp:ListItem Value="SA" Text='<%$CartCode: Utils.GetResourceString("SA")%>'></asp:ListItem>
                                                <asp:ListItem Value="CH" Text='<%$CartCode: Utils.GetResourceString("CH")%>'></asp:ListItem>
                                                <asp:ListItem Value="SN" Text='<%$CartCode: Utils.GetResourceString("SN")%>'></asp:ListItem>
                                                <asp:ListItem Value="RS" Text='<%$CartCode: Utils.GetResourceString("RS")%>'></asp:ListItem>
                                                <asp:ListItem Value="SC" Text='<%$CartCode: Utils.GetResourceString("SC")%>'></asp:ListItem>
                                                <asp:ListItem Value="SL" Text='<%$CartCode: Utils.GetResourceString("SL")%>'></asp:ListItem>
                                                <asp:ListItem Value="SG" Text='<%$CartCode: Utils.GetResourceString("SG")%>'></asp:ListItem>
                                                <asp:ListItem Value="SK" Text='<%$CartCode: Utils.GetResourceString("SK")%>'></asp:ListItem>
                                                <asp:ListItem Value="SI" Text='<%$CartCode: Utils.GetResourceString("SI")%>'></asp:ListItem>
                                                <asp:ListItem Value="SO" Text='<%$CartCode: Utils.GetResourceString("SO")%>'></asp:ListItem>
                                                <asp:ListItem Value="ES" Text='<%$CartCode: Utils.GetResourceString("ES")%>'></asp:ListItem>
                                                <asp:ListItem Value="LK" Text='<%$CartCode: Utils.GetResourceString("LK")%>'></asp:ListItem>
                                                <asp:ListItem Value="UK" Text='<%$CartCode: Utils.GetResourceString("UK")%>'></asp:ListItem>
                                                <asp:ListItem Value="SD" Text='<%$CartCode: Utils.GetResourceString("SD")%>'></asp:ListItem>
                                                <asp:ListItem Value="SR" Text='<%$CartCode: Utils.GetResourceString("SR")%>'></asp:ListItem>
                                                <asp:ListItem Value="SE" Text='<%$CartCode: Utils.GetResourceString("SE")%>' Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="SZ" Text='<%$CartCode: Utils.GetResourceString("SZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="ZA" Text='<%$CartCode: Utils.GetResourceString("ZA")%>'></asp:ListItem>
                                                <asp:ListItem Value="KR" Text='<%$CartCode: Utils.GetResourceString("KR")%>'></asp:ListItem>
                                                <asp:ListItem Value="SY" Text='<%$CartCode: Utils.GetResourceString("SY")%>'></asp:ListItem>
                                                <asp:ListItem Value="TJ" Text='<%$CartCode: Utils.GetResourceString("TJ")%>'></asp:ListItem>
                                                <asp:ListItem Value="TW" Text='<%$CartCode: Utils.GetResourceString("TW")%>'></asp:ListItem>
                                                <asp:ListItem Value="TZ" Text='<%$CartCode: Utils.GetResourceString("TZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="TD" Text='<%$CartCode: Utils.GetResourceString("TD")%>'></asp:ListItem>
                                                <asp:ListItem Value="TH" Text='<%$CartCode: Utils.GetResourceString("TH")%>'></asp:ListItem>
                                                <asp:ListItem Value="CZ" Text='<%$CartCode: Utils.GetResourceString("CZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="TG" Text='<%$CartCode: Utils.GetResourceString("TG")%>'></asp:ListItem>
                                                <asp:ListItem Value="TO" Text='<%$CartCode: Utils.GetResourceString("TO")%>'></asp:ListItem>
                                                <asp:ListItem Value="TT" Text='<%$CartCode: Utils.GetResourceString("TT")%>'></asp:ListItem>
                                                <asp:ListItem Value="TN" Text='<%$CartCode: Utils.GetResourceString("TN")%>'></asp:ListItem>
                                                <asp:ListItem Value="TR" Text='<%$CartCode: Utils.GetResourceString("TR")%>'></asp:ListItem>
                                                <asp:ListItem Value="TM" Text='<%$CartCode: Utils.GetResourceString("TM")%>'></asp:ListItem>
                                                <asp:ListItem Value="TC" Text='<%$CartCode: Utils.GetResourceString("TC")%>'></asp:ListItem>
                                                <asp:ListItem Value="TV" Text='<%$CartCode: Utils.GetResourceString("TV")%>'></asp:ListItem>
                                                <asp:ListItem Value="DE" Text='<%$CartCode: Utils.GetResourceString("DE")%>'></asp:ListItem>
                                                <asp:ListItem Value="UG" Text='<%$CartCode: Utils.GetResourceString("UG")%>'></asp:ListItem>
                                                <asp:ListItem Value="UA" Text='<%$CartCode: Utils.GetResourceString("UA")%>'></asp:ListItem>
                                                <asp:ListItem Value="HU" Text='<%$CartCode: Utils.GetResourceString("HU")%>'></asp:ListItem>
                                                <asp:ListItem Value="UY" Text='<%$CartCode: Utils.GetResourceString("UY")%>'></asp:ListItem>
                                                <asp:ListItem Value="US" Text='<%$CartCode: Utils.GetResourceString("US")%>'></asp:ListItem>
                                                <asp:ListItem Value="UZ" Text='<%$CartCode: Utils.GetResourceString("UZ")%>'></asp:ListItem>
                                                <asp:ListItem Value="VU" Text='<%$CartCode: Utils.GetResourceString("VU")%>'></asp:ListItem>
                                                <asp:ListItem Value="VA" Text='<%$CartCode: Utils.GetResourceString("VA")%>'></asp:ListItem>
                                                <asp:ListItem Value="VE" Text='<%$CartCode: Utils.GetResourceString("VE")%>'></asp:ListItem>
                                                <asp:ListItem Value="VN" Text='<%$CartCode: Utils.GetResourceString("VN")%>'></asp:ListItem>
                                                <asp:ListItem Value="BY" Text='<%$CartCode: Utils.GetResourceString("BY")%>'></asp:ListItem>
                                                <asp:ListItem Value="WF" Text='<%$CartCode: Utils.GetResourceString("WF")%>'></asp:ListItem>
                                                <asp:ListItem Value="ZM" Text='<%$CartCode: Utils.GetResourceString("ZM")%>'></asp:ListItem>
                                                <asp:ListItem Value="ZW" Text='<%$CartCode: Utils.GetResourceString("ZW")%>'></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="NewMobileNoTxt" CssClass="new-mobile_tbox enablestate" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="disclaimer">
                                        <asp:Literal runat="server" ID="MobileDisclaimerLabel1" Text='<%$CartCode: Utils.GetResourceString("Mobile_Disclaimer_Label")%>' />
                                    </div>
                                </div>
                                <div class="note">
                                    <asp:Literal runat="server" ID="InvoiceDisclaimerLabel" Text='<%$CartCode: Utils.GetResourceString("Invoice_Disclaimer_Label")%>' />
                                </div>
                            </div>
                            <div class="footer common">
                                <ul id="addressList">
                                </ul>
                            </div>
                            <div class="edge bottom">
                                <div class="left border">
                                </div>
                                <div class="right border">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="colspan-2 webpartzone">
            <div id="RightColumnZone">
                <div id="RightColumnZoneContent">
                    <div>
                        <div class="module common ">
                            <div class="edge top">
                                <div class="left border">
                                </div>
                                <div class="right border">
                                </div>
                            </div>
                            <div class="header common">
                                <span>
                                    <asp:Literal runat="server" ID="YourShoppingCartLabel" Text='<%$CartCode: Utils.GetResourceString("CheckOut_ShoppingCart_Label")%>' />
                                </span>
                            </div>
                            <div class="content common" style="padding: 0;">
                                <div>
                                    <uc:EditCart ID="EditCart1" runat="server" />
                                    <div style="clear: both;"></div>
                                </div>
                            </div>
                            
                            <div class="footer common">
                            </div>
                            <div class="edge bottom">
                                <div class="left border">
                                </div>
                                <div class="right border">
                                </div>
                            </div>
                        </div>
                        <div class="module common">
                            <div class="edge top">
                                <div class="left border">
                                </div>
                                <div class="right border">
                                </div>
                            </div>
                            <div class="header common">
                                <span>
                                    <%= Utils.GetResourceString("Cart_Total_Price_Label")%>
                                </span>
                            </div>
                            <div class="content common" style="padding: 0;">
                                <div class="promo-code">
                                    <div class="promo-lbl">
                                        <asp:Literal runat="server" ID="PromotionCodeLabel" Text='<%$CartCode: Utils.GetResourceString("CheckOut_PromotionCode_Label")%>' />:
                                    </div>
                                    <asp:TextBox ID="txtPromotionCode" runat="server" />
                                    <a href="#" id="HelpImageLink"></a>
                                </div>
                                <div class="fees"  style="display: none;">
                                    <div class="shipping" style="display: none;">
                                        <div class="shipping-lbl">
                                            <asp:Literal runat="server" ID="ChkShippingLabel" Text='<%$CartCode: Utils.GetResourceString("Shipping_Label")%>' />:
                                        </div>
                                        <div class="shipping-desc">
                                            <asp:Literal ID="CurrencyLabel" Text="" runat="server" />
                                            <asp:Literal runat="server" ID="AmountLeftForShipLabel" Text='<%$CartCode: Utils.GetResourceString("Amount_Left_For_Free_Shipping_Label")%>' />:
                                        </div>
                                    </div>
                                    <div class="shipping-amt" style="display: none;">
                                        <asp:Label ID="lblShipping" runat="server" />
                                    </div>
                                    <div class="InvoiceFee" style="display: none;">
                                        
                                    </div>
                                    <div class="InvoiceFee-amt" style="display: none;">
                                        
                                    </div>
                                </div>
                                <div class="savings" style="display: none;">
                                    <div class="savings-lbl">
                                        <asp:Label runat="server" ID="YouSaveLabel" Text='<%$CartCode: Utils.GetResourceString("You_Save_Label")%>' />:
                                    </div>
                                    <div class="savings-amt">
                                        <asp:Label ID="lblSavings" runat="server" />
                                    </div>
                                </div>
                                <div class="summary">
                                    <div class="total invoice_section">
                                        <div class="total-lbl">
                                            <asp:Literal runat="server" ID="InvoiceFeeLabel" Text='<%$CartCode: Utils.GetResourceString("InvoiceFee_Label")%>' />:</div>
                                        <div class="total-amt">
                                            <asp:Label ID="lblInvoiceFee" CssClass="InvoiceFee-value" runat="server" />
                                            kr</div>
                                    </div>
                                    <div class="total"><div class="total-lbl">&nbsp;</div><div class="total-amt">&nbsp;</div></div>
                                    <div class="total">
                                        <div class="total-lbl">
                                            <asp:Literal ID="CartTotalLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("Cart_Total_Label")%>' />
                                            (<asp:Literal ID="CartIncludeVatLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("Including_Vat_Label")%>' />):
                                        </div>
                                        <div class="total-amt">
                                            <asp:Label ID="lblBasketTotal" CssClass="lblBasketTotal" Text="0" runat="server"
                                                Font-Bold="True" /></div>
                                    </div>
                                    <div class="total">
                                        <div class="total-lbl">
                                            <%= Utils.GetResourceString("Confirmation_VAT_Label")%></div>
                                        <div class="total-amt">
                                            <asp:Label ID="lblVatValue" CssClass="lblVatValue" Text="0" runat="server" /></div>
                                    </div>
                                    <div class="total"><div class="total-lbl">&nbsp;</div><div class="total-amt">&nbsp;</div></div>
                                    <div class="total">
                                        <div class="total-payment-lbl">
                                            <%= Utils.GetResourceString("Cart_Total_Price_Label")%>:</div>
                                        <div class="total-payment">
                                            <asp:Label ID="lblToPay" CssClass="lblToPay" Text="0" runat="server"
                                                Font-Bold="True" />
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="confirmation">
                                    <input type="checkbox" id="chkAccept" runat="server" class="acceptance" /><label><%=Utils.GetResourceString("Accept_The_Post_Text") %></label>
                                    <div class="accept_error_message" style="display:none"><%=Utils.GetResourceString("ACCEPTANCE_ERROR_MESSAGE")%></div>
                                </div>
                                <div class="accept-link">
                                    <div class="inputWrapper button theme curve">
                                        <div class="edge top">
                                            <div class="left">
                                            </div>
                                            <div class="right">
                                            </div>
                                        </div>
                                        <div class="container">
                                            <asp:LinkButton runat="server" OnClick="ProcessCheckout" CssClass="lnkCheckOut validatecheckout"
                                                ID="lnkCheckOut" Text='<%$CartCode: Utils.GetResourceString("Proceed_To_Checkout_Label")%>' />
                                        </div>
                                        <div class="edge bottom">
                                            <div class="left">
                                            </div>
                                            <div class="right">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="nextpg-lbl">
                                    <span id="txtNextPage" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;
                                        font-style: italic">
                                        <asp:Literal ID="NextPageLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("View_Next_Page_Detail_Label")%>' />
                                    </span>
                                </div>
                            </div>
                            <div class="footer common">
                            </div>
                            <div class="edge bottom">
                                <div class="left border">
                                </div>
                                <div class="right border">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="BasketToCheckout" runat="server" />
<script type="text/javascript">
    $(document).ready(function () {
        var checkout_<%= this.ClientID %> = new checkout();              
        checkout_<%= this.ClientID %>.init('<%= Utils.GetResourceString("Proceed_Checkout_Online_Button_Label") %>',
                                            '<%= Utils.GetResourceString("Proceed_Checkout_Invoice_Button_Label")%>',
                                            '<%= Utils.GetResourceString("Proceed_Checkout_Internet_Button_Label")%>',
                                            '<%= Utils.GetResourceString("Proceed_Checkout_Online_Message_Text")%>',
                                            '<%= Utils.GetResourceString("Proceed_Checkout_Invoice_Message_Text") %>',
                                            '<%= Utils.GetResourceString("Proceed_Checkout_Internet_Message_Text") %>',
                                            '<%=Utils.GetResourceString("Civic_Label")%>',
                                            '<%=Utils.GetResourceString("Organisation_Label")%>',
                                            '<%= Utils.GetResourceString("Validation_General_Message_Text")%>',
                                            '<%= txtEmail.ClientID %>',
                                            '<%= txtFirstName.ClientID %>',
                                            '<%= txtLastName.ClientID %>',
                                            '<%= txtAddress.ClientID %>',
                                            '<%= txtPostalCode.ClientID %>',
                                            '<%= txtCity.ClientID %>',
                                            '<%= txtMobileNumber.ClientID %>',
                                            '<%= lnkCheckOut.ClientID %>',
                                            '<%= Utils.GetResourceString("PhoneNumberRegEx") %>',
                                            '<%= Utils.GetResourceString("PersonalNumberRegEx") %>',
                                            '<%=Utils.GetResourceString("ACCEPTANCE_ERROR_MESSAGE")%>',
                                            '<%= chkAccept.ClientID %>',
                                            '<%= txtMobileNumber.UniqueID %>',
                                            '<%= chkAccept.UniqueID %>',
                                            '<%= Utils.GetResourceString("Remove_Product_Error_Text") %>',
                                            '<%= Utils.GetResourceString("Remove_Service_Error_Text") %>',
                                            '<%= Utils.GetResourceString("Update_Product_Error_Text") %>',
                                            '<%= Utils.GetResourceString("Check_Email_Error_Text") %>',
                                            '<%= NewFirstNameTxt.ClientID %>',
                                            '<%= NewSurNameTxt.ClientID %>',
                                            '<%= NewAddressTxt.ClientID %>',
                                            '<%= NewPostalTxt.ClientID %>',
                                            '<%= NewCityTxt.ClientID %>',
                                            '<%= NewMobileNoTxt.ClientID %>',
                                            '<%= NewMobileNoTxt.UniqueID %>',
                                            '<%= Utils.GetResourceString("Cart_Error_Title") %>',
                                            '<%= Utils.GetResourceString("Postal_Code_Country_Error_Message_Text") %>',
                                            '<%= Utils.GetResourceString("Recalculate_Vat_Error_Message_Text") %>',
                                            '<%= btnSave.ClientID %>',
                                            '<%= txtPostalCode.UniqueID %>',
                                            '<%= Utils.GetResourceString("Same_As_Billing_Address_Label") %>',
                                            '<%= Utils.GetResourceString("Same_As_Invoice_Address_Label") %>',
                                            '<%= Utils.GetResourceString("CheckOut_Modal_Error_Message") %>',
                                            '<%= Utils.GetResourceString("CheckOut_Invalid_SSN_Error_Message") %>',
                                            '<%= Utils.GetResourceString("CheckOut_Invalid_CompanyNo_Error_Message") %>',
                                            '<%= Utils.GetResourceString("CheckOut_Invalid_Quantity_Error_Message") %>',
                                            '<%= Utils.GetResourceString("CheckOut_BillingAddress_Label") %>',
                                            '<%= Utils.GetResourceString("CheckOut_Select_Label") %>',
                                            '<%= Utils.GetResourceString("Cancel_PopUp_Button_Name") %>',
                                            '<%= Utils.GetResourceString("Close_PopUp_Button_Name") %>',
                                            '<%= Utils.GetResourceString("OK_PopUp_Button_Name") %>',
                                            '<%= Utils.GetResourceString("Remove_Service_Confirmation_Message") %>',
                                            '<%= Utils.GetResourceString("Remove_Service_Confirmation_Title") %>',
                                            '<%= Utils.GetResourceString("Invoice_Organization_Label") %>',
                                            '<%= GetCurrentUrl %>',
                                            '<%= Utils.GetResourceString("Invoice_SSN_Instruction_Label") %>',
                                            '<%= Utils.GetResourceString("Invoice_Organization_Instruction_Label") %>',
                                            '<%= Utils.GetResourceString("CheckOut_GetAddress_Button_Error_Message") %>',
                                            <%= MaxCartItemsinsidescroll %>);
    });

</script>
