﻿namespace Posten.Portal.Cart.Presentation.ControlTemplates
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Reflection;
    using System.Web;
    using System.Web.UI.WebControls;

    using Microsoft.SharePoint;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Presentation.ViewData;
    using Posten.Portal.Cart.Services;
    using Posten.Portal.Platform.Common.Container;
    using Posten.Portal.Platform.Common.Logging;

    using PostNord.Portal.Platform.Common.Utilities.ControlDefinitions;

    public partial class CheckOut : CheckoutBase
    {
        #region Properties

        protected override HiddenField BasketIdControl
        {
            get { return this.BasketToCheckout; }
        }

        protected override string BillingCountry
        {
            get { return ddlstCountry.SelectedValue; }
        }

        protected override IBillingAccountInfo BillingInfo
        {
            get
            {
                var retval = new BillingAccountInfo

                {
                    Name = BaseControl.CleanString(txtFirstName.Text),
                    AddressLine = BaseControl.CleanString(txtAddress.Text),
                    City = BaseControl.CleanString(txtCity.Text),
                    Email = BaseControl.CleanString(txtEmail.Text),
                    IncludeAddressInfo = true,
                    PostalCode = BaseControl.CleanString(txtPostalCode.Text),
                    Telephone = PhoneNumberControl.CleanString(txtMobileNumber.Text),
                    Organization = BaseControl.CleanString(txtCivicCompNo.Text),
                    LastName = BaseControl.CleanString(txtLastName.Text),
                    CountryName = ddlstCountry.SelectedItem.Text,
                    CountryCode = ddlstCountry.SelectedValue

                };

                if (RadioButtonList1.SelectedValue == "private")
                {
                    retval.Organization = String.Empty;
                }

                return retval;
            }
        }

        protected override IDeliveryAccountInfo DeliveryInfo
        {
            get
            {
                if (this.chkNewDeliveryAddress.Checked)
                {
                    return new DeliveryAccountInfo
                    {
                        FirstName = BaseControl.CleanString(NewFirstNameTxt.Text),
                        LastName = BaseControl.CleanString(NewSurNameTxt.Text),
                        CountryName = BaseControl.CleanString(NewCountryTxt.Text),
                        AddressLine = BaseControl.CleanString(NewAddressTxt.Text),
                        Description = "",
                        City = BaseControl.CleanString(NewCityTxt.Text),
                        CountryCode = NewCountryTxt.SelectedValue,
                        Email = BaseControl.CleanString(txtEmail.Text),
                        PostalCode = BaseControl.CleanString(NewPostalTxt.Text),
                        RegionCode = "",
                        RegionName = "",
                        Telephone = PhoneNumberControl.CleanString(NewMobileNoTxt.Text),
                        Organization = BaseControl.CleanString(NewCompanyNameTxt.Text)
                    };
                }
                else if (this.PaymentMethod == PaymentMethodType.Invoice)
                {
                    //if company get contact information from delivery which is set when a radiobutton for address is clicked.
                    var retval = new DeliveryAccountInfo
                    {
                        CountryName = BaseControl.CleanString(NewCountryTxt.Text),
                        AddressLine = BaseControl.CleanString(NewAddressTxt.Text),
                        Description = "",
                        City = BaseControl.CleanString(NewCityTxt.Text),
                        CountryCode = NewCountryTxt.SelectedValue,
                        Email = BaseControl.CleanString(txtEmail.Text),
                        PostalCode = BaseControl.CleanString(NewPostalTxt.Text),
                        RegionCode = "",
                        RegionName = "",
                        Telephone = PhoneNumberControl.CleanString(InvoiceMobileNoTextBox.Text.Replace("-", "").Replace(" ","")),  //NewMobileNoTxt.Text,
                        Organization = BaseControl.CleanString(NewCompanyNameTxt.Text)
                    };

                    if (RadioButtonList1.SelectedValue == "private")
                    {
                        retval.FirstName = BaseControl.CleanString(NewFirstNameTxt.Text);
                        retval.LastName = BaseControl.CleanString(NewSurNameTxt.Text);
                        retval.Organization = String.Empty;
                    }
                    else
                    {
                        retval.FirstName = BaseControl.CleanString(InvoiceFirstNameTextBox.Text);
                        retval.LastName = BaseControl.CleanString(InvoiceLastNameTextBox.Text);
                    }

                    return retval;

                }
                else
                {
                    var retval = new DeliveryAccountInfo
                    {
                        FirstName = BaseControl.CleanString(txtFirstName.Text),
                        LastName = BaseControl.CleanString(txtLastName.Text),
                        CountryName = ddlstCountry.SelectedItem.Text,
                        AddressLine = BaseControl.CleanString(txtAddress.Text),
                        Description = "",
                        City = BaseControl.CleanString(txtCity.Text),
                        CountryCode = ddlstCountry.SelectedItem.Value,
                        Email = BaseControl.CleanString(txtEmail.Text),
                        PostalCode = BaseControl.CleanString(txtPostalCode.Text),
                        RegionCode = "",
                        RegionName = "",
                        Telephone = PhoneNumberControl.CleanString(txtMobileNumber.Text),
                        Organization = BaseControl.CleanString(txtCivicCompNo.Text)
                    };

                    if (RadioButtonList1.SelectedValue == "private")
                    {
                        retval.Organization = String.Empty;
                    }

                    return retval;
                }
            }
        }

        protected override string FirstName
        {
            get { return BaseControl.CleanString(txtFirstName.Text); }
        }

        protected override string GetCurrentUrl
        {
            get
            {
                string host = ConfigurationManager.AppSettings["CartWebSealUrl"];
                string webSealUrl = Utils.buildWebSealUrl(host);
                return webSealUrl;
            }
        }

        protected override bool IsOrganization
        {
            get
            {
                if (string.Compare(RadioButtonList1.SelectedValue, "company", true) == 0)
                {
                    return true;
                }
                return false;
            }
        }

        protected override string LastName
        {
            get { return BaseControl.CleanString(txtLastName.Text); }
        }

        protected override int Layout
        {
            get { return 2; }
        }

        protected override string LayoutVersion
        {
            get { return "2.0"; }
        }

        protected int MaxCartItemsinsidescroll
        {
            get; set;
        }

        protected override PaymentMethodType PaymentMethod
        {
            get
            {
                if (radioButtonInternetPayment.Checked)
                {
                    return PaymentMethodType.OnlineBanking;
                }
                else if (radioButtonOnlinePayment.Checked)
                {
                    return PaymentMethodType.CreditCard;
                }
                else if (radioButtonInvoicePayment.Checked)
                {
                    return PaymentMethodType.Invoice;
                }

                return PaymentMethodType.PayPage;
            }
        }

        protected override string SocialSecurityNumber
        {
            get { return txtSocialSecNo.Text.ReformatSSN(); }
        }

        #endregion Properties

        #region Methods

        protected string GetPhoneNumberRegEx()
        {
            return this.GetGlobalResourceObject("PostenCart", "PhoneNumberRegEx").ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ICartSettingsService service = IoC.Resolve<ICartSettingsService>();
            var item = service.GetItems();

            //set max cartitems property which will be passed to the javascript
            this.MaxCartItemsinsidescroll = item.DisplayableCartItems;

            if (!IsPostBack)
            {
                try
                {

                    ltrlErrorMessage.Text = string.Empty;

                    var paymentService = IoC.Resolve<IPaymentService>();
                    foreach (var paymentMethod in paymentService.GetPaymentMethods())
                    {
                        if (paymentMethod.type == PaymentMethodType.CreditCard)
                        {
                            this.radioButtonOnlinePayment.Text = this.GetGlobalResourceObject("PostenCart", paymentMethod.name).ToString();
                            this.OnlinePayment.Visible = true;
                            this.radioButtonOnlinePayment.Checked = true;
                        }
                        else if (paymentMethod.type == PaymentMethodType.OnlineBanking)
                        {
                            this.radioButtonInternetPayment.Text = this.GetGlobalResourceObject("PostenCart", paymentMethod.name).ToString();
                            this.InternetPayment.Visible = true;
                        }
                        else if (paymentMethod.type == PaymentMethodType.Invoice)
                        {
                            this.radioButtonInvoicePayment.Text = this.GetGlobalResourceObject("PostenCart", paymentMethod.name).ToString();
                            this.InvoicePayment.Visible = true;
                        }
                    }

                    //Same Else if user not logged in and “Login required”, then don’t display
                    var invoiceoption = this.VerifyInvoiceOption(item);

                    if (item.defaultpayment == PaymentMethodType.CreditCard.ToString())
                        this.radioButtonOnlinePayment.Checked = true;
                    else if (item.defaultpayment == PaymentMethodType.Invoice.ToString() && !invoiceoption)
                        this.radioButtonInvoicePayment.Checked = true;
                    else if (item.defaultpayment == PaymentMethodType.OnlineBanking.ToString())
                        this.radioButtonInternetPayment.Checked = true;

                    var basketItem = this.BasketContext.BasketItem;

                    //If “fraud control – card payment blocked”, don’t display.
                    if (!item.EnableCreditCardPayment)
                    {
                        this.radioButtonOnlinePayment.Enabled = false;
                        this.OnlinePaymentMessageLabel.Text = Utils.GetResourceString("Message_CreditCard_Payment_Disabled");
                        this.OnlinePaymentMessageLabel.Visible = true;
                    }

                    //If “fraud control – internet bankment payment blocked”, don’t display.
                    if (!item.EnableInternetBankingPayment)
                    {
                        this.radioButtonInternetPayment.Enabled = false;
                        this.InternetPaymentMessageLabel.Text = Utils.GetResourceString("Message_Bank_Payment_Disabled");
                        this.InternetPaymentMessageLabel.Visible = true;
                    }

                    //If invoice is checked, add invoice to basket
                    if (radioButtonInvoicePayment.Checked)
                    {
                        BasketContext.Current.AddInvoiceFee();
                    }

                    this.PopulateFields();

                }

                catch (Exception ex)
                {
                    PostenLogger.LogError(ex.Message.ToString() + "\n\r" + ex.StackTrace.ToString(), PostenLogger.ApplicationLogCategory, MethodInfo.GetCurrentMethod().DeclaringType, MethodInfo.GetCurrentMethod().Name);
                }
                //Set selected country to sweden (SE)
                var dInfo = BasketContext.Current.BasketItem.DeliveryAccountInfo;
                if (dInfo != null) { ddlstCountry.SelectedValue = dInfo.CountryCode; }
            }
        }

        protected void PopulateFields()
        {
            var basketItem = this.BasketContext.BasketItem;
            this.lblShipping.Text = "0";
            this.lblInvoiceFee.Text = (BasketContext.Current.InvoiceFee == null) ? "0" : BasketContext.Current.InvoiceFee.Total.ToString();
            this.lblSavings.Text = "0";
            this.lblBasketTotal.Text = basketItem.TotalString;
            this.lblVatValue.Text = basketItem.TotalTaxString;
        }

        protected override void ShowErrorMessage(string message)
        {
            //ltrlErrorMessage.Text = message;
            string error_title = Utils.GetResourceString("Checkout_PopUp_Title_Error_Message");
            string close_button_title = Utils.GetResourceString("Close_PopUp_Button_Name");
            string script = string.Format("showPopUp('{0}', '{1}', 'popup', false, 800, '{2}');", error_title, message, close_button_title);
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", script, true);
        }

        protected bool VerifyInvoiceOption(CartSettingItems item)
        {
            if (item.invoiceoption == "Yes" && !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.radioButtonInvoicePayment.Enabled = false;
                this.LoginDescription.Visible = true;
                return true;
            }
            return false;
        }

        private void disableInvoicePaymentMethod(string message)
        {
            this.radioButtonInvoicePayment.Enabled = false;
            if (!string.IsNullOrEmpty(message)) this.LoginDescription.Text = message;
            this.LoginDescription.Visible = true;
        }

        private bool isInBlockList(string serviceId, ICollection<BlockedServices> list, out string applicationName)
        {
            applicationName = "";

            var matchservice = (from names in list
                                where string.Compare(names.ApplicationID, serviceId, true) == 0
                                select names.Name).SingleOrDefault();

            if (matchservice != null)
                return true;

            return false;
        }

        #endregion Methods
    }
}