﻿namespace Posten.Portal.Cart.Presentation.ControlTemplates
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI;

    using Posten.Portal.Cart.BusinessEntities;

    public partial class CheckOutConfirmation : UserControl
    {
        #region Properties

        protected OrderContext OrderContext
        {
            get;
            set;
        }

        private string queryStringBookingId;
        protected string QueryStringBookingId
        {
            get { return Request.QueryString["BookingId"] ?? string.Empty; }
            set { this.queryStringBookingId = value; }
        }

        #endregion Properties

        #region Methods

        protected string getOrderImageUrl(string orderNumber)
        {
            string resultUrl = ResolveUrl("~/PublishingImages/Posten/Cart/Common/no_image.jpg");
            var order = this.OrderContext.BasketItem.OrderSummaryItems.Where(o => o.OrderNumber == orderNumber).FirstOrDefault();
            if (order != null)
            {
                if (order.OrderItemImageUrl != null) resultUrl = order.OrderItemImageUrl.ToString();
            }
            return resultUrl;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                return;
            }

            // Load order
            this.OrderContext = OrderContext.GetContext(this.QueryStringBookingId);
            
            if (this.OrderContext != null)
            {
                this.PopulateFields();
            }
            btnPrintReceipt.Attributes.Add("onclick", string.Format("window.open('/_layouts/Posten/Cart/common/Receipt.ashx?BookingId={0}'); return false;", this.QueryStringBookingId));

            if (this.OrderContext != null && this.OrderContext.ServiceOrderItems.Count > 0)
            {

                //get list of confirmation urls from appsettings

                //check if it exists in the appsettings
                string confirmationurlsstring = ConfigurationManager.AppSettings["CartConfirmationUrls"];
                Dictionary<string, string> confirmationurls = new Dictionary<string, string>();

                //splie first using comma for each key value pair
                string[] confirmationurlpairs = confirmationurlsstring.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                foreach (string pair in confirmationurlpairs)
                {
                    //assume correct format is used
                    string[] keyvaluepair = pair.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    confirmationurls.Add(keyvaluepair[0], keyvaluepair[1]);
                }

                var host = Request.Url;//For WebSeal, get from Applicationconfig. Request.Url;
                var orders = new List<IServiceOrderItem>();
                foreach (var order in this.OrderContext.ServiceOrderItems)
                {
                    if (order.ConfirmationUrl == null)
                    {
                        if (confirmationurls.ContainsKey(order.ApplicationName))
                        {
                            var temporaryurl = string.Format(confirmationurls[order.ApplicationName], this.OrderContext.BookingId);
                            order.ConfirmationUrl = new Uri(temporaryurl);

                            //only add one per applicaiton
                            if (orders.Find(x => x.ApplicationName == order.ApplicationName) == null)
                                orders.Add(order);
                        }

                    }
                    else
                    {
                        //only add one per application
                        if (orders.Find(x => x.ApplicationName == order.ApplicationName) == null)
                            orders.Add(order);
                    }

                    rptServicesApplication.DataSource = orders;
                    rptServicesApplication.DataBind();

                }
            }
        }

        private void PopulateFields()
        {
            try
            {

                // If Payment Method is Invoice Hide Receipt Url and change text for transactionid
                if (this.OrderContext.BasketItem.PaymentMethod == Utils.GetResourceString("Invoice"))
                {
                    linkReceiptURL.Visible = false;
                    Confirmation_Order_Link_2_Label.Visible = false;
                    btnPrintReceipt.Visible = false;
                    lblTransactionIDDescription.Text = string.Format("{0}: ", Utils.GetResourceString("Confirmation_Your_Order_InvoiceReference_Label"), this.OrderContext.BasketItem.TransactionId);

                }
                else
                {
                    // Set label for Cart and credit cart
                    lblTransactionIDDescription.Text = string.Format("{0}: ", Utils.GetResourceString("Confirmation_Your_Order_RefereceNumber_Label"), this.OrderContext.BasketItem.TransactionId);
                    
                    /* Hide invoice info if payment type is not invoice */
                    pnlInvoiceLabel.Visible = false;
                    pnlInvoiceValue.Visible = false;
                }

                lblTransactionId.Text = this.OrderContext.BasketItem.TransactionId.ToString();

                /*booking id*/
                this.lblOrderNumber.Text = this.QueryStringBookingId;

                /*receipt URL*/
                this.linkReceiptURL.NavigateUrl = "/_layouts/Posten/Cart/common/Receipt.ashx?BookingId=" + this.QueryStringBookingId;

                /*basket totals*/
                IBasketItem basketItems = this.OrderContext.BasketItem;

                Confirmation_Total_Including_Vat_Label.Text = string.Format("{0} ({1})", Utils.GetResourceString("Cart_Total_Label"), Utils.GetResourceString("Including_Vat_Label"));
                Confirmation_Total_Label.Text = Utils.GetResourceString("Confirmation_Total_Label");
                Confirmation_VAT_Label.Text = Utils.GetResourceString("Confirmation_VAT_Label");
                InvoiceFee_Label.Text = Utils.GetResourceString("InvoiceFee_Label");

                this.Summa.Text = basketItems.TotalString;
                this.SummaMom.Text = basketItems.TotalExclTaxString;
                this.Moms.Text = basketItems.TotalTaxString;
                this.AttBetala.Text = basketItems.TotalString;
                this.ltrInvoiceFee.Text = string.Format("{0} kr", OrderContext.InvoiceFee.ToString("f2"));

                /*products*/
                pnlProduct.Visible = (this.OrderContext.ProductOrderItems.Count > 0);
                this.ProductsControl.DataSource = this.OrderContext.ProductOrderItems;
                ProductsControl.DataBind();

                /*services*/
                pnlService.Visible = (this.OrderContext.ServiceOrderItems.Count > 0);
                ServicesRepeater.DataSource = this.OrderContext.ServiceOrderItems;
                ServicesRepeater.DataBind();

                //Your Next Step
                pnlYourNextStep.Visible = (this.OrderContext.ServiceOrderItems.Count > 0);
                
                this.ShowAccountInformation();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ShowAccountInformation()
        {
            IBasketItem basketItem = this.OrderContext.BasketItem;

            // For confirmation - Where will be the data for the "Your Details" section come from?
            // Data in "Your Details will be from the basket delivery account info for now.
            IDeliveryAccountInfo deliveryAccountInfo = basketItem.DeliveryAccountInfo;
            IBillingAccountInfo billingAccountInfo = basketItem.BillingAccountInfo;

            string name = (!string.IsNullOrEmpty(deliveryAccountInfo.FirstName) && !string.IsNullOrEmpty(deliveryAccountInfo.LastName)) ?
                            string.Format("{0} {1}", deliveryAccountInfo.FirstName, deliveryAccountInfo.LastName) :
                            (!string.IsNullOrEmpty(billingAccountInfo.Name + " " + billingAccountInfo.LastName)) ? billingAccountInfo.Name + billingAccountInfo.LastName : string.Empty;

            string username = name;
            if (this.OrderContext.BasketItem.PaymentMethod.ToLower() == "invoice")
            {
                if (!string.IsNullOrEmpty(billingAccountInfo.Organization))
                {
                    name = string.Format("{0}<br/>{1}", billingAccountInfo.Organization, username);
                }
            }

            string address = (!string.IsNullOrEmpty(deliveryAccountInfo.AddressLine)) ?
                                 deliveryAccountInfo.AddressLine :
                                 (!string.IsNullOrEmpty(billingAccountInfo.AddressLine)) ? billingAccountInfo.AddressLine : string.Empty;

            string postal = (!string.IsNullOrEmpty(deliveryAccountInfo.PostalCode)) ?
                             deliveryAccountInfo.PostalCode :
                             (!string.IsNullOrEmpty(billingAccountInfo.PostalCode)) ? billingAccountInfo.PostalCode : string.Empty;

            string city = (!string.IsNullOrEmpty(deliveryAccountInfo.City)) ?
                             deliveryAccountInfo.City :
                             (!string.IsNullOrEmpty(billingAccountInfo.City)) ? billingAccountInfo.City : string.Empty;

            string country = (!string.IsNullOrEmpty(deliveryAccountInfo.CountryName)) ?
                             deliveryAccountInfo.CountryName : string.Empty;

            string email = (!string.IsNullOrEmpty(deliveryAccountInfo.Email)) ?
                             deliveryAccountInfo.Email :
                             (!string.IsNullOrEmpty(billingAccountInfo.Email)) ? billingAccountInfo.Email : string.Empty;

            string telno = (!string.IsNullOrEmpty(deliveryAccountInfo.Telephone)) ?
                             deliveryAccountInfo.Telephone :
                             (!string.IsNullOrEmpty(billingAccountInfo.Telephone)) ? billingAccountInfo.Telephone : string.Empty;

            string paymentMethod = basketItem.PaymentMethod;

            DateTime dt = basketItem.PaymentDate;

            //Update payment date format.
            YourDetails_PaymentDate.Text = dt.ToShortDateString();
            YourDetails_UserName.Text = username;

            YourDetails_DeliveryName.Text = name;

            YourDetails_DeliveryAddress.Text = address;
            YourDetails_DeliveryPostalCode.Text = postal;
            YourDetails_DeliveryCity.Text = city;
            YourDetails_DeliveryCountry.Text = country;
            YourDetails_Email.Text = email;
            YourDetails_MobileNo.Text = telno;
            YourDetails_PaymentOption.Text = basketItem.PaymentMethod;
        }

        #endregion Methods
    }
}