﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditCart.ascx.cs" Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.EditCart" %>

<script type="text/javascript" src="/_layouts/Posten/Cart/Presentation/js/cart.js?v2"></script>
<script type="text/javascript" src="/_layouts/Posten/Cart/Common/Scripts/jquery.tinyscrollbar.min.js"></script>


<asp:HiddenField ID="BasketIdControl" runat="server" />
<div class="edit-cart" >    
    
    <div class="cart">
        <div class="labels prod" runat="server" id="LabelControl1">
        <div class="title"><asp:Literal ID="ProductLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("Product_Cart_Label")%>' /></div>
        <div class="number"><asp:Literal ID="ProductPriceLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("Cart_Price_Label")%>' /></div>
        <div class="amount"><asp:Literal ID="ProductTotalLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("Cart_Total_Label")%>' /></div>
        </div>
        <div class="productsCont">
        <asp:Repeater ID="ProductsControl" runat="server" EnableViewState="false">
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate>
                <div class="items" id='<%# Eval("OrderNumber") %>'>
                    <div class="prod-name">
                        <a href="#" title='<%# Eval("ArticleName") %>'>
                            <%# Eval("ArticleName") %>
                        </a>
                        <div>
                            <%# Eval("ArticleId")%>
                        </div>
                    </div>
                    <div class="prod-quantity">
                        <asp:Textbox ID="QuantityControl" CssClass="tbox-quantity" runat="server" Text='<%# Eval("Quantity")%>' />&nbsp;st
                    </div>
                    <div class="prod-total" >
                       <div class="price-amount" style="float: left;">
                            <%# Eval("Total", "{0:N}")%>&nbsp;kr
                       </div>
                        <a href="#" class="remove-prod"></a>
                   </div>
                </div>
            </ItemTemplate>
            <FooterTemplate></FooterTemplate>
        </asp:Repeater>
        </div>
		<div class="labels" style="display:none;">
            <div class="title">Abonnemang</div>
            <div class="number">Antal</div>
            <div class="amount">Totalt</div>
        </div> 
		<asp:Repeater ID="SubscriptionsControl" runat="server" EnableViewState="true" Visible="false">
            <HeaderTemplate>a</HeaderTemplate>
            <ItemTemplate>
                <div class="items">
                    <div>b</div>
                    <div>b</div>
                    <div>b</div>
                </div>
            </ItemTemplate>
            <FooterTemplate></FooterTemplate>
        </asp:Repeater>          
        <div class="labels serv" style="border-top: 1px solid #DBD4C5;" runat="server" id="LabelControl2">
        <div class="title"><asp:Literal runat="server" ID="ServiceLabel" Text='<%$CartCode: Utils.GetResourceString("Service_Cart_Label")%>' /></div>
        <div class="number"><asp:Literal runat="server" ID="ServicePriceLabel" Text='<%$CartCode: Utils.GetResourceString("Cart_Price_Label")%>' /></div>
        <div class="amount"><asp:Literal runat="server" ID="ServiceTotalLabel" Text='<%$CartCode: Utils.GetResourceString("Cart_Total_Label")%>' /></div>
        </div>
        <div class="servicesCont scrollable">
            <div class="scrollbar">
                <div class="track">
                    <div class="thumb">
                        <div class="end"></div>
                    </div>
                </div>
            </div>
			<div class="viewport">
			    <div class="overview">
		            <asp:Repeater ID="ServicesControl" runat="server" EnableViewState="false">
                        <HeaderTemplate></HeaderTemplate>
                        <ItemTemplate>
                            <div class="items" id='<%# Eval("OrderNumber") %>'>
                                <div class="svc-name">
                                    <a href="#" title='<%# DataBinder.Eval(Container.DataItem, "Name") %>'>
                                        <%# Eval("Name") %>
                                    </a>
                                    <div>
                                      
                                      <div class="details" id='content_<%# Eval("OrderNumber") %>'>
                                        <div class="helpcontent">
                                          <div>
                                                <%# Eval("FreeTextField1")%>
                                            </div>
                                            <div>
                                                <%# Eval("FreeTextField2")%>
                                            </div>
                                        </div>
                                        
                                      </div>
                            
                                    </div>
                                    <%# FormatEditUrl(Eval("EditUrl"), Eval("OrderNumber").ToString(), Utils.GetResourceString("EditCart_Change_Service_Label"))%>
                                                         
                                </div>
                                <div class="svc-quantity">1&nbsp;st</div>
                                <div class="svc-total">
                                    <div class="price-amount" style="float: left;"><%# Eval("Total", "{0:N}")%>&nbsp;kr</div>
                                    <a href="#" class="remove-svc"></a>

                                </div>                    
                            </div>  
                        </ItemTemplate>
                        <FooterTemplate></FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <div class="empty-cart-label">
            <div runat="server" id="emptyCartControl" class="emptyCartControl" style="display:none;">
                <i><asp:Literal ID="EmptyLabel" runat="server" Text='<%$CartCode: Utils.GetResourceString("Empty_Cart_Text")%>' /></i>
            </div>
        </div>        
	</div>    
</div>