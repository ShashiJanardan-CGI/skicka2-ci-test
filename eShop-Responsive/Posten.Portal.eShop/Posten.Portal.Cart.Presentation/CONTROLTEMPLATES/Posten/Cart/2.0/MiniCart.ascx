﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniCart.ascx.cs" Inherits="Posten.Portal.Cart.Presentation.ControlTemplates.MiniCart" %>

<%-- css/jscript --%>
<link href="/_layouts/Posten/Cart/Presentation/styles/Cart.css" rel="stylesheet" type="text/css" />
<script src="/_layouts/Posten/Cart/Presentation/js/common/jquery.cookie.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Presentation/js/minicart.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Presentation/js/jqModal.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Common/Scripts/jquery.tmpl.min.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Presentation/js/common/utils.js" type="text/javascript"></script>
<script src="/_layouts/Posten/Cart/Presentation/js/jquery.ezpz_tooltip.min.js" type="text/javascript"></script>
<div id="minicarthelp">
    <div class="helpcontent">
        <%= Utils.GetResourceString("HelpText_MiniCart")%>
    </div>
</div>
<div>
    
    <%-- Modal when cart is updated--%>
    <div class="minicartnotification">
        <div class="header"><%= Utils.GetResourceString("Basket_Label")%></div>
        <div><%= Utils.GetResourceString("CartHasBeenUpdated_Label")%></div>
    </div>

    <%-- main --%>
    <div id="GlobalCart">
        <div class="edge top">
            <div class="left border">
            </div>
            <div class="right">
            </div>
        </div>
        <ul>
            <li class="basket"><a><span></span><%= Utils.GetResourceString("View_Cart_Label") %></a></li>
            <li class="info"><span id="cartsummary">0 <%= Utils.GetResourceString("Article_Label")%> | 0</span></li>
            <li class="checkout"><a id="A2" runat="server" style="cursor: pointer;"><%= Utils.GetResourceString("Checkout_Label") %></a></li>
        </ul>
        <div class="edge bottom">
            <div class="left border">
            </div>
            <div class="right">
            </div>
        </div>
    </div>
    <%-- jqModal --%>
    <div class="jqmWindow" id="dialog">
        <div class="dropDown" style="display: block;">
            <div class="indent active">
                <span></span>
            </div>
            <div class="top">
                <div class="left">
                </div>
                <div class="center header theme">
                    <span><%= Utils.GetResourceString("Basket_Label")%></span>
                    <div class="jqmClose close">
                    </div>
                </div>
                <div class="right">
                </div>
            </div>
            <div class="middle">
                <div class="left">
                    &nbsp;</div>
                <div class="center">
                    <ul>
                        <div class="prod-label" style="display:none;">Products</div>
                        <li class="gridHeader" style="display:none;">
                            <div class="article"><%= Utils.GetResourceString("Article_Label") %></div>
                            <div class="description"><%= Utils.GetResourceString("Description_Label") %></div>
                            <div class="quantity"><%= Utils.GetResourceString("Number_Label") %></div>
                            <div class="price"><%= Utils.GetResourceString("Price_Label") %></div>
                            <div class="delete">&nbsp;</div>
                        </li>
                        <span id="loadminicart">
                            <div id="product-items"></div>
                            <div id="service-items"></div>
                        </span>
                    </ul>
                    <div class="freight">
                        <span><strong><%= Utils.GetResourceString("Shipping_Label") %>:</strong> <%= Utils.GetResourceString("free_shipping_Label") %> <a href="#"></a></span><span class="rightAlign">0 kr.</span>
                    </div>
                    <div class="sum">
                        <span><%= Utils.GetResourceString("Total_incl_VAT_Label") %>: </span>
                        <span class="rightAlign theme">
                            <span id="carttotal"></span>
                        </span>
                    </div>
                </div>
                <div class="right">&nbsp;</div>
            </div>
            <div class="bottom">
                <div class="left">
                </div>
                <div class="center">
                    <div>
                        <a class="link basketUpdate" href="#"><%= Utils.GetResourceString("Continue_Shopping_Text")%><span class="arrow"></span></a>
                        <a class="linkBtn jqTransformButton" href="<%= CheckOutPageUrl() %>"><span><span><%= Utils.GetResourceString("Checkout_Label") %></span></span></a></div>
                </div>
                <div class="right">
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" >
    $(document).ready(function () {
        var minicart_<%= this.ClientID %> = new minicart();
        minicart_<%= this.ClientID %>.init('<%= Utils.GetResourceString("Empty_Cart_Text") %>',
                                           '<%= Utils.GetResourceString("Remove_Product_Error_Text") %>',
                                           '<%= Utils.GetResourceString("Remove_Service_Error_Text") %>',
                                           '<%= Utils.GetResourceString("Update_Product_Error_Text") %>',
                                           '<%= Utils.GetResourceString("View_Cart_Label") %>',
                                           '<%= Utils.GetResourceString("Hide_Cart_Label") %>',
                                           '<%= Utils.GetResourceString("Cart_Error_Title") %>',
                                           '<%= Utils.GetResourceString("Minicart_Summary_Article_Label") %>',
                                           '<%= Utils.GetResourceString("Close_PopUp_Button_Name") %>');
    });
</script>