﻿using System;
using System.Configuration;
using System.Web.UI;

namespace Posten.Portal.Cart.Presentation.ControlTemplates
{
    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Utilities;

    public partial class MiniCart : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BasketContext.Current.RemoveInvoiceFee();
            if (!Page.IsPostBack)
            {
                this.A2.HRef = this.CheckOutPageUrl();
            }
        }

        protected string CheckOutPageUrl()
        {
            return SPUrlUtility.CombineUrl(SPContext.Current.Web.ServerRelativeUrl, "_layouts/Posten/Cart/2.0/Checkout.aspx");

            string cUrl = string.Empty;
            cUrl = (ConfigurationManager.AppSettings["CheckOutPageUrl"] == null) ? "/_layouts/Posten/Cart/2.0/Checkout.aspx" : ConfigurationManager.AppSettings["CheckOutPageUrl"].ToString();
            return cUrl;
        }
    }
}
