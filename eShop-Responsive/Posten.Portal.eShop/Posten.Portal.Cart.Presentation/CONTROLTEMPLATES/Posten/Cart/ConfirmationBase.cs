﻿using System.ComponentModel;
using System.Web;
using System.Web.UI;
using Posten.Portal.Cart.Utilities;

namespace Posten.Portal.Cart.Presentation.ControlTemplates
{
    public abstract class ConfirmationBase : UserControl
    {
        public bool RequireValidChecksumUrl { get; set; }

        protected OrderContext OrderContext { get; private set; }

        protected abstract void ShowErrorMessage(string message);

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // do not cache this page
            this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            this.Response.Cache.SetNoStore();

            this.OrderContext = OrderContext.GetContext(this.Request.QueryString["BookingId"] ?? string.Empty);
            if (this.OrderContext == null)
            {
                this.ShowErrorMessage("Order with booking-id '" + this.Request.QueryString["BookingId"] + "' was not found");
                return;
            }
        }
    }
}
