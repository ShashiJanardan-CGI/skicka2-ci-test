﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="d8179d6f-2f9c-48fd-b35a-4631cadcd5b6" defaultResourceFile="PostenCart" description="$Resources:Feature_PostenCart_Infrastructure_Description;" featureId="d8179d6f-2f9c-48fd-b35a-4631cadcd5b6" imageUrl="Posten/PostenFeature.gif" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.6d587b43-6fb1-4d5c-adfb-4a066faec8a5.FullName$" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="$Resources:Feature_PostenCart_Infrastructure_Title;" version="AAEAAAD/////AQAAAAAAAAAEAQAAAA5TeXN0ZW0uVmVyc2lvbgQAAAAGX01ham9yBl9NaW5vcgZfQnVpbGQJX1JldmlzaW9uAAAAAAgICAgCAAAAAAAAAAAAAAAAAAAACw==" upgradeActionsReceiverAssembly="$SharePoint.Project.AssemblyFullName$" upgradeActionsReceiverClass="$SharePoint.Type.6d587b43-6fb1-4d5c-adfb-4a066faec8a5.FullName$" deploymentPath="$SharePoint.Feature.FileNameWithoutExtension$_2.0" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <referencedFeatureActivationDependency minimumVersion="" itemId="22392c2e-77cb-451c-8531-bb4d21bbce9a" projectPath="..\Posten.Portal.Cart.Core\Posten.Portal.Cart.Core.csproj" />
  </activationDependencies>
  <projectItems>
    <projectItemReference itemId="3f332044-d6a7-47f2-b46c-4544d34b2d48" />
    <projectItemReference itemId="772dcd8c-13c6-43ef-b018-3c1fc39c3db5" />
    <projectItemReference itemId="a4a7b4cb-6f6a-425b-b700-8503372fbc3b" />
  </projectItems>
</feature>