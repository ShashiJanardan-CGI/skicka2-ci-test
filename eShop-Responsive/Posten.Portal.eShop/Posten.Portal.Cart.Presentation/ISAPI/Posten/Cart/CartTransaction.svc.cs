﻿using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;
using Microsoft.SharePoint.Client.Services;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Presentation.ViewData;
using Posten.Portal.Cart.Services;
using Posten.Portal.Cart.Utilities;
using Posten.Portal.eShop.BusinessEntities;
using Posten.Portal.eShop.Services;
using Posten.Portal.eShop;
using Posten.Portal.Platform.Common.Container;
using Posten.Portal.Platform.Common.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Web;
using System.Text.RegularExpressions;
using Microsoft.SharePoint;

namespace Posten.Portal.Cart.Presentation
{
    [BasicHttpBindingServiceMetadataExchangeEndpoint]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CartTransaction : ICartTransaction
    {
        public string CurrentBasketId
        {
            get
            {
                return BasketContext.Current.BasketItem.BasketId;
            }
        }


        public bool AddProduct(string productNumber, string productOrderQuantity)
        {
            try
            {
                ProductModel product = ProductContext.GetProduct(productNumber);

                if (product != null)
                {
                    string result = BasketContext.Current.AddProductOrder(new ProductOrderItem
                                                    {
                                                        ArticleId = product.Id,
                                                        ArticleName = product.Name,
                                                        ArticlePrice = product.Price,
                                                        Vat = product.Vat,
                                                        VatPercentage = product.VatPercentage,
                                                        Quantity = Convert.ToInt32(productOrderQuantity),
                                                        CurrencyCode = product.CurrencyCode
                                                    });
                    if (result != null)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch
            {
                throw;
            }
        }

        public RemoveResult RemoveProduct(string productOrderNumber)
        {
            try
            {
                ////IProductBasketService prodBasket = IoC.Resolve<IProductBasketService>();
                ////bool result = prodBasket.RemoveProductOrder(this.CurrentBasketId, productOrderNumber);
                bool result = BasketContext.Current.RemoveProductOrder(productOrderNumber);
                if (result)
                {
                    ////IBasketService basket = IoC.Resolve<IBasketService>();
                    ////var item = basket.GetBasketItem(this.CurrentBasketId);
                    var item = BasketContext.Current.BasketItem;
                    var invoicefee = BasketContext.Current.InvoiceFee;
                    return new RemoveResult
                    {
                        TotalString = item.TotalString,
                        TotalTaxString = item.TotalTaxString,
                        TotalExcludingTaxString = item.TotalExclTaxString,
                        InvoiceFeeString = invoicefee == null ? "0" : string.Format("{0}", invoicefee.Total.ToString("f2"))
                    };
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public string RemoveService(string serviceOrderNumber, string basketID)
        {
            try
            {
                ////IServiceBasketService servBasket = IoC.Resolve<IServiceBasketService>();
                ////var result = servBasket.RemoveServiceOrder(this.CurrentBasketId, serviceOrderNumber);
                BasketContext.BasketID = basketID;
                bool result = BasketContext.Current.RemoveServiceOrder(serviceOrderNumber);
                if (result)
                {
                    ////IBasketService basket = IoC.Resolve<IBasketService>();
                    ////var item = basket.GetBasketItem(this.CurrentBasketId);
                    var totalbasket = BasketContext.Current.BasketItem.Total;
                    var total = totalbasket;
                    if (BasketContext.Current.InvoiceFee != null)
                    {
                        var invoicefee = BasketContext.Current.InvoiceFee.Total;
                        total = totalbasket - invoicefee;
                    }

                    if (total == 0)
                    {
                        BasketContext.Current.RemoveInvoiceFee();
                    }


                    var item = BasketContext.Current.BasketItem;
                    var invoicefeeobject = BasketContext.Current.InvoiceFee;

                    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                    var returnValue = new RemoveResult
                    {
                        TotalString = item.TotalString,
                        TotalTaxString = item.TotalTaxString,
                        TotalExcludingTaxString = item.TotalExclTaxString,
                        InvoiceFeeString = invoicefeeobject == null ? "0" : string.Format("{0}", invoicefeeobject.Total.ToString("f2"))
                    };

                    return jsSerializer.Serialize(returnValue);
                }

                return "";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public UpdateProductOrderResult UpdateProduct(string productOrderNum, string quantity)
        {
            try
            {
                ////IProductBasketService prodBasket = IoC.Resolve<IProductBasketService>();
                ////IProductOrderItem initialproditem = prodBasket.GetProductOrderItem(this.CurrentBasketId, productOrderNum);
                IProductOrderItem initialproditem = BasketContext.Current.GetProductOrder(productOrderNum);

                initialproditem.Quantity = Convert.ToInt16(quantity);
                initialproditem.Vat = (initialproditem.ArticlePrice * initialproditem.VatPercentage * initialproditem.Quantity);



                ////string newOrderNum = prodBasket.UpdateProductOrder(this.CurrentBasketId, initialproditem);
                string newOrderNum = BasketContext.Current.UpdateProductOrder(initialproditem.OrderNumber, initialproditem);

                if (!String.IsNullOrEmpty(newOrderNum))
                {
                    ////IBasketService basket = IoC.Resolve<IBasketService>();
                    //initialproditem.OrderNumber = newOrderNum;                    
                    ////var item = basket.GetBasketItem(this.CurrentBasketId);
                    var item = BasketContext.Current.BasketItem;
                    return new UpdateProductOrderResult
                    {
                        OrderNumber = newOrderNum,
                        Total = initialproditem.Total.ToString("N", CultureInfo.CurrentCulture.NumberFormat),
                        TotalString = item.TotalString,
                        TotalTaxString = item.TotalTaxString,
                        TotalExcludingTaxString = item.TotalExclTaxString
                    };
                    //return initialproditem;                                        
                }
                return null;
            }
            catch
            {
                throw;
            }
        }

        public LocationResult GetLocation(string postalcode, string countrycode)
        {
            try
            {
                //IPostalCodeService service = IoC.Resolve<IPostalCodeService>();
                //PostalCodeInfo result = service.GetPostalCode(countrycode, postalcode);

                var result = ZipCity.ZipToCity(postalcode, countrycode);

                return new LocationResult
                {
                    city = result.city,
                    country = result.country,
                    zip = result.zip
                };
            }
            catch (Exception ex)
            {
                PostenLogger.LogError(ex.Message.ToString() + "\n\r" + ex.StackTrace.ToString(), PostenLogger.ApplicationLogCategory, MethodInfo.GetCurrentMethod().DeclaringType, MethodInfo.GetCurrentMethod().Name);
                return null;
            }
        }

        public bool DoesEmailExist(string email)
        {
            IAccountService account = IoC.Resolve<IAccountService>();
            var result = account.DoesEmailAccountExist(email);

            return result;
        }

        public IAccountInfo GetAccountInformation(string email, string password)
        {
            IAccountService account = IoC.Resolve<IAccountService>();
            var result = account.GetAccountInfo(email, password);

            return result;
        }

        public BasketDataResult RecalculateVat(string postalcode, string countrycode)
        {
            try
            {
                var basket = BasketContext.Current.RecalculateVAT(countrycode, postalcode);
                var products = BasketContext.Current.ProductOrderItems;
                var services = BasketContext.Current.ServiceOrderItems;

                return new BasketDataResult
                {
                    TotalString = basket.TotalString,
                    TotalArticles = BasketContext.Current.NumberOfArticle.ToString(),
                    TotalTaxString = basket.TotalTaxString,
                    Products = MapToProductEntity(products),
                    Services = MapToServiceEntity(services),
                    TotalExcludingTaxString = basket.TotalExclTaxString
                };
            }
            catch (Exception ex)
            {
                PostenLogger.LogError(ex.Message.ToString() + "\n\r" + ex.StackTrace.ToString(), PostenLogger.ApplicationLogCategory, MethodInfo.GetCurrentMethod().DeclaringType, MethodInfo.GetCurrentMethod().Name);
                return null;
            }
        }

        /// <summary>
        /// Method which returns a list of all order numbers (for service orders) in the current basket
        /// </summary>
        /// <returns>List of current basket serviceorder order numbers</returns>
        public List<string> GetServiceOrderNumbersForBasket()
        {
            //if (BasketContext.Current.InvoiceFee != null)
            //{
            //    BasketContext.Current.RemoveInvoiceFee();
            BasketContext.ReloadCurrent();
            //}

            List<string> result = new List<string>();

            ICollection<IServiceOrderItem> serviceOrders = BasketContext.Current.ServiceOrderItems;
            foreach (IServiceOrderItem serviceOrder in serviceOrders)
            {
                result.Add(serviceOrder.OrderNumber);
            }

            return result;
        }

        public BasketDataResult GetMiniCarItems()
        {

            //Remove Invoice Payment if any

            if (BasketContext.Current.InvoiceFee != null)
            {
                BasketContext.Current.RemoveInvoiceFee();
                //INvalidate Cache 
                BasketContext.ReloadCurrent();
            }

            var basket = BasketContext.Current.BasketItem;
            var products = BasketContext.Current.ProductOrderItems;
            var services = BasketContext.Current.ServiceOrderItems;
            var total = BasketContext.Current.NumberOfArticle;

            return new BasketDataResult
            {
                TotalString = basket.TotalString,
                TotalArticles = total.ToString(),
                TotalTaxString = basket.TotalTaxString,
                Products = MapToProductEntity(products),
                Services = MapToServiceEntity(services),
                TotalExcludingTaxString = basket.TotalExclTaxString
            };
        }

        public List<MessageResults> SaveAccount(string email, string firstName, string lastName, string codeValue, string code, string houseAddress, string postal, string city, string streetAddress, string country, string mobile, string password)
        {
            IUserManagementService service = IoC.Resolve<IUserManagementService>();
            var response = service.SaveAccount(MapToAccountInfo(email, firstName, lastName, codeValue, code, houseAddress, postal, city, streetAddress, country, mobile, password));

            List<MessageResults> results = new List<MessageResults>();
            foreach (ICodeMessage item in response)
            {
                results.Add(new MessageResults { code = item.Code, message = item.Message });
            }

            return results;
        }


        public List<ILegalAddress> GetLegalAddresses(string idno)
        {
            IPaymentService service = IoC.Resolve<IPaymentService>();

            //check if invalid address is used.
            var settings = GetCartSettings();
            if (settings.BlockSSNs.Contains(idno))
                throw new ApplicationException(Utilities.Utils.GetResourceString("CHECKOUT_ERROR_BLOCKEDSSN"));

            string ipAdd = Utils.GetCurrentIpAddress();
            var addresses = service.GetLegalAddresses(ipAdd, idno.ReformatSSN());

            return addresses.ToList();
        }

        public IAccountInfo MapToAccountInfo(string email, string firstName, string lastName, string codeValue, string code, string houseAddress, string postal, string city, string streetAddress, string country, string mobile, string password)
        {
            return new AccountInfo
            {
                Address = new Posten.Portal.Cart.BusinessEntities.Address { City = city, Country = country, PostalCode = postal, StreetAddress = streetAddress },
                CareOfAddress = string.Empty,
                CompanyCode = string.Empty,
                CompanyName = (code == "1") ? codeValue : string.Empty,
                EmailAddress = email,
                FirstName = firstName,
                LastName = lastName,
                MobileNumber = mobile,
                PersonalCodeNumber = (code == "0") ? codeValue : string.Empty,
                SmsNumber = string.Empty,
                State = string.Empty,
                StateChangedBy = string.Empty,
                StateChangeReason = string.Empty,
                StateChangeTime = string.Empty,
                Type = (code == "0") ? AccountType.Private.ToString() : AccountType.Company.ToString(),
                Password = password
            };
        }

        public List<ProductItems> MapToProductEntity(ICollection<IProductOrderItem> items)
        {
            List<ProductItems> results = new List<ProductItems>();
            foreach (IProductOrderItem item in items)
            {
                results.Add(new ProductItems
                {
                    ArticleName = item.ArticleName,
                    ImageUrl = "",
                    OrderNumber = item.OrderNumber,
                    Quantity = item.Quantity,
                    Total = item.Total.ToString("f2")
                });
            }

            return results;
        }

        public List<ServiceItems> MapToServiceEntity(ICollection<IServiceOrderItem> items)
        {
            List<ServiceItems> results = new List<ServiceItems>();
            foreach (IServiceOrderItem item in items)
            {
                results.Add(new ServiceItems
                {
                    ServiceName = item.Name,
                    FreeTextField1 = item.FreeTextField1,
                    FreeTextField2 = item.FreeTextField2,
                    ImageUrl = "",
                    OrderNumber = item.OrderNumber,
                    Total = item.Total.ToString("f2")
                });
            }

            return results;
        }

        public MaximumInvoiceAmountResult IsMaximumInvoiceAmount(string type)
        {
            MaximumInvoiceAmountResult result = new MaximumInvoiceAmountResult { isMaximum = false, Message = "" };
            //type data : private or company
            ICartSettingsService service = new CartSettingsAgent();//IoC.Resolve<ICartSettingsService>();
            CartSettingItems settings = service.GetItems();

            //First check if invoice is disabled because of a setting
            if (settings.LoginRequiredForInvoicePayment && !HttpContext.Current.User.Identity.IsAuthenticated)
                return new MaximumInvoiceAmountResult { isMaximum = true, Message = Utils.GetResourceString("Message_Login_Required_Invoice_Payment") };
            if (!settings.EnableInvoicePayment)
                return new MaximumInvoiceAmountResult { isMaximum = true, Message = Utils.GetResourceString("Message_Invoice_Payment_Disabled") };

            //Check services in the basket if there is a banned service. 
            //check settings first since this is not resource intensive and has less values

            foreach (var blockedservice in settings.BlockServices)
            {
                var match = (from serviceorder in BasketContext.Current.ServiceOrderItems
                             where string.Compare(serviceorder.ApplicationId, blockedservice.ApplicationID, true) == 0
                             select serviceorder).SingleOrDefault();

                if (match != null)
                    return new MaximumInvoiceAmountResult { isMaximum = true, Message = Utils.GetResourceString("Message_Invoice_Payment_Application_Block") };
            }

            decimal invoicefee = BasketContext.Current.InvoiceFee == null ? 0 : BasketContext.Current.InvoiceFee.Total;
            decimal basketTotal = BasketContext.Current.BasketItem.Total - invoicefee;

            if (type.ToLower() == "private" && settings.PrivateInvoiceMaxAmount > 0)
            {
                result = new MaximumInvoiceAmountResult { isMaximum = basketTotal > settings.PrivateInvoiceMaxAmount, Message = string.Format(Utils.GetResourceString("Message_Invoice_Payment_Max_Amount"), settings.PrivateInvoiceMaxAmount.ToString("f2")) };
            }
            else if (type.ToLower() == "company" && settings.CompanyInvoiceMaxAmount > 0)
            {
                result = new MaximumInvoiceAmountResult { isMaximum = basketTotal > settings.CompanyInvoiceMaxAmount, Message = string.Format(Utils.GetResourceString("Message_Invoice_Payment_Max_Amount"), settings.CompanyInvoiceMaxAmount.ToString("f2")) };
            }

            return result;
        }


        public CartSettingItems GetCartSettings()
        {
            ICartSettingsService service = new CartSettingsAgent();//IoC.Resolve<ICartSettingsService>();
            return service.GetItems();
        }

        private decimal GetBasketTotal()
        {
            if (BasketContext.Current.InvoiceFee != null)
            {
                BasketContext.Current.RemoveInvoiceFee();
                //INvalidate Cache 
                BasketContext.ReloadCurrent();
            }

            return BasketContext.Current.BasketItem.Total;
        }

        public AddInvoiceResult AddInvoice(bool willadd)
        {
            var retval = new AddInvoiceResult();
            //get spcontext to set UI Values
            var context = SPContext.Current;

            if (willadd)
            {
                var total = GetBasketTotal();
                if (total > 0)
                {
                    BasketContext.Current.AddInvoiceFee();
                }
            }
            else
                BasketContext.Current.RemoveInvoiceFee();

            return new AddInvoiceResult()
            {
                InvoiceFee = BasketContext.Current.InvoiceFee == null ? 0.ToString("f2") : BasketContext.Current.InvoiceFee.Total.ToString("f2"),
                BasketTotal = BasketContext.Current.BasketItem.TotalString,
                BasketTotalExcludingTax = BasketContext.Current.BasketItem.TotalExclTaxString,
                VatTotal = BasketContext.Current.BasketItem.TotalTaxString
            };
        }


        public BasketOrderItem AddServiceToCart(string serviceOrder)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            var orderNumber = BasketContext.Current.AddServiceOrder(jsSerializer.Deserialize<ServiceOrderItem>(serviceOrder));

            return orderNumber;
        }

        public BasketOrderItem AddItemToCart(string ApplicationId, string ApplicationName, string BasketId, string ConfirmationUrl,
            string CurrencyCode, string CustomApplicationLink, string CustomApplicationLinkText, string Details, string EditUrl,
            string FreeTextField1, string FreeTextField2, string Name, string OrderNumber, string ProduceUrl, string ServiceImageUrl,
            string ServiceOrderLines,
            string SubTotal,
            string Vat,
            string Total
            )
        {
            string orderNumber = string.Empty;
            ServiceOrderItem order = new ServiceOrderItem();
            order.ApplicationId = ApplicationId;
            order.ApplicationName = ApplicationName;
            order.BasketId = BasketId;
            order.ConfirmationUrl = new Uri(ConfirmationUrl);
            order.CurrencyCode = CurrencyCode;
            order.CustomApplicationLink = new Uri(CustomApplicationLink);
            order.CustomApplicationLinkText = CustomApplicationLinkText;
            order.Details = Details;
            order.EditUrl = EditUrl;
          
            order.FreeTextField1 = FreeTextField1;
            order.FreeTextField2 = FreeTextField2;
            order.Name = Name;
            order.OrderNumber = OrderNumber;
            order.ProduceUrl = new Uri(ProduceUrl);
            order.ServiceImageUrl = null;//new Uri("http://www.capgemini.com");//new Uri(ServiceImageUrl);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            order.ServiceOrderLines = (jsSerializer.Deserialize<IEnumerable<ServiceOrderLineItem>>(ServiceOrderLines)).ToArray();
            order.SubTotal = Convert.ToDecimal(SubTotal);
            order.Vat = Convert.ToDecimal(Vat);

            BasketContext.BasketID = BasketId;

            return BasketContext.Current.AddServiceOrder(order);
        }


        public string UpdateItemInCart(string ApplicationId, string ApplicationName, string BasketId, string ConfirmationUrl,
             string CurrencyCode, string CustomApplicationLink, string CustomApplicationLinkText, string Details, string EditUrl,
             string FreeTextField1, string FreeTextField2, string Name, string OrderNumber, string ProduceUrl, string ServiceImageUrl,
             string ServiceOrderLines,
             string SubTotal,
             string Vat,
             string Total, string updateOrderId)
        {
            string basketOrderNumber = string.Empty;
            ServiceOrderItem order = new ServiceOrderItem();
            order.ApplicationId = ApplicationId;
            order.ApplicationName = ApplicationName;            
            order.ConfirmationUrl = new Uri(ConfirmationUrl);
            order.CurrencyCode = CurrencyCode;
            order.CustomApplicationLink = new Uri(CustomApplicationLink);
            order.CustomApplicationLinkText = CustomApplicationLinkText;
            order.Details = Details;
            order.EditUrl = EditUrl;
            order.FreeTextField1 = FreeTextField1;
            order.FreeTextField2 = FreeTextField2;
            order.Name = Name;
            order.OrderNumber = OrderNumber;
            order.ProduceUrl = new Uri(ProduceUrl);
            order.ServiceImageUrl = null; // new Uri("http://www.capgemini.com");
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            order.ServiceOrderLines = (jsSerializer.Deserialize<IEnumerable<ServiceOrderLineItem>>(ServiceOrderLines)).ToArray();            
            order.SubTotal = Convert.ToDecimal(SubTotal);
            order.Vat = Convert.ToDecimal(Vat);

            BasketContext.BasketID = BasketId;
            basketOrderNumber = BasketContext.Current.UpdateServiceOrder(updateOrderId, order);

            return basketOrderNumber;
        }

        public string GetData(string value)
        {
            return string.Format("You entered: {0}", value);
        }

        public string GetBasketPrice(string basketId)
        {
            var retval = new AddInvoiceResult();
            //get spcontext to set UI Values
            var context = SPContext.Current;
            BasketContext.BasketID = basketId;
            BasketContext.Current.RemoveInvoiceFee();

            return BasketContext.Current.BasketItem.TotalString;
        }

        public string ReturnBasketServices(string basketId)
        {
            BasketContext context = BasketContext.GetContext(new Guid(basketId));
            var resultSet = context.ServiceOrderItems;

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            return jsSerializer.Serialize(resultSet);
        }
        
    }
}

