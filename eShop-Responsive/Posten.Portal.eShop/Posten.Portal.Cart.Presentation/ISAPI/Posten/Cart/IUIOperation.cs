﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Presentation.ViewData;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Posten.Portal.Cart.Presentation
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICartOperation" in both code and config file together.
    [ServiceContract]
    public interface IUIOperation
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="productOrderNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool AddProduct(string productNumber, string productOrderQuantity);

        /// <summary>
        /// s
        /// </summary>
        /// <param name="productOrderNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST",RequestFormat= WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [ServiceKnownType(typeof(RemoveResult))]
        RemoveResult RemoveProduct(string productOrderNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceOrderNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [ServiceKnownType(typeof(RemoveResult))]
        RemoveResult RemoveService(string serviceOrderNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productOrderNum"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [ServiceKnownType(typeof(UpdateProductOrderResult))]
        UpdateProductOrderResult UpdateProduct(string productOrderNum, string quantity);

        /// <summary>
        /// Will return the location result based from zip and country code
        /// </summary>
        /// <param name="zip">postal code of the account</param>
        /// <param name="countrycode"> country of the account</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        LocationResult GetLocation(string postalcode, string countrycode);

        /// <summary>
        /// Will return true if email passed exist in our collection
        /// </summary>
        /// <param name="email">email of the account</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool DoesEmailExist(string email);

        /// <summary>
        /// will return the account information from user email address and password
        /// </summary>
        /// <param name="email">email of the account</param>
        /// <param name="password">password of the account</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [ServiceKnownType(typeof(AccountInfo))]
        IAccountInfo GetAccountInformation(string email, string password);

        /// <summary>
        /// Recalculate vat based on new country or postal code.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [ServiceKnownType(typeof(RemoveResult))]
        BasketDataResult RecalculateVat(string postalcode, string countrycode);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [ServiceKnownType(typeof(BasketDataResult))]
        BasketDataResult GetMiniCarItems();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="address"></param>
        /// <param name="city"></param>
        /// <param name="email"></param>
        /// <param name="addressinfo"></param>
        /// <param name="name"></param>
        /// <param name="org"></param>
        /// <param name="postalcode"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [ServiceKnownType(typeof(List<MessageResults>))]
        List<MessageResults> SaveAccount(string email, string firstName, string lastName, string codeValue, string code, string houseAddress, string postal, string city, string streetAddress, string country, string mobile, string password);

        [OperationContract]
        [ServiceKnownType(typeof(PrivateLegalAddress))]
        [ServiceKnownType(typeof(CompanyLegalAddress))]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        List<ILegalAddress> GetLegalAddresses(string idno);

        
        [OperationContract]
       [ServiceKnownType(typeof(MaximumInvoiceAmountResult))]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        MaximumInvoiceAmountResult IsMaximumInvoiceAmount(string type);

        /// <summary>
        /// AddInvoice is the async call which will add the invoice as a product to the current basket of the user.
        /// </summary>
        /// <param name="willadd"> a boolean parameter which says whether an add or a remove should be done. When true is passed, 
        /// the invoice is added to basket else, the invoice is removed.
        /// </param>
        /// <returns>returns the current invoice fee, if 0 is returned the invoice fee was removed.</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        AddInvoiceResult AddInvoice(bool willadd);



        [OperationContract]
        [ServiceKnownType(typeof(CartSettingItems))]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        CartSettingItems GetCartSettings();


        [OperationContract]        
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string AddServiceToCart(string serviceOrder);

        [OperationContract]
        [WebGet(UriTemplate = "/GetData?value={value}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetData(string value);

    }
}

