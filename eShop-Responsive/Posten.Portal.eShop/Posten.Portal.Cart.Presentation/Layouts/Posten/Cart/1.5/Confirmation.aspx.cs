﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages.Legacy
{
    using Microsoft.SharePoint.WebControls;

    public partial class ConfirmationPage : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        #endregion Properties
    }
}