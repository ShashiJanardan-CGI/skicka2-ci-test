﻿/// <reference path="jquery-1.4.1-vsdoc.js"/>
/*globals $, jQuery*/


jQuery.fn.is_or_hasparent = function(selector) {
  //
  return this.is(selector) || this.parents(selector).length > 0;
};

(function($) {

  $.StringEllipsis = function(s, max) {
    /// <summary>
    /// If the text is to long shorten it and add "..." in the end.
    /// </summary>
    /// <param name="s"></param>
    /// <param name="max">the maximum number of chars in the string</param>
    /// <returns></returns>


    //TODO: Unit-test :)
    if (null === s) { return s; }
    if (max < 3) { throw "max<3!"; }
    return s.length > max ? s.substring(0, max - 3) + "..." : s;
  };


  $.namespace = function() {
    ///	<summary>
    ///	Creates namespaces to be used for scoping variables and classes so that they are not global.  Usage:
    /// <pre><code>
    /// $.namespace('Company', 'Company.data');
    /// </code></pre>
    ///	</summary>
    ///	<param name="namespace1" type="String">
    ///	</param>
    ///	<param name="namespace2" type="String">
    ///	</param>
    ///	<param name="etc" type="String">
    ///	</param>
    ///	<returns type="" />
    var a = arguments, o = null, i, j, d, rt;
    for (i = 0; i < a.length; i = i + 1) {
      d = a[i].split(".");
      rt = d[0];
      eval('if (typeof ' + rt + ' == "undefined"){' + rt + ' = {};} o = ' + rt + ';');
      for (j = 1; j < d.length; j = j + 1) {
        o[d[j]] = o[d[j]] || {};
        o = o[d[j]];
      }
    }
  };

  $.StringFormat = function(text) {
    //http://just3ws.wordpress.com/2007/04/28/javascript-stringformat-method/
    //check if there are two arguments in the arguments list
    if (arguments.length <= 1) {
      //if there are not 2 or more arguments there’s nothing to replace
      //just return the original text
      return text;
    }
    //decrement to move to the second argument in the array
    var tokenCount = arguments.length - 2;
    for (var token = 0; token <= tokenCount; token = token + 1) {
      //iterate through the tokens and replace their placeholders from the original text in order
      text = text.replace(new RegExp("\\{" + token + "\\}", "gi"),
                                                arguments[token + 1]);

    }
    return text;
  };

})(jQuery);

