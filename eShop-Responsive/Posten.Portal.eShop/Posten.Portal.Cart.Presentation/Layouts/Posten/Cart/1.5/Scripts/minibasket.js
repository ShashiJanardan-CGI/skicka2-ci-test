﻿/*globals $, jQuery, PostenBasket, window*/
/// <reference path="/_layouts/posten/common/presentation/js/jquery-1.6.min.js" />
/// <reference path="/_layouts/posten/Kundkorg/1/Scripts/basket-common.js" />
PostenBasket.MiniBasketControl = {//TODO: $.namespace('PostenBasket.MiniBasketControl');
  ClickNoAction: function (e) { e.preventDefault(); },
  AnyLineItems: function () { return $('.cart-items .product').length !== 0; },
  CheckIfNoLineItems: function () {
    if (PostenBasket.MiniBasketControl.AnyLineItems()) {
      $("#CheckoutButton").attr("class", "button blue");
      $("#CheckoutButton").unbind("click", PostenBasket.MiniBasketControl.ClickNoAction);
    } else {
      $('.cart-items').html('<li class=\'message\'>Din varukorg &auml;r tom</li>');
      $("#CheckoutButton").attr("class", "button black disabled");
      $("#CheckoutButton").bind("click", {}, PostenBasket.MiniBasketControl.ClickNoAction);
    }
  },
  SetMiniBasketShowArrowAndText: function () {
    if ($(".basket-item-container").is(":visible")) {
      $(".arrow-right-white").attr("class", "arrow-up-white");
      $("#minibasket_show").text("Dölj varukorg:");
    }
    else {
      $(".arrow-up-white").attr("class", "arrow-right-white");
      $("#minibasket_show").text("Visa varukorg:");
    }
  },
  ToggleMiniBasketShowArrowAndText: function () {
    if ($(".basket-item-container").is(":visible")) {
      $('.basket-item-container').slideUp(800, PostenBasket.MiniBasketControl.SetMiniBasketShowArrowAndText);
    }
    else {
      $('.basket-item-container').slideDown(800, PostenBasket.MiniBasketControl.SetMiniBasketShowArrowAndText);
    }
  }
};
$(document).ready(function () {

    var currentDeleteButton = null;

    $.get(g_ServerRelativeUrl + '_layouts/Posten/Cart/1.5/MiniBasket.aspx', { date: escape(new Date().valueOf()) }, function (data) {
        $('#kundkorgen_minibasket').html(data);
        
        $(".button-delete").overlay({
            color: '#000000',
            loadSpeed: 200,
            opacity: 0.9,
            closeOnClick: false
        });

        //Center the minicart by setting the width correctly
        $("#minibasketpanel").width($("#ctl00_MSO_ContentDiv").width());

        //also handle the setting of the width when the window resizes
        $(window).bind("resize", function (e) {
            $("#minibasketpanel").width($("#ctl00_MSO_ContentDiv").width());
            $('#minibasketlineitems').width($("#ctl00_MSO_ContentDiv").width());
        });

        $(".button-delete").click(function (e) { e.preventDefault(); currentDeleteButton = this; });
        $('.basket-item-container').slideUp(0);
        PostenBasket.MiniBasketControl.CheckIfNoLineItems();
    });

    $("#s4-bodyContainer").append(PostenBasket.BasketModalTemplate);

    $("#expandLineItemTable").live('click', function () {
        PostenBasket.MiniBasketControl.ToggleMiniBasketShowArrowAndText();
    });

    $("body").click(function (event) {
        var isButtonDelete = $(event.target).is_or_hasparent(".basket-modal-ok");
        if (isButtonDelete) {
            var orderno = $(currentDeleteButton).data("orderno");
            $.ajax({
                type: 'POST',
                url: '/_layouts/Posten/Cart/Common/CartService.aspx/RemoveOrderItem',
                data: '{ "orderNumber" : "' + orderno + '" }',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function () {
                    $(currentDeleteButton).parent().parent().remove();
                    $.get(g_ServerRelativeUrl + '_layouts/Posten/Cart/1.5/MiniBasket.aspx', { date: escape(new Date().valueOf()) }, function (data) {
                        $('#minibasketpanel').html(jQuery("<div />").append(data).find('#minibasketpanel .minibasket-inner-container'));
                        $('#minibasketlineitems').width($("#ctl00_MSO_ContentDiv").width());
                        PostenBasket.MiniBasketControl.SetMiniBasketShowArrowAndText();
                        PostenBasket.MiniBasketControl.CheckIfNoLineItems();
                    }, 'html');
                }
            });
            return;
        }
        var doNotTrigger = ".basket-item-container, .minibasket-inner-container, .basket-modal, #expandLineItemTable";
        if ($(event.target).is_or_hasparent(doNotTrigger)) {
            return;
        }
        if ($(".basket-item-container").is(":visible")) {
            $('.basket-item-container').slideUp(800, PostenBasket.MiniBasketControl.SetMiniBasketShowArrowAndText);
        }
    });


    if ($.browser.msie && parseFloat($.browser.version) < 7) {
        function place_cart() {
            $(".minibasket-container").css({ top: ($(window).scrollTop() + $(window).height() - $(".minibasket-container").height()) + "px" });
            $(".basket-item-wrapper").css({ top: ($(window).scrollTop() + $(window).height() - ($(".basket-item-wrapper").height() + $(".minibasket-container").height())) + "px" });
        }

        function move_cart() {
            $("#posten").remove($(".minibasket-container"));
            $("body").append($(".minibasket-container"));
            $("#posten").remove($(".basket-item-wrapper"));
            $("body").append($(".basket-item-wrapper"));
        }

        // Register events for the cart
        $(window).scroll(function () { place_cart(); });
        $(window).resize(function () { place_cart(); });
        $(window).ready(function () { move_cart(); place_cart(); });
    }
});
