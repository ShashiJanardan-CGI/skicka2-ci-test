﻿using System;
using System.Configuration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Utilities;

namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    public partial class PaymentComplete : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = string.Empty;

            if (!string.IsNullOrEmpty(Request.QueryString["responseCode"]) &&
                Request.QueryString["responseCode"].ToUpper() == "CANCEL")
            {
                url = SPUrlUtility.CombineUrl(SPContext.Current.Web.Url, "_layouts/Posten/Cart/Common/Payment.aspx?Layout=2.0&Action=Revert&" + Request.Url.Query.Substring(1));
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CartWebSealUrl"]))
                {
                    string webSealUrl = Utils.buildWebSealUrl(ConfigurationManager.AppSettings["CartWebSealUrl"]);
                    url = SPUrlUtility.CombineUrl(webSealUrl, "_layouts/Posten/Cart/Common/Payment.aspx?Layout=2.0&Action=Revert&" + Request.Url.Query.Substring(1));
                }
            }
            else
            {
                //build websealurl.
                url = SPUrlUtility.CombineUrl(SPContext.Current.Web.Url, "_layouts/Posten/Cart/Common/Payment.aspx?Layout=2.0&Action=Finalize&" + Request.Url.Query.Substring(1));
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CartWebSealUrl"]))
                {
                    string webSealUrl = Utils.buildWebSealUrl(ConfigurationManager.AppSettings["CartWebSealUrl"]);
                    url = SPUrlUtility.CombineUrl(webSealUrl, "_layouts/Posten/Cart/Common/Payment.aspx?Layout=2.0&Action=Finalize&" + Request.Url.Query.Substring(1));
                }
            }
            Response.Redirect(url);
        }

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }
    }
}
