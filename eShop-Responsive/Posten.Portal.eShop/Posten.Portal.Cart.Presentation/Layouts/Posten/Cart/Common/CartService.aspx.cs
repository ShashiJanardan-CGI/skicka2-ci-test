﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    using System;
    using System.Linq;
    using System.Web.Services;

    using Microsoft.SharePoint.WebControls;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Translators;
    using Posten.Portal.Cart.Utilities;
    using Posten.Portal.Platform.Common.Container;

    public class CartService : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        #endregion Properties

        #region Methods

        [WebMethod]
        public static IScriptBasket EmptyBasket()
        {
            // BasketContext.Current.EmptyBasket();
            return BasketContext.Current.ToScriptBasket();
        }

        [WebMethod]
        public static IScriptBasket GetBasket()
        {
            return BasketContext.Current.ToScriptBasket();
        }

        [WebMethod]
        public static string GetPostalCodeCity(string country, string postalCode)
        {
            var service = IoC.Resolve<Services.IPostalCodeService>();
            var postalCodeInfo = service.GetPostalCode(country, postalCode);
            return postalCodeInfo != null ? postalCodeInfo.City : string.Empty;
        }

        [WebMethod]
        public static IScriptBasket RemoveOrderItem(string orderNumber)
        {
            try
            {
                var orderItem = BasketContext.Current.BasketItem.OrderSummaryItems.Where(item => item.OrderNumber == orderNumber).FirstOrDefault();
                if (orderItem != null)
                {
                    if (orderItem.OrderItemType == OrderItemType.Product)
                    {
                        BasketContext.Current.RemoveProductOrder(orderItem.OrderNumber);
                    }

                    if (orderItem.OrderItemType == OrderItemType.Service)
                    {
                        BasketContext.Current.RemoveServiceOrder(orderItem.OrderNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                // TODO: log
            }

            return BasketContext.Current.ToScriptBasket();
        }

        [WebMethod]
        public static IScriptBasket UpdateOrderItem(string orderNumber, int quantity)
        {
            try
            {
                var productOrder = BasketContext.Current.GetProductOrder(orderNumber);
                productOrder.Quantity = quantity;
                BasketContext.Current.UpdateProductOrder(orderNumber, productOrder);
            }
            catch (Exception ex)
            {
                // TODO: log
            }

            return BasketContext.Current.ToScriptBasket();
        }

        #endregion Methods
    }
}