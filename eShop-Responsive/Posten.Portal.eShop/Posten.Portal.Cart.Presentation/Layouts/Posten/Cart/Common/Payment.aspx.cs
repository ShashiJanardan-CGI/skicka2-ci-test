﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading;
    using System.Web;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Utilities;
    using Microsoft.SharePoint.WebControls;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Services;
    using Posten.Portal.Cart.Translators;
    using Posten.Portal.Cart.Utilities;
    using Posten.Portal.Platform.Common.Container;
    using Posten.Portal.Platform.Common.Logging;

    #region Enumerations

    internal enum PaymentContextAction
    {
        Revert,
        Initalize,
        Finalize,
        Complete
    }

    #endregion Enumerations

    public partial class PaymentPage : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        protected int PostBackCount
        {
            get { return Convert.ToInt32(this.PostBackCounter.Value); }
            set { this.PostBackCounter.Value = value.ToString(); }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            PaymentContext paymentContext = null;

            // do not cache this page
            this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            this.Response.Cache.SetNoStore();

            if (!this.IsPostBack)
            {
                // send an automatic postback to the same page, a trick to show the response faster to the user
                this.RegisterAutoPostBackScript(0);
                return;
            }

            try
            {
                // Load the active payment context, will return null if the checksum-url is invalid or if the basket is empty or in-process
                paymentContext = PaymentContext.FromRequest(this.Request);
                if (paymentContext == null)
                {
                    Response.Redirect(this.GetCheckoutPageUrl(null, "Could not load an active payment context"));
                    return;
                }

                //When using version 1.5 language is always sv-se
                if (paymentContext.Layout == "1.5")
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("sv-se");

                // Check if the basket has a booking-id, redirect to the confirmation page if it does
                if (!string.IsNullOrEmpty(paymentContext.BasketContext.BasketItem.BookingId))
                {
                    // Call VerifyPaymentComplete ?
                    this.Response.Redirect(paymentContext.GetConfirmationPageUrl().ToString());
                    return;
                }

                // Initialize the payment and redirect to the payment provider
                // Baskets that have been completed will never reach here.
                if (paymentContext.Action == PaymentContextAction.Initalize)
                {
                    this.InitializePayment(paymentContext);
                    return;
                }

                // Finalize the payment, we get here when the user gets back from the payment provider (even if the user presses cancel)
                if (paymentContext.Action == PaymentContextAction.Finalize
                    && (paymentContext.BasketContext.BasketItem.BasketStatus == BasketStatus.RedirectedToPayment
                        || paymentContext.BasketContext.BasketItem.BasketStatus == BasketStatus.SentToFinalizePayment))
                {
                    this.FinalizePayment(paymentContext);
                    return;
                }

                // After the payment has been finalized and updated in commerce server, we transform the basket to an order
                if (paymentContext.Action == PaymentContextAction.Complete
                    && paymentContext.BasketContext.BasketItem.BasketStatus == BasketStatus.SentToFinalizePayment)
                {
                    this.CompletePayment(paymentContext);
                    return;
                }

                // When the user presses a
                if (paymentContext.Action == PaymentContextAction.Revert)
                {
                    // TODO: what staus is ok to revert?
                    if (paymentContext.BasketContext.BasketItem.BasketStatus != BasketStatus.SentToConfiramtion)
                    {
                        this.RevertPayment(paymentContext);
                        return;
                    }
                }

                this.UnknownStateAndBasketStatus(paymentContext);
            }
            catch (ThreadAbortException ex)
            {
                ex.RecursiveLog();

                // Response.Redirect throws ThreadAbortException
                throw;
            }
            catch (PaymentCompleteException ex)
            {
                this.RedirectToErrorPage(paymentContext, ex.Message);
            }
            catch (Exception ex)
            {
                ex.RecursiveLog();

                // if there is a problem during the initalize action we can recover to the checkout page instead of the error page
                if (paymentContext != null && paymentContext.Action == PaymentContextAction.Initalize)
                {
                    // initiate revert payment flow
                    this.Response.Redirect(paymentContext.GetPaymentRevertUrl().ToString());
                    return;
                }

                this.RedirectToErrorPage(paymentContext, ex.Message);
            }
        }

        private void CompletePayment(PaymentContext paymentContext)
        {
            // load the services that we need
            var basketService = IoC.Resolve<IBasketService>();
            try
            {
                // Check to see if there are only service orders from Skicka in the basket
                bool doBypassQueue =
                    paymentContext.BasketContext.ProductOrderItems.Count == 0 &&
                    paymentContext.BasketContext.ServiceOrderItems.All(o => o.ApplicationName == "Skicka");

                // No need to call VerifyPaymentComplete here since basket will have a BookingId if the user refreshes the page
                PostenLogger.LogVerbose(string.Format("PaymentComplete called for BasketId:{0}", paymentContext.BasketId), "CartBusiness", this.GetType(), "PaymentComplete");

                bool basketResult = false;
                string basketMessage = string.Empty;
                if (doBypassQueue)
                {
                    // Call the the bypassqueue payment method for Skicka Direkt baskets (ie. basket with Skicka Direkt service orders only)
                    basketMessage = basketService.PaymentCompleteBypassQueue(paymentContext.BasketId.ToString(), paymentContext.TransactionId, out basketResult);
                }
                else
                {
                    basketMessage = basketService.PaymentComplete(paymentContext.BasketId.ToString(), paymentContext.TransactionId, out basketResult);
                }

                if (!basketResult)
                {
                    throw new PaymentCompleteException(
                        basketMessage,
                        new Exception("The BasketService.PaymentComplete()/BasketService.PaymentCompleteByPassQueue() call was unsuccessful"));
                }
            }
            catch (Exception ex) {
                ex.RecursiveLog();
                throw;
            }

            // automatic postback, should redirect to the confirmation page since BookingId will be set
            paymentContext.BasketContext.Reload();
            this.Response.Redirect(paymentContext.GetConfirmationPageUrl().ToString());
        }

        private void FinalizePayment(PaymentContext paymentContext)
        {
            try
            {
                // load the services that we need
                var paymentService = IoC.Resolve<IPaymentService>();
                var basketService = IoC.Resolve<IBasketService>();
                var itemNotesServiceBean = new ItemNotesServiceBeanAgent();// IoC.Resolve<IItemNotesServiceBean>();

                // get all orders needed for calling itemNotesServiceBean.InformPaymentComplete to set status in Skicka Direkt
                var skickaServiceOrderItems = paymentContext.BasketContext.ServiceOrderItems.Where(o => o.ApplicationName == "Skicka");
                string[] skickaServiceOrderNumbers = new string[skickaServiceOrderItems.Count()];
                int i = 0;
                foreach (IServiceOrderItem serviceOrderItem in skickaServiceOrderItems)
                {
                    skickaServiceOrderNumbers[i] = serviceOrderItem.OrderNumber;
                    i++;
                }

                // clear the session variable since we do not need it any more
                paymentContext.ClearSessionState();

                PostenLogger.LogVerbose(string.Format("PaymentFinalize called for BasketId:{0}", paymentContext.BasketId), "CartBusiness", this.GetType(), "PaymentFinalize");
                //only call paymentservice finalisepayment when it is not an invoice payment
                if (paymentContext.PaymentMethod == PaymentMethodType.Invoice)
                {
                    // Just finalize and exit.
                    var basketResult = basketService.FinalisePayment(paymentContext.BasketId.ToString(), DateTime.Now, paymentContext.PaymentMethodLocalizedString, new Uri("../receipt.ashx", UriKind.Relative), paymentContext.TransactionId, 30.ToString());

                    // automatic redirect to the next step
                    Response.Cookies["PostenPaymentMethod"].Expires = DateTime.Now.AddDays(-1);

                    this.SendInformPaymentCompleteStatusSignal(Services.Proxy.ItemNotesServiceBean.status.OK, paymentContext, itemNotesServiceBean, skickaServiceOrderItems, skickaServiceOrderNumbers, "FinalizePayment");

                    CompletePayment(paymentContext);

                    return;
                }

                IPaymentResult paymentResult = null;

                try
                {
                    paymentResult = paymentService.FinalisePayment(paymentContext.TransactionId, false, true, null);
                }
                catch (FaultException<Services.Proxy.PaymentService.FinalisePaymentException> exception)
                {
                    exception.RecursiveLog();

                    this.Response.Redirect(this.GetCheckoutPageUrl(paymentContext, Utils.GetResourceString("Payment_Reverted_Error_Text")));

                    // redirect to checkout page for Status=E
                    //if (this.Request.QueryString["Status"] == "E")
                    //{
                    //    this.Response.Redirect(paymentContext.GetPaymentRevertUrl().ToString());
                    //    return;
                    //}
                    //else if (this.Request.QueryString["Status"] == "A")
                    //{
                    //    // redirect to error page for Status=A
                    //    this.PostBackCount = 99;
                    //}

                    this.SendInformPaymentCompleteStatusSignal(Services.Proxy.ItemNotesServiceBean.status.Failed, paymentContext, itemNotesServiceBean, skickaServiceOrderItems, skickaServiceOrderNumbers, "FinalizePayment");

                    throw;
                }

                PostenLogger.LogVerbose(string.Format("Payment FinalizePayment Successfully called for BasketId:{0}", paymentContext.BasketId), "CartBusiness", this.GetType(), "PaymentFinalize");

                if (paymentResult == null || paymentResult.TransactionId != paymentContext.TransactionId)
                {
                    this.SendInformPaymentCompleteStatusSignal(Services.Proxy.ItemNotesServiceBean.status.Failed, paymentContext, itemNotesServiceBean, skickaServiceOrderItems, skickaServiceOrderNumbers, "FinalizePayment");

                    throw new Exception("The PaymentService.FinalisePayment() call was unsuccessful");
                }

                try
                {
                    var basketResult = basketService.FinalisePayment(paymentContext.BasketId.ToString(), DateTime.Now, paymentContext.PaymentMethodLocalizedString, paymentResult.ReceiptUrl, paymentResult.TransactionId, paymentResult.TransactionState.ToString());
                    if (!basketResult)
                    {
                        // TODO: return to checkout page is the payment was unsuccessful
                        throw new Exception("The BasketService.FinalisePayment() call was unsuccessful");
                    }
                    //remove cookie which stores paymentmethod
                    Response.Cookies["PostenPaymentMethod"].Expires = DateTime.Now.AddDays(-1);
                }
                catch (Exception ex)
                {
                    this.SendInformPaymentCompleteStatusSignal(Services.Proxy.ItemNotesServiceBean.status.Failed, paymentContext, itemNotesServiceBean, skickaServiceOrderItems, skickaServiceOrderNumbers, "FinalizePayment");

                    ex.RecursiveLog();
                    throw;
                }

                PostenLogger.LogVerbose(string.Format("InformPaymentComplete items:{0}", skickaServiceOrderItems.Count()), "CartBusiness", this.GetType(), "FinalizePayment");
                this.SendInformPaymentCompleteStatusSignal(Services.Proxy.ItemNotesServiceBean.status.OK, paymentContext, itemNotesServiceBean, skickaServiceOrderItems, skickaServiceOrderNumbers, "FinalizePayment");

                PostenLogger.LogVerbose(string.Format("Basket FinalizePayment Successfully called for BasketId:{0}. Redirecting to Complete", paymentContext.BasketId), "CartBusiness", this.GetType(), "PaymentFinalize");
                this.CompletePayment(paymentContext);

            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private string GetCheckoutPageUrl(PaymentContext paymentContext, string message)
        {
            string layout = paymentContext != null ? paymentContext.Layout : this.Request.QueryString["Layout"] ?? "2.0";
            return SPUrlUtility.CombineUrl(this.Web.ServerRelativeUrl, "_layouts/Posten/Cart/" + layout + "/Checkout.aspx?Message=" + HttpUtility.UrlEncode(message));
        }

        private string GetErrorPageUrl(PaymentContext paymentContext, string message)
        {
            string layout = paymentContext != null ? paymentContext.Layout : this.Request.QueryString["Layout"] ?? "2.0";
            return SPUrlUtility.CombineUrl(this.Web.ServerRelativeUrl, "_layouts/Posten/Cart/" + layout + "/Checkout.aspx?Message=" + HttpUtility.UrlEncode(message));
        }

        private void InitializePayment(PaymentContext paymentContext)
        {
            //Revert payment if the status is incorrect.
            if (paymentContext.BasketContext.BasketItem.BasketStatus != BasketStatus.ReadyForCheckout)
            {
                this.Response.Redirect(paymentContext.GetPaymentRevertUrl().ToString());
                return;
            }

            // load the services that we need
            var paymentService = IoC.Resolve<IPaymentService>();
            var basketService = IoC.Resolve<IBasketService>();
            var itemNotesServiceBean = new ItemNotesServiceBeanAgent();//IoC.Resolve<IItemNotesServiceBean>();

            // call payment service, in v2 the payment service will support sending dynamic urls directly to the service-method
            // right now there is code that will try to inject the correct urls and recalculate the MAC-parameter in the agent-method
            IPaymentResponse paymentResponse;

            //call directpayment if it is invoice only
            var paymentRequest = paymentContext.BasketContext.ToPaymentRequest(paymentContext.PaymentMethod);
            paymentRequest.UserId = HttpContext.Current.User.Identity.Name;

            paymentResponse = paymentService.InitialisePayment(paymentRequest, paymentContext.GetPaymentFinalizeUrl(), paymentContext.GetPaymentRevertUrl());
            if (paymentResponse.TransactionId <= 0)
            {
                throw new Exception("The PaymentService.InitialisePayment() call was unsuccessful");
            }

            // NOTE: if this call fails after a successful payment service call we can refresh the page and retry, although it will create a new transactionId for each try
            var basketResult = basketService.InitialisePayment(paymentContext.BasketId.ToString(), paymentResponse.DeliveryIdMappings.Select(item => item.DeliveryId).ToArray(), paymentResponse.TransactionId);
            if (!basketResult)
            {
                throw new Exception("The BasketService.InitialisePayment() call was unsuccessful");
            }

            this.SendInformPaymentPendingSignal(paymentContext, itemNotesServiceBean, paymentResponse.TransactionId);

            // NOTE: if the user has come this far whitout any errors we do not know if the user has compleated the payment or if the user never reached the payment page
            //       therfore if the user refresh the page it will redirect to the error-page when the action and status is: Action=Initalize, Status=RedirectedToPayment

            // prepare the postback to the payment provider
            this.PostBackCounter.Visible = false;
            this.PaymentForm.Action = paymentResponse.PaymentUrl.GetComponents(UriComponents.SchemeAndServer | UriComponents.Path, UriFormat.Unescaped);
            var queryString = HttpUtility.ParseQueryString(paymentResponse.PaymentUrl.Query);
            this.PaymentInputs.DataSource = queryString.AllKeys.Select(key => new KeyValuePair<string, string>(key, HttpUtility.UrlDecode(queryString[key])));
            this.PaymentInputs.DataBind();

            // save session
            paymentContext.SaveSessionState(paymentResponse.TransactionId);

            // trigger an automatic postback to the payment provider
            this.RegisterAutoPostBackScript(0);
        }

        private void RedirectToErrorPage(PaymentContext paymentContext, string message)
        {
            this.Response.Redirect(this.GetErrorPageUrl(paymentContext, message));
        }

        private void RegisterAutoPostBackScript(int delay)
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "AutoPostBackScript", "var postBackDelay = " + Math.Max(delay, 50) + ";window.onload = autoPostBack;", true);
        }

        private void RevertPayment(PaymentContext paymentContext)
        {
            // load the services that we need
            var basketService = IoC.Resolve<IBasketService>();
            if (paymentContext.BasketContext.BasketItem.BasketStatus != BasketStatus.InProcess)
            {
                try
                {
                    // reverse the basket so it can be shown on the checkout page if it is not in process;
                    var basketResult = basketService.ReverseInitialisePayment(paymentContext.BasketId.ToString(),
                                                                              paymentContext.TransactionId);

                    if (!basketResult)
                    {
                        throw new Exception("The BasketService.ReverseInitialisePayment() call was unsuccessful");
                    }
                }
                catch (Exception ex)
                {
                    ex.RecursiveLog();
                    throw;
                }
            }

            // redirect to checkout page
            this.Response.Redirect(this.GetCheckoutPageUrl(paymentContext, Utils.GetResourceString("Payment_Reverted_Error_Text")));
        }

        private void SendInformPaymentCompleteStatusSignal(Services.Proxy.ItemNotesServiceBean.status status, PaymentContext paymentContext, IItemNotesServiceBean itemNotesServiceBean, IEnumerable<IServiceOrderItem> skickaServiceOrderItems, string[] skickaServiceOrderNumbers, string callingMethodName)
        {
            PostenLogger.LogVerbose(
                string.Format("Entering SendInformPaymentCompleteStatusSignal with {1} ServiceItems for BasketId:{0}",
                paymentContext.BasketId,
                skickaServiceOrderItems.Count()),
                "CartBusiness",
                this.GetType(),
                "FinalizePayment");

            if (skickaServiceOrderItems.Count() > 0)
            {
                PostenLogger.LogInfo(string.Format("Sending InformPaymentComplete (status: {1}) to ItemNoteServiceBean for BasketId:{0}", paymentContext.BasketId, status.ToString()),
                    "CartBusiness",
                    this.GetType(),
                    "FinalizePayment");

                itemNotesServiceBean.InformPaymentComplete(skickaServiceOrderNumbers, status);
            }
        }

        private void SendInformPaymentPendingSignal(PaymentContext paymentContext, IItemNotesServiceBean itemNotesServiceBean, long transactionId)
        {
            PostenLogger.LogVerbose(string.Format("Entering SendInformPaymentPendingSignal for BasketId:{0} and TransactionId:{1}", paymentContext.BasketId, transactionId), "CartBusiness", this.GetType(), "SendInformPaymentPendingSignal");

            // Get all service orders which are sent by the Skicka Direkt application and set status in Skicka Direkt system
            var skickaServiceOrderItems = paymentContext.BasketContext.ServiceOrderItems.Where(o => o.ApplicationName == "Skicka");
            if (skickaServiceOrderItems.Count() > 0)
            {
                PostenLogger.LogInfo(string.Format("Sending InformationPaymentPending to ItemNoteServiceBean for BasketId:{0} and TransactionId:{1}", paymentContext.BasketId, transactionId), "CartBusiness", this.GetType(), "InitializePayement");
                string[] orderNumbers = new string[skickaServiceOrderItems.Count()];
                int i = 0;
                foreach (IServiceOrderItem serviceOrderItem in skickaServiceOrderItems)
                {
                    orderNumbers[i] = serviceOrderItem.OrderNumber;
                    i++;
                }

                itemNotesServiceBean.InformPaymentPending(paymentContext.BasketContext.BasketItem.BillingAccountInfo.Email, orderNumbers, transactionId.ToString());
            }
        }

        private void UnknownStateAndBasketStatus(PaymentContext paymentContext)
        {
            // TODO: logging
            var details = string.Format("Action={0}, Status={1}", paymentContext.Action, paymentContext.BasketContext.BasketItem.BasketStatus);
            this.RedirectToErrorPage(paymentContext, "Payment state is not in a valid state: " + details);
        }

        #endregion Methods
    }

    internal class PaymentContext
    {
        #region Fields

        private BasketContext basketContext;

        #endregion Fields

        #region Constructors

        private PaymentContext()
        {
        }

        #endregion Constructors

        #region Properties

        public PaymentContextAction Action
        {
            get; set;
        }

        public BasketContext BasketContext
        {
            get
            {
                if (this.basketContext == null)
                {
                    this.basketContext = BasketContext.GetContext(this.BasketId);
                }

                return this.basketContext;
            }
        }

        public Guid BasketId
        {
            get; set;
        }

        public string CompanyName
        {
            get; set;
        }

        public bool IsAnOrganization
        {
            get; set;
        }

        public string Layout
        {
            get; set;
        }

        public PaymentMethodType PaymentMethod
        {
            get; set;
        }

        public string PaymentMethodLocalizedString
        {
            get
            {

                switch (this.PaymentMethod)
                {
                    case PaymentMethodType.PayPage:
                        return Utils.GetResourceString("CARD_OR_BANK");
                    case PaymentMethodType.CreditCard:
                        return Utils.GetResourceString("CreditCard");
                    case PaymentMethodType.Invoice:
                        return Utils.GetResourceString("Invoice");
                    case PaymentMethodType.OnlineBanking:
                        return Utils.GetResourceString("OnlineBanking");
                }
                return string.Empty;
            }
        }

        public long TransactionId
        {
            get; set;
        }

        public Uri Url
        {
            get
            {
                var urlstring = SPContext.Current.Web.Url;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CartWebSealUrl"]))
                {
                    string webSealUrl = Posten.Portal.Cart.Presentation.Utils.buildWebSealUrl(ConfigurationManager.AppSettings["CartWebSealUrl"]);
                    urlstring=SPUrlUtility.CombineUrl(webSealUrl, "_layouts/Posten/Cart/Common/Payment.aspx");
                }

                var builder = new UriBuilder(urlstring);

                builder.Query = string.Format("PaymentMethod={0}&Layout={1}&Action={2}", PaymentMethod.ToString(), Layout,
                                              Action.ToString());

                return builder.Uri;
            }
        }

        #endregion Properties

        #region Methods

        public static PaymentContext FromRequest(HttpRequest request)
        {
            var queryString = HttpUtility.ParseQueryString(request.Url.Query);

            string basketid =string.Empty;
            var context = new PaymentContext();

            //Assume basketcookie is there because otherwise you can't do anything.
            basketid = request.Cookies["PostenBasketId"].Value;

            context.BasketId = basketid.ToGuid();
            context.Action = (queryString["Action"] ?? string.Empty).ToEnum(PaymentContextAction.Initalize);
            context.Layout = queryString["Layout"] ?? "2.0";
            context.TransactionId = Utilities.Utils.ToLong(queryString["TransactionId"] ?? string.Empty);

            context.IsAnOrganization = !string.IsNullOrEmpty(queryString["IsOrganization"]);

            if (!string.IsNullOrEmpty(queryString["PaymentMethod"]))
            {
            context.PaymentMethod = (queryString["PaymentMethod"] ?? string.Empty).ToEnum(PaymentMethodType.PayPage);
                var paymentmethodcookie = HttpContext.Current.Response.Cookies["PostenPaymentMethod"];
                paymentmethodcookie.Value = queryString["PaymentMethod"];
                paymentmethodcookie.Expires = DateTime.Now.AddDays(5);
            }
            else
            {
                context.PaymentMethod = (request.Cookies["PostenPaymentMethod"].Value ?? string.Empty).ToEnum(PaymentMethodType.PayPage);
            }

            var basketContext = context.BasketContext;
            if (basketContext == null || basketContext.BasketItem == null)
            {
                return null;
            }

            // load transaction id from basket if it is not already set
            if (context.TransactionId <= 0)
            {
                context.TransactionId = basketContext.BasketItem.TransactionId;
            }

            return context;
        }

        public static PaymentContext FromSession()
        {
            if (HttpContext.Current.Session != null)
            {
                var url = HttpContext.Current.Session["PaymentContextUrl"] as string;
                if (!string.IsNullOrEmpty(url))
                {
                    return FromRequest(HttpContext.Current.Request);
                }
            }

            return null;
        }

        public static Uri GetConfirmationPageUrl(BasketContext basketContext, string layout)
        {
            var context = new PaymentContext();
            context.BasketId = basketContext.BasketItem.BasketId.ToGuid();
            context.Layout = layout;
            return context.GetConfirmationPageUrl();
        }

        public static Uri GetFinalizePaymentUrlForInvoice(BasketContext basketContext, string layout, PaymentMethodType paymentMethod)
        {
            var finalizepaymentcontext = new PaymentContext();

                finalizepaymentcontext.BasketId = basketContext.BasketItem.BasketId.ToGuid();
                finalizepaymentcontext.Layout = layout;
                finalizepaymentcontext.Action = PaymentContextAction.Finalize;
                finalizepaymentcontext.PaymentMethod = paymentMethod;

                 return finalizepaymentcontext.Url;
        }

        public static Uri GetPaymentInitializeUrl(BasketContext basketContext, string layout, PaymentMethodType paymentMethod)
        {
            var context = new PaymentContext();
            context.BasketId = basketContext.BasketItem.BasketId.ToGuid();
            context.Layout = layout;
            context.PaymentMethod = paymentMethod;
            context.Action = PaymentContextAction.Initalize;
            return context.Url;
        }

        public void ClearSessionState()
        {
            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["PaymentContextUrl"] = null;
            }
        }

        public Uri GetConfirmationPageUrl()
        {
            var url = SPUrlUtility.CombineUrl(SPContext.Current.Web.Url, "_layouts/Posten/Cart/" + this.Layout + "/Confirmation.aspx?BookingId=" + HttpUtility.UrlEncode(this.BasketContext.BasketItem.BookingId));
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CartWebSealUrl"]))
            {
                string webSealUrl = Posten.Portal.Cart.Presentation.Utils.buildWebSealUrl(ConfigurationManager.AppSettings["CartWebSealUrl"]);
                url = SPUrlUtility.CombineUrl(webSealUrl, "_layouts/Posten/Cart/" + this.Layout + "/Confirmation.aspx?BookingId=" + HttpUtility.UrlEncode(this.BasketContext.BasketItem.BookingId));
            }

            return new Uri(url);
        }

        public Uri GetPaymentCompleteUrl()
        {
            this.Action= PaymentContextAction.Complete;
            return this.Url;
        }

        public Uri GetPaymentFinalizeUrl()
        {
            this.TransactionId = 0;
            this.Action = PaymentContextAction.Finalize;
            return this.Url;
        }

        public Uri GetPaymentRevertUrl()
        {
            this.TransactionId = 0;
            this.Action = PaymentContextAction.Revert;
            return this.Url;
        }

        public void SaveSessionState(long transactionId)
        {
            this.TransactionId = transactionId;
            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["PaymentContextUrl"] = this.GetPaymentFinalizeUrl().ToString();
            }
        }

        #endregion Methods
    }
}