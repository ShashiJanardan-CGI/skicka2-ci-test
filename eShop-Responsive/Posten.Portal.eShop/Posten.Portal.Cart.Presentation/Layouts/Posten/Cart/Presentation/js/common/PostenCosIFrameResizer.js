﻿
// Requirement:
// jQuery postMessage plugin
// benalman.com/projects/jquery-postmessage-plugin/

window.PostenCosFrameResizer = function () {
    var self = this;

    // For good measure
    $('body').append('<div style="clear:both;"></div>');

    // Get the parent URL
    this.parent_url;

    if (window.location.hash) {
        this.parent_url = decodeURIComponent(window.location.hash.substring(1));
    }
    else { // for POSTs and GETs (less secure)
        this.parent_url = "*";
    }

};

PostenCosFrameResizer.prototype.parent_url;

PostenCosFrameResizer.prototype.autoResize = function () {
    var self = this;
    window.setInterval(function () {
        self.resize();
    }, 200);
};

PostenCosFrameResizer.prototype.resize = function () {
    this.setHeight();
};

PostenCosFrameResizer.prototype.setHeight = function () {
    var self = this;

    var pageHeight = $('body').outerHeight(true);

    // The first param is serialized using $.param (if not a string) and passed to the
    // parent window. If window.postMessage exists, the param is passed using that,
    // otherwise it is passed in the location hash (that's why parent_url is required).
    // The second param is the targetOrigin
    $.postMessage({ if_height: pageHeight }, self.parent_url);
};