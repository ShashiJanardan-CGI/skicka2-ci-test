﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="Posten.Portal.Cart.Presentation.ApplicationPages.TestCartService" DynamicMasterPageFile="~masterurl/custom.master" %>

<asp:Content ID="MiniBasket" ContentPlaceHolderID="PlaceHolderMiniBasket" runat="server">
</asp:Content>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<script type="text/javascript" src="/_layouts/Posten/Cart/Common/Scripts/MiniBasket.js"></script>
<script type="text/javascript" src="/_layouts/Posten/Cart/Common/Scripts/jquery.tmpl.min.js"></script>
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<div class="box colspan-4">

<%-- 
<link href="/_layouts/Posten/Cart/1.5/Styles/minibasket.css" type="text/css" rel="stylesheet" />
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common.css" type="text/css" rel="stylesheet" />
<!--[if IE 6]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie6.css" type="text/css" rel="stylesheet" />
<![endif]-->
<!--[if IE]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie.css" type="text/css" rel="stylesheet" />
<![endif]-->
--%>

<div id="miniBasket" class="mini-basket"></div>

<script type="text/javascript">
    jQuery(function ($) {
        Posten.MiniBasket.init(function (basket) { $('#miniBasket').html($('#miniBasketTemplate').tmpl(basket)); });
        Posten.MiniBasket.update(function () { $('.mini-basket .item-container').hide(); });

        var updateText = function () {
            if ($('.mini-basket .item-container').is(':visible')) {
                $('.mini-basket .button-show > strong').html('Dölj varukorg:');
                $('.mini-basket .button-show').addClass('open');
            } else {
                $('.mini-basket .button-show > strong').html('Visa varukorg:');
                $('.mini-basket .button-show').removeClass('open');
            }
        };

        $('.mini-basket .button-show').live('click', function (event) {
            if ($('.mini-basket .item-container').is(':visible')) {
                $('.mini-basket .item-container').slideUp(800, updateText);
            } else {
                $('.mini-basket .item-container').slideDown(800, updateText);
            }
        });

        $('.mini-basket .button-delete').live('click', function (event) {
            Posten.MiniBasket.removeOrderItem($(this).data('ordernumber'));
        });

        $('body').bind('click', function (event) {
            if ($('.mini-basket .item-container').is(':visible') && !$(event.target).closest('.mini-basket').length || $(event.target).is('.mini-basket .item-wrapper')) {
                $('.mini-basket .item-container').slideUp(800, updateText);
            }
        });

    });
</script>

<script id="miniBasketTemplate" type="text/x-jquery-tmpl"> 
    <div class="item-wrapper">
        <div class="item-container">
            <div class="top-border"></div>
            <div class="side-border">
                <ul class="basket-items">
                    {{tmpl(OrderItems) "#orderItemTemplate"}}
                </ul>
            </div>
        </div>
    </div>
    <div class="summary-wrapper">
        <div class="summary-container">
            <div class="show-items">
                <div class="basket-separator"></div>
                <a class="button-show" href="javascript:void(0)" title="Visa varukorg">
                    <span class="arrow"></span><strong>Visa varukorg:</strong> ${NumberOfOrderItems}&nbsp;artiklar
                </a>
                <div class="basket-separator"></div>
            </div>
            <div class="checkout">
                <span class="value"><strong>Totalt:</strong> ${ValueIncludingVat}</span>
                <a class="button-checkout" href="<asp:Literal runat='server' Text='<% $SPUrl:~site/_layouts/Posten/Cart/1.5/Checkout.aspx %>' />"><span>Till kassan</span></a>
            </div>
        </div>
    </div>
</script>

<script id="orderItemTemplate" type="text/x-jquery-tmpl">
    <li class="basket-item">
        <span class="actions">
            {{if Quantity}}
            <input type="text" class="input-quantity" value="${Quantity}" />
            {{/if}}
            <span class="price">${Price}</span>
            {{if EditUrl}}
            <a class="button-edit" data-ordernumber="${OrderNumber}" href="${EditUrl}" title="&Auml;ndra">
                <image src="/_layouts/Posten/Cart/1.5/images/icon-edit.gif" alt="&Auml;ndra" />
            </a>
            {{/if}}
            <a class="button-delete" data-ordernumber="${OrderNumber}" href="javascript:void(0)" title="Ta bort">
                <img src="/_layouts/Posten/Cart/1.5/images/icon-delete.gif" alt="Ta bort" />
            </a>
        </span>
        <span class="info">
            <span class="name">${Name}</span><br />
            <span class="description">${Description}</span>
        </span>
    </li>
</script>

</div>

<style type="text/css">

.mini-basket .item-wrapper {
    bottom: 38px;
    left: 0;
    position: fixed;
    width: 100%;
}

.mini-basket .item-wrapper .item-container {
  margin: 0 auto;
  width: 782px;
}

.mini-basket .item-wrapper div.top-border {
    background: url("/_layouts/Posten/Cart/1.5/Images/cart-items-border.png") no-repeat;
    height: 9px;
}

.mini-basket .item-wrapper div.side-border {
    background: url("/_layouts/Posten/Cart/1.5/Images/cart-items-border.png") no-repeat 0 -9px;
    padding: 0 9px;
}

.mini-basket ul.basket-items 
{
    margin: 0;
    padding: 0;
    max-height: 400px;
    overflow: auto;
}

.mini-basket ul.basket-items li
{
    list-style: none;
    background: #fff;
    color: #031E50;
    padding: 10px;
}

.mini-basket ul.basket-items li + li 
{
    border-top: 1px solid #E5E5E5;
}

.mini-basket .basket-item span.name
{
    font-weight: bold;
}

.mini-basket .basket-item span.description
{
    color: #5A6C8C;
}

.mini-basket .basket-item span.info
{
    display: inline-block;
}

.mini-basket .basket-item span.actions
{
    float: right;
    display: inline-block;
    line-height: 25px;
}

.mini-basket .summary-wrapper {
  background: url('/_layouts/Posten/Cart/1.5/Images/elements-borders-sprite-2.png') repeat-x;
  color: #ffffff;
  bottom: 0;
  left: 0;
  position: fixed;
  height: 38px;
  width: 100%;
}

.mini-basket .summary-wrapper .summary-container {
  margin: 0 auto;
  width: 748px;
}

.mini-basket .summary-wrapper .basket-separator {
  background-color: #577532;
  border: 0px;
  border-left: 1px solid #91ac6e;
  float: left;
  height: 38px;
  width: 1px;
  clear: none;
}

.mini-basket .summary-wrapper a.button-show {
  background: url('/_layouts/Posten/Cart/1.5/Images/elements-borders-sprite-2.png') repeat-x 0 -114px;
  float: left;
  line-height: 38px;
  padding: 0 14px;
  color: #ffffff;
}

.mini-basket .summary-wrapper a.button-show:hover {
  background-position: 0 -76px;
}

.mini-basket .summary-wrapper a.button-show > span.arrow {
    background: url("/_layouts/Posten/Cart/1.5/Images/elements-sprite-2.png") no-repeat -27px 0;
    display: inline-block;
    height: 9px;
    margin-bottom: -1px;
    margin-right: 5px;
    width: 9px;
}

.mini-basket .summary-wrapper a.button-show.open > span.arrow {
    background-position: 0 0;
}

.mini-basket .summary-wrapper .checkout {
  float: right;
  line-height: 38px;
  padding: 0 8px;
  color: #ffffff;
}

.mini-basket .summary-wrapper a.button-checkout
{
    background: url("/_layouts/Posten/Cart/1.5/Images/button-sprite.png") no-repeat left -200px;
    color: #FFFFFF;
    font-weight: bold;
    display: inline-block;
    line-height: 18px;
    padding-left: 9px;
    margin-left: 8px;
}

.mini-basket .summary-wrapper a.button-checkout > span
{
    background: url("/_layouts/Posten/Cart/1.5/Images/button-sprite.png") no-repeat right -250px;
    cursor: pointer;
    display: inline-block;
    margin-right: -9px;
    padding-right: 9px;
}

.mini-basket .summary-wrapper a.button-checkout:hover
{
    background: url("/_layouts/Posten/Cart/1.5/Images/button-sprite.png") no-repeat left -300px;
}

.mini-basket .summary-wrapper a.button-checkout:hover > span
{
    background: url("/_layouts/Posten/Cart/1.5/Images/button-sprite.png") no-repeat right -350px;
}

</style>

</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Test Mini Basket
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
Test Mini Basket
</asp:Content>
