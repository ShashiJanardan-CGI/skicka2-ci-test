﻿namespace Posten.Portal.Cart.Presentation.TestPages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebControls;

    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Utilities;

    public partial class TestServicePage : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get
            {
                return true;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindBasketLineItems();
        }

        protected void uiAddServiceOrder_Click(object sender, EventArgs e)
        {
            string orderNo = null;

            var numberOfItems = int.Parse(NumberOfLineItemsTextBox.Text);
            for (int i = 0; i < numberOfItems; i++)
            {
                var serviceOrder = GetServiceOrder(AppIdRadioButton.SelectedValue, AppIdRadioButton.SelectedItem.Text);

                BasketOrderItem result = BasketContext.Current.AddServiceOrder(serviceOrder);

                if (orderNo == null)
                    orderNo = result.OrderId;
            }

            this.BindBasketLineItems();
        }

        private void BindBasketLineItems()
        {
            uiBasketLineItems.DataSource = BasketContext.Current.ServiceOrderItems;
            uiBasketLineItems.DataBind();
        }

        private IServiceOrderItem GetServiceOrder(string applicationId,  string applicationname)
        {
            var orderLines = new List<IServiceOrderLineItem>();

            orderLines.Add(new ServiceOrderLineItem
            {
                OrderLineNumber = 0,
                ArticleId = "1001014",
                ArticleName = "DPD Företagspaket 09:00",
                ArticlePrice = 25.5m,
                Quantity = 1,
                Total = 25.5m,
                VatPercentage = 0.25m,
                Vat = 6.375m,
                CompanyCode = "FO92",
                CustomerId = "20547566",
                TaxCountry = "SE",
                FootnoteText = "",
                VatCode="U1"

            });

            orderLines.Add(new ServiceOrderLineItem
            {
                OrderLineNumber = 1,
                ArticleId = "1001015",
                ArticleName = "DPD Företagspaket 09:00 Second Line Item",
                ArticlePrice = 25.5m,
                Quantity = 1,
                Total = 25.5m,
                VatPercentage = 0.25m,
                Vat = 6.375m,
                CompanyCode = "FO92",
                CustomerId = "20547566",
                TaxCountry = "SE",
                FootnoteText = "",
                VatCode = "U1"
            });

            return new ServiceOrderItem()
                       {
                           ApplicationId = applicationId,
                           ApplicationName= applicationname,
                           ServiceOrderLines = orderLines.ToArray(),
                           Name = txtServicename.Text,
                           FreeTextField1 = txtFTF1.Text,
                           FreeTextField2 = txtFTF2.Text,
                           Details = txtDetails.Text,
                           // "Extra <strong>details</strong>!",
                           SubTotal = 51m,
                           Vat = 12.75m,
                           CurrencyCode = "SEK",
                           ProduceUrl = string.Format("http://{0}/_layouts/Posten/Cart/TestPages/Produce.svc",
                                             HttpContext.Current.Request.Url.Host).ToUri(),

                           // Note: Supplied by external source normally!
                           EditUrl = "/_layouts/Posten/Cart/TestPages/TestServicePage.aspx?OrderNo={0}&url={1}",
                           ServiceImageUrl = null
            };
        }

        #endregion Methods
    }
}