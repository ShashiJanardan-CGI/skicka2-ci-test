﻿
using Microsoft.SharePoint;

namespace Posten.Portal.Cart.Presentation
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;

    public static class Utils
    {
        public static string FormatCurrency(decimal value, string currency)
        {
            CultureInfo ci = GetCultureInfoByCurrency(currency);
            return value.ToString("C", ci);
        }

        private static CultureInfo GetCultureInfoByCurrency(string currency)
        {
            if (currency == "SEK")
            {
                return CultureInfo.GetCultureInfo("sv-se");
            }

            return CultureInfo.CurrentUICulture;
        }

        public static string GetResourceString(string key)
        {
            return Posten.Portal.Platform.Common.Services.ResourceService.GetString(key, "PostenCart");
        }

        public static string ToOrdinal(this int item)
        {
            if (item % 100 < 20 && item % 100 > 9)
                return string.Format("{0}th", item.ToString());
            switch (item % 10)
            {
                case 1:
                    return string.Format("{0}st", item.ToString());
                case 2:
                    return string.Format("{0}nd", item.ToString());
                case 3:
                    return string.Format("{0}rd", item.ToString());
                default:
                    return string.Format("{0}th", item.ToString());
            }
        }

        public static string TranslateHost(this string item, Uri host)
        {
            try
            {
                Uri uri = new Uri(item); 
                UriBuilder builder = new UriBuilder(host);
                builder.Path = uri.AbsolutePath;
                if (uri.Query.Length > 0) builder.Query = uri.Query.Substring(1);
                return builder.Uri.ToString();
            }
            catch (ArgumentNullException ex) {
                return string.Empty;
            }
            catch (UriFormatException ex) {
                return string.Empty;
            }
        }

        public static string ToSafeString(this Uri value)
        {
            if (value == null)
                return string.Empty;

            return value.ToString();
        }

        public static bool IsIPAddress(this string ipaddress)
        {
            string pattern = @"\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b";
            return Regex.IsMatch(ipaddress, pattern);
        }

        public static string buildWebSealUrl(string hostname)
        {

            Uri SpUri = new Uri(SPContext.Current.Web.Url);
            Uri WebsealUri = new Uri(hostname);

            UriBuilder ub = new UriBuilder();
            ub.Host = WebsealUri.Host;
            ub.Fragment = SpUri.Fragment;
            ub.Query = SpUri.Query;
            ub.Scheme = WebsealUri.Scheme;
            ub.Path = WebsealUri.AbsolutePath + SpUri.AbsolutePath;

            return ub.ToString();

        }

        public static string GetCurrentIpAddress()
        {
            //Check if webseal is working
            string currentipaddressfromheader = HttpContext.Current.Request.Headers["iv-remote-address"];

            if(!string.IsNullOrEmpty(currentipaddressfromheader) && currentipaddressfromheader.IsIPAddress())
            {
                return currentipaddressfromheader;
            }
            
            //return the ip from asp.net
            string aspnetaddress = HttpContext.Current.Request.UserHostAddress;

            if (aspnetaddress.IsIPAddress() )
                return aspnetaddress;

            return "127.0.0.1";

        }

        public static string ReformatSSN(this string SSNo)
        {
            //reformat ssn
            var length = SSNo.Length;
            //format is YYYYMMDD-xxxx or YYYYMMDDxxxx, remove the first 2 characters
            if (length == 12 || length == 13)
                return SSNo.Remove(0, 2);

            return SSNo;

        }
    }
}
