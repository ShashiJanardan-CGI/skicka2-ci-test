﻿using System.IO;
using System.Reflection;
using Posten.Portal.Platform.Common.Logging;
using Posten.Portal.Cart.Presentation.ViewData;

namespace Posten.Portal.Cart.Utilities
{
    public class ZipCity
    {
        public static LocationResult ZipToCity(string zip, string country)
        {
            try
            {
                LocationResult result = new LocationResult();

                zip = zip.Replace(" ", string.Empty).Trim();
                string url = string.Format("{0}{1}/{2}", System.Configuration.ConfigurationManager.AppSettings["CartZipCodeServiceUrl"], country, zip);
                System.Net.WebRequest request = System.Net.HttpWebRequest.Create(url);
                System.Net.WebResponse response = request.GetResponse();
                System.IO.StreamReader streamReader = new StreamReader(response.GetResponseStream());

                string jsonString = streamReader.ReadToEnd();

                string[] splits = jsonString.Trim(new char[] { '{', '}' }).Split(new char[] { ',' });
                foreach (string split in splits)
                {
                    if (split.Contains("postalcity"))
                    {
                        string[] citySplit = split.Split(new char[] { ':' });
                        result.city = citySplit[1].Trim(new char[] { '\"' });
                    }
                    else if (split.Contains("country"))
                    {
                        string[] citySplit = split.Split(new char[] { ':' });
                        result.country = citySplit[1].Trim(new char[] { '\"' });
                    }
                    else if (split.Contains("postalcode"))
                    {
                        string[] citySplit = split.Split(new char[] { ':' });
                        result.zip = citySplit[1].Trim(new char[] { '\"' });
                    }
                  
                }

                return result;
            }
            catch (System.Exception ex)
            {
                PostenLogger.LogError(ex.Message.ToString() + "\n\r" + ex.StackTrace.ToString(), PostenLogger.ApplicationLogCategory, MethodInfo.GetCurrentMethod().DeclaringType, MethodInfo.GetCurrentMethod().Name);
                return null;
            }
        }
    }
}
