﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.Presentation.ViewData
{
    public class MaximumInvoiceAmountResult
    {
        public bool isMaximum { get; set; }
        public string Message { get; set; }
    }
}
