﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.Presentation.ViewData
{
    public class LocationResult
    {
        public string zip { get; set; }

        public string city { get; set; }

        public string country { get; set; }
    }
}
