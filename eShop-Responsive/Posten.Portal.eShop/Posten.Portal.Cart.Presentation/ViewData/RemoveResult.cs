﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.Presentation.ViewData
{
    public class RemoveResult
    {
        public string TotalString { get; set; }

        public string TotalExcludingTaxString { get; set; }

        public string TotalTaxString { get; set; }

        public string InvoiceFeeString { get; set; }
    }
}
