﻿using System.Collections.Generic;
using System.ComponentModel;
using Posten.Portal.MyPages.BusinessEntities;

namespace Posten.Portal.MyPages.Services
{
    [Category("My Pages"), Description("This service is used on order history page")]
    public interface IOrderHistoryService
    {
        ICollection<OrderHistoryModel> GetOrderHistory(string username);
    }
}
