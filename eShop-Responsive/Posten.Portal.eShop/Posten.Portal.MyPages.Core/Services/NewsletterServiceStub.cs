﻿using System.Collections.Generic;
using System.Linq;
using Posten.Portal.MyPages.BusinessEntities;
using System.Collections.ObjectModel;

namespace Posten.Portal.MyPages.Services
{
    public class NewsletterServiceStub : INewsletterService
    {
        public ICollection<NewsletterModel> GetNewsletterInterests()
        {
            var intrests = new NewsletterModel[] { new NewsletterModel { Id = "Stamps", Name = "Stamps" }, new NewsletterModel { Id = "Privat", Name = "Privat" } };
            return intrests.ToList();
        }

        public ICollection<NewsletterModel> GetUserNewsletter()
        {
            var intrests = new NewsletterModel[] { new NewsletterModel { Id = "Stamps", Name = "Stamps" } };
            return intrests.ToList();
        }

        public bool RegisterUserNewsletter(Collection<NewsletterModel> interests)
        {
            return true;
        }
    }
}
