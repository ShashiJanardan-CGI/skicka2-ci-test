﻿using System;
using System.Collections.Generic;
using Posten.Portal.MyPages.BusinessEntities;

namespace Posten.Portal.MyPages.Services
{
    public class OrderHistoryStub : IOrderHistoryService
    {
        private const int StartOrderId = 123456789;
        private const int NumberOfOrders = 234;

        public ICollection<OrderHistoryModel> GetOrderHistory(string username)
        {
            Random rnd = new Random(StartOrderId);

            IList<OrderHistoryModel> list = new List<OrderHistoryModel>();

            for (int i = StartOrderId; i > StartOrderId - NumberOfOrders; i--)
            {
                list.Add(NewOrder(i.ToString(), DateTime.Now.Date.AddDays(i - StartOrderId), "Beställd", rnd.Next(100, 25000)));
            }

            return list;
        }

        private static OrderHistoryModel NewOrder(string id, DateTime date, string status, decimal value)
        {
            OrderHistoryModel order = new OrderHistoryModel();
            order.OrderNumber = id;
            order.OrderDate = date;
            order.OrderAmount = value;
            order.Status = status;
            order.StatusDescription = status;
            return order;
        }
    }
}
