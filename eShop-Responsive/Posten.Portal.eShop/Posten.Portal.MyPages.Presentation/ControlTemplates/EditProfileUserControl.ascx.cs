﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.WebControls;
using Posten.Portal.MyPages;
using Posten.Portal.Platform.Common.Container;
using Posten.Portal.Cart.Services;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Platform.Common.Logging;
using System.Reflection;

namespace Posten.Portal.MyPages.Presentation.ControlTemplates
{
    public partial class EditProfileUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //if (MyPagesContext.Current.IsAuthenticated)
                //{
                //    var email = SPContext.Current.Web.CurrentUser.Email;
                //    IUserManagementService service = IoC.Resolve<IUserManagementService>();
                //    if (!string.IsNullOrEmpty(email))
                //    {
                //        var accountinfo = service.GetAccount(email);
                //        RenderCurrentUserInformation(userInformation);
                //        this.btnSave.Visible = false;
                //        this.txtNewPassword.Visible = false;
                //    }
                //}

                IAccountService service = IoC.Resolve<IAccountService>();
                var userInformation = service.GetCurrentUser(MyPagesContext.Current.Name);
                RenderCurrentUserInformation(userInformation);
            }
            catch (Exception ex) {
                PostenLogger.LogError(ex.Message.ToString() + "\n\r" + ex.StackTrace.ToString(), PostenLogger.ApplicationLogCategory, MethodInfo.GetCurrentMethod().DeclaringType, MethodInfo.GetCurrentMethod().Name);
            }
        }

        public void RenderCurrentUserInformation(IAccountInfo item)
        {
            this.txtFirstName.Text = item.FirstName;
            this.txtLastName.Text = item.LastName;
            this.txtEmail.Text = item.EmailAddress;
            this.txtAddress.Text = item.Address.StreetAddress;
            this.txtPostalCode.Text = item.Address.PostalCode;
            this.txtCity.Text = item.Address.City;
            this.ddlstCountry.SelectedValue = item.Address.Country;
            this.txtMobileNumber.Text = item.MobileNumber;
            this.txtValidate.Text = (item.Type == "Private") ? item.PersonalCodeNumber : item.CompanyCode;
            this.RadioButtonList1.SelectedIndex = (item.Type == "Private") ? 0 : 1;
        }
    }
}
