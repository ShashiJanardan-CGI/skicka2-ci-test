﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.WebControls;
using Posten.Portal.MyPages;

namespace Posten.Portal.MyPages.Presentation.ControlTemplates
{
    public partial class MyPagesGlobalUserInfoControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Visible = MyPagesContext.Current.IsAuthenticated;
        }
    }
}
