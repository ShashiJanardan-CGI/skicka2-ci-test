﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsletterUserControl.ascx.cs" Inherits="Posten.Portal.MyPages.Presentation.ControlTemplates.NewsletterUserControl" %>

<PostenWebControls:ModuleControl ID="newsletterModule" runat="server" CssClass="newsletterModule" Visible="false">

</PostenWebControls:ModuleControl>

<PostenWebControls:ModuleControl runat="server" ModuleStyle="Breaker" EdgeStyle="None">
    <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:PostenMyPages,Newsletter_Area_of_Interest_Label %>'/>
</PostenWebControls:ModuleControl>

<asp:CheckBoxList ID="Newsletters" runat="server"></asp:CheckBoxList>

<PostenWebControls:PostenButton OnClick="Subscribe_Newsletter" runat="server" Text='<%$Resources:PostenMyPages,Newsletter_Subscribe_To_Newsletter_Label %>' />
<asp:Label ID="ItemAddedLabel" CssClass="item-added" runat="server" Visible="false" Text='<%$Resources:PostenMyPages,Newsletter_Successfully_Subscribe_To_Newsletters %>' />
<asp:Label ID="ItemNotAddedLabel" CSSClass="item-notadded" runat="server" Visible="false" Text='<%$Resources:PostenMyPages,Newsletter_Successfully_Subscribe_To_Newsletters %>' />
