﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Posten.Portal.MyPages.BusinessEntities;
using Posten.Portal.MyPages.Services;
using Posten.Portal.Platform.Common.Container;
using System.Collections.ObjectModel;
using Posten.Portal.Platform.Common.Logging;
using System.Reflection;

namespace Posten.Portal.MyPages.Presentation.ControlTemplates
{
    public partial class NewsletterUserControl : UserControl
    {
        public string HeaderText
        {
            get { return this.newsletterModule.HeaderText; }
            set { this.newsletterModule.HeaderText = value; }
        }

        public string FooterText
        {
            get { return this.newsletterModule.FooterText; }
            set { this.newsletterModule.FooterText = value; }
        }

        public string FooterUrl
        {
            get { return this.newsletterModule.FooterUrl; }
            set { this.newsletterModule.FooterUrl = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    return;
                }

                var service = IoC.Resolve<INewsletterService>();
                var newsletters = service.GetNewsletterInterests();
                var userNews = service.GetUserNewsletter();

                foreach (NewsletterModel newsletterModel in newsletters)
                {
                    var item = new ListItem(newsletterModel.Name, newsletterModel.Id);
                    item.Selected = userNews.Where(u => u.Id == newsletterModel.Id).FirstOrDefault() != null;
                    this.Newsletters.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                PostenLogger.LogError(ex.Message.ToString() + "\n\r" + ex.StackTrace.ToString(), PostenLogger.ApplicationLogCategory, MethodInfo.GetCurrentMethod().DeclaringType, MethodInfo.GetCurrentMethod().Name);
            }
        }

        protected void Subscribe_Newsletter(object sender, EventArgs e)
        { 
            var service = IoC.Resolve<INewsletterService>();
            Collection<NewsletterModel> newsletters = new Collection<NewsletterModel>();
      
            foreach (ListItem item in this.Newsletters.Items) {
                if (item.Selected) {
                    NewsletterModel newsletter = new NewsletterModel();
                    newsletter.Id = item.Value;
                    newsletter.Name = item.Text;

                    //Add newsletter to collection
                    newsletters.Add(newsletter);
                }
            }
            bool result = service.RegisterUserNewsletter(newsletters);
            if (result) { this.ItemAddedLabel.Visible = true; } else { this.ItemNotAddedLabel.Visible = true; }
        }
    }
}
