﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileOverviewUserControl.ascx.cs" Inherits="Posten.Portal.MyPages.Presentation.ControlTemplates.ProfileOverviewUserControl" %>

<PostenWebControls:ModuleControl ID="orderHistoryModule" runat="server" HeaderText='<%$Resources:PostenMyPages,ProfileOverview_Header_Text %>' FooterText='<%$Resources:PostenMyPages,ProfileOverview_Footer_Text %>' FooterUrl="EditProfile.aspx">

<table cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_CustomerNumber_Text %>' /></td>
        <td><asp:Literal ID="CustomerID" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_SocialSecurityNumber_Text %>' /></td>
        <td><asp:Literal ID="PersonalSecurityNumber" runat="server"></asp:Literal></td>
    </tr>
    <tr class="space">
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_Email_Text %>' /></td>
        <td><asp:Literal ID="EmailAddress" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_CellPhoneNumber_Text %>' /></td>
        <td><asp:Literal ID="MobileNumber" runat="server"></asp:Literal></td>
    </tr>
    <tr class="space">
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_FirstName_Text %>' /></td>
        <td><asp:Literal ID="FirstName" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_LastName_Text %>' /></td>
        <td><asp:Literal ID="LastName" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_StreetAddress_Text %>' /></td>
        <td><asp:Literal ID="Address" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_PostalCode_Text %>' /></td>
        <td><asp:Literal ID="PostalCode" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_City_Text %>' /></td>
        <td><asp:Literal ID="City" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:Literal runat="server" Text='<%$Resources:PostenMyPages,ProfileOverview_Country_Text %>' /></td>
        <td><asp:Literal ID="Country" runat="server"></asp:Literal></td>
    </tr>
</table>
</PostenWebControls:ModuleControl>
