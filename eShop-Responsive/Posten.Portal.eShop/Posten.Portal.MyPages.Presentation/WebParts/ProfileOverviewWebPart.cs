﻿namespace Posten.Portal.MyPages.Presentation.WebParts
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    [Guid("96979a9e-92ee-435a-b22e-817f20af7591")]
    [ToolboxItemAttribute(false)]
    public class ProfileOverviewWebPart : WebPart
    {
        #region Fields

        private const string AscxPath = @"~/_CONTROLTEMPLATES/Posten/MyPages/ProfileOverviewUserControl.ascx";

        #endregion Fields

        #region Methods

        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(AscxPath);
            Controls.Add(control);
        }

        #endregion Methods
    }
}