﻿namespace Posten.Portal.eShop.BusinessEntities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    #region Enumerations

    public enum CatalogItemType : int
    {
        Product = 0,
        Category = 1,
    }

    public enum ProductType : int
    {
        Article = 0,
        Service = 1,
        Stamp = 2,
        Subscription = 3,
        Package = 4,
    }

    public enum RelationshipName : int
    {
        Contains = 0,
    }

    #endregion Enumerations

    public class ProductModel
    {
        #region Properties

        public int ApplicationId
        {
            get; set;
        }

        public string Colours
        {
            get; set;
        }

        public string Currency
        {
            get; set;
        }

        public string CurrencyCode
        {
            get; set;
        }

        public string DefinitionName
        {
            get; set;
        }

        public string DeliveryDetails
        {
            get; set;
        }

        public string Description
        {
            get; set;
        }

        public string Details
        {
            get; set;
        }

        public string EditImageURL
        {
            get; set;
        }

        public string EditURL
        {
            get; set;
        }

        public string ESPNumber
        {
            get; set;
        }

        public string Facts
        {
            get; set;
        }

        public string FreeText1
        {
            get; set;
        }

        public string FreeText2
        {
            get; set;
        }

        public string Id
        {
            get; set;
        }

        public ICollection<Uri> ImagesUri
        {
            get; set;
        }

        public bool IsAvailable
        {
            get; set;
        }

        public bool IsSellable
        {
            get; set;
        }

        public int ItemsInPackage
        {
            get; set;
        }

        public string LongArticleName
        {
            get; set;
        }

        public string LongDescription
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string[] ParentIds
        {
            get; set;
        }

        public int Popularity
        {
            get; set;
        }

        public decimal Price
        {
            get; set;
        }

        public decimal PriceWithoutVAT
        {
            get; set;
        }

        public ICollection<ProductPackageModel> ProductPackages
        {
            get; set;
        }

        public ProductType ProductType
        {
            get; set;
        }

        public int Quantity
        {
            get; set;
        }

        public int[] QuantityValues
        {
            get; set;
        }

        public ICollection<RelatedProductModel> RelatedProducts
        {
            get; set;
        }

        public int ReleaseYear
        {
            get; set;
        }

        public string SAPId
        {
            get; set;
        }

        public bool SellStandalone
        {
            get; set;
        }

        public string ServiceImageURL
        {
            get; set;
        }

        public string ShortArticleName
        {
            get; set;
        }

        public string ShortDescription
        {
            get; set;
        }

        public string StampPicture
        {
            get; set;
        }

        public string StockStatus
        {
            get; set;
        }

        public decimal Total
        {
            get; set;
        }

        public string Value
        {
            get; set;
        }

        public decimal Vat
        {
            get; set;
        }

        public decimal VatPercentage
        {
            get; set;
        }

        public int Weight
        {
            get; set;
        }

        #endregion Properties
    }

    public class ProductPackageIntervalModel
    {
        #region Properties

        public string Id
        {
            get; set;
        }

        public System.Nullable<int> MaxQuantity
        {
            get; set;
        }

        public System.Nullable<int> MinQuantity
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        #endregion Properties
    }

    public class ProductPackageModel
    {
        #region Properties

        public string Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public ProductPackageIntervalModel ProductPackageInterval
        {
            get; set;
        }

        public int Quantity
        {
            get; set;
        }

        #endregion Properties
    }

    public class RelatedProductModel
    {
        #region Properties

        public RelationshipName RelationshipName
        {
            get; set;
        }

        public string TargetId
        {
            get; set;
        }

        public CatalogItemType TargetItemType
        {
            get; set;
        }

        #endregion Properties
    }
}