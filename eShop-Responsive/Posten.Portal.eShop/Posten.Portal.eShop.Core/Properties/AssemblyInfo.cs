﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Posten.Portal.eShop.Core")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Posten")]
[assembly: AssemblyProduct("Posten.Portal.eShop")]
[assembly: AssemblyCopyright("Copyright © Posten 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d7ea3701-1c9e-4ec5-8f33-a748cf4f1b83")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

// Let the presentation project see internals
[assembly: InternalsVisibleTo("Posten.Portal.eShop.Presentation, PublicKey=00240000048000009400000006020000002400005253413100040000010001000daf8ed8d945cd2abb2ee7953a6039b791a725f11b4588ac6d70b3e0648f955e9ed4c3c43cb044b8b0e8a6ff4d4ffbe9e3b9297d45f688a7264534e12414e17539305207ec961da94df294e7722ccd9bdbfc95a896e996f57156705d281ec39280bd604e87724556af5807d146963f19f5b43db69e1f22695463153a553260d2")]