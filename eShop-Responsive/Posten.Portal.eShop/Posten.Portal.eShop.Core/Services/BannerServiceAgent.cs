﻿using System;
using System.Collections.Generic;
using Posten.Portal.eShop.BusinessEntities;
using Posten.Portal.eShop.Services.Proxy.BannerService;
using Posten.Portal.eShop.Translators;
using Posten.Portal.Platform.Common.Services;
using System.Security.Principal;
using System.Web;

namespace Posten.Portal.eShop.Services
{
    public class BannerServiceAgent : ServiceAgentBase, IBannerService
    {
        private Proxy.BannerService.BannerServiceClient serviceClientInstance;

        private Proxy.BannerService.BannerServiceClient ServiceClient
        {
            get
            {
                if (this.serviceClientInstance == null)
                {
                    this.serviceClientInstance = this.ConfigureServiceClient<Proxy.BannerService.BannerServiceClient, Proxy.BannerService.IBannerService>() as Proxy.BannerService.BannerServiceClient;
                }

                return this.serviceClientInstance;
            }

        }

        public ICollection<BannerItemModel> GetBannersByColumnSize(Posten.Portal.eShop.BusinessEntities.DisplaySize displaySize, int itemRequest)
        {
            if (!(Convert.ToInt16(displaySize) ==  0))
            { 

            Proxy.BannerService.DisplaySize size = (Proxy.BannerService.DisplaySize)displaySize;
            Proxy.BannerService.BannerItem[] response = new Proxy.BannerService.BannerItem[] {};
            var currentUser = ImpersonateUser();
            response = this.ServiceClient.GetBannersByColumnSize(size, itemRequest);
            RevertImpersonation(currentUser);
            return response.ToBusinessEntityCollection();
            }
            return new List<BannerItemModel>();
        }

        private static IPrincipal ImpersonateUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated) return HttpContext.Current.User;
            var tempUser = HttpContext.Current.User;
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity("guest"), null);
            return tempUser;
        }

        private static void RevertImpersonation(IPrincipal user)
        {
            if (!HttpContext.Current.User.Equals(user)) HttpContext.Current.User = user;
        }
    }
}
