﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Posten.Portal.eShop.BusinessEntities;

namespace Posten.Portal.eShop.Services
{
    public class BannerServiceStub : IBannerService
    {
        public ICollection<BannerItemModel> GetBannersByColumnSize(DisplaySize displaySize, int itemRequest)
        {
            IList<BannerItemModel> list = new List<BannerItemModel>();
            
            switch (displaySize){
                case DisplaySize.FourColumns:
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo1.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo2.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo3.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo4.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo5.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo6.png", displaySize, DisplayType.Image));
                    list.Add(NewBannerItem("http://www.google.com", "http://www.antssoft.com/swftext/sample/background_leaf.swf", displaySize, DisplayType.Flash));
                    //list.Add(NewBannerItem("http://www.google.com", "http://silverlight.newagesolution.net/DesktopModules/NASBannerRotator/BannerLoader.xap", displaySize, DisplayType.Silverlight));
                    //list.Add(NewBannerItem("http://www.google.com", "http://www.coolwebcontrols.com/ClientBin/CWC-Image-SimpleZoom-SL4.xap", displaySize, DisplayType.Silverlight));
                    break;
                case DisplaySize.ThreeColumns:
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo_3col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo1_3col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo2_3col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo3_3col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo4_3col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo5_3col.png", displaySize, DisplayType.Image));
                    list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo6_3col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "http://www.antssoft.com/swftext/sample/background_leaf.swf", displaySize, DisplayType.Flash));
                    break;
                case DisplaySize.TwoColumns:
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo_2col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo1_2col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo2_2col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo3_2col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo4_2col.png", displaySize, DisplayType.Image));
                    list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo5_2col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo6_2col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "http://www.antssoft.com/swftext/sample/background_leaf.swf", displaySize, DisplayType.Flash));
                    //list.Add(NewBannerItem("http://www.google.com", "http://silverlight.newagesolution.net/DesktopModules/NASBannerRotator/BannerLoader.xap", displaySize, DisplayType.Silverlight));
                    break;
                case DisplaySize.OneColumn:
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo_1col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo1_1col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo2_1col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo3_1col.png", displaySize, DisplayType.Image));
                    list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo4_1col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo5_1col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "/PublishingImages/TestObjects/Images/nature-photo6_1col.png", displaySize, DisplayType.Image));
                    //list.Add(NewBannerItem("http://www.google.com", "http://www.antssoft.com/swftext/sample/background_leaf.swf", displaySize, DisplayType.Flash));
                    //list.Add(NewBannerItem("http://www.google.com", "http://silverlight.newagesolution.net/DesktopModules/NASBannerRotator/BannerLoader.xap", displaySize, DisplayType.Silverlight));
                    break;
            }
            return list;
        }
        private static BannerItemModel NewBannerItem(string actionURL, string adURL, DisplaySize displaySize, DisplayType displayType)
        {
            BannerItemModel bannerItem = new BannerItemModel();
            bannerItem.ActionUrl = actionURL;
            bannerItem.AdUrl = adURL;
            bannerItem.DisplaySize = displaySize;
            bannerItem.DisplayType = displayType;
            return bannerItem;
        }
    }
}
