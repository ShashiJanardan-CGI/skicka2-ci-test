﻿namespace Posten.Portal.eShop.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    public class Utils
    {
        #region Methods

        public static void RevertToCurrentCulture(CultureInfo info)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = info;
            System.Threading.Thread.CurrentThread.CurrentUICulture = info;
        }

        public static CultureInfo SetToSharepointCulture(uint localeid)
        {
            var oldculture = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo( (int)localeid);
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo((int)localeid);
            return oldculture;
        }

        #endregion Methods
    }
}