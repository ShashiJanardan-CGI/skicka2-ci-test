<%@ Page language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"  %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"  %>
<%@ Register Tagprefix="CustomTag_0" Namespace="Microsoft.Office.Server.WebControls.FieldTypes" Assembly="Microsoft.Office.Server.UserProfiles, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"  %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
  <SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
  <PublishingWebControls:EditModePanel runat="server">
    <!-- Styles for edit mode only-->
    <SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/edit-mode-21.css %>"
      After="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
    </PublishingWebControls:EditModePanel>
  <SharePointWebControls:CssRegistration name="<%$SPUrl:~sitecollection/Style Library/eShop/Posten/eShop/1.0.0.0/eshop.css%>" runat="server"/>
  <SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
  <SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
  <SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
  <SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server">
  <SharePointWebControls:ListSiteMapPath runat="server" SiteMapProviders="CurrentNavigation" RenderCurrentNodeAsLink="false" PathSeparator="" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=321 NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/images/fgimg.png" HideInteriorRootNodes="true" SkipLinkText="" />
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
  <div class="main-wpz">
      <div class="middle-wpz">
            <h1 class="page-title">
                <SharePointWebControls:TextField FieldName="Title" InputFieldLabel="Title" runat="server"></SharePointWebControls:TextField>
            </h1>
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="true" ID="ProductItemZone" FrameType="TitleBarOnly" Title="Product Item Zone" Orientation="Vertical"></WebPartPages:WebPartZone>
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="true" ID="ProductItemListZone" FrameType="TitleBarOnly" Title="Product Item List Zone" Orientation="Vertical"></WebPartPages:WebPartZone>
      </div>
      <div class="right-wpz">
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="true" ID="RightColumnZone" FrameType="TitleBarOnly" Title="Right Zone" Orientation="Vertical"></WebPartPages:WebPartZone>
      </div>
  </div>
 

  <PublishingWebControls:EditModePanel runat="server" id="editmodepanel" CssClass="editPanel">
  </PublishingWebControls:EditModePanel>
</asp:Content>
