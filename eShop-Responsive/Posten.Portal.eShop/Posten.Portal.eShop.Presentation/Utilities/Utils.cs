﻿namespace Posten.Portal.eShop.Presentation
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    public static class Utils
    {
        #region Methods

        public static string GetResourceString(string key)
        {
            return Posten.Portal.Platform.Common.Services.ResourceService.GetString(key, "PostenEshop");
        }

        #endregion Methods
    }
}