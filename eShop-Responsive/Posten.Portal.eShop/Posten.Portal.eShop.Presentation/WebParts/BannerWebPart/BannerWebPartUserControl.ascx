﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerWebPartUserControl.ascx.cs" Inherits="Posten.Portal.eShop.Presentation.WebParts.BannerWebPart.BannerWebPartUserControl" %>


<br />
<asp:Literal ID="BannerMessage" runat="server" Text="<%$Resources:PostenEshop,WebPart_Banner_SelectMessage%>"/>

<div id="BaseDiv" runat="server">
<!--Image-->
<asp:Panel ID="ImagePanel" runat="server">
    <asp:Repeater ID="ImageBannerControl" runat="server" EnableViewState="true">
        <ItemTemplate>
            <div style="padding-bottom: 10px;">
                <a id="ImageBannerURL" href='<%# DataBinder.Eval(Container.DataItem, "ActionUrl")%>' target='_blank'>
                <img id='ImageBanner' src='<%# DataBinder.Eval(Container.DataItem, "AdUrl")%>' width="100%" height="100%" style="-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;" />
                </a>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>

<!--Flash-->
<asp:Panel ID="FlashPanel" runat="server">
    <asp:Repeater ID="FlashBannerControl" runat="server" EnableViewState="true">
        <ItemTemplate>
            <div style="padding-bottom: 10px;">
                <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="movie_name" width="100%" height="100%" style="-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;" >
                    <param name="movie" value='<%# DataBinder.Eval(Container.DataItem, "AdUrl")%>'/>
                    <!--[if !IE]>-->
                    <object type="application/x-shockwave-flash" data='<%# DataBinder.Eval(Container.DataItem, "AdUrl")%>' width="100%" height="150px" style="-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;" >
                        <param name="movie" value='<%# DataBinder.Eval(Container.DataItem, "AdUrl")%>'/>
                    <!--<![endif]-->
                        <a href="http://www.adobe.com/go/getflash">
                            <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                        </a>
                    <!--[if !IE]>-->
                    </object>
                    <!--<![endif]-->
                </object>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>

<!--Silverlight-->
<asp:Panel ID="SilverLightPanel" runat="server" >
    <asp:Repeater ID="SilverlightBannerControl" runat="server" EnableViewState="true">
        <ItemTemplate>
            <div style="padding-bottom: 10px;">
                <%--<object id="SilverlightPlugin1" data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100px" style="-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;" >
                    <param name="source" value='<%# DataBinder.Eval(Container.DataItem, "AdUrl")%>'/>
                    <!-- Display installation image. -->
                    <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.60310.0" style="text-decoration: none;">
                    <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style: none"/>
                    </a>
                </object>--%>

                <object id="Object1" width="100%" height="100%"
                    data="data:application/x-silverlight-2," 
                    type="application/x-silverlight-2" >
                    <param name="source" value='<%# DataBinder.Eval(Container.DataItem, "AdUrl")%>'/>
                    <!-- Display installation image. -->
                    <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.60310.0" style="text-decoration: none;">
                        <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style: none"/>
                    </a>
                </object>

            </div>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>

</div>
