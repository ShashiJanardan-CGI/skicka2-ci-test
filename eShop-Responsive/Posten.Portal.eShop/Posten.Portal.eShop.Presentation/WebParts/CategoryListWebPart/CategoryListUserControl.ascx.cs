﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Posten.Portal.eShop.Services;
using Posten.Portal.Platform.Common.Container;

namespace Posten.Portal.eShop.Presentation.ControlTemplates
{
    public partial class CategoryListUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var service = IoC.Resolve<ICatalogService>();
            e.Result = service.GetCategoryList();
        }
    }
}
