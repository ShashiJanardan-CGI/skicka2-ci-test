﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Posten.Portal.eShop.Presentation.ControlTemplates {
    
    
    public partial class PopularProductsUserControl {
        
        /// <summary>
        /// popularProductModule control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Posten.Portal.Platform.Common.Presentation.Controls.ModuleControl popularProductModule;
        
        /// <summary>
        /// PopularProductsRepeater control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater PopularProductsRepeater;
        
        /// <summary>
        /// hdnsource control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnsource;
    }
}
