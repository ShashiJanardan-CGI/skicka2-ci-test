﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Posten.Portal.eShop.BusinessEntities;
using Posten.Portal.Platform.Common.Container;
using Posten.Portal.eShop.Services;
using Microsoft.SharePoint;
using System.Linq;
using Posten.Portal.Cart;
using Posten.Portal.Cart.BusinessEntities;

namespace Posten.Portal.eShop.Presentation.WebParts.ProductDetailsWebPart
{
    public partial class ProductDetailsWebPartUserControl : UserControl
    {
        const string productid = "PostenEshopProductID";
        public string ProductID
        {
            get
            {
                return SPContext.Current.Item["PostenEshopProductID"].ToString();
            }
        }

        private ProductModel GetCurrentProduct()
        {
            ProductModel model = null;
            var catalogService = IoC.Resolve<ICatalogService>();
            if (!string.IsNullOrEmpty(this.ProductID)) model = catalogService.GetProduct(this.ProductID);
            return model;

        }

        private void showProductInfo()
        {
            ProductModel product = GetCurrentProduct();
            if (product != null)
            {
                ProductName.Text = product.Name;
                ProductIdLiteral.Text = this.ProductID;
                ProductDetailsLiteral.Text = product.Details;
                DenominationLiteral.Text = product.Value;
                DescriptionLiteral.Text = product.Description;
                FactsLiteral.Text = product.Facts;
                PriceLiteral.Text = product.Total.ToString("f2");
                CurrencyLiteral.Text = product.Currency;
                AvailabilityLiteral.Text = product.DeliveryDetails;
                string stockAvailableLabel = "<img src=\"/_layouts/images/Posten/eshop/1.0/icon_stock_available.gif\" style=\"float: left; height: 16px; width: 16px\" alt=\"Stock Available\"/>&nbsp; {0}";
                if (!string.IsNullOrEmpty(product.StockStatus)) StockAvailableLiteral.Text = string.Format(stockAvailableLabel, product.StockStatus);
                else StockAvailableLiteral.Text = string.Format(stockAvailableLabel, Utils.GetResourceString("ProductDetails_StockNotAvailable_Label"));


                if (product.ProductPackages.Count > 0)
                {
                    var packages = from p in product.ProductPackages
                                   select new { Quantity = p.Quantity, Name = p.Name };

                    ProductPackagesDropDownList.DataSource = packages;
                    ProductPackagesDropDownList.DataBind();
                }
                else { ProductPackagesDropDownList.Visible = false; MultiplyLabel.Visible = false; }

                var images = from e in product.ImagesUri
                             where e.ToString().Contains("_small")
                             select new { UrlThumb = e.ToString(), UrlImage = e.ToString().Replace("_small", ""), UrlLarge = e.ToString().Replace("_small", "_large") };
                ThumbnailsRepeater.DataSource = images;
                ThumbnailsRepeater.DataBind();
            }
            else
            {
                ProductDetailsPanel.Visible = false;
                ErrorPanel.Visible = true;
                ErrorMessageLiteral.Text = "No Product Specified. Kindly Specify the Product ID.";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                showProductInfo();
            }
        }

        protected void btnBuy_Click(object sender, EventArgs e)
        {
            var catalogService = IoC.Resolve<ICatalogService>();
            var product = catalogService.GetProduct(this.ProductID);

            int productQty = 1;
            int currentQty = string.IsNullOrEmpty(QuantityTextBox.Text) ? 1 : Convert.ToInt16(QuantityTextBox.Text);
            if (product.ProductPackages.Count > 0) productQty = currentQty * Convert.ToInt16(ProductPackagesDropDownList.SelectedValue);
            else productQty = currentQty;

            BasketContext.Current.AddProductOrder(new ProductOrderItem
            {
                ArticleId = product.Id,
                ArticleName = product.Name,
                ArticlePrice = product.Price,
                Vat = product.Vat,
                VatPercentage = product.VatPercentage,
                Quantity =  productQty, 
                CurrencyCode = product.CurrencyCode,
            });
        }
    }
}
