﻿namespace Posten.Portal.eShop.Presentation.ControlTemplates
{
    using System;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;

    using Posten.Portal.Cart;
    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.Cart.Services;
    using Posten.Portal.eShop.Services;
    using Posten.Portal.Platform.Common.Container;

    public partial class ProductListUserControl : UserControl
    {
        #region Methods

        protected void BuyProduct(object sender, CommandEventArgs e)
        {
            var catalogService = IoC.Resolve<ICatalogService>();
            var product = catalogService.GetProduct(e.CommandArgument as string);

            BasketContext.Current.AddProductOrder(new ProductOrderItem
                                                {
                                                    ArticleId = product.Id,
                                                    ArticleName = product.Name,
                                                    ArticlePrice = product.Price,
                                                    Vat = product.Vat,
                                                    VatPercentage = product.VatPercentage,
                                                    Quantity = 1,
                                                    CurrencyCode = product.CurrencyCode,

                                                });

            var msg = (string) HttpContext.GetGlobalResourceObject("PostenEshop", "Product_Added_Message");
            //var msg = "hello";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "message", string.Format("floatMessage('{0}','{1}')", product.Id, msg), true);
        }

        protected void OnSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var service = IoC.Resolve<ICatalogService>();
            e.Result = service.GetProductList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion Methods
    }
}