﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Import Namespace="System.Web" %> 
<%@ Import Namespace="System.Web.UI" %>  
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerWebPart_TestUserControl.ascx.cs" Inherits="Posten.Portal.eShop.TestObjects.WebParts.BannerWebPart_Test.BannerWebPart_TestUserControl" %>
<link href="/_layouts/Posten/eShop/2.0/Carousel/styles/style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/_layouts/Posten/eShop/2.0/Carousel/js/scripts.js"></script>
<br />
<asp:Panel ID="ImagePanel" runat="server">
<div id="header">
    <div class="wrap">
        <div id="slide-holder">
            <div id="slide-runner">
                <a href=""><img id="Img1" src="/PublishingImages/TestObjects/Images/nature-photo.png" class="slide" alt=""/></a>
                <a href=""><img id="Img2" src="/PublishingImages/TestObjects/Images/nature-photo1.png" class="slide" alt=""/></a>
                <a href=""><img id="Img3" src="/PublishingImages/TestObjects/Images/nature-photo2.png" class="slide" alt=""/></a>
                <a href=""><img id="Img4" src="/PublishingImages/TestObjects/Images/nature-photo3.png" class="slide" alt=""/></a>
                <a href=""><img id="Img5" src="/PublishingImages/TestObjects/Images/nature-photo4.png" class="slide" alt=""/></a>
                <a href=""><img id="Img6" src="/PublishingImages/TestObjects/Images/nature-photo5.png" class="slide" alt=""/></a>
                <a href=""><img id="Img7" src="/PublishingImages/TestObjects/Images/nature-photo6.png" class="slide" alt=""/></a>
	            <div id="slide-controls">
                    <p id="slide-client" class="text"><strong>post: </strong><span></span></p>
                    <p id="slide-desc" class="text"></p>
                    <p id="slide-nav"></p>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            if (!window.slider) var slider = {}; slider.data = [
            { "id": "Img1", "client": "nature beauty 1", "desc": "nature beauty photography" },
            { "id": "Img2", "client": "nature beauty 2", "desc": "add your description here" },
            { "id": "Img3", "client": "nature beauty 3", "desc": "add your description here" },
            { "id": "Img4", "client": "nature beauty 4", "desc": "add your description here" },
            { "id": "Img5", "client": "nature beauty 5", "desc": "add your description here" },
            { "id": "Img6", "client": "nature beauty 6", "desc": "add your description here" },
            { "id": "Img7", "client": "nature beauty 7", "desc": "add your description here"}];
        </script>
    </div>
</div>
</asp:Panel>

<asp:Panel ID="FlashPanel" runat="server">
<div class="wrap">
    <div id="slide-holder">
        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="100%" height="100" id="movie_name" align="middle" style="-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;">
            <param name="movie" value="http://www.antssoft.com/swftext/sample/background_leaf.swf"/>
            <!--[if !IE]>-->
            <object type="application/x-shockwave-flash" data="http://www.antssoft.com/swftext/sample/background_leaf.swf" width="100%" height="100" style="-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;" >
                <param name="movie" value="http://www.antssoft.com/swftext/sample/background_leaf.swf"/>
            <!--<![endif]-->
                <a href="http://www.adobe.com/go/getflash">
                    <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                </a>
            <!--[if !IE]>-->
            </object>
            <!--<![endif]-->
        </object>
    </div>
</div>
</asp:Panel>

<asp:Panel ID="SilverLightPanel" runat="server" >
</asp:Panel>
