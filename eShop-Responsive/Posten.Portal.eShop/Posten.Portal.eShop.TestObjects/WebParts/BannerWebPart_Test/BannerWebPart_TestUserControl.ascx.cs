﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Posten.Portal.eShop.TestObjects.WebParts.BannerWebPart_Test
{
    public partial class BannerWebPart_TestUserControl : UserControl
    {
        public BannerWebPart_Test WebPart { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Determine Banner Type
                switch (this.WebPart.displayType.ToString())
                {
                    case "Image":
                        this.DisplayImageBanner();
                        break;
                    case "Flash":
                        this.DisplayFlashBanner();
                        break;
                }
            }
            catch (Exception)
            {
                //TODO: Add Logging Mechanism here
            }
        }
        private void DisplayImageBanner()
        {
            ImagePanel.Visible = true;
            FlashPanel.Visible = false;
            SilverLightPanel.Visible = false;
        }

        private void DisplayFlashBanner()
        {
            ImagePanel.Visible = false;
            FlashPanel.Visible = true;
            SilverLightPanel.Visible = false;
        }
    }
}
